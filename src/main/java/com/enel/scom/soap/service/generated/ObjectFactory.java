
package com.enel.scom.soap.service.generated;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.enel.scom.soap.service.generated package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ExecuteOrdenesOperation_QNAME = new QName("http://wss.service.eorder.synapsis.cl/", "executeOrdenesOperation");
    private final static QName _ExecuteOrdenesOperationResponse_QNAME = new QName("http://wss.service.eorder.synapsis.cl/", "executeOrdenesOperationResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.enel.scom.soap.service.generated
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ExecuteOrdenesOperation }
     * 
     */
    public ExecuteOrdenesOperation createExecuteOrdenesOperation() {
        return new ExecuteOrdenesOperation();
    }

    /**
     * Create an instance of {@link ExecuteOrdenesOperationResponse }
     * 
     */
    public ExecuteOrdenesOperationResponse createExecuteOrdenesOperationResponse() {
        return new ExecuteOrdenesOperationResponse();
    }

    /**
     * Create an instance of {@link ResponseDTO }
     * 
     */
    public ResponseDTO createResponseDTO() {
        return new ResponseDTO();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExecuteOrdenesOperation }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ExecuteOrdenesOperation }{@code >}
     */
    @XmlElementDecl(namespace = "http://wss.service.eorder.synapsis.cl/", name = "executeOrdenesOperation")
    public JAXBElement<ExecuteOrdenesOperation> createExecuteOrdenesOperation(ExecuteOrdenesOperation value) {
        return new JAXBElement<ExecuteOrdenesOperation>(_ExecuteOrdenesOperation_QNAME, ExecuteOrdenesOperation.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExecuteOrdenesOperationResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ExecuteOrdenesOperationResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://wss.service.eorder.synapsis.cl/", name = "executeOrdenesOperationResponse")
    public JAXBElement<ExecuteOrdenesOperationResponse> createExecuteOrdenesOperationResponse(ExecuteOrdenesOperationResponse value) {
        return new JAXBElement<ExecuteOrdenesOperationResponse>(_ExecuteOrdenesOperationResponse_QNAME, ExecuteOrdenesOperationResponse.class, null, value);
    }

}
