
package com.enel.scom.soap.service.generated;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.3.2
 * Generated source version: 2.2
 * 
 */
@WebService(name = "OrdenesWss", targetNamespace = "http://wss.service.eorder.synapsis.cl/")
@XmlSeeAlso({
    ObjectFactory.class
})
public interface OrdenesWss {


    /**
     * 
     * @param fechaVencimientoANSLegal
     * @param tipoTDC
     * @param wsdl
     * @param tipoOperacion
     * @param fechaVencimientoANSInterno
     * @param distribuidora
     * @param origen
     * @param llaveSecreta
     * @param codigoExternoDelTDC
     * @return
     *     returns com.enel.scom.soap.service.generated.ResponseDTO
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "executeOrdenesOperation", targetNamespace = "http://wss.service.eorder.synapsis.cl/", className = "com.enel.scom.soap.service.generated.ExecuteOrdenesOperation")
    @ResponseWrapper(localName = "executeOrdenesOperationResponse", targetNamespace = "http://wss.service.eorder.synapsis.cl/", className = "com.enel.scom.soap.service.generated.ExecuteOrdenesOperationResponse")
    public ResponseDTO executeOrdenesOperation(
        @WebParam(name = "wsdl", targetNamespace = "")
        String wsdl,
        @WebParam(name = "distribuidora", targetNamespace = "")
        String distribuidora,
        @WebParam(name = "origen", targetNamespace = "")
        String origen,
        @WebParam(name = "tipoTDC", targetNamespace = "")
        String tipoTDC,
        @WebParam(name = "codigoExternoDelTDC", targetNamespace = "")
        String codigoExternoDelTDC,
        @WebParam(name = "tipoOperacion", targetNamespace = "")
        Integer tipoOperacion,
        @WebParam(name = "fechaVencimientoANSLegal", targetNamespace = "")
        String fechaVencimientoANSLegal,
        @WebParam(name = "fechaVencimientoANSInterno", targetNamespace = "")
        String fechaVencimientoANSInterno,
        @WebParam(name = "llaveSecreta", targetNamespace = "")
        String llaveSecreta);

}
