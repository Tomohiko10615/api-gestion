
package com.enel.scom.soap.service.generated;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para executeOrdenesOperation complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="executeOrdenesOperation"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="wsdl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="distribuidora" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="origen" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="tipoTDC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="codigoExternoDelTDC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="tipoOperacion" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="fechaVencimientoANSLegal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="fechaVencimientoANSInterno" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="llaveSecreta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "executeOrdenesOperation", propOrder = {
    "wsdl",
    "distribuidora",
    "origen",
    "tipoTDC",
    "codigoExternoDelTDC",
    "tipoOperacion",
    "fechaVencimientoANSLegal",
    "fechaVencimientoANSInterno",
    "llaveSecreta"
})
public class ExecuteOrdenesOperation {

    protected String wsdl;
    protected String distribuidora;
    protected String origen;
    protected String tipoTDC;
    protected String codigoExternoDelTDC;
    protected Integer tipoOperacion;
    protected String fechaVencimientoANSLegal;
    protected String fechaVencimientoANSInterno;
    protected String llaveSecreta;

    /**
     * Obtiene el valor de la propiedad wsdl.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWsdl() {
        return wsdl;
    }

    /**
     * Define el valor de la propiedad wsdl.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWsdl(String value) {
        this.wsdl = value;
    }

    /**
     * Obtiene el valor de la propiedad distribuidora.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDistribuidora() {
        return distribuidora;
    }

    /**
     * Define el valor de la propiedad distribuidora.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDistribuidora(String value) {
        this.distribuidora = value;
    }

    /**
     * Obtiene el valor de la propiedad origen.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrigen() {
        return origen;
    }

    /**
     * Define el valor de la propiedad origen.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrigen(String value) {
        this.origen = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoTDC.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoTDC() {
        return tipoTDC;
    }

    /**
     * Define el valor de la propiedad tipoTDC.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoTDC(String value) {
        this.tipoTDC = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoExternoDelTDC.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoExternoDelTDC() {
        return codigoExternoDelTDC;
    }

    /**
     * Define el valor de la propiedad codigoExternoDelTDC.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoExternoDelTDC(String value) {
        this.codigoExternoDelTDC = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoOperacion.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTipoOperacion() {
        return tipoOperacion;
    }

    /**
     * Define el valor de la propiedad tipoOperacion.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTipoOperacion(Integer value) {
        this.tipoOperacion = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaVencimientoANSLegal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaVencimientoANSLegal() {
        return fechaVencimientoANSLegal;
    }

    /**
     * Define el valor de la propiedad fechaVencimientoANSLegal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaVencimientoANSLegal(String value) {
        this.fechaVencimientoANSLegal = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaVencimientoANSInterno.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaVencimientoANSInterno() {
        return fechaVencimientoANSInterno;
    }

    /**
     * Define el valor de la propiedad fechaVencimientoANSInterno.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaVencimientoANSInterno(String value) {
        this.fechaVencimientoANSInterno = value;
    }

    /**
     * Obtiene el valor de la propiedad llaveSecreta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLlaveSecreta() {
        return llaveSecreta;
    }

    /**
     * Define el valor de la propiedad llaveSecreta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLlaveSecreta(String value) {
        this.llaveSecreta = value;
    }

}
