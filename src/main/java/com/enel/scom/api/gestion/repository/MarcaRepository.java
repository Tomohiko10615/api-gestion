package com.enel.scom.api.gestion.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.model.MarcaEntity;

@Repository
public interface MarcaRepository extends JpaRepository<MarcaEntity, Long>{
	@Query(value="SELECT nextval('sqmedmarca')", nativeQuery=true)
	public Long generaId();

	@Query(value = "SELECT * FROM med_marca WHERE id=:id", nativeQuery = true)
	MarcaEntity obtenerMarca(@Param("id") Long  id);

	@Query(value = "select mm.id, mm.cod_marca, mm.des_marca, mm.fec_registro, u.username from med_marca mm\r\n"
			+ "left join usuario u on mm.id_usuario_registro = u.id\r\n"
			+ "where mm.activo = 'S' ORDER BY mm.fec_registro asc", nativeQuery = true)
	Page<Object[]> getMarcasPaginacion(Pageable paging);

	@Query(value = "SELECT cod_marca, id, des_marca FROM med_marca WHERE activo = 'S' ORDER BY cod_marca ASC", nativeQuery = true)
	List<Object[]> getMarcasActive();
	
	@Query(value = "UPDATE med_marca SET activo = 'N' WHERE id = :id", nativeQuery = true)
	MarcaEntity desactivar(@Param("id") Long id);
	
	@Query(value = "SELECT count(*) as total FROM med_marca WHERE LOWER(cod_marca) = LOWER(:codMarca)", nativeQuery = true)
	Integer existeCodigo(@Param("codMarca") String codMarca);
}
