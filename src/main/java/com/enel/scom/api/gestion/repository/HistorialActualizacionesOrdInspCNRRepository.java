package com.enel.scom.api.gestion.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.enel.scom.api.gestion.model.HistorialActualizacionesOrdInspCNR;

public interface HistorialActualizacionesOrdInspCNRRepository extends JpaRepository<HistorialActualizacionesOrdInspCNR, Long> {

}
