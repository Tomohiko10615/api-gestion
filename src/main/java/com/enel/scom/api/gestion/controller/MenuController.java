package com.enel.scom.api.gestion.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.enel.scom.api.gestion.dto.MenuDTO;
import com.enel.scom.api.gestion.service.MenuService;

@RestController
@CrossOrigin
@RequestMapping("/api/menu")
public class MenuController {
	
	@Autowired
	MenuService menuService;
	
	@GetMapping(value = "/menuPerfil/{idPerfil}") 
	public List<MenuDTO> findMenuPerfil(@PathVariable("idPerfil") Long idPerfil) { 
		return menuService.getMenuByIdPerfil(idPerfil); 
	}
}
