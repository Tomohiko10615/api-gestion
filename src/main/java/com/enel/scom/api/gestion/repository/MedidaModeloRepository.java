package com.enel.scom.api.gestion.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.model.FactorEntity;
import com.enel.scom.api.gestion.model.MedidaModeloEntity;

@Repository
public interface MedidaModeloRepository extends JpaRepository<MedidaModeloEntity, Long>{
	@Query(value="SELECT nextval('sqmedmedidamodelo')", nativeQuery=true)
	public Long generaId();

	@Query(value = "SELECT * FROM med_medida_modelo WHERE id_modelo = :id", nativeQuery = true)
	List<MedidaModeloEntity> getMedidaModelo(@Param("id") Long id);
	
	@Modifying
	@Transactional
	@Query(value = "DELETE FROM med_medida_modelo WHERE id_modelo = :id", nativeQuery = true)
	void deleteModelo(@Param("id") Long id);

	@Modifying
	@Transactional
	@Query(value = "delete from med_medida_modelo "
					+ "USING med_fac_med_mod "
					+ "WHERE med_medida_modelo.id = med_fac_med_mod.id_medida_modelo  "
					+ "and med_medida_modelo.id_modelo = :id", nativeQuery = true)
	void deleteMedidaFactor(@Param("id") Long id);
	
	@Query(value = "select mmm.id_medida as id_medida, mm.des_medida, med.id as id_ent_dec, med.cant_enteros, med.cant_decimales, "
			+ "mmm.id, mtip.des_tip_calculo "
			+ "from schscom.med_medida_modelo mmm "
			+ "inner join schscom.med_ent_dec med on mmm.id_ent_dec = med.id "
			+ "inner join schscom.med_medida mm on mmm.id_medida = mm.id "
			+ "inner join schscom.med_tip_calculo mtip on mmm.id_tip_calculo = mtip.id "
			+ "where mmm.id_modelo = :id", nativeQuery = true)
	List<Object[]> getModelo(@Param("id") Long id);
	
	@Query(value = "SELECT * FROM med_medida_modelo WHERE id = :id", nativeQuery = true)
	MedidaModeloEntity findId(@Param("id") Long id);

}
