//R.I. CORRECION 11/09/2023 INICIO
package com.enel.scom.api.gestion.dto.xml;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

//import javax.xml.bind.annotation.XmlElement;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Lectura {

    private String tipoLectura;
    private String horarioLectura;
    private String estadoLeido;
    private String fechaLectura;
    
    private String accionMedidor;
    
    /*
    @XmlElement(name = "tipoLectura")
    public String getTipoLectura() {
        return tipoLectura;
    }

    public void setTipoLectura(String tipoLectura) {
        this.tipoLectura = tipoLectura;
    }

    @XmlElement(name = "horarioLectura")
    public String getHorarioLectura() {
        return horarioLectura;
    }

    public void setHorarioLectura(String horarioLectura) {
        this.horarioLectura = horarioLectura;
    }

    @XmlElement(name = "estadoLeido")
    public String getEstadoLeido() {
        return estadoLeido;
    }

    public void setEstadoLeido(String estadoLeido) {
        this.estadoLeido = estadoLeido;
    }

    @XmlElement(name = "fechaLectura")
    public String getFechaLectura() {
        return fechaLectura;
    }

    public void setFechaLectura(String fechaLectura) {
        this.fechaLectura = fechaLectura;
    }
    */
}
//R.I. CORRECION 11/09/2023 FIN