// R.I. REQSCOM06 06/10/2023 INICIO
package com.enel.scom.api.gestion.enums;

public enum TipoOrden {
    ORINSP(8, "ORINSP"),
    NORM(7, "NORM"),
    CONT(22, "CONT"),
    MANT(2, "MANT"),
	INVALID(0, "INVALID");

    private final Long idTipoOrden;
    private final String codigo;

    TipoOrden(long idTipoOrden, String codigo) {
        this.idTipoOrden = idTipoOrden;
        this.codigo = codigo;
    }

    public Long getIdTipoOrden() {
        return idTipoOrden;
    }

    public String getCodigo() {
        return codigo;
    }

    public static TipoOrden fromIdTipoOrden(Long idTipoOrden) {
        for (TipoOrden tipoOrden : values()) {
            if (tipoOrden.idTipoOrden.equals(idTipoOrden)) {
                return tipoOrden;
            }
        }
        return INVALID;
    }
}
//R.I. REQSCOM06 06/10/2023 FIN