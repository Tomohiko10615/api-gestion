package com.enel.scom.api.gestion.model;

import javax.persistence.*;

import lombok.Data;

@Data
@Entity(name = "med_fac_med_mod")
public class MedFacMedModEntity {

    @Id
    @Column(name = "id_medida_modelo")
    private Long medidaModelo;

    @OneToOne
    @JoinColumn(name = "id_factor")
    private FactorEntity factor;
}
