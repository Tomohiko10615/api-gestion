package com.enel.scom.api.gestion.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import lombok.Data;

@Entity(name = "ord_historico")
@Data
public class HistoricoEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "id_auditevent")
	private Long idAuditevent;
	
	@Column(name = "estado_inicial")
	private String estadoInicial;
	
	@OneToOne
	@JoinColumn(name="id_buzon")
	private BuzonEntity buzonEntity;
	
	@Column(name = "estado_final")
	private String estadoFinal;
	
	@Column(name = "actividad")
	private String actividad;
}
