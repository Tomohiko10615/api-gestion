package com.enel.scom.api.gestion.dto;

import java.math.BigInteger;
import java.util.Date;


public class UsuarioDTO {

    private BigInteger codUsuario;
    private String nombre;
    private Date fecRegistro;
    private Character estado;
    private String dni;
    private String username;
    private String apePaterno;
    private String apeMaterno;
    private Date ultFecModif;
    private BigInteger codperfil;
    private String email;
	public BigInteger getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(BigInteger codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Date getFecRegistro() {
		return fecRegistro;
	}
	public void setFecRegistro(Date fecRegistro) {
		this.fecRegistro = fecRegistro;
	}
	public Character getEstado() {
		return estado;
	}
	public void setEstado(Character estado) {
		this.estado = estado;
	}
	public String getDni() {
		return dni;
	}
	public void setDni(String dni) {
		this.dni = dni;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getApePaterno() {
		return apePaterno;
	}
	public void setApePaterno(String apePaterno) {
		this.apePaterno = apePaterno;
	}
	public String getApeMaterno() {
		return apeMaterno;
	}
	public void setApeMaterno(String apeMaterno) {
		this.apeMaterno = apeMaterno;
	}
	public Date getUltFecModif() {
		return ultFecModif;
	}
	public void setUltFecModif(Date ultFecModif) {
		this.ultFecModif = ultFecModif;
	}
	public BigInteger getCodperfil() {
		return codperfil;
	}
	public void setCodperfil(BigInteger codperfil) {
		this.codperfil = codperfil;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
    
    
	
}
