package com.enel.scom.api.gestion.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Entity(name = "med_tip_componente")
@Data
public class MedTipComponenteEntity implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "cod_tip_componente")
	private String codTipComponente;
	
	@Column(name = "des_tip_componente")
	private String desTipComponente;
	
	@Column(name = "niv_presentacion")
	private String nivPresentacion;
	
	@Column(name = "cod_interno")
	private String codInterno;
}
