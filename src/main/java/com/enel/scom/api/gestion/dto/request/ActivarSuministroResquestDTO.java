package com.enel.scom.api.gestion.dto.request;

import lombok.Data;

@Data
public class ActivarSuministroResquestDTO {
	
	private Long idAutoActiva;
	
	private Long idSet;
	private Long idAlimentador;
	private Long idSed;
	private Long idLlave;
	
	private String sector;
	private String zona;
	private String correlativo;
	
	/*
	private Long idLecturaSector;
	private String codLecturaSector;
	
	private Long idLecturaZona;
	private String codLecturaZona;
	private String lecturaCorrelativo;
	
	private Long idRutaCorteSector;
	private String codRutaCorteSector;
	
	private Long idRutaCorteZona;
	private String codRutaCorteZona;
	private String rutaCorteCorrelativo;
	
	private Long idRutaFacturacionSector;
	private String codRutaFacturacionSector;
	
	private Long idRutaFacturacionZona;
	private String codRutaFacturacionZona;
	private String rutaFacturacionCorrelativo;
	
	private Long idRutaRepartoSector;
	private String codRutaRepartoSector;
	
	private Long idRutaRepartoZona;
	private String codRutaRepartoZona;
	private String rutaRepartoCorrelativo;
	
	*/
	
}
