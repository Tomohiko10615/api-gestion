package com.enel.scom.api.gestion.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.enel.scom.api.gestion.dto.request.CambiarEstadoTareaRequestDTO;
import com.enel.scom.api.gestion.service.OrdenTareaService;

@RestController
@RequestMapping("/api/disOrdTarea")
public class OrdenTareaController {

	@Autowired
	OrdenTareaService ordenTareaService;
	
	@PostMapping("/cambiarEstado")
	public ResponseEntity<String> cambiarEstado(@RequestBody CambiarEstadoTareaRequestDTO cambiarEstado) {
		
		ordenTareaService.cambiarEstado(cambiarEstado);
		
		return new ResponseEntity<>("Actualizado", HttpStatus.OK);
	}
}
