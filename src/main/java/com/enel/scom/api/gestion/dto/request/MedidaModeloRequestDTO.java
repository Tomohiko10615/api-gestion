package com.enel.scom.api.gestion.dto.request;

import lombok.Data;

@Data
public class MedidaModeloRequestDTO {
	private String cant_decimales;
	private String cant_enteros;
	private String factor_codigo;
	private String factor_descripcion;
	private String factor_valor;
	private Long id_ent_dec;
	private Long id_factor;
	private Long id_medida;
	private String medida_descripcion;
	private Long id_medida_modelo;
}
