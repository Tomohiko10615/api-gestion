package com.enel.scom.api.gestion.service;

import java.util.List;

import com.enel.scom.api.gestion.dto.response.ParametroResponseDTO;

public interface ParametroService {
	
	List<ParametroResponseDTO> getTipoInspeccion();
	
	List<ParametroResponseDTO> getSubTipo();
	
	/**
	 * Listado de los tipo de procesos manejados en el SCOM
	 * @return
	 */
	List<ParametroResponseDTO> getTipoProceso();
	
	/**
	 * Obtiene el listado de los estados de las ordenes de trabajo
	 * @return listado de todos los estados
	 */
	List<ParametroResponseDTO> getEstados();
	
	/**
	 * Obtiene el listado de todos los estados que se manejan en la transfer
	 * @return listado de todos los estados
	 */
	List<ParametroResponseDTO> getEstadosTransfer();
	
	/**
	 * Obtiene el listado de los tipos de ubicacion de los medidores med_component
	 * @return listado de tipos de ubicacion
	 */
	List<ParametroResponseDTO> getTiposUbicacionMedidor();
	ParametroResponseDTO obtenerRangoFechaBusqueda(String sistema, String entidad, String codigo);
	
}
