package com.enel.scom.api.gestion.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Entity(name = "com_voltaje")
@Data
public class VoltajeEntity implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "cod_voltaje")
	private String codVoltaje;
	
	@Column(name = "des_voltaje")
	private String desVoltaje;
	
	@Column(name = "val_voltaje")
	private String valVoltaje;

}
