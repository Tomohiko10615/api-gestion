package com.enel.scom.api.gestion.exception;

public class AnularNotFoundException extends RuntimeException {
	public AnularNotFoundException(String message) {
        super(message);
    }
}
