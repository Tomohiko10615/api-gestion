// R.I. REQSCOM02 13/07/2023 INICIO
package com.enel.scom.api.gestion.dto;

public interface LecturasDTO {
	String getTipoLectura();
    String getHorarioLectura();
    String getEstadoLeido();
    String getFechaLectura();
    Long getIdComponente();
    String getAccionMedidor();
}
//R.I. REQSCOM02 13/07/2023 FIN
