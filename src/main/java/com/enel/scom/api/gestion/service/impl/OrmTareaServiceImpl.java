package com.enel.scom.api.gestion.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.scom.api.gestion.dto.response.OrmTareaResponseDTO;
import com.enel.scom.api.gestion.repository.OrmTareaRepository;
import com.enel.scom.api.gestion.service.OrmTareaService;

@Service
public class OrmTareaServiceImpl implements OrmTareaService{
    @Autowired
    OrmTareaRepository ormTareaRepository;

    @Override
    public List<OrmTareaResponseDTO> getActivos(Long idTipo) {
        List<Object[]> ormTareas = ormTareaRepository.getActivos(idTipo);

        return ormTareas.stream().map(object -> new OrmTareaResponseDTO(Long.valueOf(object[0].toString()), 
                                                                        (object[1] == null) ? "" : object[1].toString(), 
                                                                        (object[2] == null) ? "" : object[2].toString(), 
                                                                        (object[3] == null) ? "" : object[3].toString(), 
                                                                        (object[4] == null) ? "" : object[4].toString(), 
                                                                        (object[5] == null) ? "" : object[5].toString(), 
                                                                        (object[6] == null) ? "" : object[6].toString(), 
                                                                        (object[7] == null) ? "" : object[7].toString())
                                                ).collect(Collectors.toList());
    }

}
