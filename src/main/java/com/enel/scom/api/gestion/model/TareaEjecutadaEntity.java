package com.enel.scom.api.gestion.model;

import java.util.Date;

import javax.persistence.*;

import lombok.Data;

@Entity(name = "orm_tarea_ejec")
@Data
public class TareaEjecutadaEntity {
    @Id
    @Column(name = "id")
    private Long id;

    @OneToOne
    @JoinColumn(name = "id_tarea")
    private OrmTareaEntity tarea;

    @Column(name = "id_orden")
    private Long orden;

    @Column(name = "fecha_creacion")
    private Date fechaCreacion;

    @Column(name = "ejecutada")
    private String ejecutada;

    @Column(name = "costo")
    private Long costo;

    @Column(name = "costo_por_sistema")
    private String costoPorSistema;

    @Column(name = "facturar")
    private String facturar;

    @Column(name = "lleva_cambio_medidor")
    private String llevaCambioMedidor;

    @Column(name = "lleva_contraste_medidor")
    private String llevaContrasteMedidor;

    @Column(name = "condicion_trabajo")
    private String condicionTrabajo;

    @Column(name = "tipo_mantenimiento")
    private String tipoMantenimiento;
    
    @Column(name = "cantidad_ejec")
    private Long cantidadEjec;
}
