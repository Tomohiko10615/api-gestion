package com.enel.scom.api.gestion.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity(name = "med_marca")
@Table(name = "med_marca", schema = "schscom")
@Data
public class MarcaEntity implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "id")
	private Long id;
	
	@Column(name = "cod_marca")
	private String codigo;
	
	@Column(name = "des_marca")
	private String descripcion;
	
	@Column(name = "activo", length = 1)
	private String activo;
	
	@OneToOne
	@JoinColumn(name = "id_tip_componente")
	private MedTipComponenteEntity tipoComponente;
	
	@OneToOne
	@JoinColumn(name = "id_usuario_registro")
	private UsuarioEntity usuarioRegistro;
	
	@OneToOne
	@JoinColumn(name = "id_usuario_modif")
	private UsuarioEntity usuarioModif;

	@Column(name = "fec_registro")
	private Date fecRegistro;
	
	@Column(name = "fec_modif")
	private Date fecModif;
}
