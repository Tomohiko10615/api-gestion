package com.enel.scom.api.gestion.service;

import com.enel.scom.api.gestion.dto.response.ClienteInspeccionResponseDTO;
import com.enel.scom.api.gestion.exception.NroServicioNotFoundException;
import com.enel.scom.api.gestion.model.ClienteEntity;

public interface ClienteService {
	ClienteEntity findId(Long id);
	ClienteEntity getIdServicio(Long idServicio);
	ClienteInspeccionResponseDTO getNroServicio(Long nroServicio) throws NroServicioNotFoundException;
	ClienteEntity getNroCuenta(Long nroCuenta);
}
