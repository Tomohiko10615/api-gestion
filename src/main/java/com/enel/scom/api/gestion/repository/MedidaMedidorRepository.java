package com.enel.scom.api.gestion.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.model.MedidaMedidorEntity;

@Repository
public interface MedidaMedidorRepository extends JpaRepository<MedidaMedidorEntity, Long>{
	@Query(value="SELECT nextval('sqmedmedidamedidor')", nativeQuery=true)
	public Long generaId();

	@Modifying
	@Transactional
	@Query(value = "DELETE FROM med_medida_medidor WHERE id_componente = :id")
	void deleteMedidor(@Param("id") Long id);
	
	@Query(value = "select mm.id as id_medida, mm.des_medida, med.id as id_ent_dec, med.cant_enteros, med.cant_decimales, mf.id as id_factor, mf.cod_factor, mf.des_factor, mf.val_factor, mtc.id as id_tip_calculo from med_medida_medidor mmm\r\n"
			+ "left join med_ent_dec med on mmm.id_ent_dec = med.id\r\n"
			+ "left join med_factor mf on mmm.id_factor = mf.id\r\n"
			+ "left join med_medida mm on mmm.id_medida = mm.id\r\n"
			+ "left join med_tip_calculo mtc on mmm.id_tip_calculo = mtc.id\r\n"
			+ "where mmm.id_componente = :id", nativeQuery = true)
	List<Object[]> getMedidor(@Param("id") Long id);
}
