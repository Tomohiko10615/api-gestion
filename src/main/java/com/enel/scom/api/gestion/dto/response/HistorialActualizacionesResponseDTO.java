package com.enel.scom.api.gestion.dto.response;

import lombok.Data;

@Data
public class HistorialActualizacionesResponseDTO {

	public String fechaEdicion;
	public String userName;
	public Long nroSuministro;
	public Long nroNotificacion;
	public String nombreCampoActualizado;
	public String valorAntes;
	public String valorDespues;
	public String comentario;
}
