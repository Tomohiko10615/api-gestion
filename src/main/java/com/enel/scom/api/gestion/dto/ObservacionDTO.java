// R.I. Corrección 19/09/2023 INICIO
package com.enel.scom.api.gestion.dto;

public interface ObservacionDTO {
	String getTexto();
    String getFechaObservacion();
}
// R.I. Corrección 19/09/2023 FIN