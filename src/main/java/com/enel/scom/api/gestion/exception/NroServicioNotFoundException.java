package com.enel.scom.api.gestion.exception;

public class NroServicioNotFoundException extends Exception{
	public NroServicioNotFoundException(String message) {
        super(message);
    }
}
