package com.enel.scom.api.gestion.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.InputMismatchException;
import java.util.UUID;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.enel.scom.api.gestion.dto.request.ActualizaDatosCNR_RequestDTO;
import com.enel.scom.api.gestion.dto.request.OrdenInspeccionRequestDTO;
import com.enel.scom.api.gestion.dto.response.ActualizaOrdIspecCNRResponseDTO;
import com.enel.scom.api.gestion.dto.response.HistorialActualizacionesResponseDTO;
import com.enel.scom.api.gestion.dto.response.OrdenDataResponseDTO;
import com.enel.scom.api.gestion.dto.response.OrdenInspeccionActualizaDatosCNRResponseDTO;
import com.enel.scom.api.gestion.enums.EstadosRespuestaService;
import com.enel.scom.api.gestion.exception.DatosNoValidosParaEditarCamposException;
import com.enel.scom.api.gestion.exception.NoDataToEditException;
import com.enel.scom.api.gestion.exception.NroServicioNotFoundException;
import com.enel.scom.api.gestion.exception.RegistrarHistoricoEdicionCamposException;
import com.enel.scom.api.gestion.model.OrdenEntity;
import com.enel.scom.api.gestion.service.OrdenInspeccionService;
import com.enel.scom.api.gestion.util.Funciones;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api/ordenInspeccion")
public class OrdenInpeccionController {

	@Autowired
	OrdenInspeccionService ordenInspeccionService;
	
	@PostMapping("/store")
	public ResponseEntity<OrdenEntity> storeUpdate(@RequestBody OrdenInspeccionRequestDTO ordenDTO) throws NroServicioNotFoundException {
		OrdenEntity response = ordenInspeccionService.storeUpdate(ordenDTO);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<OrdenDataResponseDTO> getId(@PathVariable Long id) {
		OrdenDataResponseDTO response = ordenInspeccionService.getId(id);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@GetMapping("/filtroOrdenInspActualizarXPaginacion")
	public ResponseEntity<Page<OrdenInspeccionActualizaDatosCNRResponseDTO>> filtroOrdenInspActualizarXPaginacion(
			@RequestParam(required=false) String numSuministro,
			@RequestParam(required=false) String numNotificacion,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size,
            @RequestParam(defaultValue = "id") String order,
            @RequestParam(defaultValue = "true") boolean asc
    ){
		log.info("/filtroOrdenInspActualizarXPaginacion/{}/{} ",numSuministro, numNotificacion);
		
		Long vNumSuministro = null;
		Long vNumNotificacion = null;
		
		if(Funciones.isNumeric(numSuministro)) {
			vNumSuministro = Long.valueOf(numSuministro); 
		}
		if(Funciones.isNumeric(numNotificacion)) {
			vNumNotificacion = Long.valueOf(numNotificacion); 
		}

        Page<OrdenInspeccionActualizaDatosCNRResponseDTO> ordenes = new PageImpl<>(
        		new ArrayList<OrdenInspeccionActualizaDatosCNRResponseDTO>(),  PageRequest.of(page, size, Sort.by(order)), 0);
        
		try {
		
			ordenes = ordenInspeccionService.findOrdenInspActualizarPaginacion(
			        PageRequest.of(page, size, Sort.by(order)),
			        vNumSuministro, 
			        vNumNotificacion,
			        null, null);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			log.error("/filtroOrdenInspActualizarXPaginacion Error Exception",e);//imprime la linea de codigo que genero el error
		}

		log.info("/filtroOrdenInspActualizarXPaginacion/{}/{} list size result: {}",numSuministro, numNotificacion,ordenes==null?"NULO": ordenes.getTotalElements());
        return new ResponseEntity<Page<OrdenInspeccionActualizaDatosCNRResponseDTO>>(ordenes, HttpStatus.OK); 
    }
	
	
	
	@PostMapping("/actualizarDatosInspCNR")
	public ResponseEntity<ActualizaOrdIspecCNRResponseDTO> actualizarDatosOrdenInspeccionCNR(@RequestBody ActualizaDatosCNR_RequestDTO dto){
		log.info("/actualizarDatosInspCNR/ Usuario = "+dto.userName);
		log.info("request dto = "+Funciones.objectToJsonString(dto));

		HttpStatus hStatus = HttpStatus.OK;
		
		ActualizaOrdIspecCNRResponseDTO  dtoRpta = new ActualizaOrdIspecCNRResponseDTO();

		Date vINICIO = new java.util.Date();
		
		String uniqueID = UUID.randomUUID().toString();
		log.info("Log track ID (RequestID) = "+uniqueID);
		
		try {
			dtoRpta = ordenInspeccionService.actualizarDatosOrdenInspeccionCNR(uniqueID, dto);
			
		} catch (NoDataToEditException noDataex) {
			dtoRpta = new ActualizaOrdIspecCNRResponseDTO();
			dtoRpta.codigo_respuesta = EstadosRespuestaService.VALIDACION_FALLIDA.toString();
			dtoRpta.error = noDataex.toString();// "Los datos recibidos son iguales a datos datos actuales, no se actualizaron los datos.";
			
		 
		} catch (DatosNoValidosParaEditarCamposException iex) {
			dtoRpta = new ActualizaOrdIspecCNRResponseDTO();
			dtoRpta.codigo_respuesta = EstadosRespuestaService.VALIDACION_FALLIDA.toString();
			dtoRpta.error = "Los Datos de entrada No son validos, por favor verifique.";
			
		}catch (InputMismatchException iex) {
			dtoRpta = new ActualizaOrdIspecCNRResponseDTO();
			dtoRpta.codigo_respuesta = EstadosRespuestaService.VALIDACION_FALLIDA.toString();
			dtoRpta.error = "Datos de entrada invalidos, por favor verifique.";
			
		}catch (RegistrarHistoricoEdicionCamposException iex) {
			dtoRpta = new ActualizaOrdIspecCNRResponseDTO();
			dtoRpta.codigo_respuesta = EstadosRespuestaService.VALIDACION_FALLIDA.toString();
			dtoRpta.error = "Error al registrar datos historicos.";
			
		} catch (Exception e) {
			dtoRpta = new ActualizaOrdIspecCNRResponseDTO();
			
			dtoRpta.codigo_respuesta = EstadosRespuestaService.ERROR.toString();
			dtoRpta.error = "Ocurrio un error inesperado.";
			log.error("/actualizarDatosInspCNR Error Exception",e);//imprime la linea de codigo que genero el error
		}
		finally {
			dtoRpta.requestID = uniqueID;
			Date vFIN = new java.util.Date();
			dtoRpta.tiempoEjecucion = Funciones.getDuracion(vINICIO, vFIN);
		}
		
		//evaluamos el resultado para asignar la cabezera http de respuesta
		if(dtoRpta.codigo_respuesta == null) {
			hStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		}
		else if(dtoRpta.codigo_respuesta.equalsIgnoreCase(EstadosRespuestaService.OK.toString())) {
			hStatus = HttpStatus.OK;
		}
		else if(dtoRpta.codigo_respuesta.equalsIgnoreCase(EstadosRespuestaService.VALIDACION_FALLIDA.toString())) {
			hStatus = HttpStatus.BAD_REQUEST;
		}
		else if(dtoRpta.codigo_respuesta.equalsIgnoreCase(EstadosRespuestaService.ERROR.toString())) {
			hStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		}
		else {
			hStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		}
		
		log.info("/actualizarDatosInspCNR Respuesta DTO = "+Funciones.objectToJsonString(dtoRpta));
		log.info("/actualizarDatosInspCNR Respuesta HTTPSTATUS = "+hStatus.toString());
		
        return new ResponseEntity<ActualizaOrdIspecCNRResponseDTO>(dtoRpta, hStatus); 
    }
	
	
	@GetMapping("/filtrarHistorialActualizacionesXPaginacion")
	public ResponseEntity<Page<HistorialActualizacionesResponseDTO>> filtrarHistorialActualizacionesXPaginacion(
			@RequestParam(required=false) String numSuministro,
			@RequestParam(required=false) String numNotificacion,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size,
            @RequestParam(defaultValue = "id") String order,
            @RequestParam(defaultValue = "true") boolean asc
    ){
		log.info("/filtrarHistorialActualizacionesXPaginacion/{}/{} ",numSuministro, numNotificacion);
		
		Long vNumSuministro = null;
		Long vNumNotificacion = null;
		boolean datosValidos = true;
		
		if(Funciones.isNumeric(numSuministro)) {
			vNumSuministro = Long.valueOf(numSuministro);
		}
		else {
			datosValidos = false;
		}
		
		if(Funciones.isNumeric(numNotificacion)) {
			vNumNotificacion = Long.valueOf(numNotificacion);
		}
		else {
			datosValidos = false;
		}

        Page<HistorialActualizacionesResponseDTO> ordenes = new PageImpl<>(
        		new ArrayList<HistorialActualizacionesResponseDTO>(),  PageRequest.of(page, size, Sort.by(order)), 0);
        
        if(datosValidos) {
        	try {
    			ordenes = ordenInspeccionService.findHistorialActualizacionesPaginacion(
    			        PageRequest.of(page, size, Sort.by(order)),
    			        vNumSuministro, 
    			        vNumNotificacion);
    		} catch (Exception e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    			log.error("/filtrarHistorialActualizacionesXPaginacion Error Exception",e);//imprime la linea de codigo que genero el error
    		}
        }
        else {
        	log.error("datos de consulta invalidos");
        }
		

		log.info("/filtrarHistorialActualizacionesXPaginacion/{}/{} list size result: {}",numSuministro, numNotificacion,ordenes==null?"NULO": ordenes.getTotalElements());
        return new ResponseEntity<Page<HistorialActualizacionesResponseDTO>>(ordenes, HttpStatus.OK); 
    }
	
}
