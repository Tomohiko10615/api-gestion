package com.enel.scom.api.gestion.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.model.UnidadMedidaConstanteEntity;

@Repository
public interface UnidadMedidaConstanteRepository extends JpaRepository<UnidadMedidaConstanteEntity, Long>{
	@Query(value = "SELECT * FROM med_unidad_medida_cte WHERE activo = 'S' ORDER BY des_unidad_medida_cte ASC", nativeQuery = true)
	List<UnidadMedidaConstanteEntity> getUMCActivo();
	
	@Query(value = "SELECT * FROM med_unidad_medida_cte WHERE id = :id", nativeQuery = true)
	UnidadMedidaConstanteEntity findId(@Param("id") Long id);
}
