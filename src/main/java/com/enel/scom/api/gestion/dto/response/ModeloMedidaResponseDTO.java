package com.enel.scom.api.gestion.dto.response;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class ModeloMedidaResponseDTO {
	private Long id_medida;
	private String medida_descripcion;
	private Long id_ent_dec;
	private String cant_enteros;
	private String cant_decimales;
	private Long id_medida_modelo;
	private String des_tip_calculo;
	private List<FactorResponseDTO> factores = new ArrayList<>();
	
	
}
