package com.enel.scom.api.gestion.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


import com.enel.scom.api.gestion.model.OrdenInspeccionEntity;

@Repository
public interface OrdenInspeccionRepository extends JpaRepository<OrdenInspeccionEntity, Long>{
	
	@Query(value="SELECT nextval('sqordeninspeccion')", nativeQuery=true)
	public Long generarNumeroOrden();

	@Query(value = "SELECT * FROM dis_ord_insp WHERE id_orden = :id", nativeQuery = true)
	OrdenInspeccionEntity findId(@Param("id") Long id);
	
	@Query(value = "select \r\n"
			+ "	oo.id as id_orden,\r\n"
			+ "	oo.fec_registro,\r\n"
			+ "	oo.fecha_finalizacion,\r\n"
			+ "	oo.nro_orden,\r\n"
			+ "	omc.des_motivo,\r\n"
			+ "	omc.id as id_motivo,\r\n"
			+ "	u.username,\r\n"
			+ "	doi.entre_calles,\r\n"
			+ "	doi.auto_generada,\r\n"
			+ "	doi.con_info_completa,\r\n"
			+ "	concat(np1.nombre, ' ', np1.apellido_pat, ' ', np1.apellido_mat) as jefe_producto,\r\n"
			+ "	djp.id as id_jefe_producto,\r\n"
			+ "	concat(np2.nombre, ' ', np2.apellido_pat, ' ', np2.apellido_mat) as contratista,\r\n"
			+ "	cc.id as id_contratista,\r\n"
			+ "	dp.des_producto,\r\n"
			+ "	dp.id as id_producto,\r\n"
			+ "	doi.tip_inspeccion,\r\n"
			+ "	c.nro_servicio, c.nro_cuenta, c.nombre, c.apellido_pat, c.apellido_mat,\r\n"
			+ "	c.distrito, c.cod_ruta,\r\n"
			+ "	oob.texto as observacion,\r\n"
			+ "	doi.id_orden as id_orden_inspeccion, \r\n"
			+ "	c.texto_direccion as direccion, \r\n"
			+ "	doi.nro_notificacion, \r\n"
			+ " doi.fec_ini_eje, \r\n"
			+ " oo.id_orden_sc4j \r\n"
			+ "from ord_orden oo\r\n"
			+ "left join ord_motivo_creacion omc on omc.id = oo.id_motivo\r\n"
			+ "left join usuario u on u.id = oo.id_usuario_registro\r\n"
			+ "left join dis_ord_insp doi on oo.id = doi.id_orden\r\n"
			+ "left join dis_jefe_producto djp on djp.id = doi.id_jefe_producto\r\n"
			+ "left join nuc_persona np1 on np1.id = djp.id_persona\r\n"
			+ "left join ord_orden_cont ooc on ooc.id_orden = oo.id\r\n"
			+ "left join com_contratista cc on cc.id = ooc.id_contratista\r\n"
			+ "left join nuc_persona np2 on np2.id = cc.id_persona\r\n"
			+ "left join dis_producto dp on dp.id = doi.id_producto\r\n"
			+ "left join ord_observacion oob on oob.id_orden = oo.id\r\n"
			+ "left join cliente c on c.id_servicio = oo.id_servicio where oo.id = :id ORDER BY oob.fecha_observacion DESC LIMIT 1", nativeQuery = true)
	List<Object[]> findIdOrden(@Param("id") Long id);
	
	@Query(value = "SELECT * FROM dis_ord_insp WHERE id_orden =:id", nativeQuery = true)
	OrdenInspeccionEntity getIdOrden(@Param("id") Long id);
	
	@Query(value = "select concat(cc2.cod_contratista, ' - ', np.nombre) as contratista\r\n"
	        + "from\r\n"
	        + "    SCHSCOM.com_cuadrilla cc\r\n"
	        + "    inner join SCHSCOM.com_contratista cc2 on cc2.id = cc.id_contratista  \r\n"
	        + "    inner join SCHSCOM.nuc_persona np on np.id = cc2.id_persona  \r\n"
	        + "where 1 = 1\r\n"
	        + "    and cc.id_cuadrilla in (select distinct id_cuadrilla from SCHSCOM.dis_ord_insp doi where id_orden = :id)", nativeQuery = true)
	String contratista(@Param("id") Long id);

	@Query(value = "select concat(ce.cod_ejecutor, ' - ', np.nombre, ' ', np.apellido_pat, ' ', np.apellido_mat) as tecnico\r\n"
	        + "from\r\n"
	        + "    SCHSCOM.com_ejecutor ce  \r\n"
	        + "    inner join SCHSCOM.nuc_persona np on np.id = ce.id_persona  \r\n"
	        + "where 1 = 1\r\n"
	        + "    and ce.id_ejecutor in (select distinct id_ejecutor from SCHSCOM.dis_ord_insp doi where id_orden = :id)", nativeQuery = true)
	String ejecutor(@Param("id") Long id); 
	
	

	/*------------------------ update SCOM: -----------------------------*/
	@Modifying
	@Query(value = "UPDATE ord_medi_insp "
			+ " SET carga_r =:paramNuevoValor_cargaR "
			+ "WHERE id_medi_insp = :paramIdMediInsp", nativeQuery = true)
	int actualizaSCOM_cargaR(
			@Param("paramIdMediInsp") Long paramIdMediInsp, 
			@Param("paramNuevoValor_cargaR") Double paramCargaR_nuevoValor);
	
	
	@Modifying
	@Query(value = "UPDATE ord_medi_insp "
			+ " SET carga_s =:paramNuevoValor_cargaS "
			+ "WHERE id_medi_insp = :paramIdMediInsp", nativeQuery = true)
	int actualizaSCOM_cargaS(
			@Param("paramIdMediInsp") Long paramIdMediInsp, 
			@Param("paramNuevoValor_cargaS") Double paramNuevoValor_cargaS);
	
	
	@Modifying
	@Query(value = "UPDATE ord_medi_insp "
			+ " SET carga_t =:paramNuevoValor_cargaT "
			+ "WHERE id_medi_insp = :paramIdMediInsp", nativeQuery = true)
	int actualizaSCOM_cargaT(
			@Param("paramIdMediInsp") Long paramIdMediInsp, 
			@Param("paramNuevoValor_cargaT") Double paramNuevoValor_cargaT);
	

	
	@Modifying
	@Query(value = "UPDATE dis_ord_insp "
					/*+ " SET fec_fin_eje =  CAST (:paramNuevoValor_fechaNotificacion as timestamp without time zone) "*/
					+ " SET fec_fin_eje =  to_timestamp(:paramNuevoValor_fechaNotificacion, 'YYYY-MM-DD') "
					+ " WHERE id = :paramIdOrdInsp", nativeQuery = true)
	int actualizaSCOM_fechaNotificacion(
			@Param("paramIdOrdInsp") Long paramIdOrdInsp, 
			@Param("paramNuevoValor_fechaNotificacion") String paramNuevoValor_fechaNotificacion);

	/*------------------------ update CNR: -----------------------------*/
	

	@Modifying
	@Query(value = "UPDATE schrecupero.rec_expediente "
			+ " SET carga_r =:paramNuevoValor_cargaR "
			+ " WHERE cod_expediente = :paramCodExpediente", nativeQuery = true)
	int actualizaCNR_cargaR(
			@Param("paramCodExpediente") Long paramCodExpediente, 
			@Param("paramNuevoValor_cargaR") Double paramCargaR_nuevoValor);
	
	
	@Modifying
	@Query(value = "UPDATE schrecupero.rec_expediente "
			+ " SET carga_s =:paramNuevoValor_cargaS "
			+ " WHERE cod_expediente = :paramCodExpediente", nativeQuery = true)
	int actualizaCNR_cargaS(
			@Param("paramCodExpediente") Long paramCodExpediente, 
			@Param("paramNuevoValor_cargaS") Double paramNuevoValor_cargaS);
	

	@Modifying
	@Query(value = "UPDATE schrecupero.rec_expediente "
			+ " SET carga_t =:paramNuevoValor_cargaT "
			+ " WHERE cod_expediente = :paramCodExpediente", nativeQuery = true)
	int actualizaCNR_cargaT(
			@Param("paramCodExpediente") Long paramCodExpediente, 
			@Param("paramNuevoValor_cargaT") Double paramNuevoValor_cargaT);
		
	@Modifying
	@Query(value = "UPDATE schrecupero.rec_expediente"
					+ " SET lectura_notificacion =  :paramLectura_nuevoValor "
					+ " WHERE cod_expediente = :paramCodExpediente", nativeQuery = true)
	int actualizaCNR_lectura_tbl_expediente(
			@Param("paramCodExpediente") Long paramCodExpediente, 
			@Param("paramLectura_nuevoValor") Integer paramLectura_nuevoValor);
		
	@Modifying
	@Query(value = "select schrecupero.revaloriza_calculo_libre_auto(?1, ?2, ?3)",nativeQuery = true)
	Object[] actualizaCNR_lectura_REVALORIZAR(
			Long paramCodExpediente, 
			String paramUserName, 
			String paramComentarios);
	
	
	@Modifying
	@Query(value = "UPDATE schrecupero.rec_inspeccion"
					+ " SET lectura =  :paramLectura_nuevoValor "
					+ " WHERE cod_inspeccion = :paramCodInspeccion", nativeQuery = true)
	int actualizaCNR_lectura_tbl_inspeccion(
			@Param("paramCodInspeccion") Long paramCodInspeccion, 
			@Param("paramLectura_nuevoValor") Long paramLectura_nuevoValor);
	
	@Modifying
	@Query(value = "UPDATE schrecupero.rec_inspeccion"
					+ " SET fecha_inspeccion =  to_timestamp(:paramNuevoValor_fechaNotificacion, 'YYYY-MM-DD')"
					+ " WHERE cod_inspeccion = :paramCodInspeccion", nativeQuery = true)
	int actualizaCNR_fechaNotificacion_tbl_inspeccion(
			@Param("paramCodInspeccion") Long paramCodInspeccion, 
			@Param("paramNuevoValor_fechaNotificacion") String paramNuevoValor_fechaNotificacion);
	
	@Modifying
	/*@Transactional*/
	@Query(value = "UPDATE  schrecupero.rec_expediente "
					+ " SET fec_notificacion =  to_timestamp(:paramNuevoValor_fechaNotificacion, 'YYYY-MM-DD') "
					+ " WHERE cod_expediente = :paramCodExpediente", nativeQuery = true)
	int actualizaCNR_fechaNotificacion(
			@Param("paramCodExpediente") Long paramCodExpediente, 
			@Param("paramNuevoValor_fechaNotificacion") String paramNuevoValor_fechaNotificacion);

	// R.I. REQSCOM03 16/10/2023 INICIO
	// R.I. Se requiere que se obtenga de la BBDD 16/10/2023 INICIO
	// R.I. Correccion, ahora se usa el codigo y descripcion del resultado 17/10/2023 
	@Query(value = "SELECT\r\n"
			+ "  cod_resultado||'-'||des_resultado AS resultado\r\n"
			+ "FROM dis_resultado_insp dri\r\n"
			+ "INNER JOIN dis_ord_insp doi ON doi.id_resultado = dri.id\r\n"
			+ "WHERE doi.id_orden = ?", nativeQuery = true)
	String getCodigoResultado(Long idOrden);
	// R.I. Se requiere que se obtenga de la BBDD 16/10/2023 FIN
	// R.I. REQSCOM03 16/10/2023 FIN


}
