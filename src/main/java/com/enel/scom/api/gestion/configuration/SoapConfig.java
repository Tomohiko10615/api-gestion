package com.enel.scom.api.gestion.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.client.core.WebServiceTemplate;

@Configuration
public class SoapConfig {
    
    
    @Bean
    public Jaxb2Marshaller marshaller() {
	Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
	marshaller.setContextPath("com.enel.scom.soap.service.generated");
	marshaller.setCheckForXmlRootElement(true);
	return marshaller;
    }

    @Bean(name = "allowancePostpaidWebServiceTemplate")
    public WebServiceTemplate allowancePostpaidWebServiceTemplate() {
	WebServiceTemplate webServiceTemplate = new WebServiceTemplate ();
	webServiceTemplate.setMarshaller(marshaller());
	webServiceTemplate.setUnmarshaller(marshaller());
	webServiceTemplate.setDefaultUri("http://tmefltmws-pre.enelint.global/peruwin/distribution/maf/service/P005_AnulacionSuspension");
	return webServiceTemplate;
    }
}