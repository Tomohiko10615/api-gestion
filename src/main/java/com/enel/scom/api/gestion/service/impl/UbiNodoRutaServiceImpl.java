package com.enel.scom.api.gestion.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.scom.api.gestion.dto.response.AutoCompleteResponseDTO;
import com.enel.scom.api.gestion.oracle.repository.UbiNodoRutaRepository;
import com.enel.scom.api.gestion.service.UbiNodoRutaService;

@Service
public class UbiNodoRutaServiceImpl implements UbiNodoRutaService {
	
	@Autowired
	private UbiNodoRutaRepository ubiNodoRutaRepository;
	
	/**
	 * Busca concidencidencias de codigo Sector o Zona Ruta
	 * @param codNodo
	 * @param idNivel (Sector: 1, Zona: 2 y Correlativo: 3)
	 */

	@Override
	public List<AutoCompleteResponseDTO> buscarSector(String codNodo, Long idTipoRuta) {
		List<Object[]> codNodoRuta = ubiNodoRutaRepository.buscarSector(codNodo, idTipoRuta);
		return codNodoRuta.stream().map(object -> new AutoCompleteResponseDTO(
				Long.valueOf(object[0].toString()), 
				object[1].toString().trim()
				)).collect(Collectors.toList());
	}

	@Override
	public List<AutoCompleteResponseDTO> buscarZonaPorSector(String codNodo, Long idTipoRuta, Long idNodoPadre) {
		List<Object[]> codNodoRuta = ubiNodoRutaRepository.buscarZonaPorSector(codNodo, idTipoRuta, idNodoPadre);
		return codNodoRuta.stream().map(object -> new AutoCompleteResponseDTO(
				Long.valueOf(object[0].toString()), 
				object[1].toString().trim()
				)).collect(Collectors.toList());
	}

}
