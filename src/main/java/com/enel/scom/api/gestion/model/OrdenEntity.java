package com.enel.scom.api.gestion.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

import lombok.Data;
import lombok.ToString;

/**
 * 
 * @author ngbarrios
 * Tabla de campos en comun ordenes de trabajo
 */

@Entity(name="ord_orden")
@Data
@ToString
public class OrdenEntity implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column
	private Long id;
	
	@Column(name="id_orden_sc4j")
	private Long idOrdenSc4j;
	
	@Column(name="nro_orden")
	private String nroOrden;
	
	@OneToOne
	@JoinColumn(name = "id_workflow")
	private WorkflowEntity workflow;
	
	@Column(name = "id_servicio")	
	private Long idServicio;
	
	@OneToOne
	@JoinColumn(name = "id_motivo")
	private MotivoEntity motivo;
	
	@OneToOne
	@JoinColumn(name = "id_tipo_orden")
	private TipoOrdenEntity tipoOrden;

	@OneToOne
	@JoinColumn(name = "id_usuario_creador")
	private UsuarioEntity usuarioCrea;

	@OneToOne
	@JoinColumn(name = "id_usuario_registro")
	private UsuarioEntity usuarioRegistro;

	@OneToOne
	@JoinColumn(name = "id_usuario_modif")
	private UsuarioEntity usuarioModif;
    
    @Column(name = "fecha_ingreso_estado_actual")
    private Date fechaIngresoEstadoActual;
    
    @Column(name = "fecha_vencimiento")
    private Date fechaVencimiento;
	
	@Column(name="discriminador")
	private String discriminador;
	
	@Column(name="fecha_finalizacion")
	private Date fechaFinalizacion;
	
	@Column(name="fecha_creacion")
	private Date fechaCreacion;
	
	@Column(name="fec_registro")
	private Date fechaRegistro;
	
	@Column(name="fec_modif")
	private Date fechaModif;
	
	@Column(name="activo")
	private Boolean activo;
	
	@Column(name = "leido")
	private String leido;
	
	@OneToOne
	@JoinColumn(name="id_motivo_anulacion")
	private MotivoAnulacionEntity motivoAnulacion;
	
	@OneToOne
	@JoinColumn(name="id_buzon")
	private BuzonEntity buzonEntity;
}
