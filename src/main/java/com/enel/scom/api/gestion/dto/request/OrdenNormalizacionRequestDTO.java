package com.enel.scom.api.gestion.dto.request;

import java.util.List;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class OrdenNormalizacionRequestDTO {
	private Long idOrden;
	private Long idOrdenNormalizacion;
	private Long idOrdenInspeccion;
	private Long idUsuario;
	private String subtipo;
	private String observaciones;
	private Long nroServicio;
	private List<AnormalidadesRequestDTO> irregularidades;
}
