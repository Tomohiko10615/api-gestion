package com.enel.scom.api.gestion.dto.response;

import java.util.Date;
import java.util.List;

import lombok.Data;

@Data
public class TransferPaginacionResponseDTO {
	
	private Long idtransferencia;
	private String tipoOrdenLegacy;
	private String nroOrdenLegacy;
	private String tipoOrdenTdc;
	private Long nroTdc;
	private String estado;
	private String pararilizada;
	private Date fecEstadoActual;
	private Long nroCuenta;
	private String origenCrea;
	private Long idMotivo;
	private Date fechaCrea;
	private String nroOrdenOrdOrden;
	private String generacion;
	private String estadoActivacion;
	private Long idAutoAct;
	
	
	
	public TransferPaginacionResponseDTO(Long idtransferencia, String tipoOrdenLegacy, String nroOrdenLegacy,
			String tipoOrdenTdc, Long nroTdc, String estado, String pararilizada, Date fecEstadoActual,
			Long nroCuenta, String origenCrea, Long idMotivo, Date fechaCrea, String nroOrdenOrdOrden, String generacion) {

		this.idtransferencia = idtransferencia;
		this.tipoOrdenLegacy = tipoOrdenLegacy;
		this.nroOrdenLegacy = nroOrdenLegacy;
		this.tipoOrdenTdc = tipoOrdenTdc;
		this.nroTdc = nroTdc;
		this.estado = estado;
		this.pararilizada = pararilizada;
		this.fecEstadoActual = fecEstadoActual;
		this.nroCuenta = nroCuenta;
		this.origenCrea = origenCrea;
		this.idMotivo = idMotivo;
		this.fechaCrea = fechaCrea;
		this.nroOrdenOrdOrden = nroOrdenOrdOrden;
		this.generacion= generacion;
	}	
	
	public TransferPaginacionResponseDTO(Long idtransferencia, String tipoOrdenLegacy, String nroOrdenLegacy,
			String tipoOrdenTdc, Long nroTdc, String estado, String pararilizada, Date fecEstadoActual,
			Long nroCuenta, String origenCrea, Long idMotivo, Date fechaCrea, String nroOrdenOrdOrden, String generacion, String estadoActivacion, Long idAutoAct) {

		this.idtransferencia = idtransferencia;
		this.tipoOrdenLegacy = tipoOrdenLegacy;
		this.nroOrdenLegacy = nroOrdenLegacy;
		this.tipoOrdenTdc = tipoOrdenTdc;
		this.nroTdc = nroTdc;
		this.estado = estado;
		this.pararilizada = pararilizada;
		this.fecEstadoActual = fecEstadoActual;
		this.nroCuenta = nroCuenta;
		this.origenCrea = origenCrea;
		this.idMotivo = idMotivo;
		this.fechaCrea = fechaCrea;
		this.nroOrdenOrdOrden = nroOrdenOrdOrden;
		this.generacion = generacion;
		this.estadoActivacion = estadoActivacion;
		this.idAutoAct = idAutoAct;
	}	
	
	
	
}
