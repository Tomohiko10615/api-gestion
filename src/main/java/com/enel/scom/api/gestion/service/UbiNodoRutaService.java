package com.enel.scom.api.gestion.service;

import java.util.List;

import org.springframework.data.repository.query.Param;

import com.enel.scom.api.gestion.dto.response.AutoCompleteResponseDTO;

public interface UbiNodoRutaService {
	
	/**
	 * Busca concidencidencias de codigo Sector o Zona Ruta
	 * @param codNodo
	 * @param idNivel (Sector: 1, Zona: 2 y Correlativo: 3)
	 * @return
	 */
	List<AutoCompleteResponseDTO> buscarSector(String codNodo, Long idTipoRuta);
	
	List<AutoCompleteResponseDTO> buscarZonaPorSector(String codNodo, Long idTipoRuta, Long idNodoPadre);
}
