package com.enel.scom.api.gestion.dto.response;

import lombok.Data;

@Data
public class OrmTareaResponseDTO {

    public OrmTareaResponseDTO(Long id, String codigo, String tarea, String tipoTarea, String cambio, String contraste, String estado, String valor) {
        this.id = id;
        this.codigo = codigo;
        this.tarea = tarea;
        this.tipoTarea = tipoTarea;
        this.cambio = cambio;
        this.contraste = contraste;
        this.estado = estado;
        this.valor = valor;
    }

    private Long id;
    private String codigo;
    private String tarea;
    private String tipoTarea;
    private String cambio;
    private String contraste;
    private String estado;
    private String valor;
}
