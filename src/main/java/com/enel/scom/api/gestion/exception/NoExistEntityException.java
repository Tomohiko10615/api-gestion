package com.enel.scom.api.gestion.exception;

import org.springframework.http.HttpStatus;

public class NoExistEntityException extends Exception {

	private String code;
    private HttpStatus status;
    
    public NoExistEntityException(String message, String code, HttpStatus status) {
        super(message);
        this.code = code;
        this.status = status;
    }
}
