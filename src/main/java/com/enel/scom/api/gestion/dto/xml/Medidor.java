//R.I. CORRECION 11/09/2023 INICIO
package com.enel.scom.api.gestion.dto.xml;

//import javax.xml.bind.annotation.XmlElement;
//import javax.xml.bind.annotation.XmlElements;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Medidor {

    private String accionMedidor;
    private String marcaMedidor;
    private String modeloMedidor;
    private String numeroMedidor;
    private List<Lectura> lecturas;

    /*
    @XmlElement(name = "accionMedidor")
    public String getAccionMedidor() {
        return accionMedidor;
    }

    public void setAccionMedidor(String accionMedidor) {
        this.accionMedidor = accionMedidor;
    }

    @XmlElement(name = "marcaMedidor")
    public String getMarcaMedidor() {
        return marcaMedidor;
    }

    public void setMarcaMedidor(String marcaMedidor) {
        this.marcaMedidor = marcaMedidor;
    }

    @XmlElement(name = "modeloMedidor")
    public String getModeloMedidor() {
        return modeloMedidor;
    }

    public void setModeloMedidor(String modeloMedidor) {
        this.modeloMedidor = modeloMedidor;
    }

    @XmlElement(name = "numeroMedidor")
    public String getNumeroMedidor() {
        return numeroMedidor;
    }

    public void setNumeroMedidor(String numeroMedidor) {
        this.numeroMedidor = numeroMedidor;
    }

    @XmlElements({
            @XmlElement(name = "lecturas", type = Lectura.class)
    })
    public List<Lectura> getLecturas() {
        return lecturas;
    }

    public void setLecturas(List<Lectura> lecturas) {
        this.lecturas = lecturas;
    }
    */
}
//R.I. CORRECION 11/09/2023 FIN