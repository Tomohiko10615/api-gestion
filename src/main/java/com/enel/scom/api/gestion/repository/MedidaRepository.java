package com.enel.scom.api.gestion.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.model.MedidaEntity;

@Repository
public interface MedidaRepository extends JpaRepository<MedidaEntity, Long>{
	
	@Query(value = "SELECT * FROM med_medida WHERE activo = true ORDER BY des_medida ASC", nativeQuery = true)
	List<MedidaEntity> getActivos();
	
	@Query(value = "SELECT * FROM med_medida WHERE id = :id", nativeQuery = true)
	MedidaEntity findId(@Param("id") Long id);
	
}
