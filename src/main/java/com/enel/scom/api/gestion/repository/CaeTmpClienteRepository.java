package com.enel.scom.api.gestion.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.model.CaeTmpClienteEntity;

@Repository
public interface CaeTmpClienteRepository extends JpaRepository<CaeTmpClienteEntity, Long> {
	
	@Query(value="Select count(*) from cae_tmp_cliente where numero_cliente=:nroCuenta", nativeQuery = true)
	public int validarExistenciaPorNroCuenta(@Param("nroCuenta") String nroCuenta);
	
	@Query(value="Select * from cae_tmp_cliente where numero_cliente=:nroCuenta", nativeQuery = true)
	CaeTmpClienteEntity obtenerCaeTmpClienteScomPorNroCuenta(@Param("nroCuenta") Long nroCuenta);
}
