package com.enel.scom.api.gestion.oracle.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;

@Entity(name = "srv_sed")
@Data
public class SrvSedEntity implements Serializable {/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
    @Column(name="id_sed")
    private Long idSed;
	
	@Column(name="id_alimentador")
    private Long idAlimentador;

    @Column(name = "cod_sed")
    private String codSed;
    
    @Column(name = "des_sed")
    private String desSed;
    
    @Column(name = "activo")
    private String activo;
}
