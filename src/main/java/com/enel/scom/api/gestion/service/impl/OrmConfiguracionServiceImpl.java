package com.enel.scom.api.gestion.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.scom.api.gestion.dto.response.TrabajoResponseDTO;
import com.enel.scom.api.gestion.repository.OrmConfiguracionRepository;
import com.enel.scom.api.gestion.service.OrmConfiguracionService;

@Service
public class OrmConfiguracionServiceImpl implements OrmConfiguracionService{

    @Autowired
    OrmConfiguracionRepository ormConfiguracionRepository;

    @Override
    public List<TrabajoResponseDTO> getTrabajos() {
        
        List<Object[]> trabajos = ormConfiguracionRepository.getTrabajos();
        List<TrabajoResponseDTO> responseDto = new ArrayList<>();

        trabajos.stream().forEach(k -> {
            TrabajoResponseDTO dto = new TrabajoResponseDTO();
            dto.setId(Long.valueOf(k[0].toString()));
            dto.setDescripcion(k[1].toString());

            responseDto.add(dto);
        });
        
        return responseDto;
    }
    
}
