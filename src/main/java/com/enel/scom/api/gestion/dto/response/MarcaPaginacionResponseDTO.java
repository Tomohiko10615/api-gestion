package com.enel.scom.api.gestion.dto.response;

import java.util.Date;

import lombok.Data;

@Data
public class MarcaPaginacionResponseDTO {
	
	public MarcaPaginacionResponseDTO(Long id, String codigo, String descripcion, Date fecha, String usuario) {
		this.id = id;
		this.codigo = codigo;
		this.descripcion = descripcion;
		this.fecha = fecha;
		this.usuario = usuario;
	}
	
	private Long id;
	private String codigo;
	private String descripcion;
	private Date fecha;
	private String usuario;
}
