package com.enel.scom.api.gestion.model;

import javax.persistence.*;

import lombok.Data;

@Entity(name = "ord_responsable")
@Data
public class ResponsableEntity {
    
    @Id
    @Column
    private Long id;

    @Column(name = "id_object")
    private Long idObject;

    @Column(name = "tipo_responsable")
    private String tipoResponsable;

    @Column(name = "id_buzon_default")
    private Long idBuzon;
}
