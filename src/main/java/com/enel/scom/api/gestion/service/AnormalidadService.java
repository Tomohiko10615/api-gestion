package com.enel.scom.api.gestion.service;

import java.util.List;

import com.enel.scom.api.gestion.dto.response.AnormalidadesResponseDTO;

public interface AnormalidadService {
	List<AnormalidadesResponseDTO> getActivos();
}
