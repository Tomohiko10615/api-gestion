package com.enel.scom.api.gestion.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity(name = "DYO_OBJECT")
public class DyoobjectEntity {

    @Id
    @Column
    private Long id;
}
