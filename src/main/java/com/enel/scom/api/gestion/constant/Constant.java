package com.enel.scom.api.gestion.constant;

public class Constant {
	
	//VALORES USADOS PARA LA ANULACION EN EL SERVICIO SOAP
	public static final String SOAP_SERVICE_ANULA_PUENTE_WSDL = "https://tmefltmws-pre.enelint.global/peruwin/distribution/maf/service/P005_AnulacionSuspension";
	public static final String SOAP_SERVICE_ANULA_PARAM_DISTRIBUIDORA = "EDE";
	public static final String SOAP_SERVICE_ANULA_PARAM_ORIGEN = "EDESYN";
	public static final String SOAP_SERVICE_ANULA_PARAM_LLAVE = "FLMFOGAC";
	
	//CODIGOS DE TIPO DE ORDEN 
	public static final String COD_TIPO_ORDEN_INSPECCION = "ORINSP";
	public static final String COD_TIPO_ORDEN_NORMALIZACION = "NORM";
	public static final String COD_TIPO_ORDEN_CONTRASTE = "CONT";
	public static final String COD_TIPO_ORDEN_MANTENIMIENTO = "MANT";
	
	
	//ESTADOS DE LAS ORDENES MANEJADAS EN LA WORKFLOW
	public static final String ESTADO_ORDEN_ANULADA = "Anulada";
	
	
	//ESTADOS MANEJADOS EN LA TRANSFER
	public static final int ESTADO_TRANSFER_VAL_NUM_ENVIADO = 13;
	public static final int ESTADO_TRANSFER_VAL_NUM_ANULADO = 3;
	public static final int ESTADO_TRANSFER_VAL_NUM_ENVIADO_CON_ERROR = 4;
	public static final int ESTADO_TRANSFER_VAL_NUM_PENDIENTE_DE_ANULACION = 12;
	
	//CONFIGURACION ORACLE
	public static final String DATASOURCE_ORACLE = "scomOracleDataSource";
	public static final String PROPERTIES_ORACLE = "scomOracleDataSourceProperties";
	public static final String TRANSACTION_MANAGER_ORACLE = "scomOracleTransactionManager";
	public static final String ENTITY_MANAGER_FACTORY_ORACLE = "scomOracleEntityManagerFactory";
	
}
