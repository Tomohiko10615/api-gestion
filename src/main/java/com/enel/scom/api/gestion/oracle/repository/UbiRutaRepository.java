package com.enel.scom.api.gestion.oracle.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.oracle.model.UbiRutaEntity;

@Repository
public interface UbiRutaRepository extends JpaRepository<UbiRutaEntity, Long>{
	
	@Query(value="SELECT count(*) FROM ubi_ruta ubi where ubi.cod_ruta=:codRuta", nativeQuery = true)
	public Integer obtenerContadorUbiRutaPorCodRuta(@Param("codRuta") String codRuta);
	
}
