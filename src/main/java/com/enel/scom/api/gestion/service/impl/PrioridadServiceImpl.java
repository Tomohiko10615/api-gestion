package com.enel.scom.api.gestion.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.scom.api.gestion.model.PrioridadEntity;
import com.enel.scom.api.gestion.repository.PrioridadRepository;
import com.enel.scom.api.gestion.service.PrioridadService;

@Service
public class PrioridadServiceImpl implements PrioridadService{

    @Autowired
    PrioridadRepository prioridadRepository;

    @Override
    public List<PrioridadEntity> getPrioridades() {
        return prioridadRepository.getActivos();
    }
    
}
