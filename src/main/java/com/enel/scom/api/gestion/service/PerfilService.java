package com.enel.scom.api.gestion.service;


import java.util.List;
import java.util.Optional;

import com.enel.scom.api.gestion.dto.RolesDTO;
import com.enel.scom.api.gestion.model.PerfilEntity;

public interface PerfilService {
   /* PerfilEntity crearPerfil(PerfilEntity perfil);
    PerfilEntity actualizarPerfil(PerfilEntity perfil);
    void eliminarPerfil(Long id) ;
    Optional<PerfilEntity> buscarPorIdPerfil(Long id);
    
    public List<RolesDTO> buscarMenuPorPerfil(Long codperfil); 
	*/
	public List<PerfilEntity> buscarTodosPerfiles();
}
