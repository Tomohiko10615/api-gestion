package com.enel.scom.api.gestion.dto.request;

import lombok.Data;

@Data
public class MedidaMedidorRequestDTO {
	private Long cant_decimales;
	private Long cant_enteros;
	private Float factor_codigo;
	private String factor_descripcion;
	private Float factor_valor;
	private Long id_ent_dec;
	private Long id_factor;
	private Long id_medida;
	private String medida_descripcion;
	private Long id_tip_calculo; // CHR 07-09-23 INC000115648937
}
