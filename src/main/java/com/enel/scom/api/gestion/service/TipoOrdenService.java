package com.enel.scom.api.gestion.service;

import java.util.List;

import com.enel.scom.api.gestion.dto.response.TipoOrdenResponseDTO;

public interface TipoOrdenService {
	 
	/**
	 * Obtiene los tipos de procesos segun su codigo de proceso
	 * @param codTipoProceso codigo del proceso
	 * @return lista de tipos de ordenes filtrada por proceso
	 */
	List<TipoOrdenResponseDTO> obtenerTipoOrdenByCodProceso(String codTipoProceso);
	

	String obtenerIdsTipoOrdenByCodProceso(String codTipoProceso);
}
