package com.enel.scom.api.gestion.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;

@Entity(name = "cae_tmp_cliente")
@Data
public class CaeTmpClienteEntity implements Serializable {/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="id_cae_tmp_cliente")
	private Long idCaeTmpCliente;
	
	@Column(name = "numero_cliente")
	private Long numeroCliente;
	
	@Column(name = "nombre")
	private String nombre;
	
	@Column(name = "tarifa")
	private String tarifa;
	
	@Column(name = "sucursal")
	private Long sucursal;
	
	@Column(name = "telefono")
	private String telefono;
	
	@Column(name = "direccion")
	private String direccion;
	
	@Column(name = "sector")
	private String sector;
	
	@Column(name = "zona")
	private String zona;
	
	@Column(name = "corr_ruta")
	private String corrRuta;
	
	@Column(name = "cod_giro")
	private String codGiro;
	
	@Column(name = "tipo_cliente")
	private String tipoCliente;
	
	@Column(name = "comuna")
	private String comuna;

	@Column(name = "rut")
	private String rut;
	
	@Column(name = "tipo_ident")
	private String tipoIdent;
	
	@Column(name = "estado_cliente")
	private String estadoCliente;
	
	@Column(name = "estado_suministro")
	private String estadoSuministro;
	
	
	@Column(name = "fecha_activacion")
	private Date fechaActivacion;
	
	@Column(name = "fecha_retiro")
	private Date fechaRetiro;
	
	@Column(name = "dv_numero_cliente")
	private Long dvNumeroCliente;
	
	@Column(name = "info_adic_lectura")
	private String infoAdicLectura;
	
	@Column(name = "potencia_cont_fp")
	private Long potenciaContFp;
	
	@Column(name = "potencia_inst_fp")
	private Long potenciaInstFp;
	
	@Column(name = "potencia_inst_hp")
	private Long potenciaInstHp;
	
	@Column(name = "id_pcr")
	private Long idPcr;
	
	@Column(name = "pcr")
	private Long pcr;
	
	@Column(name = "llave")
	private String llave;
	
	@Column(name = "subestac_distrib")
	private String subestacDistrib;
	
	@Column(name = "alimentador")
	private String alimentador;
	
	@Column(name = "subestac_transmi")
	private String subestacTransmi;
	
	@Column(name = "tipo_accion")
	private String tipoAccion;
	
	@Column(name = "estado_proceso")
	private String estadoProceso;
	
	@Column(name = "motivo_rechazo")
	private String motivoRechazo;
	
	@Column(name = "nro_envios")
	private Long nroEnvios;
	
	
}
