package com.enel.scom.api.gestion.exception;

public class ActualizarValoresDeCamposException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ActualizarValoresDeCamposException(String message) {
		super(message);
	}

}
