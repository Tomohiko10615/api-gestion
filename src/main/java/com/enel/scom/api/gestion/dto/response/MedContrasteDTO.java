package com.enel.scom.api.gestion.dto.response;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import lombok.Data;

@Data
public class MedContrasteDTO {
	
	private Long idMedidor;
	private Date fechaEjecucion;
	private BigDecimal conPromReal;
	private List<MedContrastePruebaDTO> pruebaBaja;
	private List<MedContrastePruebaDTO> pruebaNominal;
	private List<MedContrastePruebaDTO> pruebaAlta;
	private List<MedContrastePruebaDTO> pruebaAislamiento;
}
