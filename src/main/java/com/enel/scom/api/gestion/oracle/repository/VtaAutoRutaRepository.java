package com.enel.scom.api.gestion.oracle.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.oracle.model.VtaAutoRutaEntity;

@Repository
public interface VtaAutoRutaRepository extends JpaRepository<VtaAutoRutaEntity, Long> {
	
	@Query(value="SELECT SQVTAAUTORUTA.nextval FROM dual", nativeQuery=true)
	public Long generarIdVtaAutoRuta();
	
	@Query(value = "SELECT * FROM vta_auto_ruta rut WHERE rut.id_auto_act =:idAutoActivacion", nativeQuery = true)
	VtaAutoRutaEntity obtenerRutaPorIdAutoActivacion(@Param("idAutoActivacion") Long idAutoActivacion);
}
