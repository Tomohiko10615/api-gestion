package com.enel.scom.api.gestion.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.enel.scom.api.gestion.model.TecnologiaEntity;
import com.enel.scom.api.gestion.repository.TecnologiaRepository;

@RestController
@RequestMapping("/api/tecnologia")
public class TecnologiaController {

	@Autowired
	TecnologiaRepository tecnologiaRepository;
	
	@GetMapping
	public ResponseEntity<List<TecnologiaEntity>> getAll() {
		List<TecnologiaEntity> responses = tecnologiaRepository.getTecnologiasActivo();
		return new ResponseEntity<>(responses, HttpStatus.OK);
	}
}
