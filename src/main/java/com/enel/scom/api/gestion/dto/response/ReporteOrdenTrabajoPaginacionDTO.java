package com.enel.scom.api.gestion.dto.response;

import java.util.Date;

import lombok.Data;

@Data
public class ReporteOrdenTrabajoPaginacionDTO {
	
	private Long nroCuenta;
	private Long nroServicio;
	private Date fechaCreacion;
	private String tipoProceso;
	private String tipoOrden;
	private String estado;
	private String situacion;
	private Long idMotivo;
	
	
	public ReporteOrdenTrabajoPaginacionDTO(
			Long nroCuenta, 
			Long nroServicio, 
			Date fechaCreacion, 
			String tipoProceso,
			String tipoOrden, 
			String estado, 
			String situacion,
			Long idMotivo) {
		this.nroCuenta = nroCuenta;
		this.nroServicio = nroServicio;
		this.fechaCreacion = fechaCreacion;
		this.tipoProceso = tipoProceso;
		this.tipoOrden = tipoOrden;
		this.estado = estado;
		this.situacion = situacion;
		this.idMotivo = idMotivo;
	}
	
	
}
