package com.enel.scom.api.gestion.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.scom.api.gestion.dto.request.MedFacMedModRequestDTO;
import com.enel.scom.api.gestion.dto.request.MedidaModeloRequestDTO;
import com.enel.scom.api.gestion.dto.response.FactorResponseDTO;
import com.enel.scom.api.gestion.dto.response.ParametroResponseDTO;
import com.enel.scom.api.gestion.exception.NroServicioNotFoundException;
import com.enel.scom.api.gestion.model.FactorEntity;
import com.enel.scom.api.gestion.model.MedFacMedModEntity;
import com.enel.scom.api.gestion.model.MedidaModeloEntity;
import com.enel.scom.api.gestion.repository.FactorRepository;
import com.enel.scom.api.gestion.repository.MedFacMedModRepository;
import com.enel.scom.api.gestion.repository.MedidaModeloRepository;
import com.enel.scom.api.gestion.service.FactorService;

@Service
public class FactorServiceImpl implements FactorService{

	@Autowired
	private FactorRepository factorRepository;
	
	@Autowired
	private MedFacMedModRepository medFacMedModRepository;
	
	@Autowired
	private MedidaModeloRepository medidaModeloRepository;
	
	@Override
	public List<FactorResponseDTO> getAll() {
		List<Object[]> factores = factorRepository.getAll();
		return factores.stream().map(object -> new FactorResponseDTO(Long.valueOf(object[0].toString()), object[1].toString(), object[2].toString(), object[3].toString())).collect(Collectors.toList());
	}

	
	@Override
	public MedFacMedModEntity crearFactor(MedFacMedModRequestDTO medModelo) throws NroServicioNotFoundException {
		
		FactorEntity factor = factorRepository.findId(medModelo.getIdFactor());
		System.out.print("Meida" +medModelo.getIdMedidaModelo() + "|");
		MedidaModeloEntity medidaModeloEntity = medidaModeloRepository.findId(medModelo.getIdMedidaModelo());
		
		if(medidaModeloEntity == null) throw new NroServicioNotFoundException("No se encontró idMeidaModelo");
		
		MedFacMedModEntity medFacMedModEntity = new MedFacMedModEntity();
		medFacMedModEntity.setFactor(factor);
		medFacMedModEntity.setMedidaModelo(medidaModeloEntity.getId());
		medFacMedModRepository.createFactor(medidaModeloEntity.getId(), factor.getId());
		return medFacMedModEntity;
	}

	@Override
	public MedFacMedModEntity delete(Long idMedidaModelo, Long idFactor) {
		MedFacMedModEntity medFacMedModEntity = medFacMedModRepository.findIdMedidaModelo(idMedidaModelo);
		medFacMedModRepository.deleteFactor(idMedidaModelo, idFactor);
		return medFacMedModEntity;
	}


	@Override
	public List<FactorResponseDTO> getFactoresPorMedidaModelo(Long idMedidaModelo) {
		List<Object[]> objects = medFacMedModRepository.getFactoresPorMedidaModelo(idMedidaModelo);
		List<FactorResponseDTO> dtos = new ArrayList<>();
		
		objects.stream().forEach(k -> {
			FactorResponseDTO dto = new FactorResponseDTO();
			dto.setId(Long.valueOf(k[0].toString()));
			dto.setDesFactor(k[1].toString());
			dto.setCodFactor(k[2].toString());
			dto.setValFactor(k[3].toString());
			dtos.add(dto);
		});
		return dtos;
	}

}
