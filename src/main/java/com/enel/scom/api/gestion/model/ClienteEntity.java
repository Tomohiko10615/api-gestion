package com.enel.scom.api.gestion.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;
import lombok.ToString;

@Entity(name = "cliente")
@Data
@ToString
public class ClienteEntity implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "id_servicio")
	private Long idServicio;
	
	@Column(name = "nro_cuenta")
	private String nroCuenta;
	
	@Column(name = "nro_servicio")
	private Long nroServicio;
	
	@Column
	private String nombre;
	
	@Column(name = "apellido_pat")
	private String apellidoPat;
	
	@Column(name = "apellido_mat")
	private String apellidoMat;
	
	@Column(name = "cod_ruta")
	private String codRuta;
	
	@Column(name = "texto_direccion")
	private String textoDireccion;
	
	// R.I. REQSCOM06 Correccion Se requiere el nombre de la columna 06/10/2023
	@Column(name = "distrito")
	private String distrito;

	@Column(name = "nro_docto_ident")
	private String documento;
	
	@Column(name = "srv_tipo_acometida_codigo")
	private String tipoAcometida;

}
