package com.enel.scom.api.gestion.dto.response;

import lombok.Data;

@Data
public class SuministroResponseDTO {
	
	private String nroCuenta;
	
	//CADENA ELECTRICA
	private Long set;
	private Long sed;
	private Long alimentador;
	private Long llave;
	
	//RUTA DE LECTURA
	private String sectorLectura;
	private String zonaLectura;
	private String correlativoLectura;
	
	//RUTA DE CORTE
	private String sectorCorte;
	private String zonaCorte;
	private String correlativoCorte;
	
	//RUTA DE FACTURACION
	private String sectorFacturacion;
	private String zonaFacturacion;
	private String correlativoFacturacion;
	
	//RUTA DE REPARTO
	private String sectorReparto;
	private String zonaReparto;
	private String correlativoReparto;
	
}
