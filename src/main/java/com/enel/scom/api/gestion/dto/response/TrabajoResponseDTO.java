package com.enel.scom.api.gestion.dto.response;

import lombok.Data;

@Data
public class TrabajoResponseDTO {
    private Long id;
    private String descripcion;
}
