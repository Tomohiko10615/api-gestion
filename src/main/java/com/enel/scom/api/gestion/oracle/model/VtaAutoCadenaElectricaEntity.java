package com.enel.scom.api.gestion.oracle.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;

@Entity(name = "vta_auto_dt")
@Data
public class VtaAutoCadenaElectricaEntity implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_auto_dt")
    private Long idAutoDt;
	
    @Column(name="id_auto_act")
    private Long idAutoAct;

    @Column(name = "id_srv_venta")
    private Long idSrvVenta;
    
    @Column(name="id_punto_entrega")
    private Long idPuntoEntrega;
    
    @Column(name="propiedad_empalme")
    private String propiedadEmpalme;
    
    @Column(name="capacidad_interruptor")
    private String capacidadInterruptor;
    
    @Column(name="sistema_proteccion")
    private String sistemaProteccion;
    
    @Column(name="caja_medicion")
    private String cajaMedicion;
    
    @Column(name="caja_toma")
    private String cajaToma;
    
    @Column(name="longitud_acometida")
    private Double longitudAcometida;
    
    @Column(name="id_srv_ele_der")
    private Long idSrvEleDer;
    
    @Column(name="id_srv_ele_izq")
    private Long idSrvEleIzq;
    
    @Column(name="id_set")
    private Long idSet;
    
    @Column(name="id_alimentador")
    private Long idAlimentador;
    
    @Column(name="id_sed")
    private Long idSed;
    
    @Column(name="id_llave")
    private Long idLlave;
    
    @Column(name="id_pcr")
    private Long idPcr;
    
    @Column(name="id_pcr_tension")
    private Long idPcrTension;
    
    @Column(name="tipo_conductor")
    private String tipoConductor;
   
}
