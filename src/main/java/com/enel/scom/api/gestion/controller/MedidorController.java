package com.enel.scom.api.gestion.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.enel.scom.api.gestion.dto.request.MedidorRequestDTO;
import com.enel.scom.api.gestion.dto.response.MedidorDataResponseDTO;
import com.enel.scom.api.gestion.dto.response.MedidorPaginacionResponseDTO;
import com.enel.scom.api.gestion.exception.NroServicioNotFoundException;
import com.enel.scom.api.gestion.model.MedidorEntity;
import com.enel.scom.api.gestion.service.MedidorService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api/medidor")
public class MedidorController {
	
	@Autowired
	MedidorService medidorService;
	
	@GetMapping
    public ResponseEntity<Page<MedidorPaginacionResponseDTO>> getModeloPaginacion(
			@RequestParam(required=false) String nro_medidor,
			@RequestParam(required=false) Long id_marca,
			@RequestParam(required=false) Long id_modelo,
			@RequestParam(required=false) Long id_estado,
			@RequestParam(required=false) String usuario,
			@RequestParam(required=false) String fecha_inicio,
			@RequestParam(required=false) String fecha_fin,
			@RequestParam(required=false) String fecha_inicio_estado,
			@RequestParam(required=false) String fecha_fin_estado,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "5") int size,
            @RequestParam(defaultValue = "id") String order,
            @RequestParam(defaultValue = "true") boolean asc
    ){
		log.info("GET /api/medidor getModeloPaginacion() INICIA");
		log.info(" nro_medidor ="+nro_medidor);
		log.info(" id_marca ="+id_marca);
		log.info(" id_modelo ="+id_modelo);
		log.info(" id_estado ="+id_estado);
		log.info(" usuario ="+usuario);
		log.info(" fecha_inicio ="+fecha_inicio);
		log.info(" fecha_fin ="+fecha_fin);
		log.info(" fecha_inicio_estado ="+fecha_inicio_estado);
		log.info(" fecha_fin_estado ="+fecha_fin_estado);
		log.info(" page ="+page);
		log.info(" size ="+size);
		log.info(" order ="+order);
		log.info(" asc ="+asc);
		
//        Page<MedidorPaginacionResponseDTO> marcas = medidorService.getMedidorPaginacion(
//                PageRequest.of(page, size, Sort.by(order)), nro_medidor, id_marca, id_modelo, usuario, fecha_inicio, fecha_fin, fecha_inicio_estado, fecha_fin_estado, id_estado);
        
		//v2 Works!
		Page<MedidorPaginacionResponseDTO> marcas = medidorService.getMedidorPaginacion_V2(
                PageRequest.of(page, size, Sort.by(order)), nro_medidor, id_marca, id_modelo, usuario, fecha_inicio, fecha_fin, fecha_inicio_estado, fecha_fin_estado, id_estado);

//        if(!asc)
//            marcas = medidorService.getMedidorPaginacion(
//                    PageRequest.of(page, size, Sort.by(order).descending()),nro_medidor, id_marca, id_modelo, usuario, fecha_inicio, fecha_fin, fecha_inicio_estado, fecha_fin_estado, id_estado);
        log.info("GET /api/medidor getModeloPaginacion() FIN");
        return new ResponseEntity<>(marcas, HttpStatus.OK); 
    }
	
	@PostMapping("/store")
	public ResponseEntity<MedidorEntity> postStore(@RequestBody MedidorRequestDTO medidorRequest) throws NroServicioNotFoundException {
		
		log.info("POST /api/medidor/store postStore()");
		MedidorEntity medidor = medidorService.postStore(medidorRequest);
		
		log.info("POST /api/medidor/store postStore() FIN");
		return new ResponseEntity<>(medidor, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<MedidorDataResponseDTO> getData(@PathVariable Long id) {
		
		log.info("GET /api/medidor/ ID ="+id);
		
		MedidorDataResponseDTO medidor = medidorService.getData(id);
		
		log.info("GET /api/medidor/ ID ="+id +", FIN");
		return new ResponseEntity<>(medidor, HttpStatus.OK);
	}

	@DeleteMapping("/delete/{id}")
	public ResponseEntity<String> delete(@PathVariable Long id) {
		log.info("GET /api/medidor/delete ID ="+id);
		
		medidorService.delete(id);
		log.info("GET /api/medidor/delete ID ="+id+", FIN");
		return new ResponseEntity<>("Medidor Eliminado", HttpStatus.OK);
	}
	
}
