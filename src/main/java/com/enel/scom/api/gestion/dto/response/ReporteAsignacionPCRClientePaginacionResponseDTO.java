package com.enel.scom.api.gestion.dto.response;

import java.util.Date;

import lombok.Data;

@Data
public class ReporteAsignacionPCRClientePaginacionResponseDTO {
	
	private String nroCliente;
	private String nombreCliente;
	private Date fechaActivacion;
	private String situacionPCR;
	
	public ReporteAsignacionPCRClientePaginacionResponseDTO(String nroCliente, String nombreCliente,
			Date fechaActivacion, String situacionPCR) {
		super();
		this.nroCliente = nroCliente;
		this.nombreCliente = nombreCliente;
		this.fechaActivacion = fechaActivacion;
		this.situacionPCR = situacionPCR;
	}
	
	
	
	
	
}
