package com.enel.scom.api.gestion.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Entity(name = "nuc_persona")
@Data
public class PersonaEntity implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column
	private String nombre;
	
	@Column(name = "apellido_mat")
	private String apellidoMat;
	
	@Column(name = "apellido_pat")
	private String apellidoPat;
	
	@Column
	private String estado;
}
