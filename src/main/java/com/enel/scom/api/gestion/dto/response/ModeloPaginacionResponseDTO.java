package com.enel.scom.api.gestion.dto.response;

import java.util.Date;

import lombok.Data;

@Data
public class ModeloPaginacionResponseDTO {
	
	public ModeloPaginacionResponseDTO(Long id, String codigo, String descripcion, String marca, Date fecha, String usuario) {
		this.id = id;
		this.codigo = codigo;
		this.descripcion = descripcion;
		this.marca = marca;
		this.fecha = fecha;
		this.usuario = usuario;
	}
	
	private Long id;
	private String codigo;
	private String descripcion;
	private String marca;
	private Date fecha;
	private String usuario;
}
