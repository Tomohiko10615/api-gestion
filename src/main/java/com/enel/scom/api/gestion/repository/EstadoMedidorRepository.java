package com.enel.scom.api.gestion.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.model.EstadoMedidorEntity;

@Repository
public interface EstadoMedidorRepository extends JpaRepository<EstadoMedidorEntity, Long>{

	@Query(value = "SELECT * FROM med_est_componente WHERE cod_interno = 'Creado'", nativeQuery = true)
	EstadoMedidorEntity getCreado();
	
	@Query(value = "SELECT * FROM med_est_componente WHERE activo = true ORDER BY des_est_componente ASC", nativeQuery = true)
	List<EstadoMedidorEntity> getActivos();
}
