package com.enel.scom.api.gestion.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.model.ContratistaEntity;

@Repository
public interface ContratistaRepository extends JpaRepository<ContratistaEntity, Long>{
	@Query(value = "SELECT cc.id, np.nombre, np.apellido_pat, np.apellido_mat, cc.cod_contratista FROM com_contratista cc  INNER JOIN nuc_persona np ON np.id = cc.id_persona WHERE cc.activo = 'S' ORDER BY np.nombre ASC", nativeQuery = true)
	List<Object[]> getActivos();
	
	
	@Query(value = "SELECT * FROM com_contratista WHERE id = :id", nativeQuery = true)
	ContratistaEntity findId(@Param("id") Long id);
}
