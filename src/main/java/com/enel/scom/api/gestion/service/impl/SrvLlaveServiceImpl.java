package com.enel.scom.api.gestion.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.scom.api.gestion.dto.response.AutoCompleteResponseDTO;
import com.enel.scom.api.gestion.oracle.repository.SrvLlaveRpository;
import com.enel.scom.api.gestion.service.SrvLlaveService;

@Service
public class SrvLlaveServiceImpl implements SrvLlaveService {
	
	
	@Autowired
	private SrvLlaveRpository srvLlaveRpository;

	@Override
	public List<AutoCompleteResponseDTO> buscarLlaveServicioElectrico(String descripcion, Long idSed) {
		List<Object[]> llavesCadenaElectrica = srvLlaveRpository.buscarLlaveServicioElectrico(descripcion, idSed);
		return llavesCadenaElectrica.stream().map(object -> new AutoCompleteResponseDTO(
				Long.valueOf(object[0].toString()), 
				object[1].toString().trim(),
				object[2].toString().trim()
				)).collect(Collectors.toList());
	}

}
