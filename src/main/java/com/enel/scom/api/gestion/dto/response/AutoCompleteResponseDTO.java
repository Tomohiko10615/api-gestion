package com.enel.scom.api.gestion.dto.response;

import lombok.Data;

@Data
public class AutoCompleteResponseDTO {
	
	private Long id;
	private String codigo;
	private String descripcion;
	
	public AutoCompleteResponseDTO(Long id, String codigo) {
		this.id = id;
		this.codigo = codigo;
	}

	public AutoCompleteResponseDTO(Long id, String codigo, String descripcion) {
		this.id = id;
		this.codigo = codigo;
		this.descripcion = descripcion;
	}
	
	
	
}
