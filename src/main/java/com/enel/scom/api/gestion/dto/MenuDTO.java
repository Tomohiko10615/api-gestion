package com.enel.scom.api.gestion.dto;

import java.util.List;

import lombok.Data;

@Data
public class MenuDTO {
	
	private Long id;
	private String nombre;
	private String descripcion;
	private String icono;
	private String url;
	private Long id_padre;
	private String param_nombre;
	private String nombre_modulo;
	private String clase;
	private boolean titulo_grupo;
	private List<MenuDTO> sub_items;
	
}
