package com.enel.scom.api.gestion.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Entity(name = "med_ent_dec")
@Data
public class MedEntDecEntity implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "cod_ent_dec")
	private String codEntDec;
	
	@Column(name = "cant_enteros")
	private String cantEnteros;
	
	@Column(name = "cant_decimales")
	private String cantDecimales;
}
