package com.enel.scom.api.gestion.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Entity(name = "ord_tipo_orden")
@Data
public class TipoOrdenEntity implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "des_tipo_orden")
	private String descripcion;
	
	@Column(name = "cod_tipo_orden")
	private String codigoTipoOrden;

	@Column(name = "cod_interno")
	private String codigoInterno;

	@Column(name = "discriminador")
	private String discriminador;
	
	@Column(name = "cod_tipo_proceso")
	private String codigoTipoProceso;
}
