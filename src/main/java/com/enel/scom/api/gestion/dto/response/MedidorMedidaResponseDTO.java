package com.enel.scom.api.gestion.dto.response;

import lombok.Data;

@Data
public class MedidorMedidaResponseDTO {
	private Long id_medida;
	private String medida_descripcion;
	private Long id_ent_dec;
	private String cant_enteros;
	private String cant_decimales;
	private Long id_factor;
	private String factor_codigo;
	private String factor_descripcion;
	private String factor_valor;
	private Long id_tip_calculo; // CHR 07-09-23 INC000115648937
}
