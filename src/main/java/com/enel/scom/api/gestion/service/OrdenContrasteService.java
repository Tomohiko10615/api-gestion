package com.enel.scom.api.gestion.service;

import com.enel.scom.api.gestion.dto.request.OrdenContrasteRequestDTO;
import com.enel.scom.api.gestion.dto.response.OrdenDataResponseDTO;
import com.enel.scom.api.gestion.model.OrdenContrasteEntity;
import com.enel.scom.api.gestion.model.OrdenEntity;

public interface OrdenContrasteService {
	OrdenEntity storeUpdate(OrdenContrasteRequestDTO ordenNormalizacionDTO);
	OrdenDataResponseDTO getId(Long id);
}
