package com.enel.scom.api.gestion.service.impl;

import java.io.StringWriter;
import java.util.List;
import java.util.stream.Collectors;

import org.dom4j.DocumentHelper;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.enel.scom.api.gestion.dto.response.OrdenXMLResponseDTO;
import com.enel.scom.api.gestion.dto.response.TransferResponseDTO;
import com.enel.scom.api.gestion.exception.NroServicioNotFoundException;
import com.enel.scom.api.gestion.repository.TransferRepository;
import com.enel.scom.api.gestion.service.TransferService;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;


import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class TransferServiceImpl implements TransferService{

    @Autowired
    TransferRepository transferRepository;

    @Override
    public List<TransferResponseDTO> getIdOrden(Long idOrden) {
        List<Object[]> transfers = transferRepository.findByIdOrden(idOrden);
        
        return transfers.stream().map(object -> new TransferResponseDTO((object[0] == null) ? "" : object[0].toString(), 
                                                                        (object[1] == null) ? "" : object[1].toString(), 
                                                                        (object[2] == null) ? "" : object[2].toString(), 
                                                                        (object[3] == null) ? "" : object[3].toString(), 
                                                                        (object[4] == null) ? "" : object[4].toString(), 
                                                                        (object[5] == null) ? "" : object[5].toString(), 
                                                                        (object[6] == null) ? "" : object[6].toString(), 
                                                                        (object[7] == null) ? "" : object[7].toString(), 
                                                                        (object[8] == null) ? "" : object[8].toString(), 
                                                                        (object[9] == null) ? "" : object[9].toString(), 
                                                                        (object[10] == null) ? "" : object[10].toString(),
                                                                        (object[11] == null) ? "" : object[11].toString(),
                                                                        (object[12] == null) ? "" : object[12].toString(),
                                                                        (object[13] == null) ? "" : object[13].toString(),
                                                                        (object[14] == null) ? "" : object[14].toString(),
                                                                        (object[15] == null) ? "" : object[15].toString(),
                                                                        (object[16] == null) ? "" : object[16].toString(),
                                                                        (object[17] == null) ? "" : object[17].toString(),
                                                                        (object[18] == null) ? "" : object[18].toString(),
                                                                        (object[19] == null) ? "" : object[19].toString(),
                                                                        (object[20] == null) ? 0 : Long.valueOf(object[20].toString())
                                                                        )).collect(Collectors.toList());
    }

	@Override
	public OrdenXMLResponseDTO obtenerXMLTransferencia(Long id, String accion) throws NroServicioNotFoundException {
		/*String objectXml = null;
		String xmlPretty="";
		OrdenXMLResponseDTO ordenXML = new OrdenXMLResponseDTO();
		
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		
		try {
			
			objectXml = transferRepository.obtenerXMLTransferencia(id, accion);
			if(objectXml == null) {
				throw new NroServicioNotFoundException("No hay datos de: " + accion);
			}
			
			 DocumentBuilder db;
			try {
				db = dbf.newDocumentBuilder();
			} catch (ParserConfigurationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

	          Document doc;
			try {
				doc = db.parse(objectXml);
			} catch (SAXException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

	          // optional, but recommended
	          // http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
	          doc.getDocumentElement().normalize();

	          System.out.println("Root Element :" + doc.getDocumentElement().getNodeName());
	          System.out.println("------");

	          // get <staff>
	          NodeList list = doc.getElementsByTagName("staff");

	          for (int temp = 0; temp < list.getLength(); temp++) {

	              Node node = list.item(temp);

	              if (node.getNodeType() == Node.ELEMENT_NODE) {

	                  Element element = (Element) node;

	                  // get staff's attribute
	                //  String id = element.getAttribute("id");

	                  // get text
	                  String firstname = element.getElementsByTagName("firstname").item(0).getTextContent();
	                  String lastname = element.getElementsByTagName("lastname").item(0).getTextContent();
	                  String nickname = element.getElementsByTagName("nickname").item(0).getTextContent();

	                  NodeList salaryNodeList = element.getElementsByTagName("salary");
	                  String salary = salaryNodeList.item(0).getTextContent();

	                  // get salary's attribute
	                  String currency = salaryNodeList.item(0).getAttributes().getNamedItem("currency").getTextContent();

	                  System.out.println("Current Element :" + node.getNodeName());
	                  System.out.println("Staff Id : " + id);
	                  System.out.println("First Name : " + firstname);
	                  System.out.println("Last Name : " + lastname);
	                  System.out.println("Nick Name : " + nickname);
	                  System.out.printf("Salary [Currency] : %,.2f [%s]%n%n", Float.parseFloat(salary), currency);

	              }
	          }

			

		} catch (EmptyResultDataAccessException ee) {
			log.info("Xml para el nro orden {} no existe", id);
			log.error("EmptyResultDataAccessException->" + ee.getMessage(), ee);
		} catch (DataAccessException de) {
			log.info("Error al obtener Xml");
			log.error("DataAccessException->" + de.getMessage(), de);
		}
		return ordenXML;*/
		return null;

	}
    
	
	
	/**
	 * Formatea o parsea el XML obtenido
	 */
	public String getString(String tagName, Element element) {
		
	        NodeList list = element.getElementsByTagName(tagName);
	        if (list != null && list.getLength() > 0) {
	            NodeList subList = list.item(0).getChildNodes();

	            if (subList != null && subList.getLength() > 0) {
	                return subList.item(0).getNodeValue();
	            }
	        }

	        return null;
	    
	}
	}
