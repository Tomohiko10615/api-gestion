package com.enel.scom.api.gestion.dto.request;

import lombok.Data;

@Data
public class CambiarEstadoTareaRequestDTO {
	private Long id;
	private String tareaEstado;
}
