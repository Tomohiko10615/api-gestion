package com.enel.scom.api.gestion.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.model.PrioridadEntity;

@Repository
public interface PrioridadRepository extends JpaRepository<PrioridadEntity, Long>{
    
	@Query(value = "SELECT * FROM org_prioridad ORDER BY des_prioridad ASC", nativeQuery = true)
	List<PrioridadEntity> getActivos();
}
