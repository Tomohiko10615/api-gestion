package com.enel.scom.api.gestion.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.enel.scom.api.gestion.dto.response.TransferResponseDTO;
import com.enel.scom.api.gestion.service.TransferService;

@RestController
@RequestMapping("/api/ordenTransfer")
public class TransferController {
    @Autowired
    TransferService transferService;

    @GetMapping("/idOrden/{idOrden}")
    public ResponseEntity<List<TransferResponseDTO>> getNroOrden(@PathVariable Long idOrden) {
        List<TransferResponseDTO> response = transferService.getIdOrden(idOrden);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
