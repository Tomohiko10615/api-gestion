package com.enel.scom.api.gestion.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;

@Entity(name = "activacion")
@Data
public class ActivacionEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="numero_cuenta")
	private Long numeroCuenta;
	
	@Column(name="numero_orden_conexion")
	private Long numeroOrdenConexion;
	
	@Column(name="estado_suministro")
	private String estadoSuministro;
	
	@Column(name="fecha_activacion")
	private Date fechaActivacion;
	
	@Column(name="estado_proceso")
	private String estadoProceso;
	
	@Column(name="estado_activacion")
	private String estadoActivacion;
	
	@Column(name="id_auto_act")
	private Long idAutoAct;
	
	@Column(name="numero_orden_venta")
	private Long numeroOrdenVenta;
	
	@Column(name="resultado_caetmpcliente_4j")
	private String resultadoCeatmpcliente4j;
}
