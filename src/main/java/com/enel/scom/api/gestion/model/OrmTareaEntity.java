package com.enel.scom.api.gestion.model;

import javax.persistence.Entity;
import javax.persistence.*;

import lombok.Data;

@Entity(name = "orm_tarea")
@Data
public class OrmTareaEntity {
    @Id
    @Column(name = "id")
    private Long id;
    @Column(name = "description")
    private String description;
    @Column(name = "estado")
    private String estado;
}
