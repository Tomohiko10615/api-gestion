package com.enel.scom.api.gestion.model;

import java.io.Serializable;

import javax.persistence.*;

import lombok.Data;

@Entity(name = "med_ord_come")
@Data
public class OrdenContrasteEntity implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id_orden")
	private Long idOrden;
	
	@OneToOne
	@JoinColumn(name = "id_orden_inspeccion")
	private OrdenInspeccionEntity ordenInspeccion;

	@Column(name = "id_componente")
	private Long idComponente;
	
	@Column(name="id_contraste")
	private Long idContraste;
}
