package com.enel.scom.api.gestion.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import lombok.Data;

@Entity(name = "med_medida_medidor")
@Data
public class MedidaMedidorEntity implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column
	private Long id;
	
	@Column(name = "cant_enteros")
	private Long cantEnteros;
	
	@Column(name = "cant_decimales")
	private Long cantDecimales;
	
	@Column(name = "val_factor")
	private Float valFactor;
	
	@OneToOne
	@JoinColumn(name = "id_medida")
	private MedidaEntity medida;
	
	@OneToOne
	@JoinColumn(name = "id_componente")
	private MedidorEntity medidor;
	
	@OneToOne
	@JoinColumn(name = "id_factor")
	private FactorEntity factor;

	@OneToOne
	@JoinColumn(name = "id_ent_dec")
	private MedEntDecEntity medentdec;
	
	// CHR 07-09-23 INC000115648937
	@OneToOne
	@JoinColumn(name = "id_tip_calculo")
	private MedTipCalculoEntity medtipcalculo;
}
