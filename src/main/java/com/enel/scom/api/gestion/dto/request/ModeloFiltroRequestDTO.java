package com.enel.scom.api.gestion.dto.request;

import java.util.Date;

import lombok.Data;

@Data
public class ModeloFiltroRequestDTO {
	private String cod_modelo;
	private String usuario;
	private Date fecha_inicio;
	private Date fecha_fin;
	private Long id_marca;
	private String des_modelo;
}
