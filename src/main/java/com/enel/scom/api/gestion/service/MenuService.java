package com.enel.scom.api.gestion.service;

import java.util.List;

import com.enel.scom.api.gestion.dto.MenuDTO;

public interface MenuService {
	
	List<MenuDTO> getMenuByIdPerfil(Long idPerfil);
	
}
