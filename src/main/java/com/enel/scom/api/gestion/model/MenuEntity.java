package com.enel.scom.api.gestion.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "menu", schema = "schscom")
@Data
public class MenuEntity implements Serializable {
	
	  	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

		@Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    @Column(name = "id", nullable = false)
	    private Long id;
	  	
	  	 @Column(length = 60)
	     private String nombre;
	     
	     @Column
	     private String descripcion;
	     
	     @Column(name="param_nombre")
	     private String paramNombre;
	     
	     @Column
	     private String icono;
	     
	     @Column
	     private String url;
	     
	     @Column
	     private String activo;
	     
	     @Column
	     private String componente;
	     
	     @Column
	     private Long orden;
	     
	     @Column
	     private Long idPadre;
	     
	     @Column(name="nombre_modulo")
	     private String nombreModulo;
	     
	     @Column
	     private String clase;
	     
	     @Column(name="titulo_grupo")
	     private boolean tituloGrupo;
}
