package com.enel.scom.api.gestion.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.model.MotivoEntity;

@Repository
public interface MotivoRepository extends JpaRepository<MotivoEntity, Long>{
	@Query(value = "SELECT * FROM ord_motivo_creacion WHERE activo = 'S' AND cod_interno LIKE 'INS%' ORDER BY des_motivo ASC", nativeQuery = true)
	List<MotivoEntity> getActivos();

	@Query(value = "SELECT * FROM ord_motivo_creacion WHERE activo = 'S' AND cod_interno = 'ContrasteHurto' ORDER BY des_motivo ASC", nativeQuery = true)
	List<MotivoEntity> getContraste();
	
	@Query(value = "SELECT * FROM ord_motivo_creacion WHERE id = :id", nativeQuery = true)
	MotivoEntity findId(@Param("id") Long id);

	@Query(value = "SELECT DISTINCT a.id_motivo FROM orm_motivo a, ord_motivo_creacion b WHERE  a.id_motivo=b.id AND b.cod_motivo= :codigo", nativeQuery = true)
	Long obtenerMotivoId(@Param("codigo") String codigo);
}
