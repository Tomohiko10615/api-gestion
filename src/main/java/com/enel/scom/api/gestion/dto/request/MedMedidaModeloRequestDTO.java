package com.enel.scom.api.gestion.dto.request;

import lombok.Data;

@Data
public class MedMedidaModeloRequestDTO {
	
	private Long idModelo;
	private Long idMedida;
	private Long idEntDec;
	private Long idTipoCalculo; //CHR 04-07-23
	
}
