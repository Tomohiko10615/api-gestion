package com.enel.scom.api.gestion.model;

import javax.persistence.*;

import lombok.Data;

@Data
@Entity(name = "ord_orden_cont")
public class OrdenContratistaEntity {
    @Id
    @Column
    Long id;

    @Column(name = "id_orden")
    private Long idOrden;

    @Column(name = "id_contratista")
    private Long idContratista;
}
