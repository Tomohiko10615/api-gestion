package com.enel.scom.api.gestion.service;



import java.util.List;
import java.util.Optional;

import com.enel.scom.api.gestion.dto.MenuDTO;
import com.enel.scom.api.gestion.dto.response.AccesoUsuarioResponseDTO;
import com.enel.scom.api.gestion.model.UsuarioEntity;


public interface UsuarioService {
    public Optional<UsuarioEntity> findByUsername(String username);
    
    List<UsuarioEntity> buscarTodosUsuarios();
    
    Optional<UsuarioEntity> buscarPorIdUsuario(Long id);
    
    void updateFechaUltimoAccesoUsuario(String username);
    
    Optional<UsuarioEntity> buscarPorId(Long id);
    
    AccesoUsuarioResponseDTO updatePerfilUsuario(String username, String idPerfil);
    
    
}
