package com.enel.scom.api.gestion.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.enel.scom.api.gestion.dto.response.AutoCompleteResponseDTO;
import com.enel.scom.api.gestion.service.SrvAlimentadorService;

@RestController
@RequestMapping("api/srv-alimentador")
public class SrvAlimentadorController {
	
	@Autowired
	private SrvAlimentadorService srvAlimentadorService;
	
	 @GetMapping("/search") 
	 public ResponseEntity<List<AutoCompleteResponseDTO>> buscarAlimentadorServicioElectrico(@RequestParam(required=false) String desAlimentador,
			 @RequestParam(required=false) Long idSet) { 
	 List<AutoCompleteResponseDTO> listado = srvAlimentadorService.buscarAlimentadorServicioElectrico(desAlimentador, idSet);
    	return new ResponseEntity<>(listado, HttpStatus.OK);
    }
}
