package com.enel.scom.api.gestion.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.enel.scom.api.gestion.model.MedTipCalculoEntity;
import com.enel.scom.api.gestion.repository.MedTipCalculoRepository;

@RestController
@RequestMapping("/api/tipoCalculo")
public class MedTipCalculoController {

	@Autowired
	MedTipCalculoRepository medTipCalculoRepository;
	
	@GetMapping
	public ResponseEntity<List<MedTipCalculoEntity>> getTipoCalculo() {
		List<MedTipCalculoEntity> responses =  medTipCalculoRepository.getTipoCalculo();
		return new ResponseEntity<>(responses, HttpStatus.OK);
	}
	
}
