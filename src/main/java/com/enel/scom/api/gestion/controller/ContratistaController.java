package com.enel.scom.api.gestion.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.enel.scom.api.gestion.dto.response.ContratistaResponseDTO;
import com.enel.scom.api.gestion.service.ContratistaService;

@RestController
@RequestMapping("/api/contratista")
public class ContratistaController {
	@Autowired
	ContratistaService contratistaService;
	
	@GetMapping
	public ResponseEntity<List<ContratistaResponseDTO>> getActivos() {
		List<ContratistaResponseDTO> responses = contratistaService.getActivos();
		return new ResponseEntity<>(responses, HttpStatus.OK);
	}
}
