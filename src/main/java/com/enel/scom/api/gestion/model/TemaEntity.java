package com.enel.scom.api.gestion.model;

import javax.persistence.*;

import lombok.Data;

@Entity(name = "orm_tema")
@Data
public class TemaEntity {
    @Id
    @Column(name = "id")
    private Long id;

	@Column(name = "code")
    private String code;

	@Column(name = "description")
    private String description;

	@Column(name = "estado")
    private String estado;
    
	@Column(name = "id_motivo")
    private Long id_motivo;
}
