package com.enel.scom.api.gestion.model;

import javax.persistence.*;

import lombok.Data;

@Entity(name = "orm_trabajo")
@Data
public class TrabajoEntity {

	@Id
    @Column(name = "id")
    private Long id;
    @Column(name = "code")
    private String code;
    @Column(name = "description")
    private String description;
    @Column(name = "estado")
    private String estado;
    @Column(name = "id_tema")
    private Long idTema;
    @Column(name = "id_accion_campo")
    private Long idAccionCampo;
    @Column(name = "id_tipo_obra_civil")
    private Long idTipoObraCivil;
    @Column(name = "tipo_orden_campo")
    private String tipoOrdenCampo;
}
