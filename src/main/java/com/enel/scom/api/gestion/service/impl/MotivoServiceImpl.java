package com.enel.scom.api.gestion.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.scom.api.gestion.model.MotivoEntity;
import com.enel.scom.api.gestion.repository.MotivoRepository;
import com.enel.scom.api.gestion.service.MotivoService;

@Service
public class MotivoServiceImpl implements MotivoService{

    @Autowired
    MotivoRepository motivoRepository;

    @Override
    public List<MotivoEntity> getActivos() {
        return motivoRepository.getActivos();
    }

    @Override
    public List<MotivoEntity> getContraste() {
        return motivoRepository.getContraste();
    }
    
}
