package com.enel.scom.api.gestion.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.enel.scom.api.gestion.dto.RolesDTO;
import com.enel.scom.api.gestion.model.PerfilEntity;
import com.enel.scom.api.gestion.model.RolEntity;
import com.enel.scom.api.gestion.repository.PerfilRepository;
import com.enel.scom.api.gestion.repository.RoleRepository;
import com.enel.scom.api.gestion.service.PerfilService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class PerfilServiceImpl implements PerfilService {
	
	@Autowired private PerfilRepository repository;
	
	/*
	 * 
	 * 
	 * @Autowired private RoleRepository roleRepository;
	 * 
	 * @Override
	 * 
	 * @Transactional public PerfilEntity crearPerfil(PerfilEntity o) {
	 * o.setCodPerfil(null); List<String> names = new ArrayList<>();
	 * o.getRoles().stream().forEach(rol->{ names.add(rol.getNombre());});
	 * 
	 * List<RolEntity> roleList = roleRepository.findByNombreIn(names);
	 * o.setRoles(roleList); o.setFecModif(new Date()); return repository.save(o); }
	 * 
	 * @Override public PerfilEntity actualizarPerfil(PerfilEntity o) { if
	 * (repository.findById(o.getCodPerfil()).isPresent()) { List<String> names =
	 * new ArrayList<>(); o.getRoles().stream().forEach(rol->{
	 * names.add(rol.getNombre());});
	 * 
	 * List<RolEntity> roleList = roleRepository.findByNombreIn(names);
	 * o.setRoles(roleList); o.setFecModif(new Date()); return repository.save(o); }
	 * else return null; }
	 * 
	 * @Override public void eliminarPerfil(Long id){ Optional<PerfilEntity> o =
	 * repository.findById(id); if (o.isPresent()) { repository.delete(o.get()); } }
	 * 
	 * @Override public Optional<PerfilEntity> buscarPorIdPerfil(Long id) { return
	 * repository.findById(id); }
	 * 
	 * 
	 * 
	 * @Override public List<RolesDTO> buscarMenuPorPerfil(Long codperfil) {
	 * 
	 * List<Object[]> rows = new ArrayList<Object[]>(); rows =
	 * repository.findMenuPerfil(codperfil); List<RolesDTO> enviar= new
	 * ArrayList<>();
	 * 
	 * for(Object[] row : rows) { RolesDTO dto = new RolesDTO();
	 * dto.setCodRol(row[0] == null ? null : Long.valueOf(row[0].toString()));
	 * dto.setNombre(row[1] == null ? null : row[1].toString()); dto.setMenu(row[2]
	 * == null ? null : row[2].toString()); dto.setNombreMenu(row[3] == null ? null
	 * : row[3].toString()); dto.setComponente(row[4] == null ? null :
	 * row[4].toString()); dto.setIcono(row[5] == null ? null : row[5].toString());
	 * dto.setOrden(row[6] == null ? null : Integer.valueOf(row[6].toString()));
	 * enviar.add(dto); } return enviar; }
	 */
	
	@Override public List<PerfilEntity> buscarTodosPerfiles() { 
		return repository.findAll(); }
}
