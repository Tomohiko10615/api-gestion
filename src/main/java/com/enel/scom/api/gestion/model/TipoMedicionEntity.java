package com.enel.scom.api.gestion.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;

@Entity(name = "com_tip_medicion")
@Data
public class TipoMedicionEntity{
	
	@Id
	@Column
	private Long id;
	
	@Column(name = "des_tip_medicion")
	private String descripcion;
	
	@Column
	private String activo;

}
