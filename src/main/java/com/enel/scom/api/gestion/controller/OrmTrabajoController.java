package com.enel.scom.api.gestion.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.enel.scom.api.gestion.dto.response.TrabajoResponseDTO;
import com.enel.scom.api.gestion.service.OrmConfiguracionService;

@RestController
@RequestMapping("/api/ormTrabajo")
public class OrmTrabajoController {

    @Autowired
    OrmConfiguracionService ormConfiguracionService;

    @GetMapping("/trabajos")
    public ResponseEntity<List<TrabajoResponseDTO>> getTrabajo() {
        List<TrabajoResponseDTO> responses = ormConfiguracionService.getTrabajos();
        return new ResponseEntity<>(responses, HttpStatus.OK);
    }
}
