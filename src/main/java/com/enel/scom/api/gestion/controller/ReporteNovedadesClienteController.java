package com.enel.scom.api.gestion.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.enel.scom.api.gestion.dto.response.ReporteGraficoDTO;
import com.enel.scom.api.gestion.dto.response.ReporteNovedadesClientePaginacionResponseDTO;
import com.enel.scom.api.gestion.service.ReporteNovedadesClienteService;

@RestController
@RequestMapping("api/reportes/novedades-cliente")
public class ReporteNovedadesClienteController {
	
	@Autowired
	ReporteNovedadesClienteService reporteNovedadesClienteService;
	

	@GetMapping("/general")
    public ResponseEntity<Page<ReporteNovedadesClientePaginacionResponseDTO>> obtenerReporteNovedadesClientePaginacion(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size,
            @RequestParam(defaultValue = "id") String order,
            @RequestParam(defaultValue = "true") boolean asc,
			@RequestParam(required=false) String fechaInicio,
			@RequestParam(required=false) String fechaFin,
			@RequestParam(required=false) String estadoProceso,
			@RequestParam(required=false) String nroCliente,
			@RequestParam(required=false) String tipoOrden
    ){
        Page<ReporteNovedadesClientePaginacionResponseDTO> reportePag = reporteNovedadesClienteService.obtenerReporteNovedadesClientePaginacion(
                PageRequest.of(page, size, Sort.by(order)), fechaInicio, fechaFin, estadoProceso, nroCliente, tipoOrden);
       if(!asc)
    	   reportePag = reporteNovedadesClienteService.obtenerReporteNovedadesClientePaginacion(
        			PageRequest.of(page, size, Sort.by(order)),fechaInicio, fechaFin, estadoProceso, nroCliente, tipoOrden);

        return new ResponseEntity<Page<ReporteNovedadesClientePaginacionResponseDTO>>(reportePag, HttpStatus.OK); 
    }
	
	@GetMapping("/cantidadXsituacion")
	public List<ReporteGraficoDTO> reporteNovedadesClienteCantidadXsituacion(
			@RequestParam(required=false) String fechaInicio,
			@RequestParam(required=false) String fechaFin,
			@RequestParam(required=false) String estadoProceso,
			@RequestParam(required=false) String nroCliente,
			@RequestParam(required=false) String tipoOrden) {
		List<ReporteGraficoDTO> reporteCantXestado = reporteNovedadesClienteService.obtenerReporteNovedadesClienteCantidadXsituacion(
				fechaInicio, fechaFin, estadoProceso, nroCliente, tipoOrden);
		return reporteCantXestado;
	}

}
