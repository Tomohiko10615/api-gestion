package com.enel.scom.api.gestion.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.model.OrdenContratistaEntity;

@Repository
public interface OrdenContratistaRepository extends JpaRepository<OrdenContratistaEntity, Long>{
    @Query(value="SELECT nextval('sqordencontratista')", nativeQuery=true)
    Long secuencia();

    @Query(value = "SELECT COUNT(*) AS TOTAL FROM ord_orden_cont WHERE id_orden = :idOrden", nativeQuery = true)
    Integer existeOrden(@Param("idOrden") Long idOrden);

    @Query(value = "SELECT * FROM ord_orden_cont WHERE id_orden = :idOrden", nativeQuery = true)
    OrdenContratistaEntity findByIdOrden(@Param("idOrden") Long idOrden);
}
