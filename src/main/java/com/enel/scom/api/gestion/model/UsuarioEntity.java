package com.enel.scom.api.gestion.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.ToString;


@Table(name = "usuario", schema = "schscom")
@Entity(name = "usuario")
@Data
@ToString
public class UsuarioEntity implements Serializable  {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
	@Column(name = "username")
    private String username;
    
    @Column(name="fecha_alta")
    private Date fechaAlta;
    
    @Column(name="fecha_baja")
    private Date fechaBaja;
    
    @Column(name="fecha_ultimo_ingreso")
    private Date fechaUltimoIngreso;
    
    @Column
    private boolean activo;
    
    @ManyToOne
    @JoinColumn(name = "id_perfil")
    private PerfilEntity perfil;

}