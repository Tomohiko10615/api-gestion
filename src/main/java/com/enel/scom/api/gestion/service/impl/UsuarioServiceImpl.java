package com.enel.scom.api.gestion.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;

import com.enel.scom.api.gestion.dto.MenuDTO;
import com.enel.scom.api.gestion.dto.response.AccesoUsuarioResponseDTO;
import com.enel.scom.api.gestion.dto.response.RolResponseDTO;
import com.enel.scom.api.gestion.exception.NoExistEntityException;
import com.enel.scom.api.gestion.model.PerfilEntity;
import com.enel.scom.api.gestion.model.PerfilRol;
import com.enel.scom.api.gestion.model.UsuarioEntity;
import com.enel.scom.api.gestion.repository.PerfilRepository;
import com.enel.scom.api.gestion.repository.PerfilRolRepository;
import com.enel.scom.api.gestion.repository.UsuarioRepository;
import com.enel.scom.api.gestion.service.MenuService;
import com.enel.scom.api.gestion.service.UsuarioService;
import com.enel.scom.api.gestion.util.Funciones;

import lombok.extern.slf4j.Slf4j;


@Slf4j
@Service
public class UsuarioServiceImpl implements UsuarioService {

    @Autowired
    private UsuarioRepository repository;
    
    @Autowired
    private PerfilRepository perfilRepository;
    
    @Autowired
    private PerfilRolRepository perfilRolRepository;
    
    @Autowired
    private MenuService menuService;

    @Override
    public Optional<UsuarioEntity> buscarPorIdUsuario(Long id) {
    	Optional<UsuarioEntity> result = repository.findById(id);
    	if(result.isPresent()) {
    		return result;
    	}
		return result;
    }

    @Override
    public List<UsuarioEntity> buscarTodosUsuarios() {
    	List<UsuarioEntity> result = repository.findAll();
        return result;
    }

    @Override
    public Optional<UsuarioEntity> findByUsername(String username) {
    	Optional<UsuarioEntity> result = repository.findByUsername(username);
        return result;
    }

	@Override
	public Optional<UsuarioEntity> buscarPorId(Long id) {
		Optional<UsuarioEntity> result = repository.findById(id);
		return result;
	}

	@Override
	@Transactional
	public void updateFechaUltimoAccesoUsuario(String username) {
		UsuarioEntity usuario= repository.getUsuarioByUserName(username);
		usuario.setFechaUltimoIngreso(new Date());
	}

	@Override
	@Transactional
	public AccesoUsuarioResponseDTO updatePerfilUsuario(String username, String idPerfil) {
		
		log.info("updatePerfilUsuario()");
		
		AccesoUsuarioResponseDTO dto = new AccesoUsuarioResponseDTO();
		
		UsuarioEntity usuario= repository.getUsuarioByUserName(username);
		log.info(Funciones.objectToJsonString(usuario));
		
		PerfilEntity perfil = perfilRepository.obtenerPerfilPorId(Long.valueOf(idPerfil));
		log.info(Funciones.objectToJsonString(perfil));
		
		List<PerfilRol> roles = perfilRolRepository.obtenerRolesPorIdPerfil(perfil.getId());
		log.info(Funciones.objectToJsonString(roles));
		
		List<RolResponseDTO> rolDtoes = roles.stream()
					.map(rol -> new RolResponseDTO(rol.getRol().getNombre()))
				.collect(Collectors.toList());
		
		usuario.setPerfil(perfil);
		
		dto.setIdPerfil(perfil.getId());
		dto.setNombrePerfil(perfil.getNombre());
		dto.setUsername(usuario.getUsername());
		dto.setParamNombre(perfil.getParamNombre());
		dto.setMenu(menuService.getMenuByIdPerfil(usuario.getPerfil().getId()));
		dto.setRoles(rolDtoes);
		
		return dto;
		
	}
}
