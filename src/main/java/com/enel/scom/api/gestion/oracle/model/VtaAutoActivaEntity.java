package com.enel.scom.api.gestion.oracle.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;

@Entity(name = "vta_auto_act")
@Data
public class VtaAutoActivaEntity implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @Column(name="id_auto_act")
    private Long idAutoAct;

    @Column(name = "nro_orden")
    private String nroOrden;
}
