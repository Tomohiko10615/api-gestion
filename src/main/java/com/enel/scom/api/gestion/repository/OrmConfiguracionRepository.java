package com.enel.scom.api.gestion.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.model.OrmConfiguracionEntity;

@Repository
public interface OrmConfiguracionRepository extends JpaRepository<OrmConfiguracionEntity, Long>{
    @Query(value = "select oc.id_trabajo, ot.description from orm_configuracion oc inner join orm_trabajo ot on ot.id = oc.id_trabajo where oc.id_tema = 66 and id_motivo = 1240 and ot.estado = 'S'", nativeQuery = true)
    List<Object[]> getTrabajos();
}
