package com.enel.scom.api.gestion.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.model.AmperajeEntity;

@Repository
public interface AmperajeRepository extends JpaRepository<AmperajeEntity, Long>{

	@Query(value = "SELECT id, des_amperaje FROM com_amperaje WHERE activo = 'S' ORDER BY des_amperaje ASC", nativeQuery = true)
	List<Object[]> getAll();
	
	@Query(value = "SELECT * FROM com_amperaje WHERE id = :id", nativeQuery = true)
	AmperajeEntity findId(@Param("id") Long id);
}
