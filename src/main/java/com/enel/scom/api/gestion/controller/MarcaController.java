package com.enel.scom.api.gestion.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.enel.scom.api.gestion.dto.request.MarcaRequestDTO;
import com.enel.scom.api.gestion.dto.response.MarcaPaginacionResponseDTO;
import com.enel.scom.api.gestion.dto.response.MarcaResponseDTO;
import com.enel.scom.api.gestion.exception.NroServicioNotFoundException;
import com.enel.scom.api.gestion.model.MarcaEntity;
import com.enel.scom.api.gestion.service.MarcaService;
import com.enel.scom.api.gestion.service.UsuarioService;

@RestController
@RequestMapping("/api/marca")
public class MarcaController {
	
	@Autowired
	MarcaService marcaService;
	
	@Autowired
	UsuarioService usuarioService;
	
	@GetMapping
    public ResponseEntity<Page<MarcaPaginacionResponseDTO>> getMarcasPaginacion(
    		@RequestParam(required=false) String codMarca,
			@RequestParam(required=false) String desMarca,
			@RequestParam(required=false) String usuario,
			@RequestParam(required=false) String fechaInicio,
			@RequestParam(required=false) String fechaFin,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size,
            @RequestParam(defaultValue = "id") String order,
            @RequestParam(defaultValue = "true") boolean asc
    ){
        Page<MarcaPaginacionResponseDTO> marcas = marcaService.getMarcasPaginacion(
                PageRequest.of(page, size, Sort.by(order)), codMarca, desMarca, usuario, fechaInicio, fechaFin);
        if(!asc)
            marcas = marcaService.getMarcasPaginacion(
                    PageRequest.of(page, size, Sort.by(order).descending()), codMarca, desMarca, usuario, fechaInicio, fechaFin);
        return new ResponseEntity<>(marcas, HttpStatus.OK); 
    }
	
	@GetMapping("/all")
	public List<MarcaResponseDTO> getMarca() {
		return marcaService.getMarcaActive();
	}
	
	@PostMapping("/save")
	public ResponseEntity<MarcaEntity> store(@RequestBody MarcaRequestDTO marcaRequest) throws NroServicioNotFoundException {
		MarcaEntity marca = marcaService.save(marcaRequest);
		return new ResponseEntity<>(marca, HttpStatus.OK);
	}
	
	@PutMapping("/update/{id}")
	public MarcaEntity update(@RequestBody MarcaRequestDTO marcaRequest, @PathVariable Long id) {
		return marcaService.update(marcaRequest, id);
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<MarcaEntity> delete(@PathVariable Long id) {
		MarcaEntity marca = marcaService.delete(id);
		return new ResponseEntity<>(marca, HttpStatus.OK);
	}
}
