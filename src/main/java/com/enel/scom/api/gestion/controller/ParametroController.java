package com.enel.scom.api.gestion.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.enel.scom.api.gestion.dto.response.ParametroResponseDTO;
import com.enel.scom.api.gestion.service.ParametroService;

@RestController
@RequestMapping("/api/parametro")
public class ParametroController {
	@Autowired
	ParametroService parametroService;
	

	@GetMapping("/tipoInspeccion")
	public ResponseEntity<List<ParametroResponseDTO>> getTipoInspeccion() {
		List<ParametroResponseDTO> responses = parametroService.getTipoInspeccion();
		return new ResponseEntity<>(responses, HttpStatus.OK);
	}
	

	@GetMapping("/subtipos")
	public ResponseEntity<List<ParametroResponseDTO>> getSubTipo() {
		List<ParametroResponseDTO> responses = parametroService.getSubTipo();
		return new ResponseEntity<>(responses, HttpStatus.OK);
	}
	
	/**
	 * API que lista los tipos de procesos manejados dentro del SCOM
	 * @return
	 */
	@GetMapping("/tipo_proceso")
	public ResponseEntity<List<ParametroResponseDTO>> getTipoProceso() {
		List<ParametroResponseDTO> responses = parametroService.getTipoProceso();
		return new ResponseEntity<>(responses, HttpStatus.OK);
	}
	
	/**
	 * API que lista los estados de las ordenes de trabajo
	 * @return
	 */
	@GetMapping("/estados")
	public ResponseEntity<List<ParametroResponseDTO>> getEstados() {
		List<ParametroResponseDTO> responses = parametroService.getEstados();
		return new ResponseEntity<>(responses, HttpStatus.OK);
	}
	
	/**
	 * API que lista los estados menejados en la transfer
	 * @return
	 */
	@GetMapping("/estadosTransfer")
	public ResponseEntity<List<ParametroResponseDTO>> getEstadosTransferencia() {
		List<ParametroResponseDTO> responses = parametroService.getEstadosTransfer();
		return new ResponseEntity<List<ParametroResponseDTO>>(responses, HttpStatus.OK);
	}
	
	@GetMapping("/tiposUbicacionMed")
	public ResponseEntity<List<ParametroResponseDTO>> getTiposUbicacionMedidor() {
		List<ParametroResponseDTO> responses = parametroService.getTiposUbicacionMedidor();
		return new ResponseEntity<List<ParametroResponseDTO>>(responses, HttpStatus.OK);
	}
	@GetMapping("/rangoFechaBusqueda")
	@RequestMapping(value = "/rangoFechaBusqueda", method = RequestMethod.GET)
	public ResponseEntity<ParametroResponseDTO> obtenerRangoFechaBusqueda(@RequestParam(value = "rango", defaultValue = "RANGODIAS") String rango) {
	    ParametroResponseDTO responses = parametroService.obtenerRangoFechaBusqueda("SCOM", "METABUSQUEDA", rango);
	    return new ResponseEntity<ParametroResponseDTO>(responses, HttpStatus.OK);
	}

}

