package com.enel.scom.api.gestion.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.enel.scom.api.gestion.model.RegistradorEntity;
import com.enel.scom.api.gestion.repository.RegistradorRepository;

@RestController
@RequestMapping("/api/registrador")
public class RegistradorController {

	@Autowired
	RegistradorRepository registradorRepository;
	
	@GetMapping
	public List<RegistradorEntity> getAll() {
		List<RegistradorEntity> responses = registradorRepository.getRegistradoresActivo();
		return responses;
	}
}
