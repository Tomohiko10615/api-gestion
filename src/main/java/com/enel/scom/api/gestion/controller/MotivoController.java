package com.enel.scom.api.gestion.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.enel.scom.api.gestion.model.MotivoEntity;
import com.enel.scom.api.gestion.service.MotivoService;

@RestController
@RequestMapping("/api/motivo")
public class MotivoController {

	@Autowired
	MotivoService motivoService;
	
	@GetMapping
	public ResponseEntity<List<MotivoEntity>> getActivos() {
		List<MotivoEntity> responses = motivoService.getActivos();
		return new ResponseEntity<>(responses, HttpStatus.OK);
	}

	@GetMapping("/contrastes")
	public ResponseEntity<List<MotivoEntity>> getContraste() {
		List<MotivoEntity> responses = motivoService.getContraste();
		return new ResponseEntity<>(responses, HttpStatus.OK);
	}
}
