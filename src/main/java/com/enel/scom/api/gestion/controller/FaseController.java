package com.enel.scom.api.gestion.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.enel.scom.api.gestion.model.FaseEntity;
import com.enel.scom.api.gestion.repository.FaseRepository;

@RestController
@RequestMapping("/api/fase")
public class FaseController {

	@Autowired
	FaseRepository faseRepository;
	
	@GetMapping
	public ResponseEntity<List<FaseEntity>> getFases() {
		List<FaseEntity> responses = faseRepository.getActivos();
		return new ResponseEntity<>(responses, HttpStatus.OK);
	}
}
