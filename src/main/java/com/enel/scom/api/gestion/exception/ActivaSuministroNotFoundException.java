package com.enel.scom.api.gestion.exception;

public class ActivaSuministroNotFoundException extends Exception {
	public ActivaSuministroNotFoundException(String message) {
        super(message);
    }
}
