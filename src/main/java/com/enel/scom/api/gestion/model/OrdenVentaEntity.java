package com.enel.scom.api.gestion.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

/**
 * Tabla donde se guardas las Ordenes de trabajo de tipo conexion
 * @author ngbarrios
 *
 */
@Entity(name = "vta_ord_vta")
@Data
public class OrdenVentaEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="id_ord_venta")
	private Long idOrdVenta;
	
	@Column(name="id_grp_cuad_eje")
	private Long idGrpCuadEje;
	
	@Column(name="id_cuad_eje")
	private Long idCuadEje;
	
	@Column(name="id_eje_eje")
	private String idEjeEje;
	
	@Column(name="fecha_cre_eje")
	private Date fechaCreEje;
	
	@Column(name="fecha_eje_eje")
	private Date fechaEjeEje;
	
	@Column(name="fecha_cita_eje")
	private Date fechaCitaEje;
	
	@Column(name="turno_cita_eje")
	private String turnoCitaEje;
	
	@Column(name="id_grp_cuad_ins")
	private Long idGrpCuadIns;
	
	@Column(name="id_cuad_ins")
	private Long idCuadIns;
	
	@Column(name="id_eje_ins")
	private Long idEjeIns;
	
	@Column(name="fecha_cre_ins")
	private Date fechaCreIns;
	
	@Column(name="fecha_eje_ins")
	private Date fechaEjeIns;
	
	@Column(name="fecha_cita_ins")
	private Date fechaCitaIns;
	
	@Column(name="turno_cita_ins")
	private String turnoCitaIns;
	
	@Column(name="id_solicitud")
	private Long idSolicitud;
	
	@Column(name="paralizada")
	private String paralizada;
	
	@Column(name="nro_transiciones")
	private Long nroTransiciones;
	
	@Column(name="id_seg_creacion")
	private Long idSegCreacion;
	
	@Column(name="id_seg_actual")
	private Long idSegActual;
	
	@Column(name="fecha_anulacion")
	private Date fechaAnulacion;
	
	@Column(name="modo_envio_ejec")
	private String modoEnvioEjec;
	
	@Column(name="tiene_factibilidad")
	private String tieneFactibilidad;
	
	@Column(name="estado_wcnx")
	private Long estadoWcnx;
	
	@Column(name="enviado_sap")
	private String enviadoSap;
	
	@Column(name="ticket_sap")
	private String ticketSap;
	
	

}
