package com.enel.scom.api.gestion.oracle.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;

@Entity(name = "srv_set")
@Data
public class SrvSetEntity implements Serializable {/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
    @Column(name="id_set")
    private Long idSet;

    @Column(name = "cod_set")
    private String codSet;
    
    @Column(name = "des_set")
    private String desSet;
    
    @Column(name = "activo")
    private String activo;

}
