package com.enel.scom.api.gestion.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.enel.scom.api.gestion.dto.response.MedidorPaginacionResponseDTO;
import com.enel.scom.api.gestion.dto.response.ReporteGraficoDTO;
import com.enel.scom.api.gestion.dto.response.ReporteMedidoresPaginacionResponseDTO;
import com.enel.scom.api.gestion.exception.AllNotFoundException;
import com.enel.scom.api.gestion.service.ReporteMedidoresService;
import com.enel.scom.api.gestion.util.Funciones;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ReporteMedidoresServiceImpl implements ReporteMedidoresService {
	
	@PersistenceContext
	EntityManager entityManager;

	@Override
	public Page<ReporteMedidoresPaginacionResponseDTO> obtenerReporteMedidoresPaginacion(Pageable paging,
			String fechaInicio, String fechaFin, Long idEstadoMedidor, String tipoUbicacion, String nroCuenta, String nroComponente ) {
		
		log.info("obtenerReporteMedidoresPaginacion()");
		log.info("obtenerReporteMedidoresPaginacion() paging JSON ="+Funciones.objectToJsonString(paging));
		
		/*
		select 
		tip.des_tip_componente, --tipoComponente
		mma.des_marca, -- marca
		mm.des_modelo, -- modelo
		mc.nro_componente, --nroComponente
		mec.des_est_componente, --estado componente
		mh.fec_desde, -- fecha desde
		mh.fec_desde -- fecha hasta
		from schscom.med_his_componente mh 
		join schscom.med_componente mc on mh.id_componente = mc.id
		join schscom.med_est_componente mec on mh.id_est_componente = mec.id
		join schscom.med_tip_componente tip on mc.cod_tip_componente = tip.cod_tip_componente
		join schscom.med_modelo mm on mc.id_modelo = mm.id
		join schscom.med_marca mma on mm.id_marca = mma.id;*/
		
		/*String sql = "select new com.enel.scom.api.gestion.dto.response.ReporteMedidoresPaginacionResponseDTO( "
				+ "tip.desTipComponente, "
				+ "mma.descripcion, "
				+ "mm.desModelo, "
				+ "mc.nroComponente, "
				+ "mec.descripcion, "
				+ "mh.fecDesde, "
				+ "mh.fecHasta,"
				+ "mec.descripcion "
				+ ") "
				+ "from med_his_componente mh "
				+ "join med_componente mc on mh.medidor.id= mc.id "
				+ "join med_est_componente mec on mec.id = mc.estadoMedidor.id "
				+ "join med_modelo mm on mc.modelo.id = mm.id "
				+ "join med_marca mma on mm.marca.id = mma.id "
				+ "join med_tip_componente tip on mc.codTipComponente = tip.codTipComponente "
				+ "left join cliente c on mc.ubicacion.id = c.idServicio";*/
		
		String sql = "select new com.enel.scom.api.gestion.dto.response.ReporteMedidoresPaginacionResponseDTO( "
				+ "tip.desTipComponente, "
				+ "mma.descripcion, "
				+ "mm.desModelo, "
				+ "mc.nroComponente, "
				+ "mec.descripcion, "
				+ "to_char(mh.fecDesde, 'DD/MM/YYYY'), "
				+ "to_char(mh.fecHasta, 'DD/MM/YYYY'), "
				+ "CASE "
				+ "WHEN SUBSTR(mh.typeUbicacion,23,80) = 'common.domain.Contratista' "
				+ "then (select np.nombre from com_contratista con inner join nuc_persona np on con.persona.id = np.id where con.id = mh.ubicacion.id) "
				+ "WHEN SUBSTR(mh.typeUbicacion,23,80) = 'med.domain.componente.Laboratorio' "
				+ "then (select lab.desLaboratorio from med_laboratorio lab where lab.idLaboratorio = mh.ubicacion.id) "
				+ "WHEN SUBSTR(mh.typeUbicacion,23,80) = 'med.domain.componente.AlmacenTrazabilidad' "
				+ "then (select al.desAlmacen from med_alm_trazabilidad al where al.idAlmTrazabilidad = mh.ubicacion.id) "
				+ "WHEN SUBSTR(mh.typeUbicacion,23,80) = 'med.domain.componente.Almacen' "
				+ "then (select alm.descripcion from med_almacen alm where alm.id = mh.ubicacion.id) "
				+ "WHEN SUBSTR(mh.typeUbicacion,23,80) = 'nucleo.domain.interfaces.Cliente' "
				+ "then (select CONCAT('Nro Cliente: ',tnc.nroCliente) from tmp_nuc_cliente tnc where tnc.idCliente = mh.ubicacion.id) "
				+ "WHEN SUBSTR(mh.typeUbicacion,23,80) = 'nucleo.domain.interfaces.servicioelectrico.ServicioElectrico' "
				+ "then (select CONCAT('Nro Servicio: ',cli.nroServicio) from cliente cli where cli.idServicio = mh.ubicacion.id) "
				+ "ELSE '' END "
				+ "as ubicacion, "
				+ "usu.username, "
				+ "CASE "
				+ "WHEN SUBSTR(mh.typeUbicacion,23,80) = 'nucleo.domain.interfaces.servicioelectrico.ServicioElectrico' "
				+ "then (select CONCAT(cli.nroCuenta, '') from cliente cli where cli.idServicio = mh.ubicacion.id) "
				+ "WHEN SUBSTR(mh.typeUbicacion,23,80) = 'nucleo.domain.interfaces.Cliente' "
				+ "then (select CONCAT(cli.nroCuenta, '') from cliente cli where cli.idServicio = mh.ubicacion.id) "
				+ "ELSE '' END as nroCuenta "
				+ ") "
				+ "from med_his_componente mh "
				+ "join med_componente mc on mc.id = mh.medidor.id "
				+ "join med_est_componente mec on mh.estadoFinal.id = mec.id "
				+ "join med_tip_componente tip on mc.codTipComponente = tip.codTipComponente "
				+ "join med_modelo mm on mc.modelo.id = mm.id "
				+ "join med_marca mma on mm.marca.id = mma.id "
				+ "left join fwk_auditevent aud on aud.id = mh.id "
				+ "left join usuario usu on usu.id = aud.usuarioEntity.id "
				+ "left join cliente c on mh.ubicacion.id = c.idServicio";
		
		if ((idEstadoMedidor!=null && idEstadoMedidor !=0) || 
			(!StringUtils.isBlank(tipoUbicacion)) || 
			(!StringUtils.isBlank(nroCuenta)) || 
			(!StringUtils.isBlank(nroComponente)) ||
			(!StringUtils.isBlank(fechaInicio)) || 
			(!StringUtils.isBlank(fechaFin))) {
			
            sql +=" where";
        }
        

        if (idEstadoMedidor!=null && idEstadoMedidor !=0) {
        	sql +=  " mh.estadoFinal.id='" + idEstadoMedidor + "'";
        }
        
        if (!StringUtils.isBlank(tipoUbicacion)) {
          	 if(idEstadoMedidor!=null && idEstadoMedidor !=0) {
          		sql +=  " and mh.typeUbicacion='" + tipoUbicacion + "'";
          	 }else {
          		sql +=  " mh.typeUbicacion='" + tipoUbicacion + "'";
          	 } 
        }
        
        if (!StringUtils.isBlank(nroCuenta)) {
          	
        	if((idEstadoMedidor!=null && idEstadoMedidor !=0) || (!StringUtils.isBlank(tipoUbicacion))) {
        		 sql +=  " and c.nroCuenta='" + nroCuenta + "'";
        	}else {
        		 sql +=  " c.nroCuenta='" + nroCuenta + "'";
        	}
        }
        

        if (!StringUtils.isBlank(nroComponente)) {
          	
        	if((idEstadoMedidor!=null && idEstadoMedidor !=0) || (!StringUtils.isBlank(tipoUbicacion) || (!StringUtils.isBlank(nroCuenta)))) {
        		 sql +=  " and mc.nroComponente='" + nroComponente + "'";
        	}else {
        		 sql +=  " mc.nroComponente='" + nroComponente + "'";
        	}
        }
       
        
        if ((!StringUtils.isBlank(fechaInicio)) || (!StringUtils.isBlank(fechaFin))) {
        	try {
        		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
                Date currentdateFehaInicio = sdf.parse(fechaInicio);
                SimpleDateFormat sdf2=new SimpleDateFormat("yyyy-MM-dd");
                String fechaInicioConvertido = sdf2.format(currentdateFehaInicio);
                
                SimpleDateFormat sdf4=new SimpleDateFormat("yyyy-MM-dd");
                Date currentdateFehaFin=sdf4.parse(fechaFin);
                SimpleDateFormat sdf3=new SimpleDateFormat("yyyy-MM-dd");
                String fechaFinConvertido = sdf3.format(currentdateFehaFin);
                
            	if((idEstadoMedidor!=null && idEstadoMedidor !=0) || (!StringUtils.isBlank(tipoUbicacion)) || (!StringUtils.isBlank(nroCuenta)) || (!StringUtils.isBlank(nroComponente))) {
            		sql +=  " and DATE_TRUNC('day',mc.fecCreacion) between "+ "'"+fechaInicioConvertido +"'"+" and "+"'"+ fechaFinConvertido+"'";
            	}else {
            		sql +=  " DATE_TRUNC('day',mc.fecCreacion) between "+ "'"+fechaInicioConvertido +"'"+" and "+"'"+ fechaFinConvertido+"'";
            	}
            	
        	}catch (Exception e) {
				e.printStackTrace();
			}	
             
        }
        
        try {
        	sql +=" order by mma.descripcion, mm.desModelo, mh.fecDesde asc";
        	
        	log.info("query sql ="+sql);
        	
        	List<ReporteMedidoresPaginacionResponseDTO> dto = entityManager.createQuery(sql, ReporteMedidoresPaginacionResponseDTO.class).getResultList();
        	log.info("dto size="+dto.size());
    		
    		List<ReporteMedidoresPaginacionResponseDTO> list = dto.stream().collect(Collectors.toList());
    		
    		log.info("list size="+list.size());
    		
    		int pageOffset = (int) paging.getOffset();
    		int total = (pageOffset + paging.getPageSize()) > list.size() ? list.size() : (pageOffset + paging.getPageSize());
    		
    		log.info("pageOffset ="+pageOffset+", total ="+total);
    		
    		log.info("obtenerReporteMedidoresPaginacion() FIN OK");
    		
    		return new PageImpl<>(list.subList(pageOffset, total), paging, list.size());
        } catch (Exception e) {
        	e.printStackTrace();
        	log.info("obtenerReporteMedidoresPaginacion() FIN ERROR");
        	log.error("Error al consultar el reporte",e);
        	throw new AllNotFoundException("Error al consultar el reporte"); 
		} finally {
			entityManager.close();
			log.info("obtenerReporteMedidoresPaginacion() FIN");
		}
		
		
	}
	

	

	@Override
	public Page<ReporteMedidoresPaginacionResponseDTO> obtenerReporteMedidoresPaginacion_V2(Pageable paging,
			String fechaInicio, String fechaFin, Long idEstadoMedidor, String tipoUbicacion, String nroCuenta, String nroComponente,String marcaMedidor, String modeloMedidor) {
		
		log.info("obtenerReporteMedidoresPaginacion_V2() INI");
		log.info(" paging JSON ="+Funciones.objectToJsonString(paging));
		log.info(" fechaInicio ="+fechaInicio);
		log.info(" fechaFin ="+fechaFin);
		log.info(" idEstadoMedidor ="+idEstadoMedidor);
		log.info(" tipoUbicacion ="+tipoUbicacion);
		log.info(" nroCuenta ="+nroCuenta);
		log.info(" nroComponente ="+nroComponente);
		
		String sqlSelect = "";
		String sqlFrom = "";
		String sqlWhere = "";
		String sqlOrderBy= "";
		String sqlFinal= "";
		
		sqlSelect = "select new com.enel.scom.api.gestion.dto.response.ReporteMedidoresPaginacionResponseDTO( "
				+ "tip.desTipComponente, "
				+ "mma.descripcion, "
				+ "mm.desModelo, "
				+ "mc.nroComponente, "
				+ "mec.descripcion, "
				+ "to_char(mh.fecDesde, 'DD/MM/YYYY'), "
				+ "to_char(mh.fecHasta, 'DD/MM/YYYY'), "
				+ "CASE "
				+ "WHEN SUBSTR(mh.typeUbicacion,23,80) = 'common.domain.Contratista' "
				+ "then (select np.nombre from com_contratista con inner join nuc_persona np on con.persona.id = np.id where con.id = mh.ubicacion.id) "
				+ "WHEN SUBSTR(mh.typeUbicacion,23,80) = 'med.domain.componente.Laboratorio' "
				+ "then (select lab.desLaboratorio from med_laboratorio lab where lab.idLaboratorio = mh.ubicacion.id) "
				+ "WHEN SUBSTR(mh.typeUbicacion,23,80) = 'med.domain.componente.AlmacenTrazabilidad' "
				+ "then (select al.desAlmacen from med_alm_trazabilidad al where al.idAlmTrazabilidad = mh.ubicacion.id) "
				+ "WHEN SUBSTR(mh.typeUbicacion,23,80) = 'med.domain.componente.Almacen' "
				+ "then (select alm.descripcion from med_almacen alm where alm.id = mh.ubicacion.id) "
				+ "WHEN SUBSTR(mh.typeUbicacion,23,80) = 'nucleo.domain.interfaces.Cliente' "
				+ "then (select CONCAT('Nro Cliente: ',tnc.nroCliente) from tmp_nuc_cliente tnc where tnc.idCliente = mh.ubicacion.id) "
				+ "WHEN SUBSTR(mh.typeUbicacion,23,80) = 'nucleo.domain.interfaces.servicioelectrico.ServicioElectrico' "
				+ "then (select CONCAT('Nro Servicio: ',cli.nroServicio) from cliente cli where cli.idServicio = mh.ubicacion.id) "
				+ "ELSE '' END "
				+ "as ubicacion, "
				+ "usu.username, "
				+ "CASE "
				+ "WHEN SUBSTR(mh.typeUbicacion,23,80) = 'nucleo.domain.interfaces.servicioelectrico.ServicioElectrico' "
				+ "then (select CONCAT(cli.nroCuenta, '') from cliente cli where cli.idServicio = mh.ubicacion.id) "
				+ "WHEN SUBSTR(mh.typeUbicacion,23,80) = 'nucleo.domain.interfaces.Cliente' "
				+ "then (select CONCAT(cli.nroCuenta, '') from cliente cli where cli.idServicio = mh.ubicacion.id) "
				+ "ELSE '' END as nroCuenta, "
				+ "mb.codPrimarioApe, " // INICIO - REQ # 7 - JEGALARZA
				+ "mb.codSecundarioApe, "
				+ "mb.codPrimarioCaja, "
				+ "mb.codSecundarioCaja, "
				+ "mb.codMedidor " // FIN - REQ # 7 - JEGALARZA
				+ ") ";
		
			sqlFrom= "from med_his_componente mh "
				+ "join med_componente mc on mc.id = mh.medidor.id "
				+ "join med_est_componente mec on mh.estadoFinal.id = mec.id "
				+ "join med_tip_componente tip on mc.codTipComponente = tip.codTipComponente "
				+ "join med_modelo mm on mc.modelo.id = mm.id "
				+ "join med_marca mma on mm.marca.id = mma.id "
				+ "left join fwk_auditevent aud on aud.id = mh.id "
				+ "left join usuario usu on usu.id = aud.usuarioEntity.id "
				+ "left join cliente c on mh.ubicacion.id = c.idServicio "
				+ " left join med_barcode mb on mc.id = mb.idComponente "; // REQ # 7 - JEGALARZA 
		
		if ((idEstadoMedidor!=null && idEstadoMedidor !=0) || 
			(!StringUtils.isBlank(tipoUbicacion)) || 
			(!StringUtils.isBlank(nroCuenta)) || 
			(!StringUtils.isBlank(marcaMedidor)) || // REQ # 8 - JEGALARZA
			(!StringUtils.isBlank(nroComponente)) ||
			(!StringUtils.isBlank(fechaInicio)) || 
			(!StringUtils.isBlank(fechaFin))) {
			
            sqlWhere +=" where";
        }
        

        if (idEstadoMedidor!=null && idEstadoMedidor !=0) {
        	sqlWhere +=  " mh.estadoFinal.id='" + idEstadoMedidor + "'";
        }
        
        if (!StringUtils.isBlank(tipoUbicacion)) {
          	 if(idEstadoMedidor!=null && idEstadoMedidor !=0) {
          		sqlWhere +=  " and mh.typeUbicacion='" + tipoUbicacion + "'";
          	 }else {
          		sqlWhere +=  " mh.typeUbicacion='" + tipoUbicacion + "'";
          	 } 
        }
        
        if (!StringUtils.isBlank(nroCuenta)) {
          	
        	if((idEstadoMedidor!=null && idEstadoMedidor !=0) || (!StringUtils.isBlank(tipoUbicacion))) {
        		sqlWhere +=  " and c.nroCuenta='" + nroCuenta + "'";
        	}else {
        		sqlWhere +=  " c.nroCuenta='" + nroCuenta + "'";
        	}
        }
        

        if (!StringUtils.isBlank(nroComponente)) {
          	
        	if((idEstadoMedidor!=null && idEstadoMedidor !=0) || (!StringUtils.isBlank(tipoUbicacion) || (!StringUtils.isBlank(nroCuenta)))) {
        		sqlWhere +=  " and mc.nroComponente='" + nroComponente + "'";
        	}else {
        		sqlWhere +=  " mc.nroComponente='" + nroComponente + "'";
        	}
        }
        
     // INICIO REQ # 8 - JEGALARZA
        if (!StringUtils.isBlank(marcaMedidor)) {
          	
        	if((idEstadoMedidor!=null && idEstadoMedidor !=0) || (!StringUtils.isBlank(tipoUbicacion) || (!StringUtils.isBlank(nroComponente)))) {
        		sqlWhere +=  " and mma.id='" + marcaMedidor + "'";
        	}else {
        		sqlWhere +=  " mma.id='" + marcaMedidor + "'";
        	}
        }
        
        if (!StringUtils.isBlank(modeloMedidor)) {
        	if((idEstadoMedidor!=null && idEstadoMedidor !=0) || (!StringUtils.isBlank(tipoUbicacion) || (!StringUtils.isBlank(nroComponente))) || !StringUtils.isBlank(marcaMedidor)) {
        		sqlWhere +=  " and mm.id='" + modeloMedidor + "'";
        	}else {
        		sqlWhere +=  " mm.id='" + modeloMedidor + "'";
        	}
        }
       
        
        if ((!StringUtils.isBlank(fechaInicio)) || (!StringUtils.isBlank(fechaFin))) {
        	try {
        		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
                Date currentdateFehaInicio = sdf.parse(fechaInicio);
                SimpleDateFormat sdf2=new SimpleDateFormat("yyyy-MM-dd");
                String fechaInicioConvertido = sdf2.format(currentdateFehaInicio);
                
                SimpleDateFormat sdf4=new SimpleDateFormat("yyyy-MM-dd");
                Date currentdateFehaFin=sdf4.parse(fechaFin);
                SimpleDateFormat sdf3=new SimpleDateFormat("yyyy-MM-dd");
                String fechaFinConvertido = sdf3.format(currentdateFehaFin);
                
                //REQ # 8 - JEGALARZA - (CAMBIO EN LA LINEA 246)
            	if((idEstadoMedidor!=null && idEstadoMedidor !=0) || (!StringUtils.isBlank(tipoUbicacion)) || (!StringUtils.isBlank(nroCuenta)) || (!StringUtils.isBlank(nroComponente)) || (!StringUtils.isBlank(marcaMedidor))) {
            		sqlWhere +=  " and DATE_TRUNC('day',mc.fecCreacion) between "+ "'"+fechaInicioConvertido +"'"+" and "+"'"+ fechaFinConvertido+"'";
            	}else {
            		sqlWhere +=  " DATE_TRUNC('day',mc.fecCreacion) between "+ "'"+fechaInicioConvertido +"'"+" and "+"'"+ fechaFinConvertido+"'";
            	}
            	
        	}catch (Exception e) {
				e.printStackTrace();
			}	
             
        }
        
        try {
        	int offset = paging.getPageNumber() * paging.getPageSize();
			int limit = paging.getPageSize();
			log.info("/offset: {}", offset);
			log.info("/limit: {}", limit);
			
        	
        	//2023.06.27
        	String sqlCantidad = "select count(1) ".concat(sqlFrom).concat(sqlWhere);
			log.info("/query cantidad: {}", sqlCantidad);
			Long cantidadTotalRegistros = (Long) entityManager.createQuery(sqlCantidad).getSingleResult();
			log.info("/cantidad total de registros: {}", cantidadTotalRegistros);
			
			PageImpl<ReporteMedidoresPaginacionResponseDTO> objfinal = null;
			
			if(cantidadTotalRegistros == 0L) {
				log.info("getMedidorPaginacion_V2() FIN OK (NO DATA FOUND)");
	    		objfinal =  new PageImpl<>(new ArrayList<ReporteMedidoresPaginacionResponseDTO>(), paging, cantidadTotalRegistros);
	    		return objfinal;
			}

        	sqlOrderBy +=" order by mma.descripcion, mm.desModelo, mh.fecDesde asc";
        	log.info("query sql with order ="+sqlOrderBy);
        	
        	sqlFinal= sqlSelect.concat(sqlFrom).concat(sqlWhere).concat(sqlOrderBy);
			log.info("query final ="+ sqlFinal);
        	
        	log.info("ejecutando query...");
        	List<ReporteMedidoresPaginacionResponseDTO> dto = entityManager.createQuery(sqlFinal, ReporteMedidoresPaginacionResponseDTO.class).setFirstResult(offset).setMaxResults(limit).getResultList();
        	log.info("query ejecutado, rows ="+dto.size());
    		log.info(dto.toString());
    		log.info("definiendo a retornar...");
    		objfinal =  new PageImpl<>(dto, paging, cantidadTotalRegistros);
    		log.info("objeto de retorno definido fin.");
    		
    		log.info("obtenerReporteMedidoresPaginacion_V2() FIN OK");
    		return objfinal;
    		
        } catch (Exception e) {
        	e.printStackTrace();
        	log.info("obtenerReporteMedidoresPaginacion_V2() FIN ERROR");
        	log.error("Error al consultar el reporte",e);
        	throw new AllNotFoundException("Error al consultar el reporte"); 
        	
		} finally {
			entityManager.close();
			log.info("obtenerReporteMedidoresPaginacion_V2() finally");
		}
		
		
	}
	
	@Override
	public List<ReporteGraficoDTO> obtenerReporteMedidoresCantidadXestado(String fechaInicio,
			String fechaFin, Long idEstadoMedidor, String tipoUbicacion, String nroCuenta) {
		
		String sql = "select new com.enel.scom.api.gestion.dto.response.ReporteGraficoDTO( "
				+ "mec.descripcion, "
				+ "count(*)"
				+ ") "
				+ "from med_componente mc "
				+ "join med_est_componente mec on mec.id = mc.estadoMedidor.id "
				+ "join med_his_componente mh on mc.id = mh.medidor.id and mh.estadoFinal.id = mc.estadoMedidor.id and mc.typeUbicacion = mh.typeUbicacion "
				+ "join med_modelo mm on mc.modelo.id = mm.id "
				+ "join med_marca mma on mm.marca.id = mma.id "
				+ "left join cliente c on mc.ubicacion.id = c.idServicio";
		
		if ((idEstadoMedidor!=null && idEstadoMedidor !=0) || 
			(!StringUtils.isBlank(tipoUbicacion)) || 
			(!StringUtils.isBlank(nroCuenta)) || 
			(!StringUtils.isBlank(fechaInicio)) || 
			(!StringUtils.isBlank(fechaFin))) {
			
            sql +=" where";
        }
        

        if ((idEstadoMedidor!=null && idEstadoMedidor !=0)) {
        	sql +=  " mec.id='" + idEstadoMedidor + "'";
        }
        
        if (!StringUtils.isBlank(tipoUbicacion)) {
          	 if((idEstadoMedidor!=null && idEstadoMedidor !=0)) {
          		sql +=  " and mc.typeUbicacion='" + tipoUbicacion + "'";
          	 }else {
          		sql +=  " mc.typeUbicacion='" + tipoUbicacion + "'";
          	 } 
        }
        
        if (!StringUtils.isBlank(nroCuenta)) {
          	
        	if((idEstadoMedidor !=0) || (!StringUtils.isBlank(tipoUbicacion))) {
        		 sql +=  " and c.nroCuenta='" + nroCuenta + "'";
        	}else {
        		 sql +=  " c.nroCuenta='" + nroCuenta + "'";
        	}
        }
       
        
        if ((!StringUtils.isBlank(fechaInicio)) || (!StringUtils.isBlank(fechaFin))) {
        	try {
        		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
                Date currentdateFehaInicio = sdf.parse(fechaInicio);
                SimpleDateFormat sdf2=new SimpleDateFormat("yyyy-MM-dd");
                String fechaInicioConvertido = sdf2.format(currentdateFehaInicio);
                
                SimpleDateFormat sdf4=new SimpleDateFormat("yyyy-MM-dd");
                Date currentdateFehaFin=sdf4.parse(fechaFin);
                SimpleDateFormat sdf3=new SimpleDateFormat("yyyy-MM-dd");
                String fechaFinConvertido = sdf3.format(currentdateFehaFin);
                
            	if(((idEstadoMedidor!=null && idEstadoMedidor !=0)) || (!StringUtils.isBlank(tipoUbicacion)) || (!StringUtils.isBlank(nroCuenta))) {
            		sql +=  " and DATE_TRUNC('day',mc.fecCreacion) between "+ "'"+fechaInicioConvertido +"'"+" and "+"'"+ fechaFinConvertido+"'";
            	}else {
            		sql +=  " DATE_TRUNC('day',mc.fecCreacion) between "+ "'"+fechaInicioConvertido +"'"+" and "+"'"+ fechaFinConvertido+"'";
            	}
            	
        	}catch (Exception e) {
				e.printStackTrace();
			}	
             
        }
        
        sql += " group by mec.descripcion";
        
        try {
        
        	return entityManager.createQuery(
    				sql, ReporteGraficoDTO.class).getResultList();
        	
        } catch (Exception e) {
        	e.printStackTrace();
        	throw new AllNotFoundException("Error al consultar el reporte"); 
		} finally {
			
			entityManager.close();
		}
		
	}

}
