package com.enel.scom.api.gestion.exception;

public class NoExistFileException extends Exception {

	private static final long serialVersionUID = 1L;

	public NoExistFileException(String message) {
        super(message);
    }
}
