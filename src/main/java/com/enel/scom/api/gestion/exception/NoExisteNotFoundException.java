package com.enel.scom.api.gestion.exception;

public class NoExisteNotFoundException extends Exception{
    public NoExisteNotFoundException(String mensaje) {
        super(mensaje);
    }
}
