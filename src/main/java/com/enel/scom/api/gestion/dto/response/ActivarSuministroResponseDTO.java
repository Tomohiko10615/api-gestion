package com.enel.scom.api.gestion.dto.response;

import lombok.Data;

@Data
public class ActivarSuministroResponseDTO {
	
	private String nroOrdenLgcRelac;
	private Long numeroCuenta;
	
	//CADENA ELECTRICA
	private Long idSet;
	private String codSet;
	
	private Long idAlimentador;
	private String codAlimentador;
	
	private Long idSed;
	private String codSed;
	
	private Long idLlave;
	private String codLlave;
	private String desLlave;
	
	private String sector;
	private String zona;
	private String correlativo;
	
	/*
	//RUTAS LECTURA
	private Long idLecturaSector;
	private String codLecturaSector;
	
	private Long idLecturaZona;
	private String codLecturaZona;
	
	private String lecturaCorrelativo;
	
	//RUTA CORTE
	private Long idRutaCorteSector;
	private String codRutaCorteSector;
	
	private Long idRutaCorteZona;
	private String codRutaCorteZona;
	
	private String rutaCorteCorrelativo;
	
	//RUTA FACTURA
	private Long idRutaFacturacionSector;
	private String codRutaFacturacionSector;
	
	private Long idRutaFacturacionZona;
	private String codRutaFacturacionZona;
	
	private String rutaFacturacionCorrelativo;
	
	
	//RUTA REPARTO
	private Long idRutaRepartoSector;
	private String codRutaRepartoSector;
	
	private Long idRutaRepartoZona;
	private String codRutaRepartoZona;
	
	private String rutaRepartoCorrelativo;
	*/
}
