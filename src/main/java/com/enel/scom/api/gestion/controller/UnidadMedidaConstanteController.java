package com.enel.scom.api.gestion.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.enel.scom.api.gestion.model.UnidadMedidaConstanteEntity;
import com.enel.scom.api.gestion.repository.UnidadMedidaConstanteRepository;

@RestController
@RequestMapping("/api/unidad-medida-constante")
public class UnidadMedidaConstanteController {

	@Autowired
	UnidadMedidaConstanteRepository unidadMedidaConstanteRepository;

	@GetMapping
	public List<UnidadMedidaConstanteEntity> getAll() {
		List<UnidadMedidaConstanteEntity> responses = unidadMedidaConstanteRepository.getUMCActivo();
		return responses;
	}
}
