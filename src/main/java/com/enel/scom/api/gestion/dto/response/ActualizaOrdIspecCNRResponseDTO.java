package com.enel.scom.api.gestion.dto.response;

import lombok.Data;

@Data
public class ActualizaOrdIspecCNRResponseDTO {

	public String codigo_respuesta;
	public String error;
	public String detalle;
	public String tiempoEjecucion;
	public String requestID;
}
