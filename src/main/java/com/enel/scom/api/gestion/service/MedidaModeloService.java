package com.enel.scom.api.gestion.service;

import java.util.List;

import com.enel.scom.api.gestion.dto.request.MedMedidaModeloRequestDTO;
import com.enel.scom.api.gestion.dto.response.ModeloMedidaResponseDTO;
import com.enel.scom.api.gestion.exception.NroServicioNotFoundException;
import com.enel.scom.api.gestion.model.MedidaModeloEntity;

public interface MedidaModeloService {

	List<MedidaModeloEntity> getMedidaModelo(Long id);
	
	MedidaModeloEntity crearMedidaModelo(MedMedidaModeloRequestDTO medModelo) throws NroServicioNotFoundException;
	
	List<ModeloMedidaResponseDTO> obtenerMedidasPorModelo(Long idModelo);
	
}
