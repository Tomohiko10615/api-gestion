package com.enel.scom.api.gestion.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.model.DisSegInspEntity;

@Repository
public interface DisSegInpsRepository extends JpaRepository<DisSegInspEntity, Long>{
    @Query(value="SELECT nextval('sqdisseginsp')", nativeQuery=true)
	public Long generaId();
    
    @Query(value = "SELECT * FROM dis_seg_insp WHERE id = :id", nativeQuery = true)
    DisSegInspEntity obtenerPorIdSeguimiento(@Param("id") Long id);
}
