package com.enel.scom.api.gestion.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.enel.scom.api.gestion.dto.request.OrdenNormalizacionRequestDTO;
import com.enel.scom.api.gestion.dto.response.OrdenDataResponseDTO;
import com.enel.scom.api.gestion.exception.NroServicioNotFoundException;
import com.enel.scom.api.gestion.model.OrdenEntity;
import com.enel.scom.api.gestion.model.OrdenNormalizacionEntity;
import com.enel.scom.api.gestion.service.OrdenNormalizacionService;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/api/ordenNormalizacion")
@Slf4j
public class OrdenNormalizacionController {
	
	@Autowired
	OrdenNormalizacionService ordenNormalizacionService;
	
	@PostMapping("/store")
	public ResponseEntity<OrdenEntity> storeUpdate(@RequestBody OrdenNormalizacionRequestDTO ordenDTO) throws NroServicioNotFoundException {
		OrdenEntity response = new OrdenEntity();
		try {
			response = ordenNormalizacionService.storeUpdate(ordenDTO);
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (NroServicioNotFoundException e) {
			log.error(e.getMessage());
			response.setId(0L);
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception e) {
			response.setId(-1L);
			return new ResponseEntity<>(response, HttpStatus.OK);
		}
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<OrdenDataResponseDTO> getId(@PathVariable Long id) {
		OrdenDataResponseDTO response = ordenNormalizacionService.getId(id);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
}
