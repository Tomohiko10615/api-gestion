package com.enel.scom.api.gestion.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.scom.api.gestion.dto.response.ContratistaResponseDTO;
import com.enel.scom.api.gestion.repository.ContratistaRepository;
import com.enel.scom.api.gestion.service.ContratistaService;

@Service
public class ContratistaServiceImpl implements ContratistaService{

	@Autowired
	ContratistaRepository contratistaRepository;
	
	@Override
	public List<ContratistaResponseDTO> getActivos() {
		List<Object[]> objects = contratistaRepository.getActivos();
		List<ContratistaResponseDTO> dtos = new ArrayList<>();
		
		objects.stream().forEach(k -> {
			ContratistaResponseDTO dto = new ContratistaResponseDTO();
			dto.setId(Long.valueOf(k[0].toString()));
			dto.setNombre(k[1].toString());
			dto.setApellidoPat((k[2] == null) ? "-" : k[2].toString());
			dto.setApellidoMat((k[3] == null) ? "-" : k[3].toString());
			dto.setCodContratista((k[4] == null) ? "-" : k[4].toString());
			dtos.add(dto);
		});
		
		return dtos;
	}

}
