package com.enel.scom.api.gestion.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.enel.scom.api.gestion.dto.response.ReporteGraficoDTO;
import com.enel.scom.api.gestion.dto.response.ReporteNovedadesClientePaginacionResponseDTO;

public interface ReporteNovedadesClienteService {
	
	
	Page<ReporteNovedadesClientePaginacionResponseDTO> obtenerReporteNovedadesClientePaginacion(Pageable paging, String fechaInicio, String fechaFin, String situacion, String nroCuenta, String tipoOrden);
	
	List<ReporteGraficoDTO> obtenerReporteNovedadesClienteCantidadXsituacion(String fechaInicio, String fechaFin, String situacion, String nroCuenta, String tipoOrden);

}
