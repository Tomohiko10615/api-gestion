package com.enel.scom.api.gestion.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.scom.api.gestion.dto.response.TareaAnormalidadResponseDTO;
import com.enel.scom.api.gestion.repository.DisTareaAnormalidadRepository;
import com.enel.scom.api.gestion.service.DisTareaAnormalidadService;

@Service
public class DisTareaAnormalidadServiceImpl implements DisTareaAnormalidadService{

	@Autowired
	DisTareaAnormalidadRepository disTareaAnormalidadRepository;
	
	@Override
	public List<TareaAnormalidadResponseDTO> findByAnormalidad(Long id) {
		List<Object[]> tareaAnormalidades = disTareaAnormalidadRepository.findByIdAnormalidad(id);
		
		return tareaAnormalidades.stream().map(t -> new TareaAnormalidadResponseDTO(
													(t[0] == null) ? 0 : Long.valueOf(t[0].toString()), 
													(t[1] == null) ? 0 : Long.valueOf(t[1].toString()), 
													(t[2] == null) ? "" : t[2].toString(),
													(t[3] == null) ? "" : t[3].toString(),
													(t[4] == null) ? "" : t[4].toString()
												)).collect(Collectors.toList()); 
	}

}
