package com.enel.scom.api.gestion.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.enel.scom.api.gestion.dto.request.MedMedidaModeloRequestDTO;
import com.enel.scom.api.gestion.dto.response.ModeloMedidaResponseDTO;
import com.enel.scom.api.gestion.exception.NroServicioNotFoundException;
import com.enel.scom.api.gestion.model.MedidaModeloEntity;
import com.enel.scom.api.gestion.service.MedidaModeloService;

@RestController
@RequestMapping("api/medida-modelo")
public class MedidaModeloController {
	
	@Autowired
	MedidaModeloService medidaModeloService;
	
	@GetMapping("/{id}")
	public ResponseEntity<List<MedidaModeloEntity>> getMedidasModelos(@PathVariable("id") Long id) {
		
		List<MedidaModeloEntity> medidas = medidaModeloService.getMedidaModelo(id);
		
		return new ResponseEntity<>(medidas, HttpStatus.OK);
	}
	
	@GetMapping("por-modelo/{idModelo}")
	public ResponseEntity<List<ModeloMedidaResponseDTO>> obtenerMedidasPorModelo(@PathVariable("idModelo") Long idModelo) {
		
		List<ModeloMedidaResponseDTO> medidas = medidaModeloService.obtenerMedidasPorModelo(idModelo);
		
		return new ResponseEntity<>(medidas, HttpStatus.OK);
	}
	
	
	@PostMapping("/crear")
	public ResponseEntity<MedidaModeloEntity> agregarMedidaModelo(@RequestBody MedMedidaModeloRequestDTO medModelo) throws NroServicioNotFoundException {
		MedidaModeloEntity response = medidaModeloService.crearMedidaModelo(medModelo);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

}
