package com.enel.scom.api.gestion.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.model.ActivacionEntity;

@Repository
public interface ActivacionRepository extends JpaRepository<ActivacionEntity, Long> {
	
	@Query(value = "SELECT * FROM activacion act WHERE act.id_auto_act=:idAutoActiva", nativeQuery = true)
	ActivacionEntity obtenerActivacionPorIdAutoActiva(@Param("idAutoActiva") Long idAutoActiva);
}
