package com.enel.scom.api.gestion.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.enel.scom.api.gestion.dto.request.OrdenContrasteRequestDTO;
import com.enel.scom.api.gestion.model.OrdenContrasteEntity;
import com.enel.scom.api.gestion.model.OrdenEntity;
import com.enel.scom.api.gestion.service.OrdenContrasteService;

@RestController
@RequestMapping("api/ordenContraste")
public class OrdenContrasteController {

	@Autowired
	OrdenContrasteService ordenContrasteService;
	
	@PostMapping("/store")
	public ResponseEntity<OrdenEntity> postStore(@RequestBody OrdenContrasteRequestDTO ordenDTO) {
		OrdenEntity response = ordenContrasteService.storeUpdate(ordenDTO);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
}
