package com.enel.scom.api.gestion.service;

import com.enel.scom.api.gestion.dto.request.OrdenNormalizacionRequestDTO;
import com.enel.scom.api.gestion.dto.response.OrdenDataResponseDTO;
import com.enel.scom.api.gestion.exception.NroServicioNotFoundException;
import com.enel.scom.api.gestion.model.OrdenEntity;
import com.enel.scom.api.gestion.model.OrdenNormalizacionEntity;

public interface OrdenNormalizacionService {
	OrdenEntity storeUpdate(OrdenNormalizacionRequestDTO ordenNormalizacionDTO) throws NroServicioNotFoundException;
	OrdenDataResponseDTO getId(Long id);
}
