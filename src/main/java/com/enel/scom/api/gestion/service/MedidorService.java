package com.enel.scom.api.gestion.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.enel.scom.api.gestion.dto.request.MedidorRequestDTO;
import com.enel.scom.api.gestion.dto.response.MedidorDataResponseDTO;
import com.enel.scom.api.gestion.dto.response.MedidorPaginacionResponseDTO;
import com.enel.scom.api.gestion.exception.NroServicioNotFoundException;
import com.enel.scom.api.gestion.model.MedidorEntity;

public interface MedidorService {
	Page<MedidorPaginacionResponseDTO> getMedidorPaginacion(Pageable paging, String nro_medidor, Long id_marca, Long id_modelo, String usuario, String fecha_inicio, String fecha_fin, String fecha_inicio_estado, String fecha_fin_estado, Long id_estado);
	Page<MedidorPaginacionResponseDTO> getMedidorPaginacion_V2(Pageable paging, String nro_medidor, Long id_marca, Long id_modelo, String usuario, String fecha_inicio, String fecha_fin, String fecha_inicio_estado, String fecha_fin_estado, Long id_estado);
	
	MedidorEntity postStore(MedidorRequestDTO medidor) throws NroServicioNotFoundException;
	void delete(Long id);
	MedidorDataResponseDTO getData(Long id);
}
