package com.enel.scom.api.gestion.service;

import java.util.List;

import com.enel.scom.api.gestion.dto.response.TareaAnormalidadResponseDTO;

public interface DisTareaAnormalidadService {
	List<TareaAnormalidadResponseDTO> findByAnormalidad(Long id);
}
