package com.enel.scom.api.gestion.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.model.ProductoEntity;

@Repository
public interface ProductoRepository extends JpaRepository<ProductoEntity, Long>{
	@Query(value = "SELECT * FROM dis_producto WHERE activo = 'S'", nativeQuery = true)
	List<ProductoEntity> getActivos();
	
	@Query(value = "SELECT * FROM dis_producto WHERE id = :id", nativeQuery = true)
	ProductoEntity findId(@Param("id") Long id);
	
	@Query(value = "SELECT * FROM dis_producto WHERE activo = 'S' AND id_motivo = :idMotivo ORDER BY des_producto ASC", nativeQuery = true)
	List<ProductoEntity> getMotivos(@Param("idMotivo") Long idMotivo);
}
