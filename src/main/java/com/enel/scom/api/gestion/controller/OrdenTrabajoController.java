package com.enel.scom.api.gestion.controller;


import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.enel.scom.api.gestion.dto.request.TransferAnularRequestDTO;
import com.enel.scom.api.gestion.dto.response.OrdenTrabajoPaginacionResponseDTO;
import com.enel.scom.api.gestion.dto.response.OrdenTrabajoSCOMPaginacionResponseDTO;
import com.enel.scom.api.gestion.dto.response.TransferPaginacionResponseDTO;
import com.enel.scom.api.gestion.exception.AnularNotFoundException;
import com.enel.scom.api.gestion.service.OrdenTrabajoService;
import com.enel.scom.api.gestion.util.Funciones;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api/orden-trabajo")
public class OrdenTrabajoController {
	
	@Autowired
	OrdenTrabajoService ordenTrabajoService;
	
	
	@GetMapping
    public ResponseEntity<Page<TransferPaginacionResponseDTO>> getOrdenesTrabajoPaginacion(
			@RequestParam(required=false) String idTipoOrden,
			@RequestParam(required=false) String fechaInicio,
			@RequestParam(required=false) String fechaFin,
			@RequestParam(required=false) String numOrden,
			@RequestParam(required=false) Long numOrdenTdc,
			@RequestParam(required=false) String nroCuenta,
			@RequestParam(required=false) String estado,
			@RequestParam(required=false) String codTipoProceso,
			@RequestParam(required=false) Long codEstadTransfer,
			@RequestParam(required=false) String codTipoOrdenEorder,
			@RequestParam(required= false) String autoActivacion,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size,
            @RequestParam(defaultValue = "id") String order,
            @RequestParam(defaultValue = "true") boolean asc
    ){
		String uniqueID = UUID.randomUUID().toString();
		log.info("/ ini {}", uniqueID);
		log.info("/ ini {}", Funciones.logMemoryJVM());
        Page<TransferPaginacionResponseDTO> ordenes = ordenTrabajoService.getOrdenTransferenciaPaginacion(
                PageRequest.of(page, size, Sort.by(order)), idTipoOrden, fechaInicio, fechaFin, numOrden, numOrdenTdc, nroCuenta, estado, codTipoProceso, codEstadTransfer, codTipoOrdenEorder, autoActivacion);
		//    if(!asc)
		//     	ordenes = ordenTrabajoService.getOrdenTransferenciaPaginacion(
		//     			PageRequest.of(page, size, Sort.by(order)), idTipoOrden, fechaInicio, fechaFin, numOrden, numOrdenTdc, nroCuenta, estado, codTipoProceso, codEstadTransfer, codTipoOrdenEorder, autoActivacion);
		log.info("/ fin {}", Funciones.logMemoryJVM());
		log.info("/ fin {}", uniqueID);
        return new ResponseEntity<Page<TransferPaginacionResponseDTO>>(ordenes, HttpStatus.OK); 
    }
	
	@PostMapping("/anulacion")
	public List<TransferAnularRequestDTO> anulacionMasivaTransferencia(@RequestBody List<TransferAnularRequestDTO> transferRequest) throws AnularNotFoundException {
		List<TransferAnularRequestDTO> transferenciasAnuladas = ordenTrabajoService.anulacionMasivaTransferencia(transferRequest);
		return transferenciasAnuladas;
	}
	
	
	@GetMapping("/orden-trabajo-scom")
    public ResponseEntity<Page<OrdenTrabajoSCOMPaginacionResponseDTO>> getOrdenesTrabajoSCOMPaginacion(
			@RequestParam(required=false) String idTipoOrden,
			@RequestParam(required=false) String fechaInicio,
			@RequestParam(required=false) String fechaFin,
			@RequestParam(required=false) String numOrden,
			@RequestParam(required=false) String estado,
			@RequestParam(required=false) String codTipoProceso,
			@RequestParam(required=false) Long idServicio,
			@RequestParam(required=false) String nroCuenta,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size,
            @RequestParam(defaultValue = "id") String order,
            @RequestParam(defaultValue = "true") boolean asc
    ){
		String uniqueID = UUID.randomUUID().toString();
		log.info("/orden-trabajo-scom ini {}", uniqueID);
		log.info("/orden-trabajo-scom ini {}", Funciones.logMemoryJVM());
        Page<OrdenTrabajoSCOMPaginacionResponseDTO> ordenes = ordenTrabajoService.getOrdenTrabajoSCOMPaginacion(
                PageRequest.of(page, size, Sort.by(order)), idTipoOrden, fechaInicio, fechaFin, numOrden, estado, codTipoProceso, idServicio, nroCuenta);
		//    if(!asc)
				// ordenes = ordenTrabajoService.getOrdenTrabajoSCOMPaginacion(
						// PageRequest.of(page, size, Sort.by(order)), idTipoOrden, fechaInicio, fechaFin, numOrden, estado, codTipoProceso, idServicio);
		log.info("/orden-trabajo-scom fin {}", Funciones.logMemoryJVM());
		log.info("/orden-trabajo-scom fin {}", uniqueID);
        return new ResponseEntity<Page<OrdenTrabajoSCOMPaginacionResponseDTO>>(ordenes, HttpStatus.OK); 
    }
	

}
