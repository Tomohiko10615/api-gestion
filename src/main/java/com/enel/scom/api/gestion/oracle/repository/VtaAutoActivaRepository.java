package com.enel.scom.api.gestion.oracle.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.oracle.model.VtaAutoActivaEntity;

@Repository
public interface VtaAutoActivaRepository extends JpaRepository<VtaAutoActivaEntity, Long>{
	
	@Query(value = "SELECT act.id_auto_act FROM vta_auto_act act WHERE act.nro_orden=:nroOrden", nativeQuery = true)
	Long obtenerIdAutoActivaPorNroOrden(@Param("nroOrden") String nroOrden);

}
