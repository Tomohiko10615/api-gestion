package com.enel.scom.api.gestion.auth;

	import java.util.Arrays;

	import org.springframework.boot.web.servlet.FilterRegistrationBean;
	import org.springframework.context.annotation.Bean;
	import org.springframework.context.annotation.Configuration;
	import org.springframework.core.Ordered;
	import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
	import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
	import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
	import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
	import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
	import org.springframework.web.cors.CorsConfiguration;
	import org.springframework.web.cors.CorsConfigurationSource;
	import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
	import org.springframework.web.filter.CorsFilter;

	@Configuration
	@EnableResourceServer
	public class ResourceServerConfig extends ResourceServerConfigurerAdapter {
		
		@Override
		public void configure(HttpSecurity http) throws Exception {
			http.authorizeRequests()
			.antMatchers("http://localhost:9004/oauth/token").permitAll()
			.antMatchers("/v2/**").permitAll()
			.antMatchers("/webjars/**").permitAll()
			.antMatchers("/swagger-resources/**").permitAll()
			.antMatchers("/swagger-ui.html/**").permitAll()
			// .anyRequest().authenticated()
			.and().cors().configurationSource(corsConfigurationSource())
			;
		}

		@Override
		public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
			resources.tokenStore(tokenStore());
		}

		@Bean
		public CorsConfigurationSource corsConfigurationSource() {
			CorsConfiguration config = new CorsConfiguration();
			config.setAllowedOrigins(Arrays.asList(
					"http://claapiapwj02:8080",
					"http://localhost:9004/oauth/token", 
					"https://wsscom.enel.com/api-security/oauth/token",
					"http://10.152.169.41:8080", 
					"https://wsscom.enel.com/api-security/",
					"http://claapiapwj03:8080/api-security/oauth/token", 
					"http://localhost:4200", 
					"http://claapiapwj03:8080", 
					"http://localhost:9002/swagger-ui.html", 
					"https://webscom.enel.com/app-scom/", 
					"https://webscom.enel.com/",
					"http://webscom.enel.com/app-scom/", 
					"http://webscom.enel.com/"));
			config.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "DELETE", "OPTIONS"));
			config.setAllowCredentials(true);
			config.setAllowedHeaders(Arrays.asList("Content-Type", "Authorization"));
			
			UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
			source.registerCorsConfiguration("/**", config);
			return source;
		}
		
		@Bean
		public FilterRegistrationBean<CorsFilter> corsFilter(){
			FilterRegistrationBean<CorsFilter> bean = new FilterRegistrationBean<CorsFilter>(new CorsFilter(corsConfigurationSource()));
			bean.setOrder(Ordered.HIGHEST_PRECEDENCE);
			return bean;
		}

		@Bean
		public JwtTokenStore tokenStore() {
			return new JwtTokenStore(accessTokenConverter());
		}
		
		@Bean
		public JwtAccessTokenConverter accessTokenConverter() {

			JwtAccessTokenConverter jwtAccessTokenConverter = new JwtAccessTokenConverter();
			jwtAccessTokenConverter.setSigningKey("JWTSuperSecretKey");
			return jwtAccessTokenConverter;
		}
	}
