package com.enel.scom.api.gestion.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.dto.LecturasDTO;
import com.enel.scom.api.gestion.dto.MedidorDTO;
import com.enel.scom.api.gestion.dto.TareaDTO;
import com.enel.scom.api.gestion.dto.XMLInspeccionRecepcionDTO;
import com.enel.scom.api.gestion.dto.XmlCargasDTO;
import com.enel.scom.api.gestion.dto.XmlDTO;
import com.enel.scom.api.gestion.dto.XmlGestionPerdidasDTO;
import com.enel.scom.api.gestion.model.TransferEntity;

@Repository
public interface TransferRepository extends JpaRepository<TransferEntity, Long> {
	
	@Query(value = "SELECT * FROM schscom.eor_ord_transfer "
			+ "WHERE nro_orden_legacy=:nroOrden", nativeQuery = true)
	TransferEntity findByNroOrdenAndTipoOrden(@Param("nroOrden") Long nroOrden);
	
	@Query(value = "SELECT count(nro_orden_legacy) FROM schscom.eor_ord_transfer "
			+ "WHERE nro_orden_legacy=:nroOrden", nativeQuery = true)
	Integer obtenerOrdenPorNroOrden (@Param("nroOrden") String nroOrden);
	
	@Query(value = "SELECT * FROM schscom.eor_ord_transfer "
			+ "WHERE id_ord_transfer=:id", nativeQuery = true)
	TransferEntity obtenerTransferenciaPorId(@Param("id") Long id);

	@Query(value = "SELECT "
	+"eot.COD_TIPO_ORDEN_LEGACY, "
	+"eot.NRO_ORDEN_LEGACY, "
	+"cptoe.descripcion as COD_TIPO_ORDEN_EORDER, "
	+"eot.NRO_ORDEN_EORDER, "
	+"cpet.descripcion as COD_ESTADO_ORDEN, "
	+"eot.NRO_ENVIOS, "
	+"eot.NRO_RECEPCIONES, "
	+"eot.SUSPENDIDA, "
	+"eot.FEC_OPERACION, "
	+"eot.TIENE_ANEXOS, "
	+"cpea.descripcion as COD_ESTADO_ANEXO, "
	+"eot.NOM_ARCH_ANEXO, "
	+"eot.OBSERVACIONES, "
	+"eot.FEC_ESTADO, "
	+"eot.NRO_CUENTA, "
	+"eot.CREADA_EORDER, "
	+"eot.FECHA_CREACION, "
	+"eot.generacion, 'OCNX', actv.estado_activacion, actv.id_auto_act "
	//+"eot.generacion, OTO.DES_TIPO_ORDEN, actv.estado_activacion, actv.id_auto_act "
	+"FROM schscom.eor_ord_transfer eot "
	+"LEFT JOIN schscom.com_parametros cpet ON cpet.valor_num = eot.cod_estado_orden and cpet.entidad = 'ESTADO_TRANSFERENCIA' "
	+"LEFT JOIN schscom.com_parametros cpea ON cpea.valor_num = eot.cod_estado_anexo and cpea.entidad = 'ESTADO_ANEXOS' "
	+"LEFT JOIN schscom.com_parametros cptoe ON cptoe.valor_alf = eot.cod_tipo_orden_eorder and cptoe.entidad = 'TIPO_TDC' "
	//+"LEFT JOIN SCHSCOM.ORD_TIPO_ORDEN OTO ON eot.COD_TIPO_ORDEN_LEGACY = OTO.COD_TIPO_ORDEN "
	+"LEFT JOIN activacion actv ON eot.nro_orden_legacy = actv.numero_orden_conexion "
	+"WHERE 1=1 "
	//+"WHERE eot.COD_TIPO_ORDEN_LEGACY = OTO.COD_TIPO_ORDEN "
	+"AND eot.id_ord_transfer = :idOrden", nativeQuery = true)
	List<Object[]> findByIdOrden(@Param("idOrden") Long idOrden);
	
	
	
	@Query(value = "SELECT tra.nro_orden_lgc_relac FROM eor_ord_transfer tra "
			+ "WHERE nro_orden_legacy=:nroOrden", nativeQuery = true)
	String obtenerNroOrdenLgcRelacPorNroOrdenLegacy (@Param("nroOrden") String nroOrden);
	
	
	@Query(value = "SELECT (reg_xml::::Varchar) as xml FROM eor_ord_transfer eot "
			+ "INNER JOIN eor_ord_transfer_det eotd on eot.id_ord_transfer = eotd.id_ord_transfer "
			+ "WHERE eot.id_ord_transfer= :id AND accion = :accion", nativeQuery = true)
	String obtenerXMLTransferencia(@Param("id") Long id, @Param("accion") String accion);

	// R.I. REQSCOM06 07/07/2023 INICIO
	@Query(value = "SELECT\r\n"
			+ "  to_char((xpath('//Fin_Operacion/text()', eotd.reg_xml))[1]::::text::::timestamp, 'DD/MM/YYYY HH24:MI:SS') AS fechaEjecucion,\r\n"
			+ "  (xpath('//Notas_Operacion/text()', eotd.reg_xml))[1]::::text AS observacion\r\n"
			+ "FROM\r\n"
			+ "  eor_ord_transfer_det AS eotd\r\n"
			+ "  JOIN eor_ord_transfer AS eot ON eotd.id_ord_transfer = eot.id_ord_transfer\r\n"
			+ "  join ord_orden as oo on eot.nro_orden_legacy = oo.nro_orden \r\n"
			+ "WHERE\r\n"
			+ "  eotd.accion = 'RECEPCION'\r\n"
			+ "  AND eot.nro_orden_legacy = ?1\r\n"
			+ "  and oo.id_tipo_orden = ?2", nativeQuery = true)
	XmlDTO obtenerXmlOrdenTrabajo(String numOrden, Long idTipoOrden);
	// R.I. REQSCOM06 07/07/2023 FIN

	// R.I. REQSCOM02 13/07/2023 INICIO
	@Query(value = "        SELECT\r\n"
			+ "  (xpath('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/medidores/item[accionMedidor=\"Instalado\"]/lecturas/item/estadoLeido/text()', eotd.reg_xml))[1]::::text AS estadoLeidoInstalado,\r\n"
			+ "  (xpath('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/medidores/item[accionMedidor=\"Encontrado\"]/lecturas/item/estadoLeido/text()', eotd.reg_xml))[1]::::text AS estadoLeidoEncontrado\r\n"
			+ "FROM\r\n"
			+ "  eor_ord_transfer_det AS eotd\r\n"
			+ "JOIN\r\n"
			+ "  eor_ord_transfer AS eot ON eotd.id_ord_transfer = eot.id_ord_transfer\r\n"
			+ "  join\r\n"
			+ "        ord_orden as oo \r\n"
			+ "            on eot.nro_orden_legacy = oo.nro_orden   \r\n"
			+ "WHERE\r\n"
			+ "        eotd.accion = 'RECEPCION'    \r\n"
			+ "        AND eot.nro_orden_legacy = ?1\r\n"
			+ "        and oo.id_tipo_orden = 7", nativeQuery = true)
	LecturasDTO obtenerLecturas(String nroOrden);
	// R.I. REQSCOM02 13/07/2023 FIN

	// R.I. REQSCOM03 14/07/2023 INICIO
	@Query(value = "SELECT \r\n"
			+ "  (xpath('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/Codigo_Resultado/text()', reg_xml))[1]::::text AS codigoResultado,\r\n"
			+ "  (xpath('/RecepcionarResultadoTDC/datosGestionPerdidasNormalizaciones/se_cambio_el_medidor/text()', reg_xml))[1]::::text AS seCambioElMedidor,\r\n"
			+ "  (SELECT nombre FROM schscom.com_contratista cc\r\n"
			+ "   LEFT JOIN schscom.nuc_persona np ON cc.id_persona = np.id\r\n"
			+ "   WHERE cc.cod_contratista = (xpath('/RecepcionarResultadoTDC/datosGestionPerdidasNormalizaciones/Codigo_Contratista/text()', reg_xml))[1]::::text) AS codigoContratista,\r\n"
			//+ "  (xpath('/RecepcionarResultadoTDC/datosGestionPerdidasNormalizaciones/nro_notificacion_inspeccion/text()', reg_xml))[1]::::text AS nroNotificacionInspeccion,\r\n"
			+ "(select nro_ot_fisica from dis_ord_norm don, ord_orden oo where don.id_orden = oo.id and oo.nro_orden = ?1) AS nroNotificacionInspeccion,"
			+ "  (xpath('/RecepcionarResultadoTDC/datosGestionPerdidasNormalizaciones/codigo_tdc_inspeccion_asociada/text()', reg_xml))[1]::::text AS codidoTdcInspeccionAsociada,\r\n"
			+ "  (xpath('/RecepcionarResultadoTDC/datosGestionPerdidasNormalizaciones/Tipo_anomalia/text()', reg_xml))[1]::::text AS tipoAnomalia\r\n"
			+ "FROM eor_ord_transfer_det\r\n"
			+ "  JOIN eor_ord_transfer ON eor_ord_transfer_det.id_ord_transfer = eor_ord_transfer.id_ord_transfer\r\n"
			+ "  JOIN ord_orden ON eor_ord_transfer.nro_orden_legacy = ord_orden.nro_orden\r\n"
			+ "WHERE eor_ord_transfer_det.accion = 'RECEPCION' \r\n"
			+ "  AND eor_ord_transfer.nro_orden_legacy = ?1\r\n"
			+ "  AND ord_orden.id_tipo_orden = 7\r\n"
			+ "", nativeQuery = true)
	XmlGestionPerdidasDTO obtenerXmlGestionPerdidas(String nroOrden);
	
	@Query(value = "  select xmltable.*\r\n"
			+ "from eor_ord_transfer_det eotd\r\n"
			+ "JOIN eor_ord_transfer ON eotd.id_ord_transfer = eor_ord_transfer.id_ord_transfer\r\n"
			+ "JOIN ord_orden ON eor_ord_transfer.nro_orden_legacy = ord_orden.nro_orden,\r\n"
			+ "     XMLTABLE('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/medidores/item/lecturas/item/tipoLectura' PASSING eotd.reg_xml\r\n"
			+ "     columns\r\n"
			+ "            accionMedidor varchar(20) PATH '../../../accionMedidor',\r\n"
			+ "            marcaMedidor varchar(20) PATH '../../../marcaMedidor',\r\n"
			+ "            modeloMedidor varchar(20) PATH '../../../modeloMedidor',\r\n"
			+ "            numeroMedidor varchar(20) PATH '../../../numeroMedidor',\r\n"
			+ "            tipoLectura varchar(5) PATH '.',\r\n"
			+ "            horarioLectura varchar(5) PATH '../horarioLectura',\r\n"
			+ "            estadoLeido numeric(8,2) PATH '../estadoLeido',\r\n"
			+ "            fechaLectura varchar(10) PATH '../fechaLectura')\r\n"
			+ "where eotd.accion = 'RECEPCION'\r\n"
			+ "    AND eor_ord_transfer.nro_orden_legacy = ?1\r\n"
			+ "    AND ord_orden.id_tipo_orden = 7", nativeQuery = true)
	List<MedidorDTO> obtenerMedidores(String nroOrden);
	
	@Query(value = "SELECT \r\n"
			+ "  dt.des_tarea AS desTarea,\r\n"
			+ "  predeterminada,\r\n"
			+ "  ejecutada\r\n"
			+ "FROM (\r\n"
			+ "  SELECT \r\n"
			+ "    unnest(xpath('/RecepcionarResultadoTDC/datosGestionPerdidasNormalizaciones/tareas/item/codigoTarea/text()', reg_xml))::::text AS codigoTarea,\r\n"
			+ "    unnest(xpath('/RecepcionarResultadoTDC/datosGestionPerdidasNormalizaciones/tareas/item/predeterminada/text()', reg_xml))::::text AS predeterminada,\r\n"
			+ "    unnest(xpath('/RecepcionarResultadoTDC/datosGestionPerdidasNormalizaciones/tareas/item/ejecutada/text()', reg_xml))::::text AS ejecutada\r\n"
			+ "  FROM eor_ord_transfer_det\r\n"
			+ "    JOIN eor_ord_transfer ON eor_ord_transfer_det.id_ord_transfer = eor_ord_transfer.id_ord_transfer\r\n"
			+ "    JOIN ord_orden ON eor_ord_transfer.nro_orden_legacy = ord_orden.nro_orden\r\n"
			+ "  WHERE eor_ord_transfer_det.accion = 'RECEPCION' \r\n"
			+ "    AND eor_ord_transfer.nro_orden_legacy = ?1\r\n"
			+ "    AND ord_orden.id_tipo_orden = 7\r\n"
			+ ") AS subquery\r\n"
			+ "JOIN dis_tarea dt ON dt.cod_tarea = subquery.codigoTarea;", nativeQuery = true)
	List<TareaDTO> obtenerTareas(String nroOrden);
	//R.I. REQSCOM03 14/07/2023 FIN

	@Query(value = "SELECT unnest(xpath('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/medidores', reg_xml))::xml AS medidores_xml\r\n"
			+ "FROM eor_ord_transfer_det\r\n"
			+ "JOIN eor_ord_transfer ON eor_ord_transfer_det.id_ord_transfer = eor_ord_transfer.id_ord_transfer\r\n"
			+ "JOIN ord_orden ON eor_ord_transfer.nro_orden_legacy = ord_orden.nro_orden\r\n"
			+ "WHERE eor_ord_transfer_det.accion = 'RECEPCION' \r\n"
			+ "AND eor_ord_transfer.nro_orden_legacy = ?1\r\n"
			+ "AND ord_orden.id_tipo_orden = 7", nativeQuery = true)
	String obtenerXmlMedidores(String nroOrden);

	// R.I. CORRECCION 11/09/2023 INICIO
	@Query(value = "  SELECT\r\n"
			+ "  unnest(xpath('/RecepcionarResultadoTDC/DatosComunesProcesosTDC/Codigo_Resultado/text()', eotd.reg_xml))::::text AS Codigo_Resultado,\r\n"
			+ "  unnest(xpath('/RecepcionarResultadoTDC/datosGestionPerdidasInspecciones/nro_informe_inspeccion/text()', eotd.reg_xml))::::text AS Nro_informe_inspeccion,\r\n"
			+ "  unnest(xpath('/RecepcionarResultadoTDC/datosGestionPerdidasInspecciones/motivoInspeccion/text()', eotd.reg_xml))::::text AS motivoInspeccion,\r\n"
			+ "  unnest(xpath('/RecepcionarResultadoTDC/datosGestionPerdidasInspecciones/identificadorSuministro/text()', eotd.reg_xml))::::text AS identificadorSuministro,\r\n"
			+ "  unnest(xpath('/RecepcionarResultadoTDC/datosGestionPerdidasInspecciones/Trabajo_coordinado/text()', eotd.reg_xml))::::text AS Trabajo_coordinado,\r\n"
			+ "  unnest(xpath('/RecepcionarResultadoTDC/datosGestionPerdidasInspecciones/Aprueba_Inspeccion/text()', eotd.reg_xml))::::text AS Aprueba_Inspeccion,\r\n"
			+ "  unnest(xpath('/RecepcionarResultadoTDC/datosGestionPerdidasInspecciones/Fecha_de_Notificacion_de_Aviso_intervencion/text()', eotd.reg_xml))::::text AS Fecha_de_Notificacion_de_Aviso_intervencion,\r\n"
			+ "  unnest(xpath('/RecepcionarResultadoTDC/datosGestionPerdidasInspecciones/Registro_de_cumplimiento_establecido_Art_171_RLCE/text()', eotd.reg_xml))::::text AS Registro_de_cumplimiento_establecido_Art_171_RLCE,\r\n"
			+ "  unnest(xpath('/RecepcionarResultadoTDC/datosGestionPerdidasInspecciones/Registro_de_cumplimiento_del_numeral_71_Norma_Reintegros_Recuperos/text()', eotd.reg_xml))::::text AS Registro_de_cumplimiento_del_numeral_71,\r\n"
			+ "  unnest(xpath('/RecepcionarResultadoTDC/datosGestionPerdidasInspecciones/reg_eval_general_de_la_conexion_electrica/text()', eotd.reg_xml))::::text AS reg_eval_general_de_la_conexion_electrica,\r\n"
			+ "  unnest(xpath('/RecepcionarResultadoTDC/datosGestionPerdidasInspecciones/reg_eval_general_del_sistema_medicion/text()', eotd.reg_xml))::::text AS Reg_Eval_Sistema_Medicion,\r\n"
			+ "  unnest(xpath('/RecepcionarResultadoTDC/datosGestionPerdidasInspecciones/Fecha_Propuesta_de_inicio_intervencion_Aviso_intervencion/text()', eotd.reg_xml))::::text AS Fecha_Propuesta_Aviso_Intervencion,\r\n"
			+ "  unnest(xpath('/RecepcionarResultadoTDC/datosGestionPerdidasInspecciones/Hora_Propuesta_de_inicio_intervencion_Aviso_Previo_intervencion/text()', eotd.reg_xml))::::text AS Hora_Propuesta_Aviso_Previo_Intervencion,\r\n"
			+ "  unnest(xpath('/RecepcionarResultadoTDC/datosGestionPerdidasInspecciones/Fecha_de_recepcion_del_Aviso_Previo_intervencion/text()', eotd.reg_xml))::::text AS Fecha_Recepcion_Aviso_Previo_Intervencion,\r\n"
			+ "  unnest(xpath('/RecepcionarResultadoTDC/datosGestionPerdidasInspecciones/Hora_de_recepcion_del_Aviso_Previo_intervencion/text()', eotd.reg_xml))::::text AS Hora_de_recepcion_del_Aviso_Previo_intervencion,\r\n"
			+ "  unnest(xpath('/RecepcionarResultadoTDC/datosGestionPerdidasInspecciones/Prueba_Vacio_AD/text()', eotd.reg_xml))::::text AS Prueba_Vacio_AD,\r\n"
			+ "  unnest(xpath('/RecepcionarResultadoTDC/datosGestionPerdidasInspecciones/Sticker_de_contraste/text()', eotd.reg_xml))::::text AS Sticker_de_contraste,\r\n"
			+ "  unnest(xpath('/RecepcionarResultadoTDC/datosGestionPerdidasInspecciones/Sin_mica_Mica_Rota/text()', eotd.reg_xml))::::text AS Sin_mica_Mica_Rota,\r\n"
			+ "  unnest(xpath('/RecepcionarResultadoTDC/datosGestionPerdidasInspecciones/Caja_sin_tapa_Tapa_mal_estado/text()', eotd.reg_xml))::::text AS Caja_sin_tapa_Tapa_mal_estado,\r\n"
			+ "    unnest(xpath('/RecepcionarResultadoTDC/datosGestionPerdidasInspecciones/Sin_cerradura_cerradura_en_mal_estado/text()', eotd.reg_xml))::::text AS Sin_cerradura_cerradura_en_mal_estado,\r\n"
			+ "  unnest(xpath('/RecepcionarResultadoTDC/datosGestionPerdidasInspecciones/estadoConexion/text()', eotd.reg_xml))::::text AS estadoConexion,\r\n"
			+ "  unnest(xpath('/RecepcionarResultadoTDC/datosGestionPerdidasInspecciones/Cliente_permite_inventario_carga/text()', eotd.reg_xml))::::text AS Cliente_permite_inventario_carga,\r\n"
			+ "  unnest(xpath('/RecepcionarResultadoTDC/datosGestionPerdidasInspecciones/cortado_art_90/text()', eotd.reg_xml))::::text AS cortado_art_90,\r\n"
			+ "  unnest(xpath('/RecepcionarResultadoTDC/datosGestionPerdidasInspecciones/ESTADO_CAJA_PORTAMEDIDOR/text()', eotd.reg_xml))::::text AS ESTADO_CAJA_PORTAMEDIDOR,\r\n"
			+ "  unnest(xpath('/RecepcionarResultadoTDC/datosGestionPerdidasInspecciones/TOMA_CON_REJA/text()', eotd.reg_xml))::::text AS TOMA_CON_REJA,\r\n"
			+ "  unnest(xpath('/RecepcionarResultadoTDC/datosGestionPerdidasInspecciones/CAJA_CON_REJA/text()', eotd.reg_xml))::::text AS CAJA_CON_REJA\r\n"
			+ "FROM\r\n"
			+ "  eor_ord_transfer_det AS eotd\r\n"
			+ "  JOIN eor_ord_transfer AS eot ON eotd.id_ord_transfer = eot.id_ord_transfer\r\n"
			+ "  JOIN ord_orden AS oo ON eot.nro_orden_legacy = oo.nro_orden \r\n"
			+ "WHERE\r\n"
			+ "  eotd.accion = 'RECEPCION'\r\n"
			+ "  AND eot.nro_orden_legacy = ?1\r\n"
			+ "  AND oo.id_tipo_orden = ?2", nativeQuery = true)
	XMLInspeccionRecepcionDTO obtenerXmlInspeccionRecepcion(String nroOrden, Long idTipoOrden);
	
	/*
	@Query(value = "SELECT\r\n"
			+ "  unnest(xpath('//pruebaCarga/item/Pruebas_Carga_en_el_primario/text()', eotd.reg_xml))::::text AS Pruebas_Carga_en_el_primario,\r\n"
			+ "  unnest(xpath('//pruebaCarga/item/fase/text()', eotd.reg_xml))::::text AS Fase\r\n"
			+ "FROM\r\n"
			+ "  eor_ord_transfer_det AS eotd\r\n"
			+ "  JOIN eor_ord_transfer AS eot ON eotd.id_ord_transfer = eot.id_ord_transfer\r\n"
			+ "  JOIN ord_orden AS oo ON eot.nro_orden_legacy = oo.nro_orden\r\n"
			+ "WHERE\r\n"
			+ "  eotd.accion = 'RECEPCION'\r\n"
			+ "  AND eot.nro_orden_legacy = ?1\r\n"
			+ "  AND oo.id_tipo_orden = ?2", nativeQuery = true)
	List<XmlCargasDTO> obtenerCargas(String nroOrden, Long idTipoOrden);
	*/
	
	@Query(value = "  SELECT omi.carga_r AS Pruebas_Carga_en_el_primario, 'R' AS Fase\r\n"
			+ "FROM ord_medi_insp AS omi\r\n"
			+ "JOIN ord_orden AS oo ON omi.id_orden = oo.id\r\n"
			+ "WHERE oo.nro_orden = ?1 AND omi.carga_r IS NOT NULL\r\n"
			+ "UNION ALL\r\n"
			+ "SELECT omi.carga_t AS Pruebas_Carga_en_el_primario, 'T' AS Fase\r\n"
			+ "FROM ord_medi_insp AS omi\r\n"
			+ "JOIN ord_orden AS oo ON omi.id_orden = oo.id\r\n"
			+ "WHERE oo.nro_orden = ?1 AND omi.carga_t IS NOT NULL\r\n"
			+ "UNION ALL\r\n"
			+ "SELECT omi.carga_s AS Pruebas_Carga_en_el_primario, 'S' AS Fase\r\n"
			+ "FROM ord_medi_insp AS omi\r\n"
			+ "JOIN ord_orden AS oo ON omi.id_orden = oo.id\r\n"
			+ "WHERE oo.nro_orden = ?1 AND omi.carga_s IS NOT NULL", nativeQuery = true)
	List<XmlCargasDTO> obtenerCargas(String nroOrden);
	// R.I. CORRECCION 11/09/2023 FIN

	
}
