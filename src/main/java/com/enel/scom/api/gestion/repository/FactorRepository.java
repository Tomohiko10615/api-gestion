package com.enel.scom.api.gestion.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.model.FactorEntity;

@Repository
public interface FactorRepository extends JpaRepository<FactorEntity, Long>{
	@Query(value = "SELECT id, des_factor, cod_factor, val_factor FROM med_factor WHERE activo = true ORDER BY des_factor ASC", nativeQuery = true)
	List<Object[]> getAll();
	
	@Query(value = "SELECT * FROM med_factor WHERE id = :id", nativeQuery = true)
	FactorEntity findId(@Param("id") Long id);
	
	@Query(value = "select mf.id, mf.des_factor, mf.val_factor from schscom.med_fac_med_mod medmod "
			+ "inner join schscom.med_factor mf on medmod.id_factor = mf.id "
			+ "where medmod.id_medida_modelo =:idMedidaModelo", nativeQuery = true)
	List<Object[]> getFactorPorIdMedidaModelo(@Param("idMedidaModelo") Long idMedidaModelo);
}
