package com.enel.scom.api.gestion.service;

import java.util.List;

import com.enel.scom.api.gestion.dto.response.AutoCompleteResponseDTO;

public interface SrvSedService {

	/**
	 * Busca concidencidencias de codigo de SED CADENA ELECTRICA
	 * @param codSed
	 * @param idAlimentador
	 * @return
	 */
	List<AutoCompleteResponseDTO> buscarSedServicioElectrico(String codSed, Long idAlimentador);
}
