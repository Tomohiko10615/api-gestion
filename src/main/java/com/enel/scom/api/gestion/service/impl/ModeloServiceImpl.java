package com.enel.scom.api.gestion.service.impl;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.enel.scom.api.gestion.dto.request.MedidaModeloRequestDTO;
import com.enel.scom.api.gestion.dto.request.ModeloRequestDTO;
import com.enel.scom.api.gestion.dto.response.FactorResponseDTO;
import com.enel.scom.api.gestion.dto.response.MedidasResponseDTO;
import com.enel.scom.api.gestion.dto.response.ModeloDataResponseDTO;
import com.enel.scom.api.gestion.dto.response.ModeloMarcaResponseDTO;
import com.enel.scom.api.gestion.dto.response.ModeloMedidaResponseDTO;
import com.enel.scom.api.gestion.dto.response.ModeloPaginacionResponseDTO;
import com.enel.scom.api.gestion.exception.NroServicioNotFoundException;
import com.enel.scom.api.gestion.model.AmperajeEntity;
import com.enel.scom.api.gestion.model.FactorEntity;
import com.enel.scom.api.gestion.model.FaseEntity;
import com.enel.scom.api.gestion.model.MarcaEntity;
import com.enel.scom.api.gestion.model.MarcaEntity_;
import com.enel.scom.api.gestion.model.MedEntDecEntity;
import com.enel.scom.api.gestion.model.MedFacMedModEntity;
import com.enel.scom.api.gestion.model.MedidaEntity;
import com.enel.scom.api.gestion.model.MedidaModeloEntity;
import com.enel.scom.api.gestion.model.ModeloEntity;
import com.enel.scom.api.gestion.model.ModeloEntity_;
import com.enel.scom.api.gestion.model.RegistradorEntity;
import com.enel.scom.api.gestion.model.TecnologiaEntity;
import com.enel.scom.api.gestion.model.TensionEntity;
import com.enel.scom.api.gestion.model.TipoMedicionEntity;
import com.enel.scom.api.gestion.model.UnidadMedidaConstanteEntity;
import com.enel.scom.api.gestion.model.UsuarioEntity;
import com.enel.scom.api.gestion.model.UsuarioEntity_;
import com.enel.scom.api.gestion.model.VoltajeEntity;
import com.enel.scom.api.gestion.repository.AmperajeRepository;
import com.enel.scom.api.gestion.repository.FactorRepository;
import com.enel.scom.api.gestion.repository.FaseRepository;
import com.enel.scom.api.gestion.repository.MarcaRepository;
import com.enel.scom.api.gestion.repository.MedEntDecRepository;
import com.enel.scom.api.gestion.repository.MedFacMedModRepository;
import com.enel.scom.api.gestion.repository.MedidaModeloRepository;
import com.enel.scom.api.gestion.repository.MedidaRepository;
import com.enel.scom.api.gestion.repository.ModeloRepository;
import com.enel.scom.api.gestion.repository.RegistradorRepository;
import com.enel.scom.api.gestion.repository.TecnologiaRepository;
import com.enel.scom.api.gestion.repository.TensionRepository;
import com.enel.scom.api.gestion.repository.TipoMedicionRepository;
import com.enel.scom.api.gestion.repository.UnidadMedidaConstanteRepository;
import com.enel.scom.api.gestion.repository.UsuarioRepository;
import com.enel.scom.api.gestion.repository.VoltajeRepository;
import com.enel.scom.api.gestion.service.ModeloService;

/**
 * Servicio principal donde se realiza el mantenimiento de Modelo
 * @author Gilmar Moreno
 * @version 1.0
 */
@Service
public class ModeloServiceImpl implements ModeloService{

	@Autowired
	ModeloRepository modeloRepository;
	
	@Autowired
	AmperajeRepository amperajeRepository;
	
	@Autowired
	FaseRepository faseRepository;
	
	@Autowired
	MarcaRepository marcaRepository;
	
	@Autowired
	TecnologiaRepository tecnologiaRepository;
	
	@Autowired
	TensionRepository tensionRepository;
	
	@Autowired
	TipoMedicionRepository tipoMedicionRepository;
	
	@Autowired
	UnidadMedidaConstanteRepository unidadMedidaConstanteRepository;
	
	@Autowired
	UsuarioRepository usuarioRepository;
	
	@Autowired
	VoltajeRepository voltajeRepository;
	
	@Autowired
	RegistradorRepository registradorRepository;
	
	@Autowired
	MedidaModeloRepository medidaModeloRepository;
	
	@Autowired
	FactorRepository factorRepository;
	
	@Autowired
	MedEntDecRepository medEntDecRepository;
	
	@Autowired
	MedidaRepository medidaRepository;

	@Autowired
	MedFacMedModRepository medFacMedModRepository;
	
	@PersistenceContext
	EntityManager entityManager;
		
	/**
	 * Medoto para listar los modelos
	 * @param paging
	 * @param idMarca
	 * @param usuario
	 * @param desModelo
	 * @param codModeol
	 * @param fechaInicio
	 * @param fechaFin
	 */
	@Override
	public Page<ModeloPaginacionResponseDTO> getModeloPaginacion(Pageable paging, Long idMarca, String usuario, String desModelo, String codModelo, String fechaInicio, String fechaFin) {
		
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<ModeloPaginacionResponseDTO> cq = cb.createQuery(ModeloPaginacionResponseDTO.class);
		
		Root<ModeloEntity> root = cq.from(ModeloEntity.class);
		Join<ModeloEntity, MarcaEntity> joinMarca = root.join(ModeloEntity_.marca, JoinType.LEFT);
		Join<ModeloEntity, UsuarioEntity> joinUsuario = root.join(ModeloEntity_.usuarioRegistro, JoinType.LEFT);
			
		cq.multiselect(root.get(ModeloEntity_.id), root.get(ModeloEntity_.codModelo), 
						root.get(ModeloEntity_.desModelo), joinMarca.get(MarcaEntity_.codigo),
						root.get(ModeloEntity_.fecRegistro), joinUsuario.get(UsuarioEntity_.username));
			
		cq.orderBy(cb.desc(root.get(ModeloEntity_.fecRegistro)));
			
		Predicate[] predicate = getWhere(idMarca, usuario, desModelo, codModelo, fechaInicio, fechaFin, root, joinUsuario);
		
		cq.where(predicate);
		
		TypedQuery<ModeloPaginacionResponseDTO> typedQuery = entityManager.createQuery(cq);
		
		List<ModeloPaginacionResponseDTO> list = typedQuery.getResultList().stream().collect(Collectors.toList());
		int pageOffset = (int) paging.getOffset();
		int total = (pageOffset + paging.getPageSize()) > list.size() ? list.size() : (pageOffset + paging.getPageSize());
		return new PageImpl<>(list.subList(pageOffset, total), paging, list.size());
	}
	
	@SuppressWarnings("unchecked")
	private Predicate[] getWhere(Long idMarca, String usuario, String desModelo, String codModelo, String fechaInicio, String fechaFin, Root root, Join joinUsuario) {
			
			List<Predicate> listPred = new ArrayList<>();
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();

			if(!codModelo.trim().isEmpty()) {
				listPred.add(cb.equal(root.get(ModeloEntity_.codModelo), codModelo));
			}
			
			if(!desModelo.trim().isEmpty()) {
				listPred.add(cb.equal(root.get(ModeloEntity_.desModelo), desModelo));
			}
			
			if(idMarca > 0) {
				listPred.add(cb.equal(root.get(ModeloEntity_.marca), idMarca));
			}

			if(!usuario.trim().isEmpty()) {
				listPred.add(cb.equal(joinUsuario.get(UsuarioEntity_.username), usuario));
			}

			// INICIO CHR 20-07-23
			Date fechaInicioConvertido = null;
			Date fechaFinConvertido = null;
			
			try {
				SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
				fechaInicioConvertido = sdf1.parse(fechaInicio);
			}catch(Exception e) {
				System.out.println("Unparseable date: empty string value");
			}

			try {
				SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				fechaFinConvertido = sdf2.parse(fechaFin + " 23:59:59");
			}catch (Exception e) {
				System.out.println("Unparseable date: empty string value");
			}
			// FIN CHR 20-07-23
			
			listPred.add(cb.between(root.get(ModeloEntity_.fecCreacion),  fechaInicioConvertido, fechaFinConvertido));
			listPred.add(cb.equal(root.get(ModeloEntity_.activo), "S")); // CHR 25-07-23
			
			return listPred.toArray(new Predicate[listPred.size()]);
	}

	/**
	 * Se obtiene el modelo para los medidores.
	 */
	@Override
	public List<ModeloMarcaResponseDTO> getMarcaModelos(Long id) {
		List<Object[]> modelos = modeloRepository.getMarcaModelos(id);
		List<ModeloMarcaResponseDTO> dtos = new ArrayList<>();
		
		modelos.stream().forEach(k -> {
			ModeloMarcaResponseDTO dto = new ModeloMarcaResponseDTO();
			
			//dto.setId(Long.valueOf(k[0].toString()));
			dto.setId(String.valueOf(k[0].toString())); //CHR 20-07-23
			dto.setDescripcion(k[1].toString());
			dto.setCodigo(k[2].toString());
			dtos.add(dto);
		});
		
		return dtos;
	}

	/**
	 * Se obtiene las medidas de los modelos registrados
	 * @param id
	 */
	@Override
	public List<MedidasResponseDTO> getMedidasModelos(Long id) {
		List<Object[]> medidas = modeloRepository.getMedidasModelos(id);
		List<MedidasResponseDTO> dtos = new ArrayList<>();
		
		try {
			medidas.stream().forEach(k -> {
				MedidasResponseDTO dto = new MedidasResponseDTO();
				dto.setId_medida(Long.valueOf(k[0].toString()));
				dto.setDes_medida(k[1].toString());
				dto.setId_ent_dec(Long.valueOf(k[2].toString()));
				dto.setCant_entero(k[3].toString());
				dto.setCant_decimales(k[4].toString());
				dto.setId_factor(Long.valueOf(k[5].toString()));
				dto.setCod_factor(k[6].toString() == null ? "" : k[6].toString());
				dto.setDes_factor(k[7].toString() == null ? "" : k[7].toString());
				dto.setVal_factor(k[8].toString() == null ? "" : k[8].toString());
				dto.setId_tip_calculo(Long.valueOf(k[9].toString())); // CHR 07-09-23 INC000115648937
				dtos.add(dto);
			});
		}catch(NullPointerException e) {
            System.out.println("NullPointerException thrown!"); //CHR 05-07-23
        }
		
		return dtos;
	}

	/**
	 * Metodo para registrar y actualizar el Modelo
	 */
	@Override
	public ModeloEntity postStore(ModeloRequestDTO modelo) throws NroServicioNotFoundException {	
		/**
		 * Valida que la marca y codigo del modelo no se encuentre registrado.
		 */
		
		
		Boolean existeModelo = modeloRepository.existsById(modelo.getId_modelo());

		
		if(Boolean.FALSE.equals(existeModelo)) {
			Integer existeCodigo = modeloRepository.findByCodigo(modelo.getCod_modelo(), modelo.getId_marca());
			if(existeCodigo > 0) {
				throw new NroServicioNotFoundException("La marca y codigo del modelo ingresado se encuentra registrado.");
			}
		}
		
		AmperajeEntity amperaje = amperajeRepository.findId(modelo.getId_amperaje());
		FaseEntity fase = faseRepository.findId(modelo.getId_fase());
		MarcaEntity marca = marcaRepository.obtenerMarca(modelo.getId_marca());
		RegistradorEntity registrador = registradorRepository.findId(modelo.getId_registrador());
		TecnologiaEntity tecnologia = tecnologiaRepository.findId(modelo.getId_tecnologia());
		TensionEntity tension = tensionRepository.findId(modelo.getId_tension());
		TipoMedicionEntity tipo = tipoMedicionRepository.findId(modelo.getId_tipo_medicion());
		UnidadMedidaConstanteEntity unidad = unidadMedidaConstanteRepository.findId(modelo.getId_unidad_medida_constante());
		UsuarioEntity usuario = usuarioRepository.getUsuarioById(modelo.getId_usuario_registro());
		VoltajeEntity voltaje = voltajeRepository.findId(modelo.getId_voltaje());
		
		ModeloEntity data = (Boolean.FALSE.equals(existeModelo)) ? new ModeloEntity() : modeloRepository.findId(modelo.getId_modelo());
		
		data.setAmperaje(amperaje);
		data.setCantAnosAlmacen(modelo.getCant_anos_almacen());
		data.setCantAnosVida(modelo.getCant_anos_vida());
		data.setCantHilos(modelo.getCant_hilos());
		data.setClaseMedidor(modelo.getClase_medidor());
		data.setConstante(modelo.getConstante());
		data.setDesModelo(modelo.getDes_modelo().trim());
		data.setEsPatron((modelo.getEs_patron() == "true") ? "S" : "N");
		data.setEsReacondicionador((modelo.getEs_reacondicionador() == "true") ? "S" : "N");
		data.setEsReseteado((modelo.getEs_reseteado() == "true") ? "S" : "N");
		data.setEsTotalizador((modelo.getEs_totalizador() == "true") ? "S" : "N");
		data.setFase(fase);
		data.setMarca(marca);
		data.setNroRegistrador(modelo.getNro_registrador());
		data.setNroSellosBornera(modelo.getNro_sellos_bornera());
		data.setNroSellosMedidor(modelo.getNro_sellos_medidor());
		data.setRegistrador(registrador);
		data.setTecnologia(tecnologia);
		data.setTension(tension);
		data.setTipoMedicion(tipo);
		data.setUnidadMedidaConstante(unidad);
		data.setVoltaje(voltaje);
		
		if(Boolean.TRUE.equals(existeModelo)) {
			data.setFecModif(new Date());
			data.setUsuarioModif(usuario);
		}else {
			data.setCodModelo(modelo.getCod_modelo().trim());
			data.setActivo("S");
			data.setFecRegistro(new Date());
			data.setFecCreacion(new Date());
			data.setUsuarioRegistro(usuario);
			data.setId(modeloRepository.generaId());
		}
		
		ModeloEntity modeloResponse = modeloRepository.save(data);
	
		/*
		if(Boolean.TRUE.equals(existeModelo)) {
			medidaModeloRepository.deleteModelo(modelo.getId_modelo());
			medidaModeloRepository.deleteMedidaFactor(modelo.getId_modelo());
		}*/
		
		//if(Boolean.TRUE.equals(existeModelo)) {
		/*	List<ModeloMedidaResponseDTO> eliminados = new ArrayList<>();
			
			ModeloDataResponseDTO originales = getData(modelo.getId_modelo());			
			
			
			HashMap<Long, String> setActual = new HashMap<>();
			
			modelo.getMedidas().stream().forEach(k -> {
				setActual.put(k.getId_medida_modelo(), k.getId_medida_modelo().toString());
			});
		
			eliminados = originales.getMedidas().stream()
                    .filter(e -> !setActual.containsKey(e.getId_medida_modelo())) 
                    .collect(Collectors.toList());
		
			System.out.print("ELIMINADOS "+eliminados.size()+ " |");
				*/
		//}
		
		/*
		List<MedidaModeloRequestDTO> nuevos = new ArrayList<>();
		nuevos =  modelo.getMedidas().stream()
				.filter(item -> item.getId_medida_modelo() == null)
				.collect(Collectors.toList());
		System.out.print("NUEVOS "+nuevos.size()+ " |");	
		*/	
		/*
		nuevos.stream().forEach(k -> {
			FactorEntity factor = factorRepository.findId(k.getId_factor());
			MedEntDecEntity entdec = medEntDecRepository.findId(k.getId_ent_dec());
			MedidaEntity medida = medidaRepository.findId(k.getId_medida());
			
			MedidaModeloEntity medmod = new MedidaModeloEntity();
			medmod.setId(medidaModeloRepository.generaId());
			medmod.setModelo(modeloResponse);
			//medmod.setFactor(factor);
			medmod.setEntdec(entdec);
			medmod.setMedida(medida);
			 
			MedidaModeloEntity medmodResponse = medidaModeloRepository.save(medmod);

			MedFacMedModEntity medFac = new MedFacMedModEntity();
			medFac.setMedidaModelo(medmodResponse.getId());
			medFac.setFactor(factor);
			medFacMedModRepository.save(medFac);
		});
		*/
		return modeloResponse; 
	}

	/**
	 * Metodo para obtener los datos del modelo para utilizarlo en la edición y detalle
	 * @param id
	 */
	@Override
	public ModeloDataResponseDTO getData(Long id) {
		List<Object[]> objects =  modeloRepository.getData(id);
		List<Object[]> medidas = medidaModeloRepository.getModelo(id);
		List<ModeloMedidaResponseDTO> medidaDTO = new ArrayList<>();
		List<FactorResponseDTO> factorDTO = new ArrayList<>();
		
		ModeloDataResponseDTO modelo = new ModeloDataResponseDTO();
		
		objects.stream().forEach(k -> {
			modelo.setId((k[0] == null) ? 0 : Long.valueOf(k[0].toString()));
			modelo.setCod_modelo((k[1] == null) ? "" : k[1].toString());
			modelo.setDes_modelo((k[2] == null) ? "" : k[2].toString());
			modelo.setCant_anos_vida((k[3] == null) ? "" : k[3].toString());
			modelo.setCant_anos_almacen((k[4] == null) ? "" : k[4].toString());
			modelo.setNro_sellos_medidor((k[5] == null) ? "" : k[5].toString());
			modelo.setNro_sellos_bornera((k[6] == null) ? "" : k[6].toString());
			modelo.setNro_registrador((k[7] == null) ? "" : k[7].toString());
			modelo.setEs_reacondicionador((k[8] == null) ? "" : k[8].toString());
			modelo.setEs_reseteado((k[9] == null) ? "" : k[9].toString());
			modelo.setEs_patron((k[10] == null) ? "" : k[10].toString());
			modelo.setEs_totalizador((k[11] == null) ? "" : k[11].toString());
			modelo.setClase_medidor((k[12] == null) ? "" : k[12].toString());
			modelo.setConstante((k[13] == null) ? "" : k[13].toString());
			modelo.setCant_hilos((k[14] == null) ? "" : k[14].toString());
			modelo.setId_marca((k[15] == null) ? 0 : Long.valueOf(k[15].toString()));
			modelo.setId_fase((k[16] == null) ? 0 : Long.valueOf(k[16].toString()));
			modelo.setId_amperaje((k[17] == null) ? 0 : Long.valueOf(k[17].toString()));
			modelo.setId_tipo_medicion((k[18] == null) ? 0 : Long.valueOf(k[18].toString()));
			modelo.setId_voltaje((k[19] == null) ? 0 : Long.valueOf(k[19].toString()));
			modelo.setId_tension((k[20] == null) ? 0 : Long.valueOf(k[20].toString()));
			modelo.setId_tecnologia((k[21] == null) ? 0 : Long.valueOf(k[21].toString()));
			modelo.setId_registrador((k[22] == null) ? 0 : Long.valueOf(k[22].toString()));
			modelo.setId_unidad_medida_constante((k[23] == null) ? 0 :Long.valueOf(k[23].toString()));
			modelo.setCod_marca((k[24] == null) ? "" : k[24].toString());
			modelo.setDes_marca((k[25] == null) ? "" : k[25].toString());
			
			medidas.stream().forEach(t -> {
				ModeloMedidaResponseDTO medidasDTO = new ModeloMedidaResponseDTO();
				medidasDTO.setId_medida((t[0] == null) ? 0 : Long.valueOf(t[0].toString()));
				medidasDTO.setMedida_descripcion((t[1] == null) ? "" : t[1].toString());
				medidasDTO.setId_ent_dec((t[2] == null) ? 0 : Long.valueOf(t[2].toString()));
				medidasDTO.setCant_enteros((t[3] == null) ? "" : t[3].toString());
				medidasDTO.setCant_decimales((t[4] == null) ? "" : t[4].toString());
				medidasDTO.setId_medida_modelo((t[5] == null) ? 0 : Long.valueOf(t[5].toString()));
				medidasDTO.setDes_tip_calculo((t[6] == null) ? "" : t[6].toString());
				medidaDTO.add(medidasDTO);
				
				
				
				
			});
			
			modelo.setMedidas(medidaDTO);
			
			modelo.getMedidas().stream().forEach(med -> {
				List<Object[]> factores = factorRepository.getFactorPorIdMedidaModelo(med.getId_medida_modelo());
				factores.stream().forEach(fact -> {
					FactorResponseDTO factorResponseDTO = new FactorResponseDTO();
					factorResponseDTO.setId((fact[0] == null) ? 0 : Long.valueOf(fact[0].toString()));
					factorResponseDTO.setDesFactor((fact[1] == null) ? "" : fact[1].toString());
					factorResponseDTO.setValFactor((fact[2] == null) ? "" : fact[2].toString());
					med.getFactores().add(factorResponseDTO);
				});
			});
			
			
		});
		
		return modelo;
	}

	/**
	 * Metodo para eliminar el modelo
	 * @param id
	 */
	@Override
	public void delete(Long id) {
		modeloRepository.desactivar(id);
	}

}
