package com.enel.scom.api.gestion.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.enel.scom.api.gestion.dto.response.ClienteInspeccionResponseDTO;
import com.enel.scom.api.gestion.exception.NroServicioNotFoundException;
import com.enel.scom.api.gestion.model.ClienteEntity;
import com.enel.scom.api.gestion.service.ClienteService;

@RestController
@RequestMapping("/api/cliente")
public class ClienteController {
	
	@Autowired
	ClienteService clienteService;
	
	@GetMapping("/{id}")
	public ResponseEntity<ClienteEntity> getId(@PathVariable Long id) {
		ClienteEntity responses = clienteService.findId(id);
		return new ResponseEntity<>(responses, HttpStatus.OK);
	}
	
	@GetMapping("/nroServicio/{nroServicio}")
	public ResponseEntity<ClienteInspeccionResponseDTO> getNroServicio(@PathVariable Long nroServicio) throws NroServicioNotFoundException {
		
		ClienteInspeccionResponseDTO responses = clienteService.getNroServicio(nroServicio);
		return new ResponseEntity<>(responses, HttpStatus.OK);
	}
	
	@GetMapping("/nroCuenta/{nroCuenta}")
	public ResponseEntity<ClienteEntity> getNroCuenta(@PathVariable Long nroCuenta) {
		ClienteEntity responses = clienteService.getNroCuenta(nroCuenta);
		return new ResponseEntity<>(responses, HttpStatus.OK);
	}
}
