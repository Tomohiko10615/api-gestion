package com.enel.scom.api.gestion.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.model.WorkflowEntity;

@Repository
public interface WorkflowRepository extends JpaRepository<WorkflowEntity, Long>{
	@Query(value="SELECT nextval('sqworkflow')", nativeQuery=true)
	public Long generaId();

	@Query(value = "SELECT * FROM wkf_workflow WHERE id =:id", nativeQuery = true)
	WorkflowEntity obtenerEstadoOrdenPorId(@Param("id") Long id);

	@Query(value = "select * from schscom.ord_orden oo "
				+ "inner join schscom.wkf_workflow ww on oo.id_workflow = ww.id "
				+ "where id_servicio = :idServicio and oo.id_tipo_orden = :idTipoOrden order by 1 desc limit 1", nativeQuery = true)
	WorkflowEntity estadoActualOrden(@Param("idServicio") long idServicio, @Param("idTipoOrden") Long idTipoOrden);
	
	//INICIO - REQ # 1 - JEGALARZA
	@Query(value = "SELECT COUNT(*) AS COUNT  "
			+ "    FROM DIS_ORD_NORM, "
			+ "         ORD_ORDEN, "
			+ "         WKF_WORKFLOW "
			+ "    WHERE "
			+ "    (    DIS_ORD_NORM.id_ord_insp = :lInspecIdOrden "
			+ "        AND NOT (    WKF_WORKFLOW.ID_DESCRIPTOR = 'WFORDENNORMALIZACION' "
			+ "                    AND ( WKF_WORKFLOW.ID_STATE = 'SAnulada' OR WKF_WORKFLOW.ID_STATE = 'SFinalizada' OR WKF_WORKFLOW.ID_STATE = 'Anulada' ) " // R.I. Corrección 25/10/2023
			+ "                ) "
			+ "    ) "
			+ "    AND DIS_ORD_NORM.ID_ORDEN = ORD_ORDEN.ID "
			+ "    AND ORD_ORDEN.ID_WORKFLOW = WKF_WORKFLOW.ID", nativeQuery = true)
	public Long estadoActualOrden(@Param("lInspecIdOrden") Long lInspecIdOrden);
	//FIN - REQ # 1 - JEGALARZA
	
	@Query(value = "SELECT WW.ID_STATE FROM SCHSCOM.ORD_ORDEN OO\r\n"
	        + "INNER JOIN SCHSCOM.WKF_WORKFLOW WW ON OO.ID_WORKFLOW = WW.ID\r\n"
	        + "WHERE OO.ID = :idOrden", nativeQuery = true)
	String estado(@Param("idOrden") Long idOrden);
	
	//INICIO - REQ # 4 - JEGALARZA
	@Query(value = "select wf.id_state id "
			+ "from ord_orden oo , "
			+ "dis_ord_insp doi, "
			+ "dis_seg_insp dsi, "
			+ "WKF_WORKFLOW wf "
			+ "where oo.id = doi.id_orden "
			+ "and doi.id_seginsp = dsi.id  "
			+ "and dsi.id_wkf = wf.id "
			+ "and oo.id = :idinsp", nativeQuery = true)
	String estadoTrans(@Param("idinsp") Long idOrden);
	//FIN - REQ # 4 - JEGALARZA
}
