package com.enel.scom.api.gestion.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.scom.api.gestion.model.OrmTipoTarea;
import com.enel.scom.api.gestion.repository.OrmTipoTareaRepository;
import com.enel.scom.api.gestion.service.OrmTipoTareaService;

@Service
public class OrmTipoTareaServiceImpl implements OrmTipoTareaService{

    @Autowired
    private OrmTipoTareaRepository ormTipoTareaRepository;

    @Override
    public List<OrmTipoTarea> getTipos() {
        return ormTipoTareaRepository.getTipos();
    }

    
}
