package com.enel.scom.api.gestion.service;

import java.util.List;

import com.enel.scom.api.gestion.model.MotivoEntity;

public interface MotivoService {
    List<MotivoEntity> getActivos();

    List<MotivoEntity> getContraste();
}
