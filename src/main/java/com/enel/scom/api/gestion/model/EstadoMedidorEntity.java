package com.enel.scom.api.gestion.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity(name = "med_est_componente")
@Table(name = "med_est_componente", schema = "schscom")
@Data
public class EstadoMedidorEntity implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "cod_est_componente")
	private String codigo;

	@Column(name = "des_est_componente")
	private String descripcion;

	@Column(name = "cod_interno")
	private String codInterno;
	
	@Column
	private Boolean activo;
}
