package com.enel.scom.api.gestion.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import com.enel.scom.api.gestion.dto.request.ActualizaDatosCNR_RequestDTO;
import com.enel.scom.api.gestion.dto.request.OrdenInspeccionRequestDTO;
import com.enel.scom.api.gestion.dto.response.ActualizaOrdIspecCNRResponseDTO;
import com.enel.scom.api.gestion.dto.response.HistorialActualizacionesResponseDTO;
import com.enel.scom.api.gestion.dto.response.OrdenDataResponseDTO;
import com.enel.scom.api.gestion.dto.response.OrdenInspeccionActualizaDatosCNRResponseDTO;
import com.enel.scom.api.gestion.exception.NroServicioNotFoundException;
import com.enel.scom.api.gestion.model.OrdenEntity;

public interface OrdenInspeccionService {
	OrdenEntity storeUpdate(OrdenInspeccionRequestDTO ordenInspeccionDTO) throws NroServicioNotFoundException;
	OrdenDataResponseDTO getId(Long id);

	Page<OrdenInspeccionActualizaDatosCNRResponseDTO> findOrdenInspActualizarPaginacion(Pageable paging,
			Long numSuministro, Long numNotificacion, Long idOrden, Long codExpedienteCNR) throws Exception;
	
	
	ActualizaOrdIspecCNRResponseDTO actualizarDatosOrdenInspeccionCNR(String pRequestId, 
			ActualizaDatosCNR_RequestDTO dto) throws Exception;
	

	Page<HistorialActualizacionesResponseDTO> findHistorialActualizacionesPaginacion(Pageable paging,
			Long numSuministro, Long numNotificacion) throws Exception;
	
}
