package com.enel.scom.api.gestion.model;

import java.io.Serializable;

import javax.persistence.*;

import lombok.Data;

@Entity
@Table(name = "rol", schema = "schscom")
@Data
public class RolEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(length = 60)
    private String nombre;
    
    @Column
    private String descripcion;
    
    @Column(name="param_nombre")
    private String paramNombre;
    
    @Column
    private String activo;

}
