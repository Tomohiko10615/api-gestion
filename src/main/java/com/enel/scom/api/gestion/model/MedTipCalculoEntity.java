package com.enel.scom.api.gestion.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Entity(name = "med_tip_calculo")
@Data
public class MedTipCalculoEntity implements Serializable{
	
private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "cod_tip_calculo")
	private String codTipCalculo;
	
	@Column(name = "des_tip_calculo")
	private String desTipCalculo;
	
	@Column(name = "activo")
	private String activo;

	@Column(name = "cod_interno")
	private String codInterno;

}
