package com.enel.scom.api.gestion.util;

import java.text.NumberFormat;
import java.util.Date;
import java.util.stream.Stream;
import org.codehaus.jackson.map.ObjectMapper;

public class Funciones {
      /**
     * Obtien los datos de memoria durante el tiempo de ejecucion Objetivo: generar
     * datos para monitoreo en caso de error por falta de memoria virtual java
     */
    public static String logMemoryJVM() {
        
        Runtime runtime = Runtime.getRuntime();

 

        NumberFormat format = NumberFormat.getInstance();

 

        long maxMemory = runtime.maxMemory();
        long allocatedMemory = runtime.totalMemory();
        long freeMemory = runtime.freeMemory();
        long memoria_usada = runtime.totalMemory() - runtime.freeMemory();
        long memoria_libre_total = runtime.maxMemory() - memoria_usada;

 

        
        String printLine = "";
        printLine=printLine+"[Uso de memoria JVM]:" + runtime.freeMemory()+"\tmemoria libre asignada actual: " + format.format(freeMemory / 1024) + "mb";
        printLine=printLine+"\tMemoria asignada: " + format.format(allocatedMemory / 1024) + "mb";
        printLine=printLine+"\tMemoria total designada(-Xmx): " + format.format(maxMemory / 1024) + "mb";
        printLine=printLine+"\tmemoria total asignada: " + format.format((freeMemory + (maxMemory - allocatedMemory)) / 1024)
                + "mb";
        printLine=printLine+"\tmemoria usada: " + format.format(memoria_usada / 1024) + "mb";
        printLine=printLine+"\tmemoria libre total: " + format.format(memoria_libre_total / 1024) + "mb";

        // log.info(printLine); // o System.out.println(printLine)

        return printLine;

    }

	public static String objectToJsonString(Object obj) {
		
		
		ObjectMapper mapper = new ObjectMapper();
		String jsonString = "";
		mapper = new ObjectMapper();
		try {
			if (obj == null) {
				jsonString= "null";
			}
			else {
				jsonString = mapper.writeValueAsString(obj);	
			}
			 
			 
		} catch (Exception e) {
			jsonString = e.toString();
		}
		return jsonString;
	}
	

	
	public static boolean isNumeric(String strNum) {
	    if (strNum == null) {
	        return false;
	    }
	    try {
	        Long d = Long.parseLong(strNum);
	    } catch (NumberFormatException nfe) {
	        return false;
	    }
	    return true;
	}
	
	public  static String getDuracion(Date dateINI, Date dateFIN) {
		   if(dateINI==null)
		   {
			   return "Hora de Inicio Nulo";
		   }
		   else if(dateFIN==null)
		   {
			   return "Hora Fin es Nulo";
		   }
		   else {
			   String out = "";
		      
		        long diff = dateFIN.getTime() - dateINI.getTime();
		        long diffms = diff / 1L % 60L;
		        long diffSec = diff / 1000L % 60L;
		        long diffMin = diff / 60000L % 60L;
		        long diffHour = diff / 3600000L % 24L;
		        long diffDays = diff / 86400000L;

		        //out = out + " Dias: " + diffDays;
		        out = out + "Hor.: " + diffHour;
		        out = out + ", min.: " + diffMin;
		        out = out + ", seg.: " + diffSec;
		        out = out + ", mseg.: " + diffms;
		         return out;
		   }
	      
	    }
}
