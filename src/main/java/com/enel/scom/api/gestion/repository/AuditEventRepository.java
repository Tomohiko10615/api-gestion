package com.enel.scom.api.gestion.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.model.AuditEventEntity;

@Repository
public interface AuditEventRepository extends JpaRepository<AuditEventEntity, Long> {
	
	 @Query(value="SELECT nextval('sqauditevent')", nativeQuery=true)
	 public Long generarIdAuditEvent();
}
