package com.enel.scom.api.gestion.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;

@Entity(name="ord_buzon")
@Data
public class BuzonEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column
	private Long id;
	 
}
