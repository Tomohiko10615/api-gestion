package com.enel.scom.api.gestion.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.model.MedTipComponenteEntity;

@Repository
public interface MedTipComponenteRepository extends JpaRepository<MedTipComponenteEntity, Long>{
	
	@Query(value = "SELECT * FROM med_tip_componente WHERE id = :id", nativeQuery = true)
	MedTipComponenteEntity getTipById(@Param("id") Long id);
}
