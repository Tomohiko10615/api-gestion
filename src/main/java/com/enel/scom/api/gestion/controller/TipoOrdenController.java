package com.enel.scom.api.gestion.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.enel.scom.api.gestion.dto.response.TipoOrdenResponseDTO;
import com.enel.scom.api.gestion.service.TipoOrdenService;

@RestController
@RequestMapping("/api/tipo-orden")
public class TipoOrdenController {
	
	@Autowired
	TipoOrdenService tipoOrdenService;
	
	@GetMapping("/proceso/{codTipoProceso}")
	public ResponseEntity<List<TipoOrdenResponseDTO>> getModelo(@PathVariable("codTipoProceso") String codTipoProceso) {
		List<TipoOrdenResponseDTO> tiposOrden = tipoOrdenService.obtenerTipoOrdenByCodProceso(codTipoProceso);
		return new ResponseEntity<List<TipoOrdenResponseDTO>>(tiposOrden, HttpStatus.OK);
	}

}
