package com.enel.scom.api.gestion.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.enel.scom.api.gestion.model.PerfilRol;

@Repository
public interface PerfilRolRepository extends JpaRepository<PerfilRol, Long> {
	
	@Query(value = 
			"select * from schscom.perfil_rol pr " +
			"where pr.id_perfil =:idPerfil and pr.activo='S'", nativeQuery = true)
	List<PerfilRol> obtenerRolesPorIdPerfil(@Param("idPerfil") Long idPerfil);
}
