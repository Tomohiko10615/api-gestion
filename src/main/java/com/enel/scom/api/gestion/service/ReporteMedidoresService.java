package com.enel.scom.api.gestion.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.enel.scom.api.gestion.dto.response.ReporteGraficoDTO;
import com.enel.scom.api.gestion.dto.response.ReporteMedidoresPaginacionResponseDTO;

public interface ReporteMedidoresService {
	
	/**
	 * Metodo para obtner Reporte de Medidores segun filtros
	 * @param paging
	 * @param fechaInicio fecha de creacion del medidor
	 * @param fechaFin fecha de creacion del medidor
	 * @param idEstado estado actual del medidor
	 * @param tipoUbicacion tipo de ubicacion del medidor
	 * @param nroCuenta nro de cuenta solo aplicable para el medidor de tipo 'ServicioElectrico'
	 * @return Listado de registros de medidores segun los filtros ingresados
	 */
	Page<ReporteMedidoresPaginacionResponseDTO> obtenerReporteMedidoresPaginacion(Pageable paging, String fechaInicio, String fechaFin, Long idEstado, String tipoUbicacion, String nroCuenta, String nroComponente);
	
	// REQ # 8 - JEGALARZA
	Page<ReporteMedidoresPaginacionResponseDTO> obtenerReporteMedidoresPaginacion_V2(Pageable paging, String fechaInicio, String fechaFin, Long idEstado, String tipoUbicacion, String nroCuenta, String nroComponente, String marcaMedidor, String modeloMedidor);
	
	/**
	 * Metodo para obtener Reporte Gráfico de Medidores
	 * @param fechaInicio fecha de creacion del medidor
	 * @param fechaFin fecha de creacion del medidor
	 * @param idEstado estado actual del medidor
	 * @param tipoUbicacion tipo de ubicacion del medidor
	 * @param nroCuenta nro de cuenta solo aplicable para el medidor de tipo 'ServicioElectrico'
	 * @return Listado de cantidad de medidores por estado segun filtros ingresados
	 */
	List<ReporteGraficoDTO> obtenerReporteMedidoresCantidadXestado(String fechaInicio, String fechaFin, Long idEstado, String tipoUbicacion, String nroCuenta);
	

}
