package com.enel.scom.api.gestion.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class OrdenTareaResponseDTO {

	/*
	public OrdenTareaResponseDTO(Long id, String anormalidad, String tarea, String codTarea, String codAnormalidad, String tareaEstado, Long idTarea, Long idAnormalidad) {
		this.id = id;
		this.anormalidad = anormalidad;
		this.tarea = tarea;
		this.codTarea = codTarea;
		this.tareaEstado = tareaEstado;
		this.idTarea = idTarea;
		this.idAnormalidad = idAnormalidad;
		this.codAnormalidad = codAnormalidad;
	}
	public OrdenTareaResponseDTO() {

	}
	*/

	private Long id;
	private Long idTarea;
	private Long idAnormalidad;
	private String anormalidad;
	private String tarea;
	private String codTarea;
	private String codAnormalidad;
	private String tareaEstado;
	//R.I. CORRECION 13/09/2023 INICIO
	// Se eliminó los constructores y en su lugar se está usando lombok
	private String predeterminada;
	private String ejecutada;
	//R.I. CORRECION 13/09/2023 FIN
}
