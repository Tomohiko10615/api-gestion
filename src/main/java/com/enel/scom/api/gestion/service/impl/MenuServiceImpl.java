package com.enel.scom.api.gestion.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.scom.api.gestion.dto.MenuDTO;
import com.enel.scom.api.gestion.repository.MenuRepository;
import com.enel.scom.api.gestion.service.MenuService;

@Service
public class MenuServiceImpl implements MenuService {
	
	@Autowired
	MenuRepository menuRepository;

	@Override
	public List<MenuDTO> getMenuByIdPerfil(Long idPerfil) {
		
		List<Object[]> itemMenu = menuRepository.findMenuPadreByIdPerfil(idPerfil);
		List<MenuDTO> dtos= new ArrayList<>();
		
		for(Object[] row : itemMenu) { 
			MenuDTO dto = new MenuDTO();
			dto.setId(Long.valueOf(row[0].toString()));
			dto.setNombre(row[1] == null ? null : row[1].toString());
			dto.setDescripcion(row[2] == null ? null : row[2].toString());
			dto.setIcono(row[3] == null ? null : row[3].toString());
			dto.setUrl(row[4] == null ? null : row[4].toString());
			dto.setId_padre(row[5] == null ? null : Long.valueOf(row[5].toString()));
			dto.setParam_nombre(row[6] == null ? null : row[6].toString());
			dto.setNombre_modulo(row[7] == null ? null : row[7].toString());
			dto.setClase(row[8] == null ? null : row[8].toString());
			dto.setTitulo_grupo(row[9] == null ? null : Boolean.valueOf(row[9].toString()));
			
				List<Object[]> subItems = menuRepository.findSubMenuByidMenu(dto.getId(), idPerfil);
				List<MenuDTO> dtosItem= new ArrayList<>();
				for(Object[] rowItem : subItems) { 
					MenuDTO dtoItem = new MenuDTO();
					dtoItem.setId(Long.valueOf(rowItem[0].toString()));
					dtoItem.setNombre(rowItem[1] == null ? null : rowItem[1].toString());
					dtoItem.setDescripcion(rowItem[2] == null ? null : rowItem[2].toString());
					dtoItem.setIcono(rowItem[3] == null ? null : rowItem[3].toString());
					dtoItem.setUrl(rowItem[4] == null ? null : rowItem[4].toString());
					dtoItem.setId_padre(rowItem[5] == null ? null : Long.valueOf(rowItem[5].toString()));
					dtoItem.setParam_nombre(rowItem[6] == null ? null : rowItem[6].toString());
					dtoItem.setNombre_modulo(rowItem[7] == null ? null : rowItem[7].toString());
					dtoItem.setClase(rowItem[8] == null ? null : rowItem[8].toString());
					dtoItem.setTitulo_grupo(rowItem[9] == null ? null : Boolean.valueOf(rowItem[9].toString()));
					dtosItem.add(dtoItem);
					}
				dto.setSub_items(dtosItem);
			dtos.add(dto);
		}
		return dtos;
	}
}
