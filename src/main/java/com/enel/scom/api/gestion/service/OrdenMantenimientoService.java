package com.enel.scom.api.gestion.service;

import com.enel.scom.api.gestion.dto.request.OrdenMantenimientoRequestDTO;
import com.enel.scom.api.gestion.dto.response.OrdenDataResponseDTO;
import com.enel.scom.api.gestion.exception.NroServicioNotFoundException;
import com.enel.scom.api.gestion.model.OrdenEntity;

public interface OrdenMantenimientoService {
    OrdenEntity postStoreUpdate(OrdenMantenimientoRequestDTO requestDTO) throws NroServicioNotFoundException;
    OrdenDataResponseDTO getId(Long id);
}
