package com.enel.scom.api.gestion.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import lombok.Data;

@Entity(name = "dis_anormalidad")
@Data
public class AnormalidadEntity implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "cod_anormalidad")
	private String codigo;

	@Column(name = "des_anormalidad")
	private String descripcion;
	
	@Column(name = "gen_orden_norm")
	private String genOrdNorm;
	
	@Column(name = "gen_cnr")
	private String genCnr;
	
	@OneToOne
	@JoinColumn(name = "id_causal")
	private CausalEntity causal;
	
	@Column(length = 1)
	private String activo;
}
