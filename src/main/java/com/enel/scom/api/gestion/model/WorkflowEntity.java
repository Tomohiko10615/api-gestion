package com.enel.scom.api.gestion.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

import lombok.Data;

/**
 * 
 * @author ngbarrios
 * Tabla de manejo de estados para las ordenes de trabajo
 */
@Entity(name = "wkf_workflow")
@Data
public class WorkflowEntity implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column
	private Long id;
	
	@Column(name = "id_descriptor", length = 40)
	private String idDescriptor;
	
	@Column(length = 2)
	private Integer version;
	
	@Column(name = "id_state", length = 40)
	private String idState;
	
	@Column(name = "has_parent", length = 1)
	private String hasParent;
	
	@Column(name = "id_old_state", length = 40)
	private String idOldState;
	
	@Column(name = "parent_type", length = 250)
	private String parentType;
	
	@Column(name="fecha_estado")
	private Date fechaUltimoEstado;


}
