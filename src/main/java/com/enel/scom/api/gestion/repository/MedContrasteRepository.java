package com.enel.scom.api.gestion.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.model.MedContrasteEntity;

@Repository
public interface MedContrasteRepository extends JpaRepository<MedContrasteEntity, Long> {
	
	@Query(value = "SELECT * FROM med_contraste WHERE id_contraste = :idContraste", nativeQuery = true)
	MedContrasteEntity findIdContaste(@Param("idContraste") Long idContraste);
}
