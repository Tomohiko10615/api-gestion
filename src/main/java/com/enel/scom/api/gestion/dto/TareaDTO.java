// R.I. REQSCOM03 14/07/2023 INICIO
package com.enel.scom.api.gestion.dto;

public interface TareaDTO {
	String getDesTarea();
	String getPredeterminada();
	String getEjecutada();
}
//R.I. REQSCOM03 14/07/2023 FIN