package com.enel.scom.api.gestion.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.model.AnormalidadEntity;

@Repository
public interface AnormalidadRepository extends JpaRepository<AnormalidadEntity, Long>{
	@Query(value = "SELECT * FROM dis_anormalidad WHERE id = :id", nativeQuery = true)
	AnormalidadEntity findId(@Param("id") Long id);
	
	@Query(value = "SELECT da.id, da.cod_anormalidad, da.des_anormalidad, da.gen_cnr, da.gen_orden_norm, dc.des_causal FROM schscom.dis_anormalidad da "
					+ "INNER JOIN schscom.dis_causal dc on da.id_causal = dc.id "
					+ "WHERE da.activo = 'S' ORDER BY des_anormalidad ASC", nativeQuery = true)
	List<Object[]> getAll();
}
