package com.enel.scom.api.gestion.oracle.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.oracle.model.SrvAlimentadorEntity;
import com.enel.scom.api.gestion.oracle.model.SrvSetEntity;

@Repository
public interface SrvAlimentadorRepository extends JpaRepository<SrvAlimentadorEntity, Long> {
	
	/**
	 * Query para realizar autocomplete en Alimentador CADENA ELECTRICA
	 * @param desAlimentador
	 * @param idSet
	 * @return
	 */
	
	@Query(value="SELECT al.id_alimentador, al.cod_alimentador FROM srv_alimentador al where al.id_set=:idSet and al.des_alimentador like %:desAlimentador% order by al.cod_alimentador asc", nativeQuery = true)
	public List<Object[]> buscarAlimentadorServicioElectrico(@Param("desAlimentador") String desAlimentador, @Param("idSet") Long idSet);
	
	@Query(value="SELECT * FROM srv_alimentador al where al.id_alimentador=:id", nativeQuery = true)
	public SrvAlimentadorEntity obtenerAlimentadorServicioElectricoPorId(@Param("id") Long id);
}
