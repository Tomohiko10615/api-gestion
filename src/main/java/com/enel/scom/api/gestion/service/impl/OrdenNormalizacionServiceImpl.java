package com.enel.scom.api.gestion.service.impl;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.scom.api.gestion.dto.LecturasDTO;
import com.enel.scom.api.gestion.dto.MedidorDTO;
import com.enel.scom.api.gestion.dto.ObservacionDTO;
import com.enel.scom.api.gestion.dto.TareaDTO;
import com.enel.scom.api.gestion.dto.XmlDTO;
import com.enel.scom.api.gestion.dto.XmlGestionPerdidasDTO;
import com.enel.scom.api.gestion.dto.request.OrdenNormalizacionRequestDTO;
import com.enel.scom.api.gestion.dto.response.InspeccionNotificadaResponseDTO;
import com.enel.scom.api.gestion.dto.response.OrdenAnormalidadResponseDTO;
import com.enel.scom.api.gestion.dto.response.OrdenDataResponseDTO;
import com.enel.scom.api.gestion.dto.response.OrdenTareaResponseDTO;
import com.enel.scom.api.gestion.dto.xml.Lectura;
import com.enel.scom.api.gestion.dto.xml.Medidor;
import com.enel.scom.api.gestion.dto.xml.Medidores;
import com.enel.scom.api.gestion.exception.NroServicioNotFoundException;
import com.enel.scom.api.gestion.model.AnormalidadEntity;
import com.enel.scom.api.gestion.model.ClienteEntity;
import com.enel.scom.api.gestion.model.ObservacionEntity;
import com.enel.scom.api.gestion.model.OrdenEntity;
import com.enel.scom.api.gestion.model.OrdenInspeccionEntity;
import com.enel.scom.api.gestion.model.OrdenNormalizacionEntity;
import com.enel.scom.api.gestion.model.OrdenTareaEntity;
import com.enel.scom.api.gestion.model.TareaEntity;
import com.enel.scom.api.gestion.model.TipoOrdenEntity;
import com.enel.scom.api.gestion.model.UsuarioEntity;
import com.enel.scom.api.gestion.model.WorkflowEntity;
import com.enel.scom.api.gestion.oracle.repository.MedMagnitudRepository;
import com.enel.scom.api.gestion.repository.AnormalidadRepository;
import com.enel.scom.api.gestion.repository.ClienteRepository;
import com.enel.scom.api.gestion.repository.MedidorRepository;
import com.enel.scom.api.gestion.repository.MotivoRepository;
import com.enel.scom.api.gestion.repository.ObservacionRepository;
import com.enel.scom.api.gestion.repository.OrdenInspeccionRepository;
import com.enel.scom.api.gestion.repository.OrdenNormalizacionRepository;
import com.enel.scom.api.gestion.repository.OrdenRepository;
import com.enel.scom.api.gestion.repository.OrdenTareaRepository;
import com.enel.scom.api.gestion.repository.ParametroRepository;
import com.enel.scom.api.gestion.repository.TareaRepository;
import com.enel.scom.api.gestion.repository.TipoOrdenRepository;
import com.enel.scom.api.gestion.repository.TransferRepository;
import com.enel.scom.api.gestion.repository.UsuarioRepository;
import com.enel.scom.api.gestion.repository.WorkflowRepository;
import com.enel.scom.api.gestion.service.OrdenNormalizacionService;

import lombok.extern.slf4j.Slf4j;

/**
 * Servicio principal donde se realiza el registro, actualización y detalle de
 * la normalización
 * 
 * @author Gilmar Moreno
 * @version 1.0
 */
@Service
@Slf4j
public class OrdenNormalizacionServiceImpl implements OrdenNormalizacionService {

	@Autowired
	OrdenRepository ordenRepository;

	@Autowired
	UsuarioRepository usuarioRepository;

	@Autowired
	MotivoRepository motivoRepository;

	@Autowired
	ClienteRepository clienteRepository;

	@Autowired
	OrdenInspeccionRepository ordenInspeccionRepository;

	@Autowired
	OrdenNormalizacionRepository ordenNormalizacionRepository;

	@Autowired
	ObservacionRepository observacionRepository;

	@Autowired
	ParametroRepository parametroRepository;

	@Autowired
	OrdenTareaRepository ordenTareaRepository;

	@Autowired
	TareaRepository tareaRepository;

	@Autowired
	AnormalidadRepository anormalidadRepository;

	@Autowired
	TipoOrdenRepository tipoOrdenRepository;

	@Autowired
	WorkflowRepository wkfWorkflowRepository;

	// R.I. REQSCOM03 09/10/2023 INICIO
	@Autowired
	MedMagnitudRepository medMagnitudRepository;

	@Autowired
	MedidorRepository medidorRepository;
	// R.I. REQSCOM03 09/10/2023 FIN

	// R.I. REQSCOM06 12/07/2023 INICIO
	@Autowired
	TransferRepository transferRepository;
	// R.I. REQSCOM06 12/07/2023 FIN

	@Override

	public OrdenEntity storeUpdate(OrdenNormalizacionRequestDTO ordenNormalizacionDTO)
			throws NroServicioNotFoundException {
		/**
		 * Para crear una orden de normalización no debe tener una orden abierta.
		 */

		log.info("ordenNormalizacionDTO: {}", ordenNormalizacionDTO.toString());
		TipoOrdenEntity tipoOrden = tipoOrdenRepository.getNormalizacion();
		log.info("tipoOrden: {}", tipoOrden.getId());
		ClienteEntity cliente = clienteRepository.findByNroServicio(ordenNormalizacionDTO.getNroServicio());
		log.info(cliente.toString());

		// INICIO - REQ # 1 - JEGALARZA
		// WorkflowEntity workflow =
		// wkfWorkflowRepository.estadoActualOrden(cliente.getIdServicio(),
		// tipoOrden.getId());
		Long id_count = wkfWorkflowRepository.estadoActualOrden(ordenNormalizacionDTO.getIdOrdenInspeccion());
		OrdenEntity ordenResponseFinal = new OrdenEntity();

		log.info("idOrdenNormalizacion inside storeUpdate: {}", ordenNormalizacionDTO.getIdOrdenNormalizacion());
		/*
		 * if(workflow != null) { log.info("workflow: {}", workflow.toString());
		 * log.info(workflow.getIdState()); } else { log.info("workflow es null"); }
		 */

		if (id_count >= 1) {
			log.info("id_count: {}", id_count.toString());
		}

		if (ordenNormalizacionDTO.getIdOrdenNormalizacion() == 0) {
			log.info("Actualizando...");
			// if(workflow != null) {
			if (id_count >= 1) {
				throw new NroServicioNotFoundException(
						"No puede crear la orden por que tiene una normalización abierta.");
			} else {
				// if(workflow.getIdState().equals("SFinalizada") ||
				// workflow.getIdState().equals("SAnulada") ||
				// workflow.getIdState().equals("Anulada") ||
				// workflow.getIdState().equals("Finalizada")) {
				// FIN - REQ # 1 - JEGALARZA
				try {
					log.info("Estados correctos");
					log.info("Obteniendo datos");
					
					Boolean ordenExiste = ordenRepository.existsById(ordenNormalizacionDTO.getIdOrden());
					UsuarioEntity usuario = usuarioRepository.getUsuarioById(ordenNormalizacionDTO.getIdUsuario());

					OrdenInspeccionEntity ordenInspeccion = ordenInspeccionRepository
							.getIdOrden(ordenNormalizacionDTO.getIdOrdenInspeccion());
					OrdenEntity ordenInspeccion2 = ordenRepository.findId(ordenInspeccion.getIdOrden());

					int existeOrdenTarea = ordenTareaRepository.existeOrden(ordenNormalizacionDTO.getIdOrden());

					log.info("Se obtuvo los datos");
					log.info(ordenExiste.toString());
					log.info(usuario.toString());

					log.info(ordenInspeccion.toString());
					log.info(ordenInspeccion2.toString());

					WorkflowEntity wkfResponse = new WorkflowEntity();
					if (Boolean.FALSE.equals(ordenExiste)) {
						log.info("Actualizando workflow");
						WorkflowEntity wkf = new WorkflowEntity();
						wkf.setId(wkfWorkflowRepository.generaId());
						wkf.setIdDescriptor("WFORDENNORMALIZACION");
						wkf.setIdOldState("Emitida");
						wkf.setIdState("SEmitida");
						wkf.setParentType("com.enel.scom.api.gestion.service.impl.OrdenNormalizacionImpl");
						wkf.setFechaUltimoEstado(new Date());
						wkf.setVersion(0);
						wkfResponse = wkfWorkflowRepository.save(wkf);
					}

					OrdenEntity orden = (Boolean.FALSE.equals(ordenExiste)) ? new OrdenEntity()
							: ordenRepository.findId(ordenNormalizacionDTO.getIdOrden());

					if (Boolean.TRUE.equals(ordenExiste)) {
						orden.setFechaModif(new Date());
						orden.setUsuarioModif(usuario);
					} else {
						orden.setUsuarioCrea(usuario);
						orden.setUsuarioRegistro(usuario);
						orden.setFechaRegistro(new Date());
						orden.setFechaCreacion(new Date());
						orden.setActivo(true);
						orden.setId(ordenRepository.generaId());
						orden.setIdServicio(cliente.getIdServicio());
						orden.setTipoOrden(tipoOrden);
						orden.setWorkflow(wkfResponse);
						orden.setDiscriminador("NORMALIZACION");
						orden.setNroOrden(Long.toString(ordenNormalizacionRepository.generarNumeroOrden()));
						orden.setMotivo(ordenInspeccion2.getMotivo());
					}

					log.info("Actualizando orden");
					OrdenEntity ordenResponse = ordenRepository.save(orden);

					OrdenNormalizacionEntity ordenNormalizacion = (Boolean.FALSE.equals(ordenExiste))
							? new OrdenNormalizacionEntity()
							: ordenNormalizacionRepository.findId(ordenNormalizacionDTO.getIdOrden());
					ordenNormalizacion.setInspeccion(ordenInspeccion);
					ordenNormalizacion.setSubtipoOrd(ordenNormalizacionDTO.getSubtipo());
					ordenNormalizacion.setIdEjecutorAsig(ordenInspeccion.getIdEjecutor());
					if (Boolean.FALSE.equals(ordenExiste)) {
						ordenNormalizacion.setIdOrden(ordenResponse.getId());
						ordenNormalizacion.setFecNormalizacion(new Date());
					}
					log.info("Actualizando normalizacion");
					ordenNormalizacionRepository.save(ordenNormalizacion);
					/**
					 * Elimino todas las tareas registradas cuando se va actualizar las nuevas
					 * tareas
					 */
					if (existeOrdenTarea > 0) {
						log.info("Hay tareas, eliminando...");
						//ordenTareaRepository.deleteByIdOrden(ordenNormalizacionDTO.getIdOrden());
						ordenTareaRepository.deleteByIdOrden(ordenNormalizacion.getIdOrden());
					}

					/**
					 * Se registra las nuevas tareas
					 */
					log.info("Registrando nuevas tareas");

					log.info(null);
					ordenNormalizacionDTO.getIrregularidades().stream().forEach(k -> {
						AnormalidadEntity anormalidad = anormalidadRepository.findId(k.getIdAnormalidad());
						TareaEntity tarea = tareaRepository.findId(k.getIdTarea());
						OrdenEntity ordenEntity = ordenRepository.findId(ordenNormalizacion.getIdOrden());

						OrdenTareaEntity ordenTarea = new OrdenTareaEntity();
						ordenTarea.setId(ordenTareaRepository.generaId());
						ordenTarea.setAnormalidad(anormalidad);
						ordenTarea.setOrden(ordenEntity);
						ordenTarea.setTarea(tarea);
						// R.I. Corrección 25/10/2023 INICIO
						ordenTarea.setEjecutado("N");
						ordenTarea.setTareaEstado(k.getTareaEstado());
						// R.I. Corrección 25/10/2023 FIN
						log.info("Tarea a grabar: {}", ordenTarea.toString());
						ordenTareaRepository.save(ordenTarea);
					});

					log.info("Actualizando observacion");
					if (ordenNormalizacionDTO.getIdOrden() == 0) {
						ObservacionEntity observacion = new ObservacionEntity();
						observacion = new ObservacionEntity();
						observacion.setIdOrden(ordenResponse.getId());
						observacion.setUsuario(usuario);
						observacion.setStateName("CREADA");
						observacion.setDiscriminator("ObservacionOrden");
						observacion.setFechaObservacion(new Date());
						observacion.setIdObservacion(observacionRepository.generaId());
						observacion.setTexto(ordenNormalizacionDTO.getObservaciones());

						observacionRepository.save(observacion);
					} else {
						ObservacionEntity observacion = (observacionRepository
								.existeIdOrden(ordenNormalizacionDTO.getIdOrden()) == 0) ? new ObservacionEntity()
										: observacionRepository.findIdOrden(ordenNormalizacionDTO.getIdOrden());

						if (observacionRepository.existeIdOrden(ordenNormalizacionDTO.getIdOrden()) == 0) {
							observacion.setIdOrden(ordenNormalizacionDTO.getIdOrden());
							observacion.setUsuario(usuario);
							observacion.setStateName("CREADA");
							observacion.setDiscriminator("ObservacionOrden");
							observacion.setFechaObservacion(new Date());
							observacion.setIdObservacion(observacionRepository.generaId());
						}
						
						observacion.setTexto(ordenNormalizacionDTO.getObservaciones());

						observacionRepository.save(observacion);
					}
					log.info("Fin de actualizacion");
					ordenResponseFinal = ordenResponse;
				} catch (Exception e) {
					log.error(e.getMessage());
					throw e;
				}
				/*
				 * } else { throw new
				 * NroServicioNotFoundException("No puede crear la orden por que tiene una normalización abierta."
				 * ); }
				 */
			}
		}

		return ordenResponseFinal;
	}

	/**
	 * Se obtiene la orden de normalización para editar y mostrar el detalle.
	 * 
	 * @param id
	 */
	@Override
	public OrdenDataResponseDTO getId(Long id) {
		log.info("OrdenDataResponseDTO getId Inicio: id={}", id);
		OrdenNormalizacionEntity normalizacion = ordenNormalizacionRepository.findId(id);
		// List<Object[]> ordenInspeccion =
		// ordenNormalizacionRepository.findIdOrden(normalizacion.getInspeccion().getId());
		List<Object[]> ordenInspeccion = ordenNormalizacionRepository.findIdOrden(normalizacion.getIdOrden()); // CDiaz
		List<Object[]> detalles = ordenTareaRepository.findByIdOrden(normalizacion.getIdOrden());
		List<Object[]> anormalidades = ordenTareaRepository.findByIdOrdenAndAnormalidades(normalizacion.getIdOrden());
		List<Object[]> inspeccionNotificada = ordenNormalizacionRepository
				.getInspeccionAsociada(normalizacion.getIdOrden()); // CHR 19-06-23
		List<InspeccionNotificadaResponseDTO> inspeccionDTO = new ArrayList<>(); // CHR 19-06-23
		List<OrdenTareaResponseDTO> detalleDTO = new ArrayList<>();
		List<OrdenAnormalidadResponseDTO> anormalidadDTO = new ArrayList<>();
		String estadoWkf = wkfWorkflowRepository.estado(normalizacion.getIdOrden());

		OrdenDataResponseDTO ordenDTO = new OrdenDataResponseDTO();
		ordenInspeccion.stream().forEach(k -> {
			ordenDTO.setIdOrden(Long.valueOf(k[0].toString()));
			ordenDTO.setFechaCreacion((Date) k[1]);
			ordenDTO.setFechaFinalizacion((Date) k[2]);
			ordenDTO.setNroOrden((k[3] == null) ? "" : k[3].toString());
			ordenDTO.setMotivo((k[4] == null) ? "" : k[4].toString());
			ordenDTO.setIdMotivo((k[5] == null) ? 0 : Long.valueOf(k[5].toString()));
			ordenDTO.setUsuarioCreacion((k[6] == null) ? "" : k[6].toString());
			ordenDTO.setNroServicio((k[7] == null) ? "" : k[7].toString());
			ordenDTO.setNroCuenta((k[8] == null) ? "" : k[8].toString());
			ordenDTO.setNombre((k[9] == null) ? "" : k[9].toString());
			ordenDTO.setApellidoPat((k[10] == null) ? "" : k[10].toString());
			ordenDTO.setApellidoMat((k[11] == null) ? "" : k[11].toString());
			ordenDTO.setDistrito((k[12] == null) ? "" : k[12].toString());
			ordenDTO.setRutaLectura((k[13] == null) ? "" : k[13].toString());
			ordenDTO.setObservacion((k[14] == null) ? "" : k[14].toString());
			ordenDTO.setIdOrdenInspeccion((k[15] == null) ? 0 : Long.valueOf(k[15].toString()));
			ordenDTO.setIdOrdenNormalizacion((k[16] == null) ? 0 : Long.valueOf(k[16].toString()));
			ordenDTO.setSubtipo((k[17] == null) ? "" : k[17].toString());
			ordenDTO.setDireccion((k[18] == null) ? "" : k[18].toString());
			ordenDTO.setNroNotificacion((k[19] == null) ? "" : k[19].toString());
			ordenDTO.setFechaNotificacion((k[20] == null) ? new Date() : (Date) k[20]);
			ordenDTO.setIdOrdenSc4j((k[21] == null) ? 0 : Long.valueOf(k[21].toString()));
			ordenDTO.setEstadoWkf((estadoWkf == null) ? "" : estadoWkf);

			detalles.stream().forEach(t -> {
				OrdenTareaResponseDTO ordenTarea = new OrdenTareaResponseDTO();
				ordenTarea.setId((t[0] == null) ? 0 : Long.valueOf(t[0].toString()));
				ordenTarea.setAnormalidad((t[1] == null) ? "" : t[1].toString());
				ordenTarea.setTarea((t[2] == null) ? "" : t[2].toString());
				ordenTarea.setCodTarea((t[3] == null) ? "" : t[3].toString());
				ordenTarea.setCodAnormalidad((t[4] == null) ? "" : t[4].toString());
				ordenTarea.setTareaEstado((t[5] == null) ? "" : t[5].toString());
				ordenTarea.setIdTarea((t[6] == null) ? 0 : Long.valueOf(t[6].toString()));
				ordenTarea.setIdAnormalidad((t[7] == null) ? 0 : Long.valueOf(t[7].toString()));
				detalleDTO.add(ordenTarea);
			});

			ordenDTO.setTareas(detalleDTO);

			anormalidades.stream().forEach(a -> {
				OrdenAnormalidadResponseDTO anormalidad = new OrdenAnormalidadResponseDTO();
				anormalidad.setId((a[0] == null) ? 0 : Long.valueOf(a[0].toString()));
				anormalidad.setCodigo((a[1] == null) ? "" : a[1].toString());
				anormalidad.setDescripcion((a[2] == null) ? "" : a[2].toString());
				anormalidad.setGeneraCNR((a[3] == null) ? "" : a[3].toString());
				anormalidad.setGeneraOrdenNormalizacion((a[4] == null) ? "" : a[4].toString());
				anormalidad.setCausal((a[5] == null) ? "" : a[5].toString());
				anormalidadDTO.add(anormalidad);
			});

			ordenDTO.setAnormalidades(anormalidadDTO);

			// CHR 19-06-23
			inspeccionNotificada.stream().forEach(i -> {
				InspeccionNotificadaResponseDTO inspeccion = new InspeccionNotificadaResponseDTO();

				inspeccion.setId(Long.valueOf(i[0].toString()));
				inspeccion.setNroOrden((i[1] == null) ? "" : i[1].toString());
				inspeccion.setFechaCreacion((i[2] == null) ? "" : i[2].toString());
				inspeccion.setContratista((i[3] == null) ? "" : i[3].toString());
				inspeccion.setTipoInspeccion((i[4] == null) ? "" : i[4].toString());
				inspeccionDTO.add(inspeccion);
			});

			ordenDTO.setOrdInspeccionNotificada(inspeccionDTO);
			// FIN CHR 19-06-23
		});

		// R.I. REQSCOM06 12/07/2023 INICIO
		try {
			String fechaCreacionOrden = ordenRepository.obtenerFechaCreacion(ordenDTO.getNroOrden());
			ordenDTO.setFechaCreacionOrden(fechaCreacionOrden);
		} catch (Exception e) {
			e.printStackTrace();
		}

		/*
		 * XmlDTO xml =
		 * transferRepository.obtenerXmlOrdenTrabajo(ordenDTO.getNroOrden(), 7L); if
		 * (xml != null) { /* Se comenta la observación del XML if (xml.getObservacion()
		 * != null) { ordenDTO.setObservacionTecnico(xml.getObservacion()); } else {
		 * ordenDTO.setObservacionTecnico(""); }
		 * ordenDTO.setFechaEjecucion(xml.getFechaEjecucion()); }
		 */
		// Se obtiene ahora de las tablas
		try {
			ObservacionDTO observacion = ordenRepository.obtenerObservacionTecnicoNORM(id);
			ordenDTO.setObservacionTecnico(observacion.getTexto());
			ordenDTO.setFechaEjecucion(observacion.getFechaObservacion());
		} catch (Exception e) {
			e.printStackTrace();
		}
		// R.I. REQSCOM06 12/07/2023 FIN

		// R.I. REQSCOM02 13/07/2023 INICIO

		// LecturasDTO lecturas =
		// transferRepository.obtenerLecturas(ordenDTO.getNroOrden());

		// R.I. REQSCOM02 13/07/2023 FIN

		// R.I. REQSCOM03 14/07/2023 INICIO
		// R.I. Se obtiene ahora de la base de datos 06/10/2023
		try {
			ordenDTO.setCodidoTdcInspeccionAsociada(ordenNormalizacionRepository.getCodidoTdcInspeccionAsociada(id));
		} catch (Exception e) {
			ordenDTO.setCodidoTdcInspeccionAsociada("");
			e.printStackTrace();
		}

		// R.I. Se requiere que se obtenga de la BBDD 16/10/2023 INICIO
		try {
			ordenDTO.setCodigoResultado(ordenNormalizacionRepository.getCodigoResultado(id));
		} catch (Exception e) {
			ordenDTO.setCodigoResultado("");
			e.printStackTrace();
		}
		// R.I. Se requiere que se obtenga de la BBDD 16/10/2023 FIN

		try {
			XmlGestionPerdidasDTO xmlGestionPerdidas = transferRepository
					.obtenerXmlGestionPerdidas(ordenDTO.getNroOrden());
			if (xmlGestionPerdidas != null) {
				/*
				 * if (xmlGestionPerdidas.getCodigoResultado() != null) {
				 * ordenDTO.setCodigoResultado(parametroRepository.getCodigoResultado(
				 * xmlGestionPerdidas.getCodigoResultado()));; }
				 */
				if (xmlGestionPerdidas.getSeCambioElMedidor() != null) {
					ordenDTO.setSeCambioElMedidor(xmlGestionPerdidas.getSeCambioElMedidor());
				}
				if (xmlGestionPerdidas.getCodigoContratista() != null) {
					ordenDTO.setCodigoContratista(xmlGestionPerdidas.getCodigoContratista());
				}
				if (xmlGestionPerdidas.getNroNotificacionInspeccion() != null) {
					ordenDTO.setNroNotificacionInspeccion(xmlGestionPerdidas.getNroNotificacionInspeccion());
				}
				/*
				 * if (xmlGestionPerdidas.getCodidoTdcInspeccionAsociada() != null) {
				 * ordenDTO.setCodidoTdcInspeccionAsociada(xmlGestionPerdidas.
				 * getCodidoTdcInspeccionAsociada()); }
				 */
				if (xmlGestionPerdidas.getTipoAnomalia() != null) {
					String descAnomalia = ordenNormalizacionRepository
							.obtenerDescAnomalia(xmlGestionPerdidas.getTipoAnomalia());
					ordenDTO.setTipoAnomalia(xmlGestionPerdidas.getTipoAnomalia() + " - " + descAnomalia);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		List<Long> idMagnitudList = ordenNormalizacionRepository.obtenerIdMagnitud(id);
		List<LecturasDTO> lecturasDTO = medMagnitudRepository.obtenerLecturas(idMagnitudList);

		Lectura lectura;
		Map<Long, List<Lectura>> lecturasPorComponente = new HashMap<>();

		for (LecturasDTO lecturas : lecturasDTO) {
			Long idComponente = lecturas.getIdComponente();
			lectura = new Lectura();
			lectura.setEstadoLeido(lecturas.getEstadoLeido());
			lectura.setFechaLectura(lecturas.getFechaLectura());
			lectura.setHorarioLectura(lecturas.getHorarioLectura());
			lectura.setTipoLectura(lecturas.getTipoLectura());
			lectura.setAccionMedidor(lecturas.getAccionMedidor());

			List<Lectura> lecturasComponente = lecturasPorComponente.get(idComponente);
			if (lecturasComponente == null) {
				lecturasComponente = new ArrayList<>();
				lecturasPorComponente.put(idComponente, lecturasComponente);
			}
			lecturasComponente.add(lectura);
		}

		List<Medidor> medidores = new ArrayList<>();
		for (Map.Entry<Long, List<Lectura>> entry : lecturasPorComponente.entrySet()) {
			Long idComponente = entry.getKey();
			List<Lectura> lecturasComponente = entry.getValue();

			Medidor medidor = new Medidor();
			MedidorDTO medidorDTO = medidorRepository.obtenerMedidor(idComponente);
			medidor.setNumeroMedidor(medidorDTO.getNumeroMedidor());
			medidor.setMarcaMedidor(medidorDTO.getMarcaMedidor());
			medidor.setModeloMedidor(medidorDTO.getModeloMedidor());
			medidor.setLecturas(lecturasComponente);
			medidor.setAccionMedidor(lecturasComponente.get(0).getAccionMedidor());
			medidores.add(medidor);
		}
		ordenDTO.setMedidores(medidores);

		// Ahora se setea el estado de la tarea de tablas
		for (OrdenTareaResponseDTO ordenTarea : ordenDTO.getTareas()) {
			ordenTarea.setEjecutada(ordenTarea.getTareaEstado());
		}
		// R.I. REQSCOM03 14/07/2023 FIN
		log.info("idOrdenNormalizacion en ordenDTO: {}", ordenDTO.getIdOrdenNormalizacion());

		return ordenDTO;
	}

}
