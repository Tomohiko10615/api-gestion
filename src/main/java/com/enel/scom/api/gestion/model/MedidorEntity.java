package com.enel.scom.api.gestion.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;

@Table(name = "med_componente", schema = "schscom")
@Entity(name = "med_componente")
@Data
public class MedidorEntity implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column
	private Long id;
	
	@Column(name = "nro_componente")
	private String nroComponente;
	
	@Column(name = "ano_fabricacion")
	private Long anoFabricacion;
	
	@Column(name = "serial_number")
	private String serie;
	
	@Column(name = "fecha_suministro")
	private Date fechaSuministro;
	
	@OneToOne
	@JoinColumn(name = "id_modelo")
	private ModeloEntity modelo;
	
	@OneToOne
	@JoinColumn(name = "id_ubicacion")
	private UbicacionEntity ubicacion;
	
	@OneToOne
	@JoinColumn(name = "id_est_componente")
	private EstadoMedidorEntity estadoMedidor;
	
	@Column
	private String activo;
	
	@OneToOne
	@JoinColumn(name = "id_usuario_registro")
	private UsuarioEntity usuarioRegistro;
	
	@OneToOne
	@JoinColumn(name = "id_usuario_modif")
	private UsuarioEntity usuarioModif;

	@Column(name = "fec_creacion")
	private Date fecCreacion;

	@Column(name = "fec_registro")
	private Date fecRegistro;
	
	@Column(name = "fec_modif")
	private Date fecModif;
	
	@Column(name = "type_ubicacion")
	private String typeUbicacion;
	
	@Column(name = "cod_tip_componente")
	private String codTipComponente;
	
	@Column(name = "id_dynamicobject")
	private Long idDyoObject;
	
	@Column(name = "reacondicionado")
	private String reacondicionado;
}
