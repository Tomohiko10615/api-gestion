package com.enel.scom.api.gestion.dto.request;

import java.util.List;

import lombok.Data;

@Data
public class OrdenContrasteRequestDTO {
	private Long idOrden;
	private Long idOrdenContraste;
	private Long idOrdenInspeccion;
	private Long idUsuario;
	private Long idMotivo;
	private String observaciones;
	private Long nroServicio;
	private List<AnormalidadesRequestDTO> irregularidades;
}
