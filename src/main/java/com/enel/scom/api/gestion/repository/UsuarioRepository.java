package com.enel.scom.api.gestion.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.model.UsuarioEntity;

import java.util.List;
import java.util.Optional;

@Repository
public interface UsuarioRepository extends JpaRepository<UsuarioEntity, Long> {
	Optional<UsuarioEntity> findByUsername(String username);
	 
    
    @Query(value = "select " + 
    	"usr.username, usr.cod_usuario, usr.nombre, usr.dni " + 
    	"from schscom.usuario usr " + 
    	"where ( :pUsername is null or upper(usr.username) like upper(:pUsername)) "
    	+ "and ( :pDni is null or upper(usr.dni) like upper(:pDni))"
    	+ "and ( :pCodigo is null or usr.cod_usuario = :pCodigo ) ", nativeQuery = true )
    List<Object[]>  verifyUserExistsUsernameDniCodigo(@Param("pUsername") String pUsername, @Param("pDni") String pDni,
    			@Param("pCodigo") Long pCodigo);
 
    
    Optional<UsuarioEntity> findById(Long id);

    @Query(value = "SELECT * FROM schscom.usuario WHERE id = :id", nativeQuery = true)
    UsuarioEntity getUsuarioById(@Param("id") Long id);
    
    
    @Query(value = "SELECT id, username, fecha_alta, fecha_baja, fecha_ultimo_ingreso, id_perfil, activo "
    		+ "FROM schscom.usuario "
    		+ "WHERE username = :username and activo = true", nativeQuery = true)
    UsuarioEntity getUsuarioByUserName(@Param("username") String username);
    
    @Query(value = "SELECT id, username FROM usuario WHERE activo = true", nativeQuery = true)
    List<Object[]> getActivos();
}
