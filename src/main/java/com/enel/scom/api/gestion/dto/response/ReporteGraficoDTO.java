package com.enel.scom.api.gestion.dto.response;

import lombok.Data;

@Data
public class ReporteGraficoDTO {
	
	private String name;
	private Long value;
	
	
	public ReporteGraficoDTO(String name, Long value) {
		this.name = name;
		this.value = value;
	}
	
	
}
