package com.enel.scom.api.gestion.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.model.OrmTareaEntity;

@Repository
public interface OrmTareaRepository extends JpaRepository<OrmTareaEntity, Long>{
    @Query(value = "select ot.id, ot.code, ot.description as tarea, ott.description, ot.req_cambio_medidor, ot.req_contraste_medidor, ot.estado, ot.valor from orm_tarea ot"
                    + " inner join orm_tipo_tarea ott on ot.id_tipo_tarea = ott.id"
                    + " where ot.estado = 'S' and ot.id_tipo_tarea = :idTipo"
                    + " order by tarea asc", nativeQuery = true)
    List<Object[]> getActivos(@Param("idTipo") Long idTipo);

    @Query(value = "select ot.id, ot.code, ot.description as tarea, ott.description, ot.req_cambio_medidor, ot.req_contraste_medidor, ot.estado, ot.valor from orm_tarea_ejec ote"
    + " inner join orm_tarea ot on ote.id_tarea = ot.id"
    + " inner join orm_tipo_tarea ott on ott.id = ot.id_tipo_tarea"
    + " where ote.id_orden = :idOrden", nativeQuery = true)
    List<Object[]> findByIdOrden(@Param("idOrden") Long idOrden);
}
