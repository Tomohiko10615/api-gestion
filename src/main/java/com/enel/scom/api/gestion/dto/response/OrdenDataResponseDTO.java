package com.enel.scom.api.gestion.dto.response;

import java.util.Date;
import java.util.List;

import org.springframework.lang.Nullable;

import com.enel.scom.api.gestion.dto.TareaDTO;
import com.enel.scom.api.gestion.dto.XmlCargasDTO;
import com.enel.scom.api.gestion.dto.xml.Medidor;

import lombok.Data;

@Data
public class OrdenDataResponseDTO {
	private Long idOrdenInspeccion;
	private Long idOrden;
	private Long idMotivo;
	private Long idProducto;
	private Long idJefeProducto;
	private Long idContratista;
	private Long idTipInspeccion;
	private Long idOrdenSc4j;
	
	private String nroServicio;
	private String nroCuenta;
	private String nombre;
	private String apellidoPat;
	private String apellidoMat;
	private String distrito;
	private String rutaLectura;
	private String direccion;
	
	private String nroOrden;
	private String entreCalles;
	private String motivo;
	private String producto;
	private String tipoInspeccion;
	private String jefeProducto;
	private String contratista;
	private String nroNotificacion;
	private String usuarioCreacion;
	private String denunciaRealizada;
	private String ejecutor;
	private Boolean informacionComplementaria;
	private Boolean autogenerado;
	private String observacion;
	private String documento;
	private String tipoAcometida;
	private String estadoWkf;
	private String estadoTrans; //REQ # 4 - JEGALARZA
	
	private Date fechaCreacion;
	private Date fechaNotificacion;
	@Nullable
	private Date fechaFinalizacion;
	
	private Long idOrdenNormalizacion;
	private Long idSubtipo;
	private String subtipo;
	
	private Long idOrdenContraste;

	private Long idTrabajo;
	private Long idPrioridad;
	private String tipoResponsable;
	private Long idResponsable;
	
	private List<OrdenTareaResponseDTO> tareas;
	private List<OrdenAnormalidadResponseDTO> anormalidades;
	private List<OrmTareaOrdenResponseDTO> ormTareas;
	private List<InspeccionNotificadaResponseDTO> ordInspeccionNotificada; // CHR 19-06-2023
	
	private MedContrasteDTO medContrasteDTO;

	// R.I. REQSCOM06 11/07/2023 INICIO
	private String fechaCreacionOrden;
	private String fechaEjecucion;
	private String observacionTecnico;
	// R.I. REQSCOM06 11/07/2023 FIN
	
	// R.I. REQSCOM03 14/07/2023 INICIO
	private String codigoResultado;
	private String seCambioElMedidor;
	private String codigoContratista;
	private String nroNotificacionInspeccion;
	private String codidoTdcInspeccionAsociada;
	private String tipoAnomalia;
	
	private List<Medidor> medidores;
	//private List<TareaDTO> tareasXml;
	// R.I. REQSCOM03 14/07/2023 FIN
	
	// R.I. CORRECCION 11/09/2023 INICIO
    private String nroInformeInspeccion;
    private String motivoInspeccion;
    private String identificadorSuministro;
    private String trabajoCoordinado;
    private String apruebaInspeccion;
    private String fechaNotificacionAvisoIntervencion;
    private String registroCumplimientoArt171RLCE;
    private String registroCumplimientoNumeral71NormaReintegrosRecuperos;
    private String evalGeneralConexionElectrica;
    private String evalSistemaMedicion;
    private String fechaPropuestaInicioAvisoIntervencion;
    private String horaPropuestaAvisoPrevioIntervencion;
    private String fechaRecepcionAvisoPrevioIntervencion;
    private String horaRecepcionAvisoPrevioIntervencion;
    private String pruebaVacioAD;
    private String stickerContraste;
    private String sinMicaMicaRota;
    private String cajaSinTapaTapaMalEstado;
    private String sinCerraduraCerraduraMalEstado;
    private String estadoConexion;
    private String clientePermiteInventarioCarga;
    private String cortadoArt90;
    private String estadoCajaPortaMedidor;
    private String tomaConReja;
    private String cajaConReja;
    List<XmlCargasDTO> cargas;
	// R.I. CORRECCION 11/09/2023 FIN
}
