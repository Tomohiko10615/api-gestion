package com.enel.scom.api.gestion.model;

import javax.persistence.*;
import java.util.Date;
import lombok.Data;

@Entity(name = "orm_orden")
@Data
public class OrdenMantenimientoEntity {

	@Id
    @Column(name = "id_orden")
    private Long idOrden;
	
	@OneToOne
    @JoinColumn(name = "id_trabajo")
    private TrabajoEntity trabajo;
	
	@OneToOne
	@JoinColumn(name = "id_prioridad")
    private PrioridadEntity prioridad;
	
    @Column(name = "fec_ultimo_reinicio")
    private Date fecUltimoReinicio;
    
    @Column(name = "nro_notificacion")
    private Long nroNotificacion;
    
    @Column(name = "id_contratista")
    private Long idContratista;
    
    @Column(name = "id_ejecutor")
    private Long idEjecutor;
    
    @Column(name = "atendido_seguro")
    private String atendidoSeguro;
    
    @Column(name = "id_notificado")
    private Long idNotificado;
    
    @Column(name = "id_paren_notificado")
    private Long idParenNotificado;
    
    @Column(name = "lugar_notificado")
    private String lugarNotificado;
    
    @Column(name = "ejecucion_ot")
    private String ejecucionOt;
    
    @Column(name = "id_dynamic_object")
    private Long idDynamicObject;
    
    @Column(name = "tipo_creacion")
    private String tipoCreacion;
    
    @OneToOne
    @JoinColumn(name = "id_tema")
    private TemaEntity tema;
    
    @Column(name = "por_contraste")
    private String porContraste;
    
    @Column(name = "iniciativa")
    private String iniciativa;
    
    @Column(name = "id_atencion")
    private Long idAtencion;
    
    @Column(name = "cod_proceso")
    private String codProceso;
    
    @Column(name = "nombre_no_cliente")
    private String nombreNoCliente;
    
    @Column(name = "direccion_no_cliente")
    private String direccionNoCliente;
    
    @Column(name = "distrito_no_cliente")
    private String distritoNoCliente;
    
    @Column(name = "por_terreno")
    private String porTerreno;
    
    @Column(name = "fecha_hora_ini_cita")
    private Date fechaHoraIniCita;
    
    @Column(name = "fecha_hora_fin_cita")
    private Date fechaHoraFinCita;
    
    @Column(name = "cod_cita")
    private String codCita;
    
    @Column(name = "id_direccion")
    private Long idDireccion;
    
    @Column(name = "latitud")
    private String latitud;
    
    @Column(name = "longitud")
    private String longitud;

    @Column(name = "origen")
    private String origen;
}
