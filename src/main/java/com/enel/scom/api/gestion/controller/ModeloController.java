package com.enel.scom.api.gestion.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.enel.scom.api.gestion.dto.request.ModeloRequestDTO;
import com.enel.scom.api.gestion.dto.response.MedidasResponseDTO;
import com.enel.scom.api.gestion.dto.response.ModeloDataResponseDTO;
import com.enel.scom.api.gestion.dto.response.ModeloMarcaResponseDTO;
import com.enel.scom.api.gestion.dto.response.ModeloPaginacionResponseDTO;
import com.enel.scom.api.gestion.dto.response.ModeloPaginacionResponseV2DTO;
import com.enel.scom.api.gestion.exception.NroServicioNotFoundException;
import com.enel.scom.api.gestion.model.ModeloEntity;
import com.enel.scom.api.gestion.service.ModeloService;

@RestController
@RequestMapping("/api/modelo")
public class ModeloController {
	
	@Autowired
	ModeloService modeloService;

	/*@GetMapping
	public ResponseEntity<Page<ModeloPaginacionResponseDTO>> getModeloSearch(
																			@RequestParam(required=false) String cod_modelo,
																			@RequestParam(required=false) Long id_marca,
																			@RequestParam(required=false) String des_modelo,
																			@RequestParam(required=false) String usuario,
																			@RequestParam(required=false) String fecha_inicio,
																			@RequestParam(required=false) String fecha_fin,
																			@RequestParam(defaultValue = "0") int page,
																            @RequestParam(defaultValue = "10") int size,
																            @RequestParam(defaultValue = "id") String order,
																            @RequestParam(defaultValue = "true") boolean asc
																			) {
		Page<ModeloPaginacionResponseDTO> modelos = modeloService.getModeloPaginacion(PageRequest.of(page, size, Sort.by(order)),
				id_marca, usuario, des_modelo, cod_modelo, fecha_inicio, fecha_fin);
        if(!asc)
        	modelos = modeloService.getModeloPaginacion(PageRequest.of(page, size, Sort.by(order).descending()), 
        		id_marca, usuario, des_modelo, cod_modelo, fecha_inicio, fecha_fin);
        
        return new ResponseEntity<>(modelos, HttpStatus.OK);
	}*/
	
	@GetMapping
	public ResponseEntity<Page<ModeloPaginacionResponseV2DTO>> getModeloSearch(
																			@RequestParam(required=false) String cod_modelo,
																			@RequestParam(required=false) Long id_marca,
																			@RequestParam(required=false) String des_modelo,
																			@RequestParam(required=false) String usuario,
																			@RequestParam(required=false) String fecha_inicio,
																			@RequestParam(required=false) String fecha_fin,
																			@RequestParam(defaultValue = "0") int page,
																            @RequestParam(defaultValue = "10") int size,
																            @RequestParam(defaultValue = "id") String order,
																            @RequestParam(defaultValue = "true") boolean asc
																			) {
		
		Page<ModeloPaginacionResponseDTO> modelos = modeloService.getModeloPaginacion(PageRequest.of(page, size, Sort.by(order)),
				id_marca, usuario, des_modelo, cod_modelo, fecha_inicio, fecha_fin);
		
         if(!asc)
        	modelos = modeloService.getModeloPaginacion(PageRequest.of(page, size, Sort.by(order).descending()), 
        		id_marca, usuario, des_modelo, cod_modelo, fecha_inicio, fecha_fin);
        
        List<ModeloPaginacionResponseV2DTO> response = new ArrayList<>();
        
        try {
	        for (ModeloPaginacionResponseDTO modelo : modelos) {
	            
	        	ModeloPaginacionResponseV2DTO modeloPaginacion = new ModeloPaginacionResponseV2DTO();
	        	modeloPaginacion.setId(Long.toString(modelo.getId()));
	        	modeloPaginacion.setCodigo(modelo.getCodigo().toString());
	        	modeloPaginacion.setDescripcion(modelo.getDescripcion().toString());
	        	modeloPaginacion.setMarca(modelo.getMarca().toString());
	        	if(modelo.getFecha() != null) { // CHR 20-07-23
	        		modeloPaginacion.setFecha(modelo.getFecha());
	        	}
	        	
	        	if(modelo.getUsuario() != null) { // CHR 20-07-23
	        		modeloPaginacion.setUsuario(modelo.getUsuario().toString());
	        	}
	        	
	        	response.add(modeloPaginacion);
	        }
        }catch(NullPointerException e) {
            System.out.println("NullPointerException thrown!"); // CHR 20-07-23
        }
        
        Page<ModeloPaginacionResponseV2DTO> modeloPage = new PageImpl<>(response, PageRequest.of(page, size), response.size());
        
        return new ResponseEntity<>(modeloPage, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<ModeloDataResponseDTO> getModelo(@PathVariable("id") Long id) {
		ModeloDataResponseDTO modelo = modeloService.getData(id);
		return new ResponseEntity<>(modelo, HttpStatus.OK);
	}
	
	@GetMapping("/marca/{id}")
	public ResponseEntity<List<ModeloMarcaResponseDTO>> getMarcaModelos(@PathVariable("id") Long id) {
		List<ModeloMarcaResponseDTO> modelos = modeloService.getMarcaModelos(id);
		return new ResponseEntity<>(modelos, HttpStatus.OK); 
	}

	@GetMapping("/{id}/medidas")
	public ResponseEntity<List<MedidasResponseDTO>> getMedidasModelos(@PathVariable("id") Long id) {
		List<MedidasResponseDTO> modelos = modeloService.getMedidasModelos(id);
		return new ResponseEntity<>(modelos, HttpStatus.OK); 
	}
	
	@PostMapping("/store")
	public ResponseEntity<ModeloEntity> postStore(@RequestBody ModeloRequestDTO modeloRequest) throws NroServicioNotFoundException {
		ModeloEntity modelo = modeloService.postStore(modeloRequest);
		return new ResponseEntity<>(modelo, HttpStatus.OK);
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<String> delete(@PathVariable Long id) {
		modeloService.delete(id);
		return new ResponseEntity<>("Modelo eliminado", HttpStatus.OK);
	}
	
}
