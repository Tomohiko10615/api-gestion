package com.enel.scom.api.gestion.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.enel.scom.api.gestion.dto.response.ReporteAsignacionPCRClientePaginacionResponseDTO;
import com.enel.scom.api.gestion.dto.response.ReporteGraficoDTO;
import com.enel.scom.api.gestion.service.ReporteAsignacionPCRClienteService;

@RestController
@RequestMapping("api/reportes/asignacion-pcr-cliente")
public class ReporteAsignacionPCRClienteController {
	
	@Autowired
	ReporteAsignacionPCRClienteService reporteAsignacionPCRClienteService;
	

	@GetMapping("/general")
    public ResponseEntity<Page<ReporteAsignacionPCRClientePaginacionResponseDTO>> obtenerAsignacionPCRClientePaginacion(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size,
            @RequestParam(defaultValue = "id") String order,
            @RequestParam(defaultValue = "true") boolean asc,
			@RequestParam(required=false) String fechaInicio,
			@RequestParam(required=false) String fechaFin,
			@RequestParam(required=false) String situacionPcr,
			@RequestParam(required=false) String nroCliente
    ){
        Page<ReporteAsignacionPCRClientePaginacionResponseDTO> reportePag = reporteAsignacionPCRClienteService.obtenerReporteAsignacionPCRClientePaginacion(
                PageRequest.of(page, size, Sort.by(order)), fechaInicio, fechaFin, situacionPcr, nroCliente);
       if(!asc)
    	   reportePag = reporteAsignacionPCRClienteService.obtenerReporteAsignacionPCRClientePaginacion(
        			PageRequest.of(page, size, Sort.by(order)),fechaInicio, fechaFin, situacionPcr, nroCliente);

        return new ResponseEntity<Page<ReporteAsignacionPCRClientePaginacionResponseDTO>>(reportePag, HttpStatus.OK); 
    }
	
	@GetMapping("/cantidadXsituacion")
	public List<ReporteGraficoDTO> reporteAsignacionPCRClienteCantidadXsituacion(
			@RequestParam(required=false) String fechaInicio,
			@RequestParam(required=false) String fechaFin,
			@RequestParam(required=false) String situacion,
			@RequestParam(required=false) String nroCuenta,
			@RequestParam(required = false) String tipoAsignacion) {
		List<ReporteGraficoDTO> reporteCantXestado = reporteAsignacionPCRClienteService.obtenerReporteAsignacionPCRClienteCantidadXsituacion(
				fechaInicio, fechaFin, situacion, nroCuenta, tipoAsignacion);
		return reporteCantXestado;
	}

}
