package com.enel.scom.api.gestion.dto.response;

import lombok.Data;

@Data
public class OrdenAnormalidadResponseDTO {
	private Long id;
	private Long idAnormalidad;
	private Long idTarea;
	private String codigo;
	private String descripcion;
	private String generaCNR;
	private String generaOrdenNormalizacion;
	private String causal;
}
