package com.enel.scom.api.gestion.service;

import java.util.List;

import com.enel.scom.api.gestion.dto.response.TrabajoResponseDTO;

public interface OrmConfiguracionService {
    List<TrabajoResponseDTO> getTrabajos();
}
