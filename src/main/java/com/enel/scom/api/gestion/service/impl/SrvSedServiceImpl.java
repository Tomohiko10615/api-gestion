package com.enel.scom.api.gestion.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.scom.api.gestion.dto.response.AutoCompleteResponseDTO;
import com.enel.scom.api.gestion.oracle.repository.SrvSedRepository;
import com.enel.scom.api.gestion.service.SrvSedService;

@Service
public class SrvSedServiceImpl implements SrvSedService {
	
	@Autowired
	private SrvSedRepository srvSedRepository;
	
	@Override
	public List<AutoCompleteResponseDTO> buscarSedServicioElectrico(String codSed, Long idAlimentador) {
		List<Object[]> sedsCadenaElectrica = srvSedRepository.buscarSedServicioElectrico(codSed, idAlimentador);
		return sedsCadenaElectrica.stream().map(object -> new AutoCompleteResponseDTO(
				Long.valueOf(object[0].toString()), 
				object[1].toString().trim()
				)).collect(Collectors.toList());
	}

}
