package com.enel.scom.api.gestion.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.model.TareaEntity;

@Repository
public interface TareaRepository extends JpaRepository<TareaEntity, Long>{
	@Query(value = "SELECT * FROM dis_tarea WHERE id = :id", nativeQuery = true)
	TareaEntity findId(@Param("id") Long id);
}
