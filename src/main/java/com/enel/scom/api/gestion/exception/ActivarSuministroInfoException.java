package com.enel.scom.api.gestion.exception;

public class ActivarSuministroInfoException extends Exception {
	public ActivarSuministroInfoException(String message) {
        super(message);
    }
}
