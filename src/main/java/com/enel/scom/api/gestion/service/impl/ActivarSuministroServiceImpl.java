package com.enel.scom.api.gestion.service.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.scom.api.gestion.constant.Constant;
import com.enel.scom.api.gestion.dto.request.ActivarSuministroResquestDTO;
import com.enel.scom.api.gestion.dto.response.ActivarSuministroResponseDTO;
import com.enel.scom.api.gestion.exception.ActivaSuministroNotFoundException;
import com.enel.scom.api.gestion.exception.ActivarSuministroInfoException;
import com.enel.scom.api.gestion.model.ActivacionEntity;
import com.enel.scom.api.gestion.model.CaeTmpClienteEntity;
import com.enel.scom.api.gestion.oracle.model.CaeTmpClienteOracEntity;
import com.enel.scom.api.gestion.oracle.model.SrvAlimentadorEntity;
import com.enel.scom.api.gestion.oracle.model.SrvLlaveEntity;
import com.enel.scom.api.gestion.oracle.model.SrvSedEntity;
import com.enel.scom.api.gestion.oracle.model.SrvSetEntity;
import com.enel.scom.api.gestion.oracle.model.UbiNodoRutaEntity;
import com.enel.scom.api.gestion.oracle.model.VtaAutoCadenaElectricaEntity;
import com.enel.scom.api.gestion.oracle.model.VtaAutoRutaEntity;
import com.enel.scom.api.gestion.oracle.repository.CaeTmpClienteOracRepository;
import com.enel.scom.api.gestion.oracle.repository.SrvAlimentadorRepository;
import com.enel.scom.api.gestion.oracle.repository.SrvLlaveRpository;
import com.enel.scom.api.gestion.oracle.repository.SrvSedRepository;
import com.enel.scom.api.gestion.oracle.repository.SrvSetRepository;
import com.enel.scom.api.gestion.oracle.repository.UbiNodoRutaRepository;
import com.enel.scom.api.gestion.oracle.repository.UbiRutaRepository;
import com.enel.scom.api.gestion.oracle.repository.VtaAutoActivaRepository;
import com.enel.scom.api.gestion.oracle.repository.VtaAutoCadenaElectricaRepository;
import com.enel.scom.api.gestion.oracle.repository.VtaAutoRutaRepository;
import com.enel.scom.api.gestion.oracle.repository.VtaSolSrvEleRepository;
import com.enel.scom.api.gestion.repository.ActivacionRepository;
import com.enel.scom.api.gestion.repository.CaeTmpClienteRepository;
import com.enel.scom.api.gestion.repository.TransferRepository;
import com.enel.scom.api.gestion.service.ActivarSuministroService;
import com.enel.scom.api.gestion.service.TransferService;

@Service
public class ActivarSuministroServiceImpl implements ActivarSuministroService {
	
	@Autowired
	private VtaAutoActivaRepository vtaAutoActivaRepository;
	
	@Autowired
	private VtaAutoCadenaElectricaRepository vtaAutoCadenaElectricaRepository;
	
	@Autowired
	private VtaAutoRutaRepository vtaAutoRutaRepository;
	
	@Autowired
	private SrvSetRepository srvSetRepository;
	
	@Autowired
	private SrvAlimentadorRepository srvAlimentadorRepository;
	
	@Autowired
	private SrvSedRepository srvSedRepository;
	
	@Autowired
	private SrvLlaveRpository srvLlaveRpository;
	
	@Autowired
	private UbiNodoRutaRepository ubiNodoRutaRepository;
	
	@Autowired
	private VtaSolSrvEleRepository vtaSolSrvEleRepository;
	
	@Autowired
	private ActivacionRepository activacionRepository;
	
	@Autowired
	private CaeTmpClienteRepository caeTmpClienteRepository;
	
	@Autowired
	private CaeTmpClienteOracRepository caeTmpClienteOracRepository;
	
	@PersistenceContext(unitName = Constant.DATASOURCE_ORACLE)
	EntityManager entityManagerOracle;
	
	@Override
	public ActivarSuministroResquestDTO actualizarCadenaElectricaYrutas(ActivarSuministroResquestDTO dto) throws ActivaSuministroNotFoundException {
		
	
		String CORRELATIVO ="9";
		String RUTA_VALIDA="";
		String VALIDAR_RUTA_LECTURA ="";
		String VALIDAR_RUTA_CORTE ="";
		String VALIDAR_RUTA_FACTURACION ="";
		String VALIDAR_RUTA_REPARTO ="";
		
		if(StringUtils.isEmpty(dto.getSector()) || StringUtils.isEmpty(dto.getZona()) || StringUtils.isEmpty(dto.getCorrelativo())) {
			throw new ActivaSuministroNotFoundException("Debe ingresar una ruta.");
		}
		/*
		RUTA_VALIDA = CORRELATIVO.concat(dto.getSector()).concat(dto.getZona()).concat(dto.getCorrelativo());
		int rutaValida = ubiRutaRepository.obtenerContadorUbiRutaPorCodRuta(RUTA_VALIDA);
		
		if(rutaValida == 0) {
			throw new ActivaSuministroNotFoundException("Ruta ingresada NO válido.");
		}
		*/
		//Validar Ruta de lectura esta libre
		//VALIDAR_RUTA_LECTURA = CORRELATIVO.concat(dto.getCodLecturaSector()).concat(dto.getCodLecturaZona()).concat(dto.getLecturaCorrelativo());
		VALIDAR_RUTA_LECTURA = CORRELATIVO.concat(dto.getSector()).concat(dto.getZona()).concat(dto.getCorrelativo());
		System.out.print("LECTURA "+VALIDAR_RUTA_LECTURA);	
		int validarRutaLecturaPrimero = vtaSolSrvEleRepository.validarCodigoRutaLecturaAsignadaServicioPrimero(VALIDAR_RUTA_LECTURA);
		
		if(validarRutaLecturaPrimero > 0) {
			throw new ActivaSuministroNotFoundException("Código Ruta de Lectura ya está asignada a otro servicio");
		}
		
		int validarRutaLecturaSegundo = vtaSolSrvEleRepository.validarCodigoRutaLecturaAsignadaServicioSegundo(VALIDAR_RUTA_LECTURA);
		
		if(validarRutaLecturaSegundo > 0) {
			throw new ActivaSuministroNotFoundException("Código Ruta de Lectura ya está asignada a otro servicio");
		}
		
		//Validar Ruta de corte
		
		//VALIDAR_RUTA_CORTE = CORRELATIVO.concat(dto.getCodRutaCorteSector()).concat(dto.getCodRutaCorteZona()).concat(dto.getRutaCorteCorrelativo());
		VALIDAR_RUTA_CORTE = CORRELATIVO.concat(dto.getSector()).concat(dto.getZona()).concat(dto.getCorrelativo());
		
		int validarRutaCortePrimero = vtaSolSrvEleRepository.validarCodigoRutaCorteAsignadaServicioPrimero(VALIDAR_RUTA_CORTE);
		
		if(validarRutaCortePrimero > 0) {
			throw new ActivaSuministroNotFoundException("Código Ruta de Corte ya está asignada a otro servicio");
		}
		
		int validarRutaCorteSegundo = vtaSolSrvEleRepository.validarCodigoRutaCorteAsignadaServicioSegundo(VALIDAR_RUTA_CORTE);
		
		if(validarRutaCorteSegundo > 0) {
			throw new ActivaSuministroNotFoundException("Código Ruta de Corte ya está asignada a otro servicio");
		}
		
		
		//Validar Ruta de Facturacion
		
		//VALIDAR_RUTA_FACTURACION = CORRELATIVO.concat(dto.getCodRutaFacturacionSector()).concat(dto.getCodRutaFacturacionZona()).concat(dto.getRutaFacturacionCorrelativo());
		VALIDAR_RUTA_FACTURACION = CORRELATIVO.concat(dto.getSector()).concat(dto.getZona()).concat(dto.getCorrelativo());
		
		
		int validarRutaFacturacionPrimero = vtaSolSrvEleRepository.validarCodigoRutaFacturacionAsignadaServicioPrimero(VALIDAR_RUTA_FACTURACION);
		
		if(validarRutaFacturacionPrimero > 0) {
			throw new ActivaSuministroNotFoundException("Código Ruta de Facturacion ya está asignada a otro servicio");
		}
		
		int validarRutaFacturacionSegundo = vtaSolSrvEleRepository.validarCodigoRutaFacturacionAsignadaServicioSegundo(VALIDAR_RUTA_FACTURACION);
		
		if(validarRutaFacturacionSegundo > 0) {
			throw new ActivaSuministroNotFoundException("Código Ruta de Facturacion ya está asignada a otro servicio");
		}
		
		
		//Validar Ruta de Reparto
		
		//VALIDAR_RUTA_REPARTO = CORRELATIVO.concat(dto.getCodRutaRepartoSector()).concat(dto.getCodRutaRepartoZona()).concat(dto.getRutaRepartoCorrelativo());
		VALIDAR_RUTA_REPARTO = CORRELATIVO.concat(dto.getSector()).concat(dto.getZona()).concat(dto.getCorrelativo());
		
		int validarRutaRepartoPrimero = vtaSolSrvEleRepository.validarCodigoRutaRepartoAsignadaServicioPrimero(VALIDAR_RUTA_REPARTO);
		
		if(validarRutaRepartoPrimero > 0) {
			throw new ActivaSuministroNotFoundException("Código Ruta de Reparto ya está asignada a otro servicio");
		}
		
		int validarRutaRepartoSegundo = vtaSolSrvEleRepository.validarCodigoRutaRepartoAsignadaServicioSegundo(VALIDAR_RUTA_REPARTO);
		
		if(validarRutaRepartoSegundo > 0) {
			throw new ActivaSuministroNotFoundException("Código Ruta de Reparto ya está asignada a otro servicio");
		}
		
		

		VtaAutoCadenaElectricaEntity cadenaElectrica = vtaAutoCadenaElectricaRepository.obtenerCadenaElectricaPorIdAutoActivacion(dto.getIdAutoActiva());
		
		if(cadenaElectrica == null) {
			throw new ActivaSuministroNotFoundException("No se encontró Cadena Eléctrica.");
		}
		
		cadenaElectrica.setIdSet(dto.getIdSet());
		cadenaElectrica.setIdAlimentador(dto.getIdAlimentador());
		cadenaElectrica.setIdSed(dto.getIdSed());
		cadenaElectrica.setIdLlave(dto.getIdLlave());
		vtaAutoCadenaElectricaRepository.save(cadenaElectrica);
		
		
		VtaAutoRutaEntity ruta = vtaAutoRutaRepository.obtenerRutaPorIdAutoActivacion(dto.getIdAutoActiva());
		
		Long obtenerIdAutoRuta = vtaAutoRutaRepository.generarIdVtaAutoRuta();
		
		UbiNodoRutaEntity sector = ubiNodoRutaRepository.obtenerRutaSectorPorCod(dto.getSector());

		UbiNodoRutaEntity zona = ubiNodoRutaRepository.obtenerRutaZonaPorCod(dto.getZona(), sector.getIdNodo());

		
		
			if(ruta == null) {
				VtaAutoRutaEntity nuevaRuta = new VtaAutoRutaEntity();
				nuevaRuta.setIdAutoRuta(obtenerIdAutoRuta);
				nuevaRuta.setIdAutoAct(dto.getIdAutoActiva());
				nuevaRuta.setIdSrvVenta(cadenaElectrica.getIdSrvVenta());
				nuevaRuta.setIdEmpresa(3L);
				
				nuevaRuta.setIdSectorLec(sector.getIdNodo());
				nuevaRuta.setIdZonaLec(zona.getIdNodo());
				nuevaRuta.setCorrelativoLec(dto.getCorrelativo());
				
				nuevaRuta.setIdSectorCor(sector.getIdNodo());
				nuevaRuta.setIdZonaCor(zona.getIdNodo());
				nuevaRuta.setCorrelativoCor(dto.getCorrelativo());
				
				nuevaRuta.setIdSectorFac(sector.getIdNodo());
				nuevaRuta.setIdZonaFac(zona.getIdNodo());
				nuevaRuta.setCorrelativoFac(dto.getCorrelativo());
				
				nuevaRuta.setIdSectorRep(sector.getIdNodo());
				nuevaRuta.setIdZonaRep(zona.getIdNodo());
				nuevaRuta.setCorrelativoRep(dto.getCorrelativo());
				
				vtaAutoRutaRepository.save(nuevaRuta);
				
			} else {
				
				ruta.setIdSectorLec(sector.getIdNodo());
				ruta.setIdZonaLec(zona.getIdNodo());
				ruta.setCorrelativoLec(dto.getCorrelativo());
				
				ruta.setIdSectorCor(sector.getIdNodo());
				ruta.setIdZonaCor(zona.getIdNodo());
				ruta.setCorrelativoCor(dto.getCorrelativo());
				
				ruta.setIdSectorFac(sector.getIdNodo());
				ruta.setIdZonaFac(zona.getIdNodo());
				ruta.setCorrelativoFac(dto.getCorrelativo());
				
				ruta.setIdSectorRep(sector.getIdNodo());
				ruta.setIdZonaRep(zona.getIdNodo());
				ruta.setCorrelativoRep(dto.getCorrelativo());
				
				vtaAutoRutaRepository.save(ruta);
			}
		
		
		
		
		try {
			
			String comment = (String) entityManagerOracle.createNativeQuery("SELECT VTA_ACTIVAR_ORDEN(:ID_OPERACION) FROM dual").setParameter("ID_OPERACION",dto.getIdAutoActiva()).getSingleResult();
			// R.I. REQSCOM04 26/10/2023 INICIO
			// Se requiere en caso la función devuelve una respuesta de menor longitud que la esperada
			int limit = 5;
			if (comment.length() < 5) {
				limit = comment.length();
			}
			String abstraer = comment.substring(0, limit);
			// R.I. REQSCOM04 26/10/2023 FIN
			System.out.print("RESPUESTA "+ comment);
			if (!abstraer.equals("ERROR")) {
				
				//Se actualiza el estado de activacion de KO A OK
				ActivacionEntity activacion = activacionRepository.obtenerActivacionPorIdAutoActiva(dto.getIdAutoActiva());
				activacion.setEstadoActivacion("OK");
				activacion.setFechaActivacion(new Date());
				activacion.setEstadoProceso("PendientePcr");
				activacionRepository.save(activacion);
			
				//Se valida si existe el numero registro con el numero de cliente
				/*
				 * Esta porción de código se comentó porque se ha creado un proceso aparte que se ejecutará cada 5 min para hacer la inserción de nuevos registros de la tabla cae_tmp_cliente del 4J al SCOM
				CaeTmpClienteEntity clienteScomUpdate = caeTmpClienteRepository.obtenerCaeTmpClienteScomPorNroCuenta(activacion.getNumeroCuenta());
				
				if(clienteScomUpdate != null) {
					//Si ya existe el registro en scom solo se actualiza con los datos del 4j
					CaeTmpClienteOracEntity clienteOracleUpdate = caeTmpClienteOracRepository.obtenerCaeTmpClienteOracPorNroCuenta(activacion.getNumeroCuenta());
					setValoresCaeTmpCliente(clienteScomUpdate, clienteOracleUpdate);
					caeTmpClienteRepository.save(clienteScomUpdate);
					
				} else {
					CaeTmpClienteOracEntity clienteOracleUpdate = caeTmpClienteOracRepository.obtenerCaeTmpClienteOracPorNroCuenta(activacion.getNumeroCuenta());
					if(clienteOracleUpdate == null) throw new ActivaSuministroNotFoundException("No se encontró registro en CAE_TMP_CLIENTE para actualizar.");
					CaeTmpClienteEntity nuevo = new CaeTmpClienteEntity();
					nuevo.setIdCaeTmpCliente(clienteOracleUpdate.getIdCaeTmpCliente());
					nuevo.setNumeroCliente(clienteOracleUpdate.getNumeroCliente());
					nuevo.setTipoAccion(clienteOracleUpdate.getTipoAccion());
					nuevo.setMotivoRechazo(clienteOracleUpdate.getMotivoRechazo());
					
					try {
						String fechaString = clienteOracleUpdate.getFechaRetiro();
						DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
					    Date fechaDate = (Date) df.parse(fechaString);
					    nuevo.setFechaRetiro(fechaDate);
			            
			    	}catch (Exception e) {
						e.printStackTrace();
					}	
					setValoresCaeTmpCliente(nuevo, clienteOracleUpdate);
					caeTmpClienteRepository.save(nuevo);
					
				}
				*/
			} else {
				String mensaje = comment.substring(246);
				throw new ActivaSuministroNotFoundException("ERROR: " + mensaje);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			String mensaje = e.getMessage().substring(16);
        	throw new ActivaSuministroNotFoundException("ERROR: " + mensaje); 
		} finally {
			entityManagerOracle.close();
		}
		
		
		return dto;
	}
	
	
	private void setValoresCaeTmpCliente(CaeTmpClienteEntity scom, CaeTmpClienteOracEntity oracle) {
		
		scom.setTarifa(oracle.getTarifa());
		scom.setSucursal(oracle.getSucursal());
		scom.setDireccion(oracle.getDireccion());
		scom.setComuna(oracle.getComuna());
		scom.setSector(oracle.getSector());
		scom.setZona(oracle.getZona());
		scom.setCorrRuta(oracle.getCorrRuta());
		scom.setCodGiro(oracle.getCodGiro());
		scom.setTipoCliente(oracle.getTipoCliente());
		scom.setEstadoCliente(oracle.getEstadoCliente());
		scom.setEstadoSuministro(oracle.getEstadoSuministro());
		scom.setDvNumeroCliente(oracle.getDvNumeroCliente());
		scom.setInfoAdicLectura(oracle.getInfoAdicLectura());
		scom.setPotenciaContFp(oracle.getPotenciaContFp());
		scom.setPotenciaInstHp(oracle.getPotenciaInstHp());
		scom.setPotenciaInstFp(oracle.getPotenciaInstFp());
		scom.setIdPcr(oracle.getIdPcr());
		scom.setPcr(oracle.getPcr());
		scom.setLlave(oracle.getLlave());
		scom.setSubestacDistrib(oracle.getSubestacDistrib());
		scom.setAlimentador(oracle.getAlimentador());
		scom.setSubestacTransmi(oracle.getSubestacTransmi());
		scom.setNombre(oracle.getNombre());
		scom.setTelefono(oracle.getTelefono());
		scom.setRut(oracle.getRut());
		scom.setTipoIdent(oracle.getTipoIdent());
		scom.setEstadoProceso(oracle.getEstadoProceso());
		scom.setNroEnvios(oracle.getNroEnvios());
		
		try {
			String fechaString = oracle.getFechaActivacion();
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		    Date fechaDate = (Date) df.parse(fechaString);
        	scom.setFechaActivacion(fechaDate);
            
    	}catch (Exception e) {
			e.printStackTrace();
		}	
	}

	@Override
	public ActivarSuministroResponseDTO obtenerDetalleActivarSuministro(Long idAutoAct) throws ActivaSuministroNotFoundException, ActivarSuministroInfoException {
		
		ActivarSuministroResponseDTO response = new ActivarSuministroResponseDTO();
		
		/*
		String nroOrdenLgcRelac = transferRepository.obtenerNroOrdenLgcRelacPorNroOrdenLegacy(nroOrdenLegacy);
		
		if(StringUtils.isBlank(nroOrdenLgcRelac)) {
			throw new ActivaSuministroNotFoundException("No se encontró el nroOrdenLgcRelac.");
		}
		
		
		//Se obtiene el idAutoActiva para mediante este dato obtener los datos de las rutas y cadena electrica
		Long idAutoActiva = vtaAutoActivaRepository.obtenerIdAutoActivaPorNroOrden(nroOrdenLgcRelac);
		
		if(idAutoActiva == null) {
			throw new ActivaSuministroNotFoundException("No se encontró el idAutoActiva.");
		}
		*/
		
		ActivacionEntity activacionEntity = activacionRepository.obtenerActivacionPorIdAutoActiva(idAutoAct);
		
		if(activacionEntity == null) {
			throw new ActivaSuministroNotFoundException("No se encontró activación.");
		}
		
		response.setNumeroCuenta(activacionEntity.getNumeroCuenta());
		
		VtaAutoCadenaElectricaEntity cadenaElectrica = vtaAutoCadenaElectricaRepository.obtenerCadenaElectricaPorIdAutoActivacion(idAutoAct);
		
		if(cadenaElectrica == null) {
			throw new ActivaSuministroNotFoundException("No se encontró cadena electrica.");
		}
		
		SrvSetEntity set = srvSetRepository.obtenerSetServicioElectricoPorId(cadenaElectrica.getIdSet());
		
		if(set == null) {
			throw new ActivaSuministroNotFoundException("No se encontró SET cadena electrica.");
		}
		response.setIdSet(set.getIdSet() == null ? 0 : set.getIdSet());
		response.setCodSet(set.getCodSet());
		
		
		SrvAlimentadorEntity alimentador= srvAlimentadorRepository.obtenerAlimentadorServicioElectricoPorId(cadenaElectrica.getIdAlimentador());
		
		if(alimentador == null) {
			throw new ActivaSuministroNotFoundException("No se encontró ALIMENTADOR cadena electrica.");
		}
		
		response.setIdAlimentador(alimentador.getIdAlimentador());
		response.setCodAlimentador(alimentador.getCodAlimentador());
		
		
		SrvSedEntity sed = srvSedRepository.obtenerSedServicioElectricoPorId(cadenaElectrica.getIdSed());
		
		if(sed == null) {
			throw new ActivaSuministroNotFoundException("No se encontró SED cadena electrica.");
		}
		
		response.setIdSed(sed.getIdSed());
		response.setCodSed(sed.getCodSed());
		
		SrvLlaveEntity llave = srvLlaveRpository.obtenerLlaveServicioElectricoPorId(cadenaElectrica.getIdLlave());
		
		if(llave == null) {
			throw new ActivaSuministroNotFoundException("No se encontró LLAVE cadena electrica.");
		}
		
		response.setIdLlave(llave.getIdLlave());
		response.setCodLlave(llave.getCodigo());
		response.setDesLlave(llave.getDescripcion());
		
		
		VtaAutoRutaEntity ruta = vtaAutoRutaRepository.obtenerRutaPorIdAutoActivacion(idAutoAct);
		
		//Ruta de Lectura
		if(ruta != null) {
			
			UbiNodoRutaEntity lecturaSector = ubiNodoRutaRepository.obtenerUbiNodoServicioElectricoPorId(ruta.getIdSectorLec());
			UbiNodoRutaEntity lecturaZona = ubiNodoRutaRepository.obtenerUbiNodoServicioElectricoPorId(ruta.getIdZonaLec());
			
			response.setSector(lecturaSector == null ? "" : lecturaSector.getCodNodo());
			response.setZona(lecturaZona == null ? "" : lecturaZona.getCodNodo());
			
			response.setCorrelativo(ruta.getCorrelativoLec());
			
			/*
			UbiNodoRutaEntity corteSector = ubiNodoRutaRepository.obtenerUbiNodoServicioElectricoPorId(ruta.getIdSectorCor());
			UbiNodoRutaEntity corteZona = ubiNodoRutaRepository.obtenerUbiNodoServicioElectricoPorId(ruta.getIdZonaCor());
			
			UbiNodoRutaEntity facturacionSector = ubiNodoRutaRepository.obtenerUbiNodoServicioElectricoPorId(ruta.getIdSectorFac());
			UbiNodoRutaEntity facturacionZona = ubiNodoRutaRepository.obtenerUbiNodoServicioElectricoPorId(ruta.getIdZonaFac());
			
			UbiNodoRutaEntity repartoSector = ubiNodoRutaRepository.obtenerUbiNodoServicioElectricoPorId(ruta.getIdSectorRep());
			UbiNodoRutaEntity repartoZona = ubiNodoRutaRepository.obtenerUbiNodoServicioElectricoPorId(ruta.getIdZonaRep());
			*/
			
			/*
			response.setIdLecturaSector(lecturaSector == null ? 0 : lecturaSector.getIdNodo());
			response.setCodLecturaSector(lecturaSector == null ? "" : lecturaSector.getCodNodo());
			
			response.setIdLecturaZona(lecturaZona == null ? 0 : lecturaZona.getIdNodo());
			response.setCodLecturaZona(lecturaZona == null ? "" : lecturaZona.getCodNodo());
			
			response.setLecturaCorrelativo(ruta.getCorrelativoLec());
			
			
			response.setIdRutaCorteSector(corteSector == null ? 0 : corteSector.getIdNodo());
			response.setCodRutaCorteSector(corteSector == null ? "" : corteSector.getCodNodo());
			
			response.setIdRutaCorteZona(corteZona == null ? 0 : corteZona.getIdNodo());
			response.setCodRutaCorteZona(corteZona == null ? "": corteZona.getCodNodo());
			
			response.setRutaCorteCorrelativo(ruta.getCorrelativoCor());
			
			response.setIdRutaFacturacionSector(facturacionSector == null ? 0 : facturacionSector.getIdNodo());
			response.setCodRutaFacturacionSector(facturacionSector == null ? "" : facturacionSector.getCodNodo());
			
			response.setIdRutaFacturacionZona(facturacionZona == null ? 0 : facturacionZona.getIdNodo());
			response.setCodRutaFacturacionZona(facturacionZona == null ? "" : facturacionZona.getCodNodo());
			
			response.setRutaFacturacionCorrelativo(ruta.getCorrelativoFac());
			
			
			response.setIdRutaRepartoSector(repartoSector == null ? 0 : repartoSector.getIdNodo());
			response.setCodRutaRepartoSector(repartoSector == null ? "" : repartoSector.getCodNodo());
			
			response.setIdRutaRepartoZona(repartoZona == null ? 0 : repartoZona.getIdNodo());
			response.setCodRutaRepartoZona(repartoZona == null ? "" : repartoZona.getCodNodo());
			
			response.setRutaRepartoCorrelativo(ruta.getCorrelativoRep());
			*/
		}
		
			
		return response;
	}
	
	@Override
	public String testActivarSuministro(Long idOperacion) throws ActivaSuministroNotFoundException {
		
		String comment = "";
		try {
			
			comment = (String) entityManagerOracle.createNativeQuery("SELECT VTA_ACTIVAR_ORDEN(:ID_OPERACION) FROM dual").setParameter("ID_OPERACION",idOperacion).getSingleResult();
			String abstraer = comment.substring(0, 5);
			System.out.print("RESPUESTA "+ comment);
			if (!abstraer.equals("ERROR")) {
				
				//Se actualiza el estado de activacion de KO A OK
				/*
				ActivacionEntity activacion = activacionRepository.obtenerActivacionPorIdAutoActiva(idOperacion);
				activacion.setEstadoActivacion("OK");
				activacion.setFechaActivacion(new Date());
				activacion.setEstadoProceso("PendientePcr");
				activacionRepository.save(activacion);
				*/
			
				//Se valida si existe el numero registro con el numero de cliente
				/*
				 * Esta porción de código se comentó porque se ha creado un proceso aparte que se ejecutará cada 5 min para hacer la inserción de nuevos registros de la tabla cae_tmp_cliente del 4J al SCOM
				CaeTmpClienteEntity clienteScomUpdate = caeTmpClienteRepository.obtenerCaeTmpClienteScomPorNroCuenta(activacion.getNumeroCuenta());
				
				if(clienteScomUpdate != null) {
					//Si ya existe el registro en scom solo se actualiza con los datos del 4j
					CaeTmpClienteOracEntity clienteOracleUpdate = caeTmpClienteOracRepository.obtenerCaeTmpClienteOracPorNroCuenta(activacion.getNumeroCuenta());
					setValoresCaeTmpCliente(clienteScomUpdate, clienteOracleUpdate);
					caeTmpClienteRepository.save(clienteScomUpdate);
					
				} else {
					CaeTmpClienteOracEntity clienteOracleUpdate = caeTmpClienteOracRepository.obtenerCaeTmpClienteOracPorNroCuenta(activacion.getNumeroCuenta());
					if(clienteOracleUpdate == null) throw new ActivaSuministroNotFoundException("No se encontró registro en CAE_TMP_CLIENTE para actualizar.");
					CaeTmpClienteEntity nuevo = new CaeTmpClienteEntity();
					nuevo.setIdCaeTmpCliente(clienteOracleUpdate.getIdCaeTmpCliente());
					nuevo.setNumeroCliente(clienteOracleUpdate.getNumeroCliente());
					nuevo.setTipoAccion(clienteOracleUpdate.getTipoAccion());
					nuevo.setMotivoRechazo(clienteOracleUpdate.getMotivoRechazo());
					
					try {
						String fechaString = clienteOracleUpdate.getFechaRetiro();
						DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
					    Date fechaDate = (Date) df.parse(fechaString);
					    nuevo.setFechaRetiro(fechaDate);
			            
			    	}catch (Exception e) {
						e.printStackTrace();
					}	
					setValoresCaeTmpCliente(nuevo, clienteOracleUpdate);
					caeTmpClienteRepository.save(nuevo);
					
				}
				*/
			} else {
				String mensaje = comment.substring(246);
				throw new ActivaSuministroNotFoundException("ERROR: " + mensaje);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			String mensaje = e.getMessage().substring(16);
        	throw new ActivaSuministroNotFoundException("ERROR: " + mensaje); 
		} finally {
			entityManagerOracle.close();
		}
		
		
		return comment;
	}
	
	
}
