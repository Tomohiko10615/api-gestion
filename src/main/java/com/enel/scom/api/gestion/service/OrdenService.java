package com.enel.scom.api.gestion.service;

import com.enel.scom.api.gestion.dto.response.OrdenDataResponseDTO;
import com.enel.scom.api.gestion.dto.response.OrdenXMLResponseDTO;
import com.enel.scom.api.gestion.exception.NroServicioNotFoundException;

public interface OrdenService {
	OrdenDataResponseDTO getData(Long id, Long tipo) throws NroServicioNotFoundException;
	
	OrdenXMLResponseDTO getOrdenXML(Long id, String accion) throws NroServicioNotFoundException;
	
	OrdenXMLResponseDTO getDetalleXMLEnvio(Long id) throws NroServicioNotFoundException;
	
	OrdenXMLResponseDTO getDetalleXMLRecepcion(Long id) throws NroServicioNotFoundException;
	
	OrdenDataResponseDTO obtenerDetalleOrdenesScom(Long id) throws NroServicioNotFoundException;
}
