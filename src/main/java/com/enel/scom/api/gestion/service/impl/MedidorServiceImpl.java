package com.enel.scom.api.gestion.service.impl;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.enel.scom.api.gestion.dto.request.MedidorRequestDTO;
import com.enel.scom.api.gestion.dto.response.MedidorDataResponseDTO;
import com.enel.scom.api.gestion.dto.response.MedidorMedidaResponseDTO;
import com.enel.scom.api.gestion.dto.response.MedidorPaginacionResponseDTO;
import com.enel.scom.api.gestion.dto.response.ReporteMedidoresPaginacionResponseDTO;
import com.enel.scom.api.gestion.exception.AllNotFoundException;
import com.enel.scom.api.gestion.exception.NroServicioNotFoundException;
import com.enel.scom.api.gestion.model.AuditEventEntity;
import com.enel.scom.api.gestion.model.DyoobjectEntity;
import com.enel.scom.api.gestion.model.EstadoMedidorEntity;
import com.enel.scom.api.gestion.model.FactorEntity;
import com.enel.scom.api.gestion.model.MedEntDecEntity;
import com.enel.scom.api.gestion.model.MedHisComponenteEntity;
import com.enel.scom.api.gestion.model.MedTipCalculoEntity;
import com.enel.scom.api.gestion.model.MedidaEntity;
import com.enel.scom.api.gestion.model.MedidaMedidorEntity;
import com.enel.scom.api.gestion.model.MedidorEntity;
import com.enel.scom.api.gestion.model.ModeloEntity;
import com.enel.scom.api.gestion.model.MovEstMedidorEntity;
import com.enel.scom.api.gestion.model.ParametroEntity;
import com.enel.scom.api.gestion.model.UbicacionEntity;
import com.enel.scom.api.gestion.model.UsuarioEntity;
import com.enel.scom.api.gestion.repository.AuditEventRepository;
import com.enel.scom.api.gestion.repository.DyoobjectRepository;
import com.enel.scom.api.gestion.repository.EstadoMedidorRepository;
import com.enel.scom.api.gestion.repository.FactorRepository;
import com.enel.scom.api.gestion.repository.MedEntDecRepository;
import com.enel.scom.api.gestion.repository.MedHisComponenteRepository;
import com.enel.scom.api.gestion.repository.MedTipCalculoRepository;
import com.enel.scom.api.gestion.repository.MedidaMedidorRepository;
import com.enel.scom.api.gestion.repository.MedidaRepository;
import com.enel.scom.api.gestion.repository.MedidorRepository;
import com.enel.scom.api.gestion.repository.ModeloRepository;
import com.enel.scom.api.gestion.repository.MovEstMedidorRepository;
import com.enel.scom.api.gestion.repository.ParametroRepository;
import com.enel.scom.api.gestion.repository.UbicacionRepository;
import com.enel.scom.api.gestion.repository.UsuarioRepository;
import com.enel.scom.api.gestion.service.MedidorService;
import com.enel.scom.api.gestion.util.Funciones;

import lombok.extern.slf4j.Slf4j;

/**
 * Servicio principal donde se realiza el mantenimiento de Medidor
 * @author Gilmar Moreno
 * @version 1.0
 */
@Slf4j
@Service
public class MedidorServiceImpl implements MedidorService{

	@Autowired
	MedidorRepository medidorRepository;
	
	@Autowired
	UbicacionRepository ubicacionRepository;
	
	@Autowired
	UsuarioRepository usuarioRepository;

	@Autowired
	FactorRepository factorRepository;
	
	@Autowired
	MedTipCalculoRepository medTipCalculoRepository;
	
	@Autowired
	MedEntDecRepository medEntDecRepository;
	
	@Autowired
	MedidaRepository medidaRepository;
	
	@Autowired
	MedidaMedidorRepository medidaMedidorRepository;
	
	@Autowired
	MovEstMedidorRepository movEstMedidorRepository;
	
	@Autowired
	ParametroRepository parametroRepository;
	
	@Autowired
	EstadoMedidorRepository estadoMedidorRepository;
	
	@Autowired
	ModeloRepository modeloRepository;
	
	@Autowired
	AuditEventRepository auditEventRepository;

	@Autowired
	MedHisComponenteRepository medHisComponenteRepository;
	
	@Autowired
	DyoobjectRepository dyoobjectRepository;

	@PersistenceContext
	EntityManager entityManager;
	
	/**
	 * Metodo para listar todos los medidores.
	 * @param paging
	 * @param nroMedidor
	 * @param idMarca
	 * @param idModelo
	 * @param usuario
	 * @param fechaInicio
	 * @param fechaFin
	 * @param fechaInicioEstado
	 * @param fechaFinEstado
	 * @param idEstado
	 * @return
	 */
	@Override
	public Page<MedidorPaginacionResponseDTO> getMedidorPaginacion(Pageable paging, String nroMedidor, Long idMarca, Long idModelo, String usuario, String fechaInicio, String fechaFin, String fechaInicioEstado, String fechaFinEstado, Long idEstado) {
		
		log.info("getMedidorPaginacion() INI");
		log.info(" paging ="+Funciones.objectToJsonString(paging));
		log.info(" nroMedidor ="+nroMedidor);
		log.info(" idMarca ="+idMarca);
		log.info(" idModelo ="+idModelo);
		log.info(" usuario ="+usuario);
		log.info(" fechaInicio ="+fechaInicio);
		log.info(" fechaFin ="+fechaFin);
		log.info(" fechaInicioEstado ="+fechaInicioEstado);
		log.info(" fechaFinEstado ="+fechaFinEstado);
		log.info(" idEstado ="+idEstado);
		
		String sql = "";
		String FORMAT_DATE= "yyyy-MM-dd";

		sql = "select new com.enel.scom.api.gestion.dto.response.MedidorPaginacionResponseDTO( "
		+ "m.id, "
		+ "m.nroComponente, "
		+ "mma.codigo, "
		+ "mm.codModelo, "
		+ "mec.descripcion, "
		+ "mhc.fecDesde, "
		+ "m.fecCreacion, "
		+ "usu.username "
		+ ") from med_componente m "
		+ "JOIN med_modelo mm ON mm.id = m.modelo.id "
		+ "JOIN med_marca mma ON mma.id = mm.marca.id "
		+ "LEFT JOIN med_his_componente mhc ON m.id = mhc.medidor.id "
		+ "LEFT JOIN med_est_componente mec ON mec.id = mhc.estadoFinal.id "
		+ "LEFT JOIN fwk_auditevent fa ON fa.idFk = m.id "
		+ "LEFT JOIN usuario usu ON fa.usuarioEntity.id = usu.id "
		+ "where (m.activo is null or m.activo ='S')";
		
		if(!nroMedidor.trim().isEmpty()) {
			sql += " and m.nroComponente = '" + nroMedidor + "'";
		}

		if((idMarca != null) && (idMarca > 0)) {
			sql += " and mma.id = " + idMarca ;
		}
		
		if((idModelo != null) && (idModelo > 0)) {
			sql += " and mm.id = " + idModelo ;
		}

		if(!usuario.trim().isEmpty()) {
			sql += " and upper(usu.username) = '" + usuario + "'" ;
		}

		if ((!StringUtils.isBlank(fechaInicio)) || (!StringUtils.isBlank(fechaFin))) {
			try {
				SimpleDateFormat sdf=new SimpleDateFormat(FORMAT_DATE);
				Date currentdateFehaInicio = sdf.parse(fechaInicio);
				SimpleDateFormat sdf2=new SimpleDateFormat(FORMAT_DATE);
				String fechaInicioConvertido = sdf2.format(currentdateFehaInicio);
				
				SimpleDateFormat sdf4=new SimpleDateFormat(FORMAT_DATE);
				Date currentdateFehaFin=sdf4.parse(fechaFin);
				SimpleDateFormat sdf3=new SimpleDateFormat(FORMAT_DATE);
				String fechaFinConvertido = sdf3.format(currentdateFehaFin);
				
				sql +=  " and DATE_TRUNC('day',m.fecCreacion) between "+ "'"+fechaInicioConvertido +"'"+" and "+"'"+ fechaFinConvertido+"'";
				
			}catch (Exception e) {
				e.printStackTrace();
				log.error("error date format 1",e);
			}	
				
		}	

		if ((!StringUtils.isBlank(fechaInicioEstado)) || (!StringUtils.isBlank(fechaFinEstado))) {
			try {
				SimpleDateFormat sdf=new SimpleDateFormat(FORMAT_DATE);
				Date currentdateFehaInicio = sdf.parse(fechaInicioEstado);
				SimpleDateFormat sdf2=new SimpleDateFormat(FORMAT_DATE);
				String fechaInicioConvertido = sdf2.format(currentdateFehaInicio);
				
				SimpleDateFormat sdf4=new SimpleDateFormat(FORMAT_DATE);
				Date currentdateFehaFin=sdf4.parse(fechaFinEstado);
				SimpleDateFormat sdf3=new SimpleDateFormat(FORMAT_DATE);
				String fechaFinConvertido = sdf3.format(currentdateFehaFin);
				
				sql +=  " and DATE_TRUNC('day',mhc.fecDesde) between "+ "'"+fechaInicioConvertido +"'"+" and "+"'"+ fechaFinConvertido+"'";
				
			}catch (Exception e) {
				e.printStackTrace();
				log.error("error date format 2",e);
			}	
				
		}
		
		if((idEstado != null) && (idEstado > 0)) {
			sql += " and mhc.estadoFinal.id = " + idEstado;
		}

		
		try {
		
			sql +=" order by m.nroComponente desc ";

				log.info("query ="+sql);
			
				List<MedidorPaginacionResponseDTO> result = entityManager.createQuery(sql, MedidorPaginacionResponseDTO.class).getResultList();
				
				log.info("result size ="+result.size());
				
				List<MedidorPaginacionResponseDTO> lista = (List<MedidorPaginacionResponseDTO>) result.stream().collect(Collectors.toList());
				
				log.info("lista size ="+lista.size());
				
				int pageOffset = (int) paging.getOffset();
				int total = (pageOffset + paging.getPageSize()) > lista.size() ? lista.size() : (pageOffset + paging.getPageSize());

				log.info("pageOffset ="+pageOffset+", total="+total);
				
				log.info("getMedidorPaginacion() FIN OK");
				return new PageImpl<MedidorPaginacionResponseDTO>(lista.subList(pageOffset, total), paging, lista.size());
				
		} catch (Exception e) {
			e.printStackTrace();

			log.info("getMedidorPaginacion() FIN ERROR");
			log.error("Error al consultar los medidores.",e);
			throw new AllNotFoundException("Error al consultar los medidores."); 
			
		} finally {
			entityManager.close();
			log.info("getMedidorPaginacion() FIN");
		}
		
		
	}
	
	
	public Page<MedidorPaginacionResponseDTO> getMedidorPaginacion_V2(Pageable paging, String nroMedidor, Long idMarca, Long idModelo, String usuario, String fechaInicio, String fechaFin, String fechaInicioEstado, String fechaFinEstado, Long idEstado) {
		
		log.info("getMedidorPaginacion_V2() INI");
		log.info(" paging ="+Funciones.objectToJsonString(paging));
		log.info(" nroMedidor ="+nroMedidor);
		log.info(" idMarca ="+idMarca);
		log.info(" idModelo ="+idModelo);
		log.info(" usuario ="+usuario);
		log.info(" fechaInicio ="+fechaInicio);
		log.info(" fechaFin ="+fechaFin);
		log.info(" fechaInicioEstado ="+fechaInicioEstado);
		log.info(" fechaFinEstado ="+fechaFinEstado);
		log.info(" idEstado ="+idEstado);
		
		String sqlSelect = "";
		String sqlFrom = "";
		String sqlWhere = "";
		String sqlOrderBy= "";
		String sqlFinal = "";
		
		String FORMAT_DATE= "yyyy-MM-dd";

		sqlSelect = "select new com.enel.scom.api.gestion.dto.response.MedidorPaginacionResponseDTO( "
		+ "m.id, "
		+ "m.nroComponente, "
		+ "mma.codigo, "
		+ "mm.codModelo, "
		+ "mec.descripcion, "
		+ "mhc.fecDesde, "
		+ "m.fecCreacion, "
		+ "usu.username "
		+ ")";
		
		sqlFrom= " from med_componente m "
		+ "JOIN med_modelo mm ON mm.id = m.modelo.id "
		+ "JOIN med_marca mma ON mma.id = mm.marca.id "
		+ "LEFT JOIN med_his_componente mhc ON m.id = mhc.medidor.id "
		+ "LEFT JOIN med_est_componente mec ON mec.id = mhc.estadoFinal.id "
		//+ "LEFT JOIN fwk_auditevent fa ON fa.idFk = m.id "
		// R.I. Correccion 19/10/2023 INICIO
		+ "LEFT JOIN fwk_auditevent fa ON fa.id = mhc.id "
		// R.I. Correccion 19/10/2023 FIN
		+ "LEFT JOIN usuario usu ON fa.usuarioEntity.id = usu.id ";
		
		sqlWhere = "where (m.activo is null or m.activo ='S')";
		
		if(!nroMedidor.trim().isEmpty()) {
			sqlWhere += " and m.nroComponente = '" + nroMedidor + "'";
		}

		if((idMarca != null) && (idMarca > 0)) {
			sqlWhere += " and mma.id = " + idMarca ;
		}
		
		if((idModelo != null) && (idModelo > 0)) {
			sqlWhere += " and mm.id = " + idModelo ;
		}

		if(!usuario.trim().isEmpty()) {
			sqlWhere += " and upper(usu.username) = '" + usuario + "'" ;
		}

		if ((!StringUtils.isBlank(fechaInicio)) || (!StringUtils.isBlank(fechaFin))) {
			try {
				SimpleDateFormat sdf=new SimpleDateFormat(FORMAT_DATE);
				Date currentdateFehaInicio = sdf.parse(fechaInicio);
				SimpleDateFormat sdf2=new SimpleDateFormat(FORMAT_DATE);
				String fechaInicioConvertido = sdf2.format(currentdateFehaInicio);
				
				SimpleDateFormat sdf4=new SimpleDateFormat(FORMAT_DATE);
				Date currentdateFehaFin=sdf4.parse(fechaFin);
				SimpleDateFormat sdf3=new SimpleDateFormat(FORMAT_DATE);
				String fechaFinConvertido = sdf3.format(currentdateFehaFin);
				
				sqlWhere +=  " and DATE_TRUNC('day',m.fecCreacion) between "+ "'"+fechaInicioConvertido +"'"+" and "+"'"+ fechaFinConvertido+"'";
				
			}catch (Exception e) {
				e.printStackTrace();
				log.error("error date format 1",e);
			}	
				
		}	

		if ((!StringUtils.isBlank(fechaInicioEstado)) || (!StringUtils.isBlank(fechaFinEstado))) {
			try {
				SimpleDateFormat sdf=new SimpleDateFormat(FORMAT_DATE);
				Date currentdateFehaInicio = sdf.parse(fechaInicioEstado);
				SimpleDateFormat sdf2=new SimpleDateFormat(FORMAT_DATE);
				String fechaInicioConvertido = sdf2.format(currentdateFehaInicio);
				
				SimpleDateFormat sdf4=new SimpleDateFormat(FORMAT_DATE);
				Date currentdateFehaFin=sdf4.parse(fechaFinEstado);
				SimpleDateFormat sdf3=new SimpleDateFormat(FORMAT_DATE);
				String fechaFinConvertido = sdf3.format(currentdateFehaFin);
				
				sqlWhere +=  " and DATE_TRUNC('day',mhc.fecDesde) between "+ "'"+fechaInicioConvertido +"'"+" and "+"'"+ fechaFinConvertido+"'";
				
			}catch (Exception e) {
				e.printStackTrace();
				log.error("error date format 2",e);
			}	
				
		}
		
		if((idEstado != null) && (idEstado > 0)) {
			sqlWhere += " and mhc.estadoFinal.id = " + idEstado;
		}

		
		try {
				int offset = paging.getPageNumber() * paging.getPageSize();
				int limit = paging.getPageSize();
				log.info("/offset: {}", offset);
				log.info("/limit: {}", limit);
				
				String sqlCantidad = "select count(1) ".concat(sqlFrom).concat(sqlWhere); //error 
				log.info("/query cantidad: {}", sqlCantidad);
				Long cantidadTotalRegistros = (Long) entityManager.createQuery(sqlCantidad).getSingleResult();
				log.info("/cantidad total de registros: {}", cantidadTotalRegistros);
				
				PageImpl<MedidorPaginacionResponseDTO> objfinal = null;
				
				if(cantidadTotalRegistros == 0L) {
					log.info("getMedidorPaginacion_V2() FIN OK (NO DATA FOUND)");
		    		objfinal =  new PageImpl<>(new ArrayList<MedidorPaginacionResponseDTO>(), paging, cantidadTotalRegistros);
		    		log.info("getMedidorPaginacion_V2() FIN OK");
		    		return objfinal;
				}
				
				sqlOrderBy +=" order by m.nroComponente desc ";
				log.info("query order by ="+sqlOrderBy);

				sqlFinal= sqlSelect.concat(sqlFrom).concat(sqlWhere).concat(sqlOrderBy);
				log.info("query final ="+ sqlFinal);
				
        		log.info("ejecutando query...");
				List<MedidorPaginacionResponseDTO> result = entityManager.createQuery(sqlFinal, MedidorPaginacionResponseDTO.class).setFirstResult(offset).setMaxResults(limit).getResultList();
	        	log.info("query ejecutado, rows ="+result.size());
				
				log.info("definiendo a retornar...");
	    		objfinal =  new PageImpl<>(result, paging, cantidadTotalRegistros);
	    		log.info("objeto de retorno definido fin.");
	    		
	    		log.info("getMedidorPaginacion_V2() FIN OK");
	    		return objfinal;
				
				
		} catch (Exception e) {
			e.printStackTrace();

			log.info("getMedidorPaginacion_V2() FIN ERROR");
			log.error("Error al consultar los medidores.",e);
			throw new AllNotFoundException("Error al consultar los medidores."); 
			
		} finally {
			entityManager.close();
			log.info("getMedidorPaginacion_V2() finally");
		}
		
		
	}
	


	/**
	 * Metodo para registrar y actualizar los medidores.
	 */
	@Override
	public MedidorEntity postStore(MedidorRequestDTO medidorDTO) throws NroServicioNotFoundException {
		Boolean existeMedidor = medidorRepository.existsById(medidorDTO.getId_medidor());
		ModeloEntity modelo = modeloRepository.findId(medidorDTO.getId_modelo());

		/**
		 * Valida si el medidor se encuentra registrado por nro de id marca, id modelo y nro de medidor.
		 */
		if(Boolean.FALSE.equals(existeMedidor)) {

			Integer existeCodigo = medidorRepository.findByNroMedidor(medidorDTO.getNro_medidor(), medidorDTO.getId_modelo(), medidorDTO.getId_marca());
			if(existeCodigo > 0) {
				throw new NroServicioNotFoundException("El Nro de medidor ingresado se encuentra registrado.");
			}
		}
		
		UbicacionEntity ubicacion = ubicacionRepository.findId(medidorDTO.getId_ubicacion());
		UsuarioEntity usuario = usuarioRepository.getUsuarioById(medidorDTO.getId_usuario());
		ParametroEntity accion = parametroRepository.getAccion();
		EstadoMedidorEntity estadoMedidor = estadoMedidorRepository.getCreado();
		
		MedidorEntity medidor = (Boolean.FALSE.equals(existeMedidor)) ? new MedidorEntity() : medidorRepository.findId(medidorDTO.getId_medidor());
		medidor.setUbicacion(ubicacion);
		if(medidorDTO.getSerie() != null) {
		    medidor.setSerie(medidorDTO.getSerie().trim());
		}
		
		DyoobjectEntity dinamicoResponse = new DyoobjectEntity();
		if(Boolean.FALSE.equals(existeMedidor)) {
		    DyoobjectEntity di = new DyoobjectEntity();
		    di.setId(dyoobjectRepository.secuencia());
		    dinamicoResponse = dyoobjectRepository.save(di);
		}
		
		medidor.setModelo(modelo);
		if(Boolean.TRUE.equals(existeMedidor)) {
			medidor.setFecModif(new Date());
			medidor.setUsuarioModif(usuario);
		}else {
			//UbicacionEntity ubicacionAlmacen = ubicacionRepository.findId(Long.parseLong("1"));
			UbicacionEntity ubicacionAlmacen = ubicacionRepository.findId(medidorDTO.getId_ubicacion()); // CHR 25-07-23
			medidor.setId(medidorRepository.generaId());
			medidor.setNroComponente(medidorDTO.getNro_medidor().trim());
			medidor.setFecRegistro(new Date());
			medidor.setFecCreacion(new Date());
			medidor.setUsuarioRegistro(usuario);
			medidor.setActivo("S");
			medidor.setIdDyoObject(dinamicoResponse.getId());
			medidor.setCodTipComponente("ME");
			medidor.setUbicacion(ubicacionAlmacen);
			medidor.setTypeUbicacion("com.synapsis.synergia.med.domain.componente.Almacen");
			medidor.setEstadoMedidor(estadoMedidor);
			medidor.setReacondicionado("N");
			
		}
		MedidorEntity medidorResponse = medidorRepository.save(medidor);

		AuditEventEntity auditeventResponse = new AuditEventEntity();
		if((Boolean.FALSE.equals(existeMedidor))) {
			AuditEventEntity auditevent = new AuditEventEntity();
			auditevent.setId(auditEventRepository.generarIdAuditEvent());
			auditevent.setUseCase("auditevent");
			auditevent.setFechaEjecucion(new Date());
			auditevent.setObjectref("com.synapsis.synergia.med.domain.componente.Medidor");
			auditevent.setIdFk(medidorResponse.getId());
			auditevent.setUsuarioEntity(usuario);
			auditevent.setSpecificAuditevent("COMPONENTE");
			auditeventResponse = auditEventRepository.save(auditevent);
		}

		/**
		 * Se eliminan las medidas asignadas
		 */
		if(Boolean.TRUE.equals(existeMedidor)) {
			medidaMedidorRepository.deleteMedidor(medidorDTO.getId_medidor());
		}
		
		/**
		 * Se registra las medidas asiganadas.
		 */
		medidorDTO.getMedidas().stream().forEach(k -> {
			FactorEntity factor = factorRepository.findId(k.getId_factor());
			MedEntDecEntity entdec = medEntDecRepository.findId(k.getId_ent_dec());
			MedidaEntity medida = medidaRepository.findId(k.getId_medida());
			MedTipCalculoEntity tipocalculo = medTipCalculoRepository.findId(k.getId_tip_calculo()); // CHR 07-09-23 INC000115648937
			
			MedidaMedidorEntity med = new MedidaMedidorEntity();
			med.setId(medidaMedidorRepository.generaId());
			med.setFactor(factor);
			med.setMedida(medida);
			med.setMedidor(medidorResponse);
			med.setMedentdec(entdec);
			med.setCantDecimales(k.getCant_decimales());
			med.setCantEnteros(k.getCant_enteros());
			med.setValFactor(k.getFactor_valor());
			med.setMedtipcalculo(tipocalculo); // CHR 07-09-23 INC000115648937
			medidaMedidorRepository.save(med);
		});
		
		/** 
		 * Si se registra por primera vez se debe registrar en la tabla med_his_componente
		 */
		if(Boolean.FALSE.equals(existeMedidor)) {
			/*
			MovEstMedidorEntity estado = new MovEstMedidorEntity();
			estado.setId(movEstMedidorRepository.generaId());
			estado.setAccion(accion);
			estado.setEstadoFinal(estadoMedidor);
			estado.setEstadoInicial(estadoMedidor);
			estado.setFechaEstado(new Date());
			estado.setMedidor(medidorResponse);
			estado.setUbicacion(ubicacion);
			estado.setUltimoEstado(true);
			estado.setUsuario(usuario);
			
			estado = movEstMedidorRepository.save(estado); */

			MedHisComponenteEntity historia = new MedHisComponenteEntity();
			historia.setId(auditeventResponse.getId());
			historia.setFecDesde(new Date());
			historia.setEstadoFinal(estadoMedidor);
			historia.setUbicacion(ubicacion);
			historia.setMedidor(medidorResponse);
			historia.setTypeUbicacion("com.synapsis.scom.med.domain.componente.Almacen");
			historia.setFecHasta(null);

			medHisComponenteRepository.save(historia);
		}
		
		return medidorResponse;
	}

	/**
	 * Metodo para obtener el detalle del medidor, para poder editar y mostrar datos del medidor.
	 * @param id
	 */
	@Override
	public MedidorDataResponseDTO getData(Long id) {
		List<Object[]> objects =  medidorRepository.getData(id);
		List<Object[]> medidas = medidaMedidorRepository.getMedidor(id);
		List<MedidorMedidaResponseDTO> medidaDTO = new ArrayList<>();
	
		MedidorDataResponseDTO medidor = new MedidorDataResponseDTO();
		
		objects.stream().forEach(k -> {
			medidor.setId((k[0] == null) ? 0 : Long.valueOf(k[0].toString()));
			medidor.setId_marca((k[1] == null) ? 0 : Long.valueOf(k[1].toString()));
			medidor.setId_modelo(String.valueOf(k[2].toString())); // CHR 20-07-23
			medidor.setNumero((k[3] == null) ? "" : k[3].toString());
			medidor.setId_ubicacion((k[4] == null) ? 0 : Long.valueOf(k[4].toString()));
			medidor.setSerie((k[5] == null) ? "" : k[5].toString());
			medidor.setCod_modelo((k[6] == null) ? "" : k[6].toString());
			
			medidas.stream().forEach(t -> {
				MedidorMedidaResponseDTO medidasDTO = new MedidorMedidaResponseDTO();
				medidasDTO.setId_medida((t[0] == null) ? 0 : Long.valueOf(t[0].toString()));
				medidasDTO.setMedida_descripcion((t[1] == null) ? "" : t[1].toString());
				medidasDTO.setId_ent_dec((t[2] == null) ? 0 : Long.valueOf(t[2].toString()));
				medidasDTO.setCant_enteros((t[3] == null) ? "" : t[3].toString());
				medidasDTO.setCant_decimales((t[4] == null) ? "" : t[4].toString());
				medidasDTO.setId_factor((t[5] == null) ? 0 : Long.valueOf(t[5].toString()));
				medidasDTO.setFactor_codigo((t[6] == null) ? "" : t[6].toString());
				medidasDTO.setFactor_descripcion((t[7] == null) ? "" : t[7].toString());
				medidasDTO.setFactor_valor((t[8] == null) ? "" : t[8].toString());
				medidasDTO.setId_tip_calculo((t[9] == null) ? 0 : Long.valueOf(t[9].toString())); // CHR 07-09-23 INC000115648937
				medidaDTO.add(medidasDTO);
			});
			
			medidor.setMedidas(medidaDTO);
		});
		return medidor;
	}

	/**
	 * Metodo para eliminar el medidor
	 * @param id
	 */
	@Override
	public void delete(Long id) {
		medidorRepository.desactivar(id);
	}


}
