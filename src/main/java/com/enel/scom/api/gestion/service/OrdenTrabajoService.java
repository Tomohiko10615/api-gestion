package com.enel.scom.api.gestion.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.enel.scom.api.gestion.dto.request.TransferAnularRequestDTO;
import com.enel.scom.api.gestion.dto.response.OrdenTrabajoPaginacionResponseDTO;
import com.enel.scom.api.gestion.dto.response.OrdenTrabajoSCOMPaginacionResponseDTO;
import com.enel.scom.api.gestion.dto.response.TransferPaginacionResponseDTO;
import com.enel.scom.api.gestion.exception.AnularNotFoundException;

public interface OrdenTrabajoService {
	
	/**
	 * Metodo para obtener todas las ordenes de trabajo segun filtros
	 * @param paging
	 * @param idTipoOrden Tipo de Orden de trabajo
	 * @param fechaInicio Fecha de registro de la orden de trabajo
	 * @param fechaFin fecha de registro de la orden de trabajo
	 * @param numOrden numero de orden de trabajo
	 * @param nroCuenta numero de servicio relacionada con la tabla cliente
	 * @param idEstado estado de la orden de trabajo (tabla workflow)
	 * @return listado de ordenes filtradas
	 */
	Page<TransferPaginacionResponseDTO> getOrdenTransferenciaPaginacion(Pageable paging,
	String idTipoOrden, String fechaInicio, String fechaFin, String numOrden, Long numOrdenTdc, String nroCuenta, String estado, String codTipoProceso, Long codEstadTransfer, String codTipoOrdenEorder, String autoActivacion);

	
	/**
	 * Metodo que anula las ordenes de trabajo 
	 * @param ordenRequest listado de ordenes seleccionadas por el usuario para anular
	 * @return ordenes anuladas
	 */
	List<TransferAnularRequestDTO> anulacionMasivaTransferencia(List<TransferAnularRequestDTO> transferRequest) throws AnularNotFoundException;
	
	Page<OrdenTrabajoSCOMPaginacionResponseDTO> getOrdenTrabajoSCOMPaginacion(Pageable paging,
	String idTipoOrden, String fechaInicio, String fechaFin, String numOrden, String estado, String codTipoProceso, Long idServicio,String nroCuenta);
	
}
