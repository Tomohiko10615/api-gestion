package com.enel.scom.api.gestion.dto.response;

import lombok.Data;

@Data
public class TipoOrdenResponseDTO {
	
	private Long id;
	
	private String descripcion;
	
	private String codigoTipoOrden;

	private String codigoInterno;

	private String discriminador;
	
	private String codigoTipoProceso;
	
	private String nombreTipoProceso;
}
