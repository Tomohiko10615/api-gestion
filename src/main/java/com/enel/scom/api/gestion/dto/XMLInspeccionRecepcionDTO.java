package com.enel.scom.api.gestion.dto;

public interface XMLInspeccionRecepcionDTO {
    String getCodigo_Resultado();
    String getNro_informe_inspeccion();
    String getMotivoInspeccion();
    String getIdentificadorSuministro();
    String getTrabajo_coordinado();
    String getAprueba_Inspeccion();
    String getFecha_de_Notificacion_de_Aviso_intervencion();
    String getRegistro_de_cumplimiento_establecido_Art_171_RLCE();
    String getRegistro_de_cumplimiento_del_numeral_71();
    String getReg_eval_general_de_la_conexion_electrica();
    String getReg_Eval_Sistema_Medicion();
    String getFecha_Propuesta_Aviso_Intervencion();
    String getHora_Propuesta_Aviso_Previo_Intervencion();
    String getFecha_Recepcion_Aviso_Previo_Intervencion();
    String getHora_de_recepcion_del_Aviso_Previo_intervencion();
    String getPrueba_Vacio_AD();
    String getSticker_de_contraste();
    String getSin_mica_Mica_Rota();
    String getCaja_sin_tapa_Tapa_mal_estado();
    String getSin_cerradura_cerradura_en_mal_estado();
    String getEstadoConexion();
    String getCliente_permite_inventario_carga();
    String getCortado_art_90();
    String getESTADO_CAJA_PORTAMEDIDOR();
    String getTOMA_CON_REJA();
    String getCAJA_CON_REJA();
}
