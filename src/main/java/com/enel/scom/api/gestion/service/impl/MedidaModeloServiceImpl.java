package com.enel.scom.api.gestion.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.scom.api.gestion.dto.request.MedMedidaModeloRequestDTO;
import com.enel.scom.api.gestion.dto.response.FactorResponseDTO;
import com.enel.scom.api.gestion.dto.response.ModeloDataResponseDTO;
import com.enel.scom.api.gestion.dto.response.ModeloMedidaResponseDTO;
import com.enel.scom.api.gestion.exception.NroServicioNotFoundException;
import com.enel.scom.api.gestion.model.MedEntDecEntity;
import com.enel.scom.api.gestion.model.MedTipCalculoEntity;
import com.enel.scom.api.gestion.model.MedidaEntity;
import com.enel.scom.api.gestion.model.MedidaModeloEntity;
import com.enel.scom.api.gestion.model.ModeloEntity;
import com.enel.scom.api.gestion.repository.MedEntDecRepository;
import com.enel.scom.api.gestion.repository.MedTipCalculoRepository;
import com.enel.scom.api.gestion.repository.MedidaModeloRepository;
import com.enel.scom.api.gestion.repository.MedidaRepository;
import com.enel.scom.api.gestion.repository.ModeloRepository;
import com.enel.scom.api.gestion.service.MedidaModeloService;

@Service
public class MedidaModeloServiceImpl implements MedidaModeloService{

	@Autowired
	MedidaModeloRepository medidaModeloRepository;
	
	@Autowired
	ModeloRepository modeloRepository;
	
	@Autowired
	MedidaRepository medidaRepository;
	
	@Autowired
	MedTipCalculoRepository medTipCalculoRepository;
	
	@Autowired
	MedEntDecRepository medEntDecRepository;
	
	@Override
	public List<MedidaModeloEntity> getMedidaModelo(Long id) {
		
		List<MedidaModeloEntity> responses = medidaModeloRepository.getMedidaModelo(id);
		
		return responses;
	}

	@Override
	public MedidaModeloEntity crearMedidaModelo(MedMedidaModeloRequestDTO medModelo)
			throws NroServicioNotFoundException {
		ModeloEntity modeloEntity = modeloRepository.findId(medModelo.getIdModelo());
		MedidaEntity medida = medidaRepository.findId(medModelo.getIdMedida());
		MedEntDecEntity entdec = medEntDecRepository.findId(medModelo.getIdEntDec());
		MedTipCalculoEntity tipoCalculo = medTipCalculoRepository.findId(medModelo.getIdTipoCalculo()); //CHR 04-07-23
		
		MedidaModeloEntity medmod = new MedidaModeloEntity();
		medmod.setId(medidaModeloRepository.generaId());
		medmod.setModelo(modeloEntity);
		medmod.setMedida(medida);
		medmod.setEntdec(entdec);
		medmod.setTipoCalculo(tipoCalculo); //CHR 04-07-23
		medmod = medidaModeloRepository.save(medmod);
		return medmod;
	}

	@Override
	public List<ModeloMedidaResponseDTO> obtenerMedidasPorModelo(Long idModelo) {
		List<Object[]> medidas = medidaModeloRepository.getModelo(idModelo);
		List<ModeloMedidaResponseDTO> medidasDTO = new ArrayList<>();

			medidas.stream().forEach(t -> {
				ModeloMedidaResponseDTO medidaDTO = new ModeloMedidaResponseDTO();
				medidaDTO.setId_medida((t[0] == null) ? 0 : Long.valueOf(t[0].toString()));
				medidaDTO.setMedida_descripcion((t[1] == null) ? "" : t[1].toString());
				medidaDTO.setId_ent_dec((t[2] == null) ? 0 : Long.valueOf(t[2].toString()));
				medidaDTO.setCant_enteros((t[3] == null) ? "" : t[3].toString());
				medidaDTO.setCant_decimales((t[4] == null) ? "" : t[4].toString());
				medidaDTO.setId_medida_modelo((t[5] == null) ? 0 : Long.valueOf(t[5].toString()));
				medidaDTO.setDes_tip_calculo((t[6] == null) ? "" : t[6].toString());
				medidasDTO.add(medidaDTO);
	
			});
		
		return medidasDTO;
	}

}
