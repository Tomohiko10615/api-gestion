package com.enel.scom.api.gestion.dto.response;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class MedContrastePruebaDTO {
	
	private BigDecimal primerValor;
	private BigDecimal segundoValor;
	private BigDecimal tercerValor;
}
