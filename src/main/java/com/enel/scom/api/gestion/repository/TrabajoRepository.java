package com.enel.scom.api.gestion.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.model.TrabajoEntity;

@Repository
public interface TrabajoRepository extends JpaRepository<TrabajoEntity, Long>{
    
    @Query(value = "SELECT * FROM orm_trabajo WHERE id = :id", nativeQuery = true)
    TrabajoEntity getById(@Param("id") Long id);

}
