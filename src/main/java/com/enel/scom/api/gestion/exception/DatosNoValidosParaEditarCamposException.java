package com.enel.scom.api.gestion.exception;

public class DatosNoValidosParaEditarCamposException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DatosNoValidosParaEditarCamposException (String message) {
		super(message);
	}
}
