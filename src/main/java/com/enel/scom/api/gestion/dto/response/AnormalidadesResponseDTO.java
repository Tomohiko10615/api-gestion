package com.enel.scom.api.gestion.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AnormalidadesResponseDTO {
    public AnormalidadesResponseDTO(Long id, String codigo, String descripcion, String genCnr, String genOrdNorm, String desCausal) {
        this.id = id;
        this.codigo = codigo;
        this.descripcion = descripcion;
        this.genCnr = genCnr;
        this.genOrdNorm = genOrdNorm;
        this.desCausal = desCausal;
    }

    private Long id;
    private String codigo;
    private String descripcion;
    private String genCnr;
    private String genOrdNorm;
    private String desCausal;
}
