package com.enel.scom.api.gestion.oracle.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.oracle.model.SrvSetEntity;

@Repository
public interface SrvSetRepository extends JpaRepository<SrvSetEntity, Long> {
	
	/**
	 * Query para realizar autocomplete en SET CADENA ELECTRICA
	 * @param keyword
	 * @return
	 */
	
	@Query(value="SELECT srset.id_set, srset.cod_set FROM srv_set srset where srset.des_set like %:desSet% order by srset.cod_set asc", nativeQuery = true)
	public List<Object[]> buscarSetServicioElectrico(@Param("desSet") String desSet);
	
	@Query(value="SELECT * FROM srv_set srset where srset.id_set=:id", nativeQuery = true)
	public SrvSetEntity obtenerSetServicioElectricoPorId(@Param("id") Long id);
}
