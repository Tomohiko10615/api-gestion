package com.enel.scom.api.gestion.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.model.RolEntity;

import java.util.List;
import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<RolEntity, Long> {

    Optional<RolEntity> findByNombre(String rol);
    List<RolEntity> findByNombreIn(List<String> roles);
}
