package com.enel.scom.api.gestion.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.enel.scom.api.gestion.dto.request.ActivarSuministroResquestDTO;
import com.enel.scom.api.gestion.dto.request.OrdenContrasteRequestDTO;
import com.enel.scom.api.gestion.dto.response.ActivarSuministroResponseDTO;
import com.enel.scom.api.gestion.exception.ActivaSuministroNotFoundException;
import com.enel.scom.api.gestion.exception.ActivarSuministroInfoException;
import com.enel.scom.api.gestion.exception.NroServicioNotFoundException;
import com.enel.scom.api.gestion.model.OrdenEntity;
import com.enel.scom.api.gestion.service.ActivarSuministroService;

@RestController
@RequestMapping("/api/activar-suministro")
public class ActivarSuministroController {
	
	@Autowired
	ActivarSuministroService activarSuministroService;
	
	@GetMapping("/{idAutoAct}")
	public ResponseEntity<ActivarSuministroResponseDTO> obtenerDetalleActivarSuministro(@PathVariable Long idAutoAct) throws ActivaSuministroNotFoundException, ActivarSuministroInfoException {
		ActivarSuministroResponseDTO data = activarSuministroService.obtenerDetalleActivarSuministro(idAutoAct);
		return new ResponseEntity<>(data, HttpStatus.OK);
	}
	
	@PostMapping("/update")
	public ResponseEntity<ActivarSuministroResquestDTO> actualizarCadenaElectricaYrutas(@RequestBody ActivarSuministroResquestDTO dto) throws ActivaSuministroNotFoundException {
		ActivarSuministroResquestDTO response = activarSuministroService.actualizarCadenaElectricaYrutas(dto);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@PostMapping("/update/test/{idOperacion}")
	public ResponseEntity<String> testActivarSuministro(@PathVariable Long idOperacion) throws ActivaSuministroNotFoundException {
		String response = activarSuministroService.testActivarSuministro(idOperacion);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
}
