package com.enel.scom.api.gestion.dto.response;

import lombok.Data;

@Data
public class UbicacionResponseDto {
	public UbicacionResponseDto(Long id, String descripcion) {
		this.id = id;
		this.descripcion = descripcion;
	}
	
	private Long id;
	private String descripcion;
}
