package com.enel.scom.api.gestion.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.enel.scom.api.gestion.dto.response.ReporteGraficoDTO;
import com.enel.scom.api.gestion.dto.response.ReporteMedidoresPaginacionResponseDTO;
import com.enel.scom.api.gestion.service.ReporteMedidoresService;

import lombok.extern.slf4j.Slf4j;
@Slf4j
@RestController
@RequestMapping("api/reportes/medidores")
public class ReporteMedidoresController {
	

	@Autowired
	ReporteMedidoresService reporteMedidoresService;
	
	
	@GetMapping("/general")
    public ResponseEntity<Page<ReporteMedidoresPaginacionResponseDTO>> obtenerReporteMedidoresPaginacion(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size,
            @RequestParam(defaultValue = "id") String order,
            @RequestParam(defaultValue = "true") boolean asc,
			@RequestParam(required=false) String fechaInicio,
			@RequestParam(required=false) String fechaFin,
			@RequestParam(required=false) Long idEstadoMedidor,
			@RequestParam(required=false) String tipoUbicacion,
			@RequestParam(required=false) String nroCuenta,
			@RequestParam(required=false) String nroComponente
			,@RequestParam(required=false) String marcaMedidor // REQ # 8 - JEGALARZA
			,@RequestParam(required=false) String modeloMedidor // R.I. Corrección 23/10/2023
    ){
		log.info("GET /api/reportes/medidores/general INICIA");
		log.info(" page ="+page);
		log.info(" size ="+size);
		log.info(" order ="+order);
		log.info(" asc ="+asc);
		log.info(" fechaInicio ="+fechaInicio);
		log.info(" fechaFin ="+fechaFin);
		log.info(" idEstadoMedidor ="+idEstadoMedidor);
		log.info(" tipoUbicacion ="+tipoUbicacion);
		log.info(" nroCuenta ="+nroCuenta);
		log.info(" nroComponente ="+nroComponente);
		log.info(" marcaMedidor ="+marcaMedidor);
//        Page<ReporteMedidoresPaginacionResponseDTO> reportePag = reporteMedidoresService.obtenerReporteMedidoresPaginacion(
//                PageRequest.of(page, size, Sort.by(order)), fechaInicio, fechaFin, idEstadoMedidor, tipoUbicacion, nroCuenta, nroComponente);
    
		Page<ReporteMedidoresPaginacionResponseDTO> reportePag = reporteMedidoresService.obtenerReporteMedidoresPaginacion_V2(
                PageRequest.of(page, size, Sort.by(order)), fechaInicio, fechaFin, idEstadoMedidor, tipoUbicacion, nroCuenta, nroComponente, marcaMedidor, modeloMedidor); // R.I. Corrección 23/10/2023 // REQ # 8 - JEGALARZA
        
//       if(!asc)
//    	   reportePag = reporteMedidoresService.obtenerReporteMedidoresPaginacion(
//        			PageRequest.of(page, size, Sort.by(order)),fechaInicio, fechaFin, idEstadoMedidor, tipoUbicacion, nroCuenta, nroComponente);

       log.info("GET /api/reportes/medidores/general FIN"); 
       return new ResponseEntity<Page<ReporteMedidoresPaginacionResponseDTO>>(reportePag, HttpStatus.OK); 
    }
	
	@GetMapping("/cantidadXestado")
	public List<ReporteGraficoDTO> anulacionMasivaTransferencia(
			@RequestParam(required=false) String fechaInicio,
			@RequestParam(required=false) String fechaFin,
			@RequestParam(required=false) Long idEstadoMedidor,
			@RequestParam(required=false) String tipoUbicacion,
			@RequestParam(required=false) String nroCuenta) {
		
		log.info("GET /api/reportes/medidores/cantidadXestado");
		List<ReporteGraficoDTO> reporteCantXestado = reporteMedidoresService.obtenerReporteMedidoresCantidadXestado(
				fechaInicio, fechaFin, idEstadoMedidor, tipoUbicacion, nroCuenta);
		
		log.info("GET /api/reportes/medidores/cantidadXestado FIN");
		return reporteCantXestado;
	}

}
