package com.enel.scom.api.gestion.dto.request;

import lombok.Data;

@Data
public class AnormalidadesRequestDTO {
	private Long idAnormalidad;
	private Long idTarea;
	private String tareaEstado;
}
