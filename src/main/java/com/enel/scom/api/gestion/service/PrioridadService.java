package com.enel.scom.api.gestion.service;

import java.util.List;

import com.enel.scom.api.gestion.model.PrioridadEntity;

public interface PrioridadService {
    List<PrioridadEntity> getPrioridades();
}
