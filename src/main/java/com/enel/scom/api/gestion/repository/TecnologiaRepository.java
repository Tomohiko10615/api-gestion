package com.enel.scom.api.gestion.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.model.TecnologiaEntity;

@Repository
public interface TecnologiaRepository extends JpaRepository<TecnologiaEntity, Long>{
	@Query(value = "SELECT * FROM med_tecnologia WHERE activo = 'S' ORDER BY des_tecnologia ASC", nativeQuery = true)
	List<TecnologiaEntity> getTecnologiasActivo();
	
	@Query(value = "SELECT * FROM med_tecnologia WHERE id = :id", nativeQuery = true)
	TecnologiaEntity findId(@Param("id") Long id);
}
