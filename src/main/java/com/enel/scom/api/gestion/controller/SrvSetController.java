package com.enel.scom.api.gestion.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.enel.scom.api.gestion.dto.response.AutoCompleteResponseDTO;
import com.enel.scom.api.gestion.service.SrvSetService;

@RestController
@RequestMapping("api/srv-set")
public class SrvSetController {
	
	@Autowired
	private SrvSetService srvSetService;
	
	 @GetMapping("/search") 
	 public ResponseEntity<List<AutoCompleteResponseDTO>> buscarSetServicioElectrico(@RequestParam(required=false) String desSet) { 
	 List<AutoCompleteResponseDTO> listado = srvSetService.buscarSetServicioElectrico(desSet);
    	return new ResponseEntity<>(listado, HttpStatus.OK);
    }
}
