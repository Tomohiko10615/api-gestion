// R.I. REQSCOM03 14/07/2023 INICIO
package com.enel.scom.api.gestion.dto;

public interface MedidorDTO {
    String getAccionMedidor();
    String getMarcaMedidor();
    String getModeloMedidor();
    String getNumeroMedidor();
    String getTipoLectura();
    String getHorarioLectura();
    String getEstadoLeido();
    String getFechaLectura();
}
//R.I. REQSCOM03 14/07/2023 FIN