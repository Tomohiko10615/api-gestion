package com.enel.scom.api.gestion.dto.response;

import java.util.Date;

import lombok.Data;

@Data
public class OrdenTrabajoSCOMPaginacionResponseDTO {
	
	private Long id;
	
	private String nroOrden;
	
	private Long idTipoOrden;
	
	private String tipoOrden;
	
	private String codTipoOrden;
	
	private Date fechaCreacion;

	private Long nroServicio;
	
	private String estado;
	
	private Date fechaEstado;

	private String usuarioCreador;
	private Long idMotivo;
	
	// R.I. REQSCOM06 07/07/2023 INICIO
	private String fechaEjecucion;
	private String nroCuenta;
	private String distrito;
	private String observacionTecnico;
	// R.I. REQSCOM06 07/07/2023 FIN
	
	// R.I. REQSCOM06 Se agregó distrito y nroCuenta al constructor 09/10/2023
	public OrdenTrabajoSCOMPaginacionResponseDTO(Long id, String nroOrden, Long idTipoOrden, String tipoOrden,
			String codTipoOrden, Date fechaCreacion, Long nroServicio, String estado, Date fechaEstado,
			String usuarioCreador, Long idMotivo, String nroCuenta, String distrito) {
		super();
		this.id = id;
		this.nroOrden = nroOrden;
		this.idTipoOrden = idTipoOrden;
		this.tipoOrden = tipoOrden;
		this.codTipoOrden = codTipoOrden;
		this.fechaCreacion = fechaCreacion;
		this.nroServicio = nroServicio;
		this.estado = estado;
		this.fechaEstado = fechaEstado;
		this.usuarioCreador = usuarioCreador;
		this.idMotivo = idMotivo;
		this.nroCuenta = nroCuenta;
		this.distrito = distrito;
	}

	
	
	
	

}
