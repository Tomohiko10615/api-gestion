package com.enel.scom.api.gestion.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity(name = "fwk_auditevent")
@Table(name = "fwk_auditevent", schema = "schscom")
@Data
public class AuditEventEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column
	private Long id;
	
	@Column(name="usecase")
	private String useCase;
	
	@Column(name="objectref")
	private String objectref;
	
	@Column(name="id_fk")
	private Long idFk;
	
	@Column(name="fecha_ejecucion")
	private Date fechaEjecucion;
	
	@Column(name="specific_auditevent")
	private String specificAuditevent;
	
	@OneToOne
	@JoinColumn(name="id_user")
	private UsuarioEntity usuarioEntity;
}
