package com.enel.scom.api.gestion.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.model.BuzonEntity;

@Repository
public interface BuzonRepository extends JpaRepository<BuzonEntity, Long> {

	@Query(value = "SELECT * FROM ord_buzon WHERE id = :id", nativeQuery = true)
    BuzonEntity obtenerPorId(@Param("id") Long id);
	
}
