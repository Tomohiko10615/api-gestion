package com.enel.scom.api.gestion.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Entity (name = "com_parametros")
@Data
public class ParametroEntity implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id_parametro;
	
	@Column
	private String sistema;
	
	@Column
	private String entidad;
	
	@Column
	private String codigo;

	@Column
	private String descripcion;

	@Column(name = "valor_num")
	private Double valorNum;

	@Column(name = "valor_alf")
	private String valorAlf;

	@Column
	private String activo;

	@Column(name = "fecha_act")
	private Date fechaAct;

	@Column(name = "fecha_desac")
	private Date fechaDesac;

}
