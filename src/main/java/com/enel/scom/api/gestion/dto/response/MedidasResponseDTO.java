package com.enel.scom.api.gestion.dto.response;

import lombok.Data;

@Data
public class MedidasResponseDTO {
	private Long id_medida;
	private String des_medida;
	private Long id_ent_dec;
	private String cant_entero;
	private String cant_decimales;
	private Long id_factor;
	private String cod_factor;
	private String des_factor;
	private String val_factor;
	private Long id_tip_calculo; // CHR 07-09-23 INC000115648937
}
