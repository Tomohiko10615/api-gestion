package com.enel.scom.api.gestion.service;

import java.util.List;

import com.enel.scom.api.gestion.dto.response.OrmTareaResponseDTO;

public interface OrmTareaService {
    List<OrmTareaResponseDTO> getActivos(Long idTipo);
}
