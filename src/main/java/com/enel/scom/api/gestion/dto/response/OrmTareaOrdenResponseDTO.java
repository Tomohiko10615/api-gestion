package com.enel.scom.api.gestion.dto.response;

import lombok.Data;

@Data
public class OrmTareaOrdenResponseDTO {
    private Long idTarea;
    private String codigo;
    private String tarea;
    private String tipoTarea;
    private String cambio;
    private String contraste;
    private String valor;
}
