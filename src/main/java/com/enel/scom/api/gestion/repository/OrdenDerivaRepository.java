package com.enel.scom.api.gestion.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.model.OrdenDerivaEntity;

@Repository
public interface OrdenDerivaRepository extends JpaRepository<OrdenDerivaEntity, Long>{
    @Query(value = "SELECT * FROM ord_ord_deriv WHERE id_orden = :idOrden", nativeQuery = true)
    OrdenDerivaEntity findByIdOrden(@Param("idOrden") Long idOrden);
}
