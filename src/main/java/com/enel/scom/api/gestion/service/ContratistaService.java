package com.enel.scom.api.gestion.service;

import java.util.List;

import com.enel.scom.api.gestion.dto.response.ContratistaResponseDTO;

public interface ContratistaService {
	List<ContratistaResponseDTO> getActivos();
}
