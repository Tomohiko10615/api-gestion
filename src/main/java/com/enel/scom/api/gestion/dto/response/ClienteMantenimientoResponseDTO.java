package com.enel.scom.api.gestion.dto.response;

import java.util.List;

import lombok.Data;

@Data
public class ClienteMantenimientoResponseDTO {
    private Long nroServicio;
	private String nroCuenta;
	private String nombre;
	private String apellidoPat;
	private String apellidoMat;
	private String direccion;
	private String distrito;
	private String rutaLectura;
	private List<OrdenAnormalidadResponseDTO> anormalidades;
	private String nroComponente;
	private String codMarca;
	private String codTipoModelo;
	private String codFase;
	private String codModelo;
	private String documento;
	private String tipoAcometida;
}
