package com.enel.scom.api.gestion.dto.request;

import java.util.List;


import lombok.Data;

@Data
public class MedidorRequestDTO {
	private Long id_medidor;
	private Long id_ubicacion;
	private Long id_modelo;
	private String serie;
	private String nro_medidor;
	private Long id_usuario;
	private Long id_marca;
	
	private List<MedidaMedidorRequestDTO> medidas;
}
