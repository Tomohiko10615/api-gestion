package com.enel.scom.api.gestion.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.GeneratorType;

import commonj.sdo.Sequence;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;
import lombok.ToString;

@Entity
@Table(name="historial_actualizaciones_ord_insp_cnr")
@Data
@ToString
public class HistorialActualizacionesOrdInspCNR implements Serializable{
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	@GeneratedValue(generator = "SEC_ODON",strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name="SEC_ODON", sequenceName ="seq_historial_actualizaciones_ord_insp_cnr",allocationSize=1)
	public Long id;

	@Column(name = "request_id")
	public String requestId;
	
	@Column(name = "fecha")
	public Date fecha;

	@Column(name = "user_name")
	public String nombreUsuario;
	
	@Column(name = "id_orden")
	public Long idOrden;
	
	@Column(name = "id_orden_insp")
	public Long idOrdenInsp;

	@Column(name = "id_orden_medi_insp")
	public Long idOrdenMediInsp;
	
	@Column(name = "cnr_cod_expediente")
	public Long cnrCodExpediente;

	@Column(name = "nro_suministro")
	public Long nroSuministro;
	
	@Column(name = "nro_notificacion")
	public Long nro_notificacion;
	
	
	@Column(name = "id_magnitud")
	public Long idMagnitud;
	

	@Column(name = "cnr_cod_inspeccion")
	public Long codInspeccionCNR;
	
	@Column(name = "cnr_cod_valoriza")
	public Long cnrCodValoriza;
	
	@Column(name = "cnr_cod_valoriza_deta")
	public Long cnrCodValoriza_deta;
	

	@Column(name = "campo_actualizado")
	public String campo_actualizado;

	@Column(name = "valor_antes")
	public String valor_antes;

	@Column(name = "valor_despues")
	public String valor_despues;
	
	
	@Column(name = "comentario")
	public String comentario;


}
