package com.enel.scom.api.gestion.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.model.MovEstMedidorEntity;

@Repository
public interface MovEstMedidorRepository extends JpaRepository<MovEstMedidorEntity, Long>{
    @Query(value="SELECT nextval('sqmedmedidamedidor')", nativeQuery=true)
	public Long generaId();
}
