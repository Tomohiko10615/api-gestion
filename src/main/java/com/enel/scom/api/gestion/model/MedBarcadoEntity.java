package com.enel.scom.api.gestion.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;
import lombok.ToString;

//CREACION - REQ # 7 - JEGALARZA

@Entity(name = "med_barcode")
@Data
@ToString
public class MedBarcadoEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id_barcode")
	private Long idBarcode;
	
	@Column(name = "id_componente")
    private Long idComponente;

	@Column(name = "cod_primario_pallet")
	private String codPrimarioApe;

	@Column(name = "cod_secundario_pallet")
	private String codSecundarioApe;

	@Column(name = "cod_primario_caja")
	private String codPrimarioCaja;

	@Column(name = "cod_secundario_caja")
	private String codSecundarioCaja;

	@Column(name = "cod_medidor")
	private String codMedidor;

}