package com.enel.scom.api.gestion.dto.request;

import java.util.Date;

import lombok.Data;

@Data
public class MarcaFiltroRequestDTO {
	private String cod_marca;
	private String des_marca;
	private String usuario;
	private Date fecha_inicio;
	private Date fecha_fin;
}
