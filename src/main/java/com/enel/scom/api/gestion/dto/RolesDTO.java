package com.enel.scom.api.gestion.dto;

public class RolesDTO {
	
	private Long codRol;
	private String nombre;
	private String descripcion;
	private Long codPerfil;
	private String menu;
	private String nombreMenu;
	private String componente;
	private String icono;
	private Integer orden;
	
	public Long getCodRol() {
		return codRol;
	}
	public void setCodRol(Long codRol) {
		this.codRol = codRol;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Long getCodPerfil() {
		return codPerfil;
	}
	public void setCodPerfil(Long codPerfil) {
		this.codPerfil = codPerfil;
	}
	public String getMenu() {
		return menu;
	}
	public void setMenu(String menu) {
		this.menu = menu;
	}
	public String getNombreMenu() {
		return nombreMenu;
	}
	public void setNombreMenu(String nombreMenu) {
		this.nombreMenu = nombreMenu;
	}
	public String getComponente() {
		return componente;
	}
	public void setComponente(String componente) {
		this.componente = componente;
	}
	public String getIcono() {
		return icono;
	}
	public void setIcono(String icono) {
		this.icono = icono;
	}
	public Integer getOrden() {
		return orden;
	}
	public void setOrden(Integer orden) {
		this.orden = orden;
	}
	
	
	
}
