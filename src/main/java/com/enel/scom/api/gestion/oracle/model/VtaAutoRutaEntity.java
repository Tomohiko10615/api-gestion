package com.enel.scom.api.gestion.oracle.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;

@Entity(name = "vta_auto_ruta")
@Data
public class VtaAutoRutaEntity implements Serializable {/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="id_auto_ruta")
	private Long idAutoRuta;
	
    @Column(name="id_auto_act")
    private Long idAutoAct;
	
	@Column(name="id_srv_venta")
	private Long idSrvVenta;
	
	@Column(name="id_sector_lec")
	private Long idSectorLec;
	
	@Column(name="id_zona_lec")
	private Long idZonaLec;
	
	@Column(name="correlativo_lec")
	private String correlativoLec;
	
	@Column(name="id_sector_rep")
	private Long idSectorRep;
	
	@Column(name="id_zona_rep")
	private Long idZonaRep;
	
	@Column(name="correlativo_rep")
	private String correlativoRep;
	
	@Column(name="id_sector_fac")
	private Long idSectorFac;
	
	@Column(name="id_zona_fac")
	private Long idZonaFac;
	
	@Column(name="correlativo_fac")
	private String correlativoFac;
	
	@Column(name="id_sector_cor")
	private Long idSectorCor;
	
	@Column(name="id_zona_cor")
	private Long idZonaCor;
	
	@Column(name="correlativo_cor")
	private String correlativoCor;
	
	@Column(name="id_empresa")
	private Long idEmpresa;
}
