package com.enel.scom.api.gestion.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.model.ParametroEntity;

@Repository
public interface ParametroRepository extends JpaRepository<ParametroEntity, Long>{
	@Query(value = "SELECT * FROM schscom.com_parametros WHERE entidad = 'ACC_CAM_EST_MEDIDOR' AND codigo = 'CREA'", nativeQuery = true)
	ParametroEntity getAccion();
	
	@Query(value = "SELECT id_parametro, descripcion FROM schscom.com_parametros WHERE entidad = 'TIPO_INSPECCION' AND activo = 'S'", nativeQuery = true)
	List<Object[]> getTipoInspeccion();
	
	@Query(value = "select id_parametro, VALOR_ALF from schscom.COM_PARAMETROS WHERE SISTEMA ='SCOM' AND ENTIDAD ='GP_NORMALIZACION' AND CODIGO IN ('NORMAL','ALP') AND VALOR_NUM =0 ORDER BY VALOR_ALF ASC", nativeQuery = true)
	List<Object[]> getSubTipo();
	
	@Query(value = "SELECT * FROM schscom.com_parametros WHERE entidad = 'EMPRESA'", nativeQuery = true)
	ParametroEntity getEmpresa();
	
	@Query(value = "SELECT * FROM schscom.com_parametros WHERE id_parametro = :id", nativeQuery = true)
	ParametroEntity findId(@Param("id") Long id);
	
	/**
	 * Obtiene los 4 tipos de procesos que se manejan en el scom
	 * @return
	 */
	@Query(value = "SELECT cp.id_parametro, cp.codigo, cp.descripcion, cp.valor_num, cp.valor_alf "
			+ "FROM schscom.com_parametros cp WHERE cp.entidad = 'TIPO_PROCESO' AND activo = 'S' and cp.sistema='SCOM'", nativeQuery = true)
	List<Object[]> getTipoProceso();
	
	/**
	 * Obtiene estados de la orden de trabajo
	 * @return listado de estados
	 */
	@Query(value = "SELECT cp.id_parametro, cp.codigo, cp.descripcion, cp.valor_num, cp.valor_alf "
			+ "FROM schscom.com_parametros cp WHERE cp.entidad = 'ESTADO_ORDEN' AND activo = 'S' and cp.sistema='SCOM'", nativeQuery = true)
	List<Object[]> getEstados();
	
	/**
	 * Obtiene los estados de la transfer
	 * @return listado de estados
	 */
	@Query(value = "SELECT cp.id_parametro, cp.codigo, cp.descripcion, cp.valor_num, cp.valor_alf FROM schscom.com_parametros cp "
			+ "WHERE cp.entidad='ESTADO_TRANSFERENCIA' AND cp.sistema='EORDER' AND activo= 'S' ORDER BY CODIGO ASC", nativeQuery = true)
	List<Object[]> getEstadosTransferencia();
	
	@Query(value = "SELECT * FROM schscom.com_parametros WHERE entidad='ESTADO_TRANSFERENCIA' AND sistema='EORDER' AND id_parametro = :id", nativeQuery = true)
	ParametroEntity getEstadoTransferenciaPorId(@Param("id") Long id);
	
	@Query(value = "SELECT cp.id_parametro, cp.codigo, cp.descripcion, cp.valor_num, cp.valor_alf FROM schscom.com_parametros cp WHERE cp.entidad='TYPE_UBICACION_MED' AND cp.sistema='SCOM' "
			+ "ORDER BY cp.codigo ASC", nativeQuery = true)
	List<Object[]> getTiposUbicacionMedidor();
	
	@Query(value = "SELECT * FROM schscom.com_parametros WHERE sistema = 'FORCEBEAT' AND entidad = 'WSDL_WS' AND codigo = 'WSDL_ANUL'", nativeQuery = true)
	ParametroEntity getPuenteWsdl();
	
	@Query(value = "SELECT * FROM schscom.com_parametros WHERE entidad = 'LLAVE_SECRETA' AND codigo = 'WS_EOR007'", nativeQuery = true)
	ParametroEntity getParamLlave();
	
	@Query(value = "SELECT * FROM schscom.com_parametros WHERE sistema = 'FORCEBEAT' AND entidad = 'URL_WS' AND codigo = 'URL_ANUL'", nativeQuery = true)
	ParametroEntity getWsdlLocation();
	ParametroEntity findBySistemaAndEntidadAndCodigo(String sistema, String entidad, String codigo);
}
