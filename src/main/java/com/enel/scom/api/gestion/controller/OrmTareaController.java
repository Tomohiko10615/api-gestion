package com.enel.scom.api.gestion.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.enel.scom.api.gestion.dto.response.OrmTareaResponseDTO;
import com.enel.scom.api.gestion.service.OrmTareaService;

@RestController
@RequestMapping("/api/ormTarea")
public class OrmTareaController {
    @Autowired
    OrmTareaService ormTareaService;

    @GetMapping("/{idTipo}")
    public ResponseEntity<List<OrmTareaResponseDTO>> getActivos(@PathVariable Long idTipo) {
        List<OrmTareaResponseDTO> responses = ormTareaService.getActivos(idTipo);
        return new ResponseEntity<>(responses, HttpStatus.OK);
    }
}
