package com.enel.scom.api.gestion.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.scom.api.gestion.dto.response.UbicacionResponseDto;
import com.enel.scom.api.gestion.repository.UbicacionRepository;
import com.enel.scom.api.gestion.service.UbicacionService;

@Service
public class UbicacionServiceImpl implements UbicacionService{

	@Autowired
	private UbicacionRepository ubicacionRepository;
	
	@Override
	public List<UbicacionResponseDto> getAll() {
		List<Object[]> ubicaciones = ubicacionRepository.getAll();
		return ubicaciones.stream().map(object -> new UbicacionResponseDto(Long.valueOf(object[0].toString()), object[1].toString())).collect(Collectors.toList());
	}

}
