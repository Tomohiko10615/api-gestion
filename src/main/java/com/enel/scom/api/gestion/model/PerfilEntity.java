package com.enel.scom.api.gestion.model;

import javax.persistence.*;

import lombok.Data;

import java.io.Serializable;

@Entity
@Table(name = "perfil", schema = "schscom")
@Data
public class PerfilEntity implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "nombre", nullable = false)
    private String nombre;

    @Column(name="activo")
    private String activo;
    
    @Column(name = "param_nombre")
    private String paramNombre;
    
    private String descripcion;
    
    
}

