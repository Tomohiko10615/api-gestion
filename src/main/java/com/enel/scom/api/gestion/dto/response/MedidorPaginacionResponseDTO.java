package com.enel.scom.api.gestion.dto.response;

import java.util.Date;

import lombok.Data;

@Data
public class MedidorPaginacionResponseDTO {
	
	private Long id;
	private String nro_medidor;
	private String marca;
	private String modelo;
	private String estado;
	private Date fecha_estado;
	private Date fecha_creacion;
	private String usuario;

	public MedidorPaginacionResponseDTO(Long id, String nro_medidor, String marca, String modelo, String estado, Date fecha_estado, Date fecha_creacion, String usuario) {
		this.id = id;
		this.nro_medidor = nro_medidor;
		this.marca = marca;
		this.modelo = modelo;
		this.estado = estado;
		this.fecha_estado = fecha_estado;
		this.fecha_creacion = fecha_creacion;
		this.usuario = usuario;
	}
}
