package com.enel.scom.api.gestion.service;

import java.util.List;

import com.enel.scom.api.gestion.dto.response.OrdenXMLResponseDTO;
import com.enel.scom.api.gestion.dto.response.TransferResponseDTO;
import com.enel.scom.api.gestion.exception.NroServicioNotFoundException;

public interface TransferService {
    List<TransferResponseDTO> getIdOrden(Long idOrden);
    
	OrdenXMLResponseDTO obtenerXMLTransferencia(Long id, String accion) throws NroServicioNotFoundException;
}
