package com.enel.scom.api.gestion.dto;

import java.util.List;

import com.enel.scom.api.gestion.enums.EstadosRespuestaService;

import lombok.Data;

@Data
public class RespCamposActualizarDTO {

	public List<CampoActualizarCNRDTO> campos;
	public String mensaje;
	public EstadosRespuestaService estado;
}
