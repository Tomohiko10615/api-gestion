package com.enel.scom.api.gestion.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Entity(name = "med_unidad_medida_cte")
@Data
public class UnidadMedidaConstanteEntity implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "cod_unidad_medida_cte")
	private String codUniMedConst;
	
	@Column(name = "des_unidad_medida_cte")
	private String desUniMedConst;
	
	@Column
	private String activo;

}
