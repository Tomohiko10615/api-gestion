package com.enel.scom.api.gestion.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.enel.scom.api.gestion.dto.response.TareaAnormalidadResponseDTO;
import com.enel.scom.api.gestion.service.DisTareaAnormalidadService;

@RestController
@RequestMapping("/api/disTareaAnormalidad")
public class DisTareaAnormalidadController {

	@Autowired
	DisTareaAnormalidadService disTareaAnormalidadService;
	
	@GetMapping("/anormalidad/{id}")
	public ResponseEntity<List<TareaAnormalidadResponseDTO>> findByAnormalidad(@PathVariable("id") Long id) {
		List<TareaAnormalidadResponseDTO> responses = disTareaAnormalidadService.findByAnormalidad(id);
		return new ResponseEntity<>(responses, HttpStatus.OK); 
	}
}
