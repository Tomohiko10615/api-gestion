package com.enel.scom.api.gestion.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Entity(name = "ord_motivo_anulacion")
@Data
public class MotivoAnulacionEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "cod_motivo_anulacion")
	private String codigo;
	
	@Column(name = "des_motivo_anulacion")
	private String descripcion;
	
	@Column(name="activo")
	private String activo;
	
	@Column(name="cod_interno")
	private String codInterno;
}
