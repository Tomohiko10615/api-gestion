package com.enel.scom.api.gestion.service;

import java.util.List;

import com.enel.scom.api.gestion.dto.response.AutoCompleteResponseDTO;

public interface SrvSetService {
	
	/**
	 * Busca concidencidencias de codigo de SET CADENA ELECTRICA
	 * @param codSet
	 * @return
	 */
	List<AutoCompleteResponseDTO> buscarSetServicioElectrico(String codSet);
}
