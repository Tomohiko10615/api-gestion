package com.enel.scom.api.gestion.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.scom.api.gestion.dto.request.CambiarEstadoTareaRequestDTO;
import com.enel.scom.api.gestion.repository.OrdenTareaRepository;
import com.enel.scom.api.gestion.service.OrdenTareaService;

@Service
public class OrdenTareaServiceImpl implements OrdenTareaService{

	@Autowired
	OrdenTareaRepository ordenTareaRepository;
	
	@Override
	public void cambiarEstado(CambiarEstadoTareaRequestDTO cambiarEstado) {
		
		ordenTareaRepository.updateEstado(cambiarEstado.getTareaEstado(), cambiarEstado.getId());
	}

}
