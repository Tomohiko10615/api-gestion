// R.I. REQSCOM03 14/07/2023 INICIO
package com.enel.scom.api.gestion.dto;

public interface XmlGestionPerdidasDTO {

	String getCodigoResultado();
	String getSeCambioElMedidor();
	String getCodigoContratista();
	String getNroNotificacionInspeccion();
	String getCodidoTdcInspeccionAsociada();
	String getTipoAnomalia();
}
//R.I. REQSCOM03 14/07/2023 FIN