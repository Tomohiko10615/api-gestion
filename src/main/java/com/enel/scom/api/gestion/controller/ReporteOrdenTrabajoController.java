package com.enel.scom.api.gestion.controller;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.enel.scom.api.gestion.dto.response.ReporteGraficoDTO;
import com.enel.scom.api.gestion.dto.response.ReporteOrdenTrabajoPaginacionDTO;
import com.enel.scom.api.gestion.service.ReporteOrdenTrabajoService;
import com.enel.scom.api.gestion.util.Funciones;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("api/reportes/orden-trabajo")
public class ReporteOrdenTrabajoController {
	
	@Autowired
	ReporteOrdenTrabajoService reporteOrdenTrabajoService;
	

	@GetMapping("/general")
    public ResponseEntity<Page<ReporteOrdenTrabajoPaginacionDTO>> obtenerReporteOrdenesTrabajoPaginacion(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size,
            @RequestParam(defaultValue = "id") String order,
            @RequestParam(defaultValue = "true") boolean asc,
			@RequestParam(required=false) String fechaInicio,
			@RequestParam(required=false) String fechaFin,
			@RequestParam(required=false) String situacion,
			@RequestParam(required=false) String idTipoProceso,
			@RequestParam(required=false) String nroCuenta,
			@RequestParam(required=false) String idTipoOrden
    ){

		String uniqueID = UUID.randomUUID().toString();
		log.info("/general ini {}", uniqueID);
		log.info("/general ini {}", Funciones.logMemoryJVM());

        Page<ReporteOrdenTrabajoPaginacionDTO> reportePag = reporteOrdenTrabajoService.obtenerReporteOrdenesTrabajoPaginacion(
                PageRequest.of(page, size, Sort.by(order)), fechaInicio, fechaFin, situacion, idTipoProceso, nroCuenta, idTipoOrden);
    //    if(!asc)
    // 	   reportePag = reporteOrdenTrabajoService.obtenerReporteOrdenesTrabajoPaginacion(
    //     			PageRequest.of(page, size, Sort.by(order)),fechaInicio, fechaFin, situacion, idTipoProceso, nroCuenta, idTipoOrden);

		log.info("/general fin {}", Funciones.logMemoryJVM());
		log.info("/general fin {}", uniqueID);
        return new ResponseEntity<Page<ReporteOrdenTrabajoPaginacionDTO>>(reportePag, HttpStatus.OK); 
    }
	
	@GetMapping("/cantidadXsituacion")
	public List<ReporteGraficoDTO> anulacionMasivaTransferencia(
			@RequestParam(required=false) String fechaInicio,
			@RequestParam(required=false) String fechaFin,
			@RequestParam(required=false) String situacion,
			@RequestParam(required=false) String idTipoProceso,
			@RequestParam(required=false) String nroCuenta,
			@RequestParam(required = false) String idTipoOrden) {
		List<ReporteGraficoDTO> reporteCantXestado = reporteOrdenTrabajoService.obtenerReporteOrdenesTrabajoCantidadXsituacion(
				fechaInicio, fechaFin, situacion, idTipoProceso, nroCuenta, idTipoOrden);
		return reporteCantXestado;
	}
}
