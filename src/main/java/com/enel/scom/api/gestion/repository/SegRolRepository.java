package com.enel.scom.api.gestion.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.enel.scom.api.gestion.model.SegRolEntity;

public interface SegRolRepository extends JpaRepository<SegRolEntity, Long>{
    @Query(value = "SELECT id_rol, descripcion FROM seg_rol", nativeQuery = true)
    List<Object[]> getActivos();
}
