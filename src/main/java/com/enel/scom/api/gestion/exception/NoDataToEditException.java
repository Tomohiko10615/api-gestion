package com.enel.scom.api.gestion.exception;

public class NoDataToEditException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NoDataToEditException(String message) {
        super(message);
    }
}
