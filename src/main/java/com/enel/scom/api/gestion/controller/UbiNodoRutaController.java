package com.enel.scom.api.gestion.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.enel.scom.api.gestion.dto.response.AutoCompleteResponseDTO;
import com.enel.scom.api.gestion.service.UbiNodoRutaService;

@RestController
@RequestMapping("/api/ubi-nodo")
public class UbiNodoRutaController {
	
	@Autowired
	UbiNodoRutaService ubiNodoRutaService;
	
	 @GetMapping("/search/sector") 
	 public ResponseEntity<List<AutoCompleteResponseDTO>> buscarSectorServicioElectricoPorNivel(@RequestParam(required=false) String codNodo,
			 @RequestParam(required=false) Long idTipoRuta) { 
		 List<AutoCompleteResponseDTO> listado = ubiNodoRutaService.buscarSector(codNodo, idTipoRuta);
		 return new ResponseEntity<>(listado, HttpStatus.OK);
    }
	
	@GetMapping("/search/zona")
	public ResponseEntity<List<AutoCompleteResponseDTO>> buscarZonaServicioElectricoPorNivel(@RequestParam(required=false) String codNodo,
			@RequestParam(required=false) Long idTipoRuta, @RequestParam(required=false) Long idNodoPadre)  {
		List<AutoCompleteResponseDTO> listado = ubiNodoRutaService.buscarZonaPorSector(codNodo, idTipoRuta, idNodoPadre);
		return new ResponseEntity<>(listado, HttpStatus.OK);
	}

}
