package com.enel.scom.api.gestion.model;

import java.util.Date;

import javax.persistence.*;

import lombok.Data;

@Data
@Entity(name = "med_his_componente")
@Table(name = "med_his_componente", schema = "schscom")
public class MedHisComponenteEntity {
    
    @Id
    @Column(name = "id_his_componente")
    private Long id;

    @OneToOne
    @JoinColumn(name = "id_componente")
    private MedidorEntity medidor;

    @OneToOne
	@JoinColumn(name = "id_est_componente")
	private EstadoMedidorEntity estadoFinal;

    @Column(name = "fec_desde")
    private Date fecDesde;
    
    @Column(name = "fec_hasta")
    private Date fecHasta;

    @Column(name = "type_ubicacion")
    private String typeUbicacion;

    @OneToOne
	@JoinColumn(name = "id_ubicacion")
	private UbicacionEntity ubicacion;
}
