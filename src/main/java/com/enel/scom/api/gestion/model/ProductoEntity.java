package com.enel.scom.api.gestion.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import lombok.Data;

@Entity(name = "dis_producto")
@Data
public class ProductoEntity implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "cod_producto")
	private String codigo;
	
	@Column(name = "des_producto")
	private String descripcion;
	
	@Column
	private String sigla;
	
	@Column
	private String activo;
	
	@OneToOne
	@JoinColumn(name = "id_motivo")
	private MotivoEntity motivo;
}
