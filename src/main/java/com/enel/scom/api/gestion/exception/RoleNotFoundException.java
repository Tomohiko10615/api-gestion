package com.enel.scom.api.gestion.exception;

public class RoleNotFoundException extends AllNotFoundException {

	private static final long serialVersionUID = 1L;

	public RoleNotFoundException(String message) {
        super(message);
    }
}
