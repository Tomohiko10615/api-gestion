package com.enel.scom.api.gestion.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.scom.api.gestion.dto.response.JefeProductoResponseDTO;
import com.enel.scom.api.gestion.repository.JefeProductoRepository;
import com.enel.scom.api.gestion.service.JefeProductoService;

@Service
public class JefeProductoServiceImpl implements JefeProductoService{

	@Autowired
	JefeProductoRepository jefeProductoRepository;
	
	@Override
	public List<JefeProductoResponseDTO> getActivos() {
		
		List<Object[]> objects = jefeProductoRepository.getActivos();
		List<JefeProductoResponseDTO> dtos = new ArrayList<>();
		
		objects.stream().forEach(k -> {
			JefeProductoResponseDTO dto = new JefeProductoResponseDTO();
			dto.setId(Long.valueOf(k[0].toString()));
			dto.setNombre(k[1].toString());
			dto.setApellidoPat((k[2] == null) ? "-" : k[2].toString());
			dto.setApellidoMat((k[3] == null) ? "-" : k[3].toString());
			dtos.add(dto);
		});
		
		return dtos;
	}

}
