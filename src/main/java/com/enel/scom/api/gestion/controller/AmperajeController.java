package com.enel.scom.api.gestion.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.enel.scom.api.gestion.dto.response.AmperajeResponseDTO;
import com.enel.scom.api.gestion.service.AmperajeService;

@RestController
@RequestMapping("/api/amperaje")
public class AmperajeController {

	@Autowired
	AmperajeService amperajeService;
	
	@GetMapping
	public ResponseEntity<List<AmperajeResponseDTO>> getAll() {
		List<AmperajeResponseDTO> amperajes = amperajeService.getAll();
		return new ResponseEntity<>(amperajes, HttpStatus.OK);
	}
}
