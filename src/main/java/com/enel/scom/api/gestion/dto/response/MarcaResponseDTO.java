package com.enel.scom.api.gestion.dto.response;

import lombok.Data;

@Data
public class MarcaResponseDTO {
	private Long id;
	private String codigo;
	private String descripcion;
}
