package com.enel.scom.api.gestion.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.model.TareaEjecutadaEntity;

@Repository
public interface TareaEjecutadaRepository extends JpaRepository<TareaEjecutadaEntity, Long>{
    @Query(value="SELECT nextval('sqtareaejecutadaordenmantenimi')", nativeQuery=true)
	public Long generarId();

    @Modifying
	@Transactional
	@Query(value = "DELETE FROM orm_tarea_ejec WHERE id_orden = :id", nativeQuery = true)
	void deleteByIdOrden(@Param("id") Long id);
}
