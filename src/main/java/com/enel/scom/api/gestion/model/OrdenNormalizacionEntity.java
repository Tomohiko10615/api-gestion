package com.enel.scom.api.gestion.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

import lombok.Data;

@Entity(name = "dis_ord_norm")
@Data
public class OrdenNormalizacionEntity implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "id_orden")
	private Long idOrden;

	@OneToOne
	@JoinColumn(name = "id_ord_insp")
	private OrdenInspeccionEntity inspeccion;

	@OneToOne
	@JoinColumn(name = "id_subtipo")
	private ParametroEntity subtipo;

	@Column(name = "fec_normalizacion")
	private Date fecNormalizacion;	
	
	@Column(name = "subtipo_ord")
	private String subtipoOrd;
	
	@Column(name = "id_ejecutor_asig")
	private Long idEjecutorAsig;
	
}
