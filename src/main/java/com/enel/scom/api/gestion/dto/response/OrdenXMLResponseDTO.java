package com.enel.scom.api.gestion.dto.response;

import lombok.Data;

@Data
public class OrdenXMLResponseDTO {
    private String xml;
}
