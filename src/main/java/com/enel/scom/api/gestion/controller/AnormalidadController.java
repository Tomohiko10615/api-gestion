package com.enel.scom.api.gestion.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.enel.scom.api.gestion.dto.response.AnormalidadesResponseDTO;
import com.enel.scom.api.gestion.service.AnormalidadService;

@RestController
@RequestMapping("api/anormalidad")
public class AnormalidadController {

	@Autowired
	AnormalidadService anormalidadService;
	
	@GetMapping
	public ResponseEntity<List<AnormalidadesResponseDTO>> getActivos() {
		List<AnormalidadesResponseDTO> responses = anormalidadService.getActivos();
		return new ResponseEntity<>(responses, HttpStatus.OK);
	}
}
