package com.enel.scom.api.gestion.service;

import java.util.List;

import com.enel.scom.api.gestion.dto.response.AutoCompleteResponseDTO;

public interface SrvLlaveService {
	
	/**
	 * Busca concidencidencias de codigo de Llave CADENA ELECTRICA
	 * @param codigo
	 * @param idSed
	 * @return
	 */
	List<AutoCompleteResponseDTO> buscarLlaveServicioElectrico(String codLlave, Long idSed);
}
