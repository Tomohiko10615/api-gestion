package com.enel.scom.api.gestion.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.model.OrdenTareaEntity;

@Repository
public interface OrdenTareaRepository extends JpaRepository<OrdenTareaEntity, Long>{
	@Query(value="SELECT nextval('sqtareaorden')", nativeQuery=true)
	public Long generaId();
	
	@Query(value = "SELECT count(*) from dis_ord_tarea WHERE id_orden = :idOrden", nativeQuery = true)
	int existeOrden(@Param("idOrden") Long idOrden);

	@Modifying
	@Transactional
	@Query(value = "DELETE FROM dis_ord_tarea WHERE id_orden = :id", nativeQuery = true)
	void deleteByIdOrden(@Param("id") Long id);
	/**
	 * Se utiliza para las tareas de las ordenes de normalización y contraste
	 */
	@Query(value = "select dot.id, da.des_anormalidad, dt.des_tarea, dt.cod_tarea, da.cod_anormalidad, dot.tarea_estado, dot.id_tarea, dot.id_anormalidad\r\n"
			+ "from dis_ord_tarea dot\r\n"
			+ "inner join dis_anormalidad da on dot.id_anormalidad = da.id\r\n"
			+ "inner join dis_tarea dt on dot.id_tarea = dt.id\r\n"
			//+ "where dot.id_orden = :id order by dt.des_tarea asc", nativeQuery = true)
			+ "where dot.id_orden = :id order by cast(dt.cod_tarea as integer)", nativeQuery = true) // R.I. Corrección 25/10/2023
	List<Object[]> findByIdOrden(@Param("id") Long id);
	/**
	 * Se utiliza para las irregularidades de las ordenes de normalización y contraste
	 */
	@Query(value = "select distinct dot.id_anormalidad, da.cod_anormalidad, da.des_anormalidad, da.gen_cnr, da.gen_orden_norm, dc.des_causal from dis_ord_tarea dot\r\n"
			+ "inner join dis_anormalidad da on dot.id_anormalidad = da.id\r\n"
			+ "inner join dis_causal dc on da.id_causal = dc.id\r\n"
			+ "where dot.id_orden = :id order by da.des_anormalidad asc", nativeQuery = true)
	List<Object[]> findByIdOrdenAndAnormalidades(@Param("id") Long id);

	@Modifying
	@Transactional
	@Query(value = "UPDATE dis_ord_tarea SET tarea_estado = :estado WHERE id = :id", nativeQuery = true)
	void updateEstado(@Param("estado") String estado, @Param("id") Long id);
}
