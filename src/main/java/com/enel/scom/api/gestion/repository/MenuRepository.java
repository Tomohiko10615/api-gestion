package com.enel.scom.api.gestion.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.model.MenuEntity;

@Repository
public interface MenuRepository extends JpaRepository<MenuEntity, Long>{
	
	@Query(value = 
			"select m.id, m.nombre, m.descripcion, m.icono, m.url, m.id_padre, m.param_nombre, m.nombre_modulo, m.clase, m.titulo_grupo " + 
			"from schscom.menu m " +
		    "inner join schscom.perfil_menu pm on m.id = pm.id_menu " +
			"where pm.id_perfil = :idPerfil and m.activo='S'", nativeQuery = true)
	List<Object[]> findMenusByPerfil(@Param("idPerfil") Long idPerfil);
	
	@Query(value = 
			"select m.id, m.nombre, m.descripcion, m.icono, m.url, m.id_padre, m.param_nombre, m.nombre_modulo, m.clase, m.titulo_grupo " + 
			"from schscom.menu m " +
		    "inner join schscom.perfil_menu pm on m.id = pm.id_menu " +
			"where pm.id_perfil =:idPerfil and m.activo='S' and m.id_padre is null", nativeQuery = true)
	List<Object[]> findMenuPadreByIdPerfil(@Param("idPerfil") Long idPerfil);
	
	@Query(value = 
			"select m.id, m.nombre, m.descripcion, m.icono, m.url, m.id_padre, m.param_nombre, m.nombre_modulo, m.clase, m.titulo_grupo " + 
			"from schscom.menu m " +
			"inner join schscom.perfil_menu pm on m.id = pm.id_menu " +
			"where m.activo='S' and m.id_padre =:idMenu and pm.id_perfil=:idPerfil order by m.orden asc", nativeQuery = true)
	List<Object[]> findSubMenuByidMenu(@Param("idMenu") Long idMenu,
										@Param("idPerfil") Long idPerfil);
}
