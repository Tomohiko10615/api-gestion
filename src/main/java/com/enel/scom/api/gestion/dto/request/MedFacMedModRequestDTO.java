package com.enel.scom.api.gestion.dto.request;

import lombok.Data;

@Data
public class MedFacMedModRequestDTO {
	private Long idMedidaModelo;
	private Long idFactor;
}
