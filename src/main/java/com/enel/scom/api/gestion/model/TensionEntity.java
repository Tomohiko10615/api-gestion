package com.enel.scom.api.gestion.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Entity(name = "com_tension")
@Data
public class TensionEntity implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "cod_tension")
	private String codTension;
	
	@Column(name = "des_tension")
	private String desTension;
	
	@Column(name = "cod_interno")
	private String codInterno;
	
	@Column
	private String activo;
	
}
