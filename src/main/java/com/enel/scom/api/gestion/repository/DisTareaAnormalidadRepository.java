package com.enel.scom.api.gestion.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.model.DisTareaAnormalidadEntity;

@Repository
public interface DisTareaAnormalidadRepository extends JpaRepository<DisTareaAnormalidadEntity, Long>{
	@Query(value = "select dta.id_anormalidad, dta.id_tarea, dt.des_tarea, dt.cod_tarea, da.cod_anormalidad\r\n"
			+ "from dis_tarea_anor dta\r\n"
			+ "inner join dis_anormalidad da on dta.id_anormalidad = da.id\r\n"
			+ "inner join dis_tarea dt on dta.id_tarea = dt.id\r\n"
			+ "where da.id = :id "
			//+ "order by dt.des_tarea asc", nativeQuery = true)
			+ "order by cast(dt.cod_tarea as integer)", nativeQuery = true) // R.I. Corrección 25/10/202
	List<Object[]> findByIdAnormalidad(@Param("id") Long id);
}
