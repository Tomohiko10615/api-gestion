package com.enel.scom.api.gestion.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.model.TipoOrdenEntity;

@Repository
public interface TipoOrdenRepository extends JpaRepository<TipoOrdenEntity, Long>{
	
	@Query(value = "SELECT tord.id, tord.des_tipo_orden, tord.cod_tipo_orden FROM schscom.ord_tipo_orden tord WHERE tord.cod_tipo_proceso=:codTipoProceso", nativeQuery = true)
	List<Object[]> obtenerTipoOrdenByCodProceso(@Param("codTipoProceso") String codTipoProceso);

	@Query(value = "SELECT * FROM schscom.ord_tipo_orden WHERE des_tipo_orden = 'NORMALIZACION'", nativeQuery = true)
	TipoOrdenEntity getNormalizacion();

	@Query(value = "SELECT * FROM schscom.ord_tipo_orden WHERE des_tipo_orden = 'Orden de Contraste'", nativeQuery = true)
	TipoOrdenEntity getContraste();

	@Query(value = "SELECT * FROM schscom.ord_tipo_orden WHERE des_tipo_orden = 'INSPECCION'", nativeQuery = true)
	TipoOrdenEntity getInspeccion();

	@Query(value = "SELECT * FROM ord_tipo_orden WHERE cod_tipo_orden = 'MANT'", nativeQuery = true)
	TipoOrdenEntity getMantenimiento();

	
	@Query(value = "SELECT tord.id FROM schscom.ord_tipo_orden tord WHERE tord.cod_tipo_proceso=:codTipoProceso", nativeQuery = true)
	String[] obtenerIdsTipoOrdenByCodProceso(@Param("codTipoProceso") String codTipoProceso);
}
