package com.enel.scom.api.gestion.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.enel.scom.api.gestion.model.TipoMedicionEntity;
import com.enel.scom.api.gestion.repository.TipoMedicionRepository;

@RestController
@RequestMapping("/api/tipo-medicion")
public class TipoMedicionController {

	@Autowired
	TipoMedicionRepository tipoMedicionRepository;
	
	@GetMapping
	public ResponseEntity<List<TipoMedicionEntity>> getAll() {
		List<TipoMedicionEntity> tipos = tipoMedicionRepository.getTipoMedicionActivo();
		return new ResponseEntity<>(tipos, HttpStatus.OK);
	}
}
