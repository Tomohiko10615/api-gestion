package com.enel.scom.api.gestion.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.scom.api.gestion.dto.response.AnormalidadesResponseDTO;
import com.enel.scom.api.gestion.repository.AnormalidadRepository;
import com.enel.scom.api.gestion.service.AnormalidadService;

@Service
public class AnormalidadServiceImpl implements AnormalidadService{

	@Autowired
	AnormalidadRepository anormalidadRepository;

	@Override
	public List<AnormalidadesResponseDTO> getActivos() {
		
		List<Object[]> anormalidades = anormalidadRepository.getAll();

		return anormalidades.stream().map(object -> new AnormalidadesResponseDTO(Long.valueOf(object[0].toString()), object[1].toString(), object[2].toString(), object[3].toString(), object[4].toString(), object[5].toString())).collect(Collectors.toList());
	}
}
