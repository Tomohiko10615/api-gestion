package com.enel.scom.api.gestion.exception;

public class AllNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public AllNotFoundException(String message) {
        super(message);
    }
}
