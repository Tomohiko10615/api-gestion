package com.enel.scom.api.gestion.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.enel.scom.api.gestion.dto.response.AutoCompleteResponseDTO;
import com.enel.scom.api.gestion.service.SrvSedService;

@RestController
@RequestMapping("api/srv-sed")
public class SrvSedController {
	
	@Autowired
	private SrvSedService srvSedService;
	
	 @GetMapping("/search") 
	 public ResponseEntity<List<AutoCompleteResponseDTO>> buscarSedServicioElectrico(@RequestParam(required=false) String desSed, 
			 @RequestParam(required=false) Long idAlimentador) { 
	 List<AutoCompleteResponseDTO> listado = srvSedService.buscarSedServicioElectrico(desSed, idAlimentador);
    	return new ResponseEntity<>(listado, HttpStatus.OK);
    }
}
