package com.enel.scom.api.gestion.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.enel.scom.api.gestion.dto.response.AccesoUsuarioResponseDTO;
import com.enel.scom.api.gestion.model.UsuarioEntity;
import com.enel.scom.api.gestion.service.UsuarioService;

import lombok.extern.slf4j.Slf4j;

import java.util.List;
@Slf4j
@RestController
@CrossOrigin
@RequestMapping("/api/usuario")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;
    
    @GetMapping("/findAll")
    public List<UsuarioEntity> findAll() {
        List<UsuarioEntity> list = usuarioService.buscarTodosUsuarios();
        return list;
    }

    @GetMapping("/findById/{id}")
    public UsuarioEntity findById(@PathVariable("id") Long id) {
        UsuarioEntity u = usuarioService.buscarPorIdUsuario(id).get();
        return u;
    }

    
    @PutMapping("/update-usuario/{username}")
    public void updateFechaUltimoAccesoUsuario(@PathVariable("username") String username) {
    	usuarioService.updateFechaUltimoAccesoUsuario(username);
    }
    
    @PostMapping("/update-perfil/{username}/{idPerfil}")
    public AccesoUsuarioResponseDTO updatePerfilUsuario(@PathVariable("username") String username,
    		@PathVariable("idPerfil") String idPerfil) {
    	
    	log.info("/api/usuario/update-perfil/{}/{}",  username,idPerfil);
    	
    	return usuarioService.updatePerfilUsuario(username, idPerfil);
    }
    
}
