package com.enel.scom.api.gestion.dto.response;

import lombok.Data;

@Data
public class ParametroResponseDTO {
	private Long id;
	private String codigo;
	private String descripcion;
	private String valorAlf;
	private Double valorNum;
}
