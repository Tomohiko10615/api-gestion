package com.enel.scom.api.gestion.oracle.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.oracle.model.SrvAlimentadorEntity;
import com.enel.scom.api.gestion.oracle.model.SrvLlaveEntity;

@Repository
public interface SrvLlaveRpository extends JpaRepository<SrvLlaveEntity, Long> {
	
	/**
	 * Query para realizar autocomplete en Llave CADENA ELECTRICA
	 * @param desAlimentador
	 * @param idSet
	 * @return
	 */
	
	@Query(value="SELECT ave.id_llave, ave.codigo, ave.descripcion FROM srv_llave ave where ave.id_sed=:idSed and ave.descripcion like %:descripcion% order by ave.descripcion asc", nativeQuery = true)
	public List<Object[]> buscarLlaveServicioElectrico(@Param("descripcion") String descripcion, @Param("idSed") Long idSed);
	
	
	@Query(value="SELECT * FROM srv_llave ave where ave.id_llave=:id", nativeQuery = true)
	public SrvLlaveEntity obtenerLlaveServicioElectricoPorId(@Param("id") Long id);
}
