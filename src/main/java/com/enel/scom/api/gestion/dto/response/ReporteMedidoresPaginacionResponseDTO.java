package com.enel.scom.api.gestion.dto.response;

import java.util.Date;

import lombok.Data;

@Data
public class ReporteMedidoresPaginacionResponseDTO {
	private String tipoComponente;
	private String marca;
	private String modelo;
	private String nroMedidor;
	private String estado;
	private String fechaDesde;
	private String fechaHasta;
	private String ubicacion;
	private String username;
	private String nroCuenta;
	private String codPrimarioApe; // INICIO - REQ # 7 - JEGALARZA
	private String codSecundarioApe;
	private String codPrimarioCaja;
	private String codSecundarioCaja;
	private String codMedidor; // FIN - REQ # 7 - JEGALARZA
	public ReporteMedidoresPaginacionResponseDTO(String tipoComponente, String marca, String modelo, String nroMedidor,
			String estado, String fechaDesde, String fechaHasta, String ubicacion, String username, String nroCuenta,
			String codPrimarioApe, String codSecundarioApe, String codPrimarioCaja, String codSecundarioCaja,
			String codMedidor) { // REQ # 7 - JEGALARZA
		this.tipoComponente = tipoComponente;
		this.marca = marca;
		this.modelo = modelo;
		this.nroMedidor = nroMedidor;
		this.estado = estado;
		this.fechaDesde = fechaDesde;
		this.fechaHasta = fechaHasta;
		this.ubicacion = ubicacion;
		this.username = username;
		this.nroCuenta = nroCuenta;
		this.codPrimarioApe = codPrimarioApe; // INICIO - REQ # 7 - JEGALARZA
		this.codSecundarioApe = codSecundarioApe;
		this.codPrimarioCaja = codPrimarioCaja;
		this.codSecundarioCaja = codSecundarioCaja;
		this.codMedidor = codMedidor; // FIN - REQ # 7 - JEGALARZA
	}
	
	
	
	
}
