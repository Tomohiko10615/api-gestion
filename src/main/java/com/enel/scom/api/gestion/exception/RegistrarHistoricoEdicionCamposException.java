package com.enel.scom.api.gestion.exception;

public class RegistrarHistoricoEdicionCamposException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public RegistrarHistoricoEdicionCamposException(String message) {
		super(message);
	}

}
