package com.enel.scom.api.gestion.dto.response;

import java.util.Date;

import lombok.Data;

@Data
public class OrdenTrabajoResponseDTO {
	
	private Long id;
	
	private String nroOrden;
	
	private Long idTipoOrden;
	
	private String tipoOrden;
	
	private String idEstado;
	
	private String estado;
	
	private Date fechaEstado;
	
}
