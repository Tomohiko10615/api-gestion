package com.enel.scom.api.gestion.service;

import java.util.List;

import com.enel.scom.api.gestion.dto.response.AmperajeResponseDTO;

public interface AmperajeService {

	List<AmperajeResponseDTO> getAll();
}
