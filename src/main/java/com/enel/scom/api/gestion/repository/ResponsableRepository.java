package com.enel.scom.api.gestion.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.enel.scom.api.gestion.model.ResponsableEntity;

public interface ResponsableRepository extends JpaRepository<ResponsableEntity, Long>{
    @Query(value = "select u.id, u.username from ord_responsable ore "
                    + "inner join usuario u on ore.id_object = u.id "
                    + "where tipo_responsable = 'Usuario' and activo = true "
                    + "order by u.username asc", nativeQuery = true)
    List<Object[]> getUsuarios();

    @Query(value = "select u.id, u.descripcion from ord_responsable ore "
                    + "inner join seg_area u on ore.id_object = u.id "
                    + "where tipo_responsable = 'Area' "
                    + "order by u.descripcion asc", nativeQuery = true)
    List<Object[]> getAreas();

    @Query(value = "select u.id_rol, u.descripcion from ord_responsable ore "
                    + "inner join seg_rol u on ore.id_object = u.id_rol "
                    + "where tipo_responsable = 'Rol' "
                    + "order by u.descripcion asc", nativeQuery = true)
    List<Object[]> getRoles();

    @Query(value = "SELECT * FROM ord_responsable WHERE id_object = :id and tipo_responsable = :area", nativeQuery = true)
    ResponsableEntity getById(@Param("id") Long id, @Param("area") String area);
}
