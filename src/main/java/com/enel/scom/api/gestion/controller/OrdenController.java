package com.enel.scom.api.gestion.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.enel.scom.api.gestion.dto.response.OrdenDataResponseDTO;
import com.enel.scom.api.gestion.dto.response.OrdenXMLResponseDTO;
import com.enel.scom.api.gestion.exception.NroServicioNotFoundException;
import com.enel.scom.api.gestion.service.OrdenService;

@RestController
@RequestMapping("api/orden")
public class OrdenController {

	@Autowired
	OrdenService ordenService;
	
	@GetMapping("/{id}/{tipo}")
	public ResponseEntity<OrdenDataResponseDTO> getData(@PathVariable Long id, @PathVariable Long tipo) throws NroServicioNotFoundException {
		OrdenDataResponseDTO data = ordenService.getData(id, tipo);
		return new ResponseEntity<>(data, HttpStatus.OK);
	}

	@GetMapping("/obtenerXMLenvio/{idOrden}")
	public ResponseEntity<OrdenXMLResponseDTO> getOrdenEnvioXML(@PathVariable Long idOrden) throws NroServicioNotFoundException {
		OrdenXMLResponseDTO ordenEnvio = ordenService.getDetalleXMLEnvio(idOrden);
		return new ResponseEntity<>(ordenEnvio, HttpStatus.OK);
	}
	
	@GetMapping("/obtenerXMLRecepcion/{idOrden}")
	public ResponseEntity<OrdenXMLResponseDTO> getOrdenRecepcionXML(@PathVariable Long idOrden) throws NroServicioNotFoundException {
		OrdenXMLResponseDTO ordenRecepcion = ordenService.getDetalleXMLRecepcion(idOrden);
		return new ResponseEntity<>(ordenRecepcion, HttpStatus.OK);
	}
	
	@GetMapping("orden-scom/{id}")
	public ResponseEntity<OrdenDataResponseDTO> obtenerDetalleOrdenesScom(@PathVariable Long id) throws NroServicioNotFoundException {
		OrdenDataResponseDTO data = ordenService.obtenerDetalleOrdenesScom(id);
		return new ResponseEntity<>(data, HttpStatus.OK);
	}
}
