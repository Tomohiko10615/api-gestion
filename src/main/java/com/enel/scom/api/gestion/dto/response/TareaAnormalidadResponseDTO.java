package com.enel.scom.api.gestion.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TareaAnormalidadResponseDTO {
    public TareaAnormalidadResponseDTO(Long idAnormalidad, Long idTarea, String desTarea, String codTarea, String codAnormalidad) {
        this.idAnormalidad = idAnormalidad;
        this.idTarea = idTarea;
        this.desTarea = desTarea;
        this.codTarea = codTarea;
        this.codAnormalidad = codAnormalidad;
    }

    private Long idAnormalidad;
    private Long idTarea;
    private String desTarea;
    private String codTarea;
    private String codAnormalidad;
}
