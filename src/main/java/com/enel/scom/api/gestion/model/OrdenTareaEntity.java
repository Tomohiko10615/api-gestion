package com.enel.scom.api.gestion.model;

import java.io.Serializable;

import javax.persistence.*;

import lombok.Data;
import lombok.ToString;

@Entity(name = "dis_ord_tarea")
@Data
@ToString
public class OrdenTareaEntity implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column
	private Long id;
	
	@OneToOne
	@JoinColumn(name = "id_orden")
	private OrdenEntity orden;
	
	@OneToOne
	@JoinColumn(name = "id_anormalidad")
	private AnormalidadEntity anormalidad;
	
	@OneToOne
	@JoinColumn(name = "id_tarea")
	private TareaEntity tarea;
	
	@Column(name = "tarea_estado", length = 1)
	private String tareaEstado;

    @Column(length = 1)
    private String ejecutado;
}
