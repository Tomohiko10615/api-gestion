package com.enel.scom.api.gestion.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.scom.api.gestion.dto.response.AutoCompleteResponseDTO;
import com.enel.scom.api.gestion.oracle.repository.SrvSetRepository;
import com.enel.scom.api.gestion.service.SrvSetService;

@Service
public class SrvSetServiceImpl implements SrvSetService {
	
	@Autowired
	private SrvSetRepository srvSetRepository;

	@Override
	public List<AutoCompleteResponseDTO> buscarSetServicioElectrico(String codSet) {
		List<Object[]> setsCadenaElectrica = srvSetRepository.buscarSetServicioElectrico(codSet);
		return setsCadenaElectrica.stream().map(object -> new AutoCompleteResponseDTO(
				Long.valueOf(object[0].toString()), 
				object[1].toString().trim()
				)).collect(Collectors.toList());
	}
}
