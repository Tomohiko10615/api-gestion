package com.enel.scom.api.gestion.dto.response;

import java.math.BigDecimal;

import lombok.Data;



/***
 * clase utilizada como objeto de respuesta del servicio
 * uso: controller de actualizacion de ordenes de inspeccion
 * requerimiento: 
 * fecha: 2023.08.29
 * 
 * 
 * @author Indra
 *
 */
@Data
public class OrdenInspeccionActualizaDatosCNRResponseDTO  {

	public Long idOrden;
	public Long idOrdenInsp; /*dato importante para actualizar la fecha de notificacion en SCOM*/
	public Long idOrdenMediInsp; /*dato importante para actualizar las cargas en SCOM*/
	public Long nroSuministro;
	public Long nroNotificacion;

	public Long codExpedienteCNR; /*dato importante para actualizar en CNR*/
	public String nombreExpedienteCNR; 

	
	public Long idMagnitud;
	public Long CodInspeccionCNR;
	
	public Double cargaR;
	public Double cargaS;
	public Double cargaT;
	
	public Integer lecturaNotificacion;
	
	public String fechaNotificacion;
	public String escenarioDescripcion;
	public String tipoCliente;

	
}
