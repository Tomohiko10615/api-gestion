package com.enel.scom.api.gestion.service.impl;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.transaction.Transaction;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.enel.scom.api.gestion.dto.ObservacionDTO;
import com.enel.scom.api.gestion.dto.XMLInspeccionRecepcionDTO;
import com.enel.scom.api.gestion.dto.XmlCargasDTO;
import com.enel.scom.api.gestion.dto.XmlDTO;
import com.enel.scom.api.gestion.dto.CampoActualizarCNRDTO;
import com.enel.scom.api.gestion.dto.RespCamposActualizarDTO;
import com.enel.scom.api.gestion.dto.RespuestaProcesoServiceDTO;
import com.enel.scom.api.gestion.dto.request.ActualizaDatosCNR_RequestDTO;
import com.enel.scom.api.gestion.dto.request.OrdenInspeccionRequestDTO;
import com.enel.scom.api.gestion.dto.response.OrdenTareaResponseDTO;
import com.enel.scom.api.gestion.enums.EnumCamposActualizaOrdInspCNR;
import com.enel.scom.api.gestion.enums.EstadosRespuestaService;
import com.enel.scom.api.gestion.exception.ActualizarValoresDeCamposException;
import com.enel.scom.api.gestion.exception.DatosNoValidosParaEditarCamposException;
import com.enel.scom.api.gestion.exception.NoDataToEditException;
import com.enel.scom.api.gestion.exception.NroServicioNotFoundException;
import com.enel.scom.api.gestion.exception.RegistrarHistoricoEdicionCamposException;
import com.enel.scom.api.gestion.dto.response.ActualizaOrdIspecCNRResponseDTO;
import com.enel.scom.api.gestion.dto.response.HistorialActualizacionesResponseDTO;

import com.enel.scom.api.gestion.dto.response.OrdenAnormalidadResponseDTO;
import com.enel.scom.api.gestion.dto.response.OrdenDataResponseDTO;
import com.enel.scom.api.gestion.dto.response.OrdenInspeccionActualizaDatosCNRResponseDTO;
import com.enel.scom.api.gestion.model.ClienteEntity;
import com.enel.scom.api.gestion.model.DisSegInspEntity;
import com.enel.scom.api.gestion.model.HistorialActualizacionesOrdInspCNR;
import com.enel.scom.api.gestion.model.JefeProductoEntity;
import com.enel.scom.api.gestion.model.MotivoEntity;
import com.enel.scom.api.gestion.model.ObservacionEntity;
import com.enel.scom.api.gestion.model.OrdenContratistaEntity;
import com.enel.scom.api.gestion.model.OrdenEntity;
import com.enel.scom.api.gestion.model.OrdenInspeccionEntity;
import com.enel.scom.api.gestion.model.ProductoEntity;
import com.enel.scom.api.gestion.model.TipoOrdenEntity;
import com.enel.scom.api.gestion.model.UsuarioEntity;
import com.enel.scom.api.gestion.model.WorkflowEntity;
import com.enel.scom.api.gestion.oracle.model.MedMagnitudEntity;
import com.enel.scom.api.gestion.oracle.repository.MedMagnitudRepository;
import com.enel.scom.api.gestion.repository.ClienteRepository;
import com.enel.scom.api.gestion.repository.ContratistaRepository;
import com.enel.scom.api.gestion.repository.DisSegInpsRepository;
import com.enel.scom.api.gestion.repository.HistorialActualizacionesOrdInspCNRRepository;
import com.enel.scom.api.gestion.repository.JefeProductoRepository;
import com.enel.scom.api.gestion.repository.MotivoRepository;
import com.enel.scom.api.gestion.repository.ObservacionRepository;
import com.enel.scom.api.gestion.repository.OrdenContratistaRepository;
import com.enel.scom.api.gestion.repository.OrdenInspeccionRepository;
import com.enel.scom.api.gestion.repository.OrdenRepository;
import com.enel.scom.api.gestion.repository.OrdenTareaRepository;
import com.enel.scom.api.gestion.repository.ParametroRepository;
import com.enel.scom.api.gestion.repository.ProductoRepository;
import com.enel.scom.api.gestion.repository.TipoOrdenRepository;
import com.enel.scom.api.gestion.repository.TransferRepository;
import com.enel.scom.api.gestion.repository.UsuarioRepository;
import com.enel.scom.api.gestion.repository.WorkflowRepository;
import com.enel.scom.api.gestion.service.OrdenInspeccionService;
import com.enel.scom.api.gestion.util.Constantes;
import com.enel.scom.api.gestion.util.Funciones;

import javassist.NotFoundException;
import lombok.extern.slf4j.Slf4j;

/**
 * Servicio principal donde se realiza el registro, actualización y detalle de
 * la inspección
 * 
 * @author Gilmar Moreno
 * @version 1.0
 */
@Slf4j
@Service
public class OrdenInspeccionServiceImpl implements OrdenInspeccionService {

	@Autowired
	OrdenInspeccionRepository ordenInspeccionRepository;

	@Autowired
	OrdenRepository ordenRepository;

	@Autowired
	ContratistaRepository contratistaRepository;

	@Autowired
	MotivoRepository motivoRepository;

	@Autowired
	ProductoRepository productoRepository;

	@Autowired
	JefeProductoRepository jefeProductoRepository;

	@Autowired
	ParametroRepository parametroRepository;

	@Autowired
	UsuarioRepository usuarioRepository;

	@Autowired
	ObservacionRepository observacionRepository;

	@Autowired
	ClienteRepository clienteRepository;

	@Autowired
	OrdenTareaRepository ordenTareaRepository;

	@Autowired
	WorkflowRepository wkfWorkflowRepository;

	@Autowired
	TipoOrdenRepository tipoOrdenRepository;

	@Autowired
	DisSegInpsRepository disSegInpsRepository;

	@Autowired
	OrdenContratistaRepository ordenContratistaRepository;
	
	@Autowired
	HistorialActualizacionesOrdInspCNRRepository historialActualizacionesOrdInspCNRRepository; 
	
	@PersistenceContext
	EntityManager entityManager;
	
	/*nueva referencia a repositorio 203.09.26 Impacto SCOM CNR, REQCNR01*/
	@Autowired
	MedMagnitudRepository medMagnitudRepository;
	
	MedMagnitudEntity oMedActualiza = null;
	
	
	// R.I. REQSCOM06 11/07/2023 INICIO
	@Autowired
	TransferRepository transferRepository;
	// R.I. REQSCOM06 11/07/2023 FIN

	/**
	 * Se utiliza para registrar y actualizar la orden de inspección
	 */
	@Override
	public OrdenEntity storeUpdate(OrdenInspeccionRequestDTO ordenInspeccionDTO) throws NroServicioNotFoundException {
		/**
		 * Para crear una orden de inspección no debe tener una orden abierta.
		 */
		TipoOrdenEntity tipoOrden = tipoOrdenRepository.getInspeccion();
		ClienteEntity cliente = clienteRepository.findByNroServicio(ordenInspeccionDTO.getNroServicio());
		WorkflowEntity workflow = wkfWorkflowRepository.estadoActualOrden(cliente.getIdServicio(), tipoOrden.getId());

		OrdenEntity ordenResponse = new OrdenEntity();

		if (ordenInspeccionDTO.getIdOrdenInspeccion() == 0) {
			if (workflow != null) {
				if (workflow.getIdState().equals("Finalizada") || workflow.getIdState().equals("Recibida")
						|| workflow.getIdState().equals("Anulada")) {

					UsuarioEntity usuario = usuarioRepository.getUsuarioById(ordenInspeccionDTO.getIdUsuario());
					ProductoEntity producto = productoRepository.findId(ordenInspeccionDTO.getIdProducto());
					JefeProductoEntity jefeProducto = jefeProductoRepository
							.findId(ordenInspeccionDTO.getIdJefeProducto());
					MotivoEntity motivo = motivoRepository.findId(ordenInspeccionDTO.getIdMotivo());

					Boolean ordenExiste = ordenRepository.existsById(ordenInspeccionDTO.getIdOrden());

					Integer existeOrdenContratista = ordenContratistaRepository
							.existeOrden(ordenInspeccionDTO.getIdOrden());

					// OrdenEntity ordenResponse = new OrdenEntity();
					DisSegInspEntity disSegInspEntityResponse = new DisSegInspEntity();
					if (Boolean.FALSE.equals(ordenExiste)) {

						WorkflowEntity wkf = new WorkflowEntity();
						wkf.setId(wkfWorkflowRepository.generaId());
						wkf.setIdDescriptor("WFORDENINSPECCION");
						wkf.setIdOldState("Emitida");
						wkf.setIdState("Emitida");
						wkf.setParentType("com.enel.scom.api.gestion.service.impl.OrdenInspeccionImpl");
						wkf.setVersion(0);
						wkf.setFechaUltimoEstado(new Date());
						WorkflowEntity wkfResponse = wkfWorkflowRepository.save(wkf);

						OrdenEntity orden = new OrdenEntity();
						orden.setId(ordenRepository.generaId());
						orden.setNroOrden(Long.toString(ordenInspeccionRepository.generarNumeroOrden()));
						orden.setActivo(true);
						orden.setUsuarioCrea(usuario);
						orden.setFechaRegistro(new Date());
						orden.setFechaCreacion(new Date());
						orden.setUsuarioRegistro(usuario);
						orden.setMotivo(motivo);
						orden.setWorkflow(wkfResponse);
						orden.setTipoOrden(tipoOrden);
						orden.setIdServicio(cliente.getIdServicio());
						orden.setDiscriminador("INSPECCION");
						ordenResponse = ordenRepository.save(orden);

						WorkflowEntity wkf2 = new WorkflowEntity();
						wkf2.setId(wkfWorkflowRepository.generaId());
						wkf2.setIdDescriptor("SEGINS");
						wkf2.setIdOldState("SInspGenerada");
						wkf2.setIdState("SInspGenerada");
						wkf2.setParentType("com.synapsis.synergia.dis.domain.SeguimientoInspeccion");
						wkf2.setVersion(0);
						wkf2.setFechaUltimoEstado(new Date());
						WorkflowEntity wkfResponse2 = wkfWorkflowRepository.save(wkf);

						DisSegInspEntity dse = new DisSegInspEntity();
						dse.setId(disSegInpsRepository.generaId());
						dse.setIdWfk(wkfResponse2.getId());
						disSegInspEntityResponse = disSegInpsRepository.save(dse);
					}

					if (Boolean.TRUE.equals(ordenExiste)) {
						OrdenEntity ordenUpdate = ordenRepository.findId(ordenInspeccionDTO.getIdOrden());
						ordenUpdate.setMotivo(motivo);
						ordenRepository.save(ordenUpdate);
					}
					/**
					 * deprecado, se usaba antes cuando el SCOM asignaba un tecnico.
					 */
					if (ordenInspeccionDTO.getIdContratista() > 0) {
						OrdenContratistaEntity ordenContratista = (existeOrdenContratista == 0)
								? new OrdenContratistaEntity()
								: ordenContratistaRepository.findByIdOrden(ordenInspeccionDTO.getIdOrden());
						ordenContratista.setIdContratista(ordenInspeccionDTO.getIdContratista());
						if (Boolean.FALSE.equals(ordenExiste)) {
							ordenContratista.setId(ordenContratistaRepository.secuencia());
							ordenContratista.setIdOrden(ordenResponse.getId());
						} else {
							ordenContratista.setIdOrden(ordenInspeccionDTO.getIdOrden());
						}
						ordenContratistaRepository.save(ordenContratista);
					}

					OrdenInspeccionEntity ordenInspeccion = (Boolean.FALSE.equals(ordenExiste))
							? new OrdenInspeccionEntity()
							: ordenInspeccionRepository.findId(ordenInspeccionDTO.getIdOrdenInspeccion());
					String autogenerada = (ordenInspeccionDTO.getAutogenerado() == "true") ? "S" : "N";
					String infoComplementaria = (ordenInspeccionDTO.getInformacionComplementaria() == "true") ? "S"
							: "N";
					ordenInspeccion.setTipInspeccion(ordenInspeccionDTO.getTipInspeccion());
					ordenInspeccion.setProducto(producto);
					ordenInspeccion.setJefeProducto(jefeProducto);
					ordenInspeccion.setEntreCalles(ordenInspeccionDTO.getEntreCalles());
					ordenInspeccion.setAutoGenerada(autogenerada);
					ordenInspeccion.setComInfoCompleta(infoComplementaria);
					if (Boolean.FALSE.equals(ordenExiste)) {
						ordenInspeccion.setId(ordenResponse.getId());
						ordenInspeccion.setIdOrden(ordenResponse.getId());
						ordenInspeccion.setDisSegInspEntity(disSegInspEntityResponse);
					}

					OrdenInspeccionEntity response = ordenInspeccionRepository.save(ordenInspeccion);

					if (ordenInspeccionDTO.getIdOrden() == 0) {
						ObservacionEntity observacion = new ObservacionEntity();
						observacion = new ObservacionEntity();
						observacion.setIdOrden(ordenResponse.getId());
						observacion.setUsuario(usuario);
						observacion.setStateName("CREADA");
						observacion.setDiscriminator("ObservacionOrden");
						observacion.setFechaObservacion(new Date());
						observacion.setIdObservacion(observacionRepository.generaId());
						observacion.setTexto(ordenInspeccionDTO.getObservaciones());

						observacionRepository.save(observacion);
					} else {
						ObservacionEntity observacion2 = (observacionRepository
								.existeIdOrden(ordenInspeccionDTO.getIdOrden()) == 0) ? new ObservacionEntity()
										: observacionRepository.findIdOrden(ordenInspeccionDTO.getIdOrden());

						if (observacionRepository.existeIdOrden(ordenInspeccionDTO.getIdOrden()) == 0) {
							observacion2.setIdOrden(ordenInspeccionDTO.getIdOrden());
							observacion2.setUsuario(usuario);
							observacion2.setStateName("CREADA");
							observacion2.setDiscriminator("ObservacionOrden");
							observacion2.setFechaObservacion(new Date());
							observacion2.setIdObservacion(observacionRepository.generaId());
						}

						observacion2.setTexto(ordenInspeccionDTO.getObservaciones());
						System.out.println(observacion2);
						System.out.println("observacion a registrar");
						observacionRepository.save(observacion2);
					}

				} else {
					throw new NroServicioNotFoundException(
							"No puede crear la orden por que tiene una inspección abierta.");
				}
			}
		}

		return ordenResponse;
	}

	/**
	 * Se obtiene la orden de inspección para editar y mostrar el detalle.
	 * 
	 * @param id
	 */
	@Override
	public OrdenDataResponseDTO getId(Long id) {

		List<Object[]> ordenInspeccion = ordenInspeccionRepository.findIdOrden(id);
		List<Object[]> tareas = ordenTareaRepository.findByIdOrden(id);
		List<Object[]> anormalidades = ordenTareaRepository.findByIdOrdenAndAnormalidades(id);
		List<OrdenTareaResponseDTO> detalleDTO = new ArrayList<>();
		List<OrdenAnormalidadResponseDTO> anormalidadDTO = new ArrayList<>();
		String constrista = ordenInspeccionRepository.contratista(id);
		String ejecutor = ordenInspeccionRepository.ejecutor(id);
		String estadoWkf = wkfWorkflowRepository.estado(id);
		// INICIO - REQ # 4 - JEGALARZA
		String estadoTrans = wkfWorkflowRepository.estadoTrans(id);

		OrdenDataResponseDTO ordenDTO = new OrdenDataResponseDTO();
		ordenInspeccion.stream().forEach(k -> {
			ordenDTO.setIdOrden(Long.valueOf(k[0].toString()));
			ordenDTO.setFechaCreacion((k[1] == null) ? new Date() : (Date) k[1]);
			ordenDTO.setFechaFinalizacion((k[2] == null) ? new Date() : (Date) k[2]);
			ordenDTO.setNroOrden((k[3] == null) ? "" : k[3].toString());
			ordenDTO.setMotivo((k[4] == null) ? "" : k[4].toString());
			ordenDTO.setIdMotivo((k[5] == null) ? 0 : Long.valueOf(k[5].toString()));
			ordenDTO.setUsuarioCreacion((k[6] == null) ? "" : k[6].toString());
			ordenDTO.setEntreCalles((k[7] == null) ? "" : k[7].toString());
			ordenDTO.setAutogenerado(
					(StringUtils.isBlank(k[8].toString()) || (k[8].toString().equals("N"))) ? false : true);
			ordenDTO.setInformacionComplementaria(
					(StringUtils.isBlank(k[9].toString()) || (k[9].toString().equals("N"))) ? false : true);
			ordenDTO.setJefeProducto((k[10] == null) ? "" : k[10].toString());
			ordenDTO.setIdJefeProducto((k[11] == null) ? 0 : Long.valueOf(k[11].toString()));
			ordenDTO.setContratista((k[12] == null) ? "" : k[12].toString());
			ordenDTO.setIdContratista((k[13] == null) ? 0 : Long.valueOf(k[13].toString()));
			ordenDTO.setProducto((k[14] == null) ? "" : k[14].toString());
			ordenDTO.setIdProducto((k[15] == null) ? 0 : Long.valueOf(k[15].toString()));
			ordenDTO.setTipoInspeccion((k[16] == null) ? "" : k[16].toString());
			ordenDTO.setNroServicio((k[17] == null) ? "" : k[17].toString());
			ordenDTO.setNroCuenta((k[18] == null) ? "" : k[18].toString());
			ordenDTO.setNombre((k[19] == null) ? "" : k[19].toString());
			ordenDTO.setApellidoPat((k[20] == null) ? "" : k[20].toString());
			ordenDTO.setApellidoMat((k[21] == null) ? "" : k[21].toString());
			ordenDTO.setDistrito((k[22] == null) ? "" : k[22].toString());
			ordenDTO.setRutaLectura((k[23] == null) ? "" : k[23].toString());
			ordenDTO.setObservacion((k[24] == null) ? "" : k[24].toString());
			ordenDTO.setIdOrdenInspeccion((k[25] == null) ? 0 : Long.valueOf(k[25].toString()));
			ordenDTO.setDireccion((k[26] == null) ? "" : k[26].toString());
			ordenDTO.setNroNotificacion((k[27] == null) ? "" : k[27].toString());
			ordenDTO.setFechaNotificacion((k[28] == null) ? new Date() : (Date) k[28]);
			ordenDTO.setIdOrdenSc4j((k[29] == null) ? 0 : Long.valueOf(k[29].toString()));
			ordenDTO.setContratista((constrista == null) ? "" : constrista);
			ordenDTO.setEjecutor((ejecutor == null) ? "" : ejecutor);
			ordenDTO.setEstadoWkf((estadoWkf == null) ? "" : estadoWkf);
			ordenDTO.setEstadoTrans((estadoTrans == null) ? "" : estadoTrans); // REQ # 4 - JEGALARZA
			// FIN - REQ # 4 - JEGALARZA
			tareas.stream().forEach(t -> {
				OrdenTareaResponseDTO tarea = new OrdenTareaResponseDTO();
				tarea.setId((t[0] == null) ? 0 : Long.valueOf(t[0].toString()));
				tarea.setAnormalidad((t[1] == null) ? "" : t[1].toString());
				tarea.setTarea((t[2] == null) ? "" : t[2].toString());
				tarea.setCodTarea((t[3] == null) ? "" : t[3].toString());
				tarea.setCodAnormalidad((t[4] == null) ? "" : t[4].toString());
				tarea.setTareaEstado((t[5] == null) ? "" : t[5].toString());
				tarea.setIdTarea((t[6] == null) ? 0 : Long.valueOf(t[6].toString()));
				tarea.setIdAnormalidad((t[7] == null) ? 0 : Long.valueOf(t[7].toString()));
				detalleDTO.add(tarea);
			});

			ordenDTO.setTareas(detalleDTO);

			anormalidades.stream().forEach(a -> {
				OrdenAnormalidadResponseDTO anormalidad = new OrdenAnormalidadResponseDTO();
				anormalidad.setId((a[0] == null) ? 0 : Long.valueOf(a[0].toString()));
				anormalidad.setCodigo((a[1] == null) ? "" : a[1].toString());
				anormalidad.setDescripcion((a[2] == null) ? "" : a[2].toString());
				anormalidad.setGeneraCNR((a[3] == null) ? "" : a[3].toString());
				anormalidad.setGeneraOrdenNormalizacion((a[4] == null) ? "" : a[4].toString());
				anormalidad.setCausal((a[5] == null) ? "" : a[5].toString());
				anormalidadDTO.add(anormalidad);
			});

			ordenDTO.setAnormalidades(anormalidadDTO);
		});

		// R.I. REQSCOM06 11/07/2023 INICIO
		try {
			String fechaCreacionOrden = ordenRepository.obtenerFechaCreacion(ordenDTO.getNroOrden());
			ordenDTO.setFechaCreacionOrden(fechaCreacionOrden);
		} catch (Exception e) {
			e.printStackTrace();
		}
		/*
		try {
			XmlDTO xml = transferRepository.obtenerXmlOrdenTrabajo(ordenDTO.getNroOrden(), 8L);
			if (xml != null) {
				 * Ya no se obtiene del XML if (xml.getObservacion() != null) {
				 * ordenDTO.setObservacionTecnico(xml.getObservacion()); } else {
				 * ordenDTO.setObservacionTecnico(""); }
				ordenDTO.setFechaEjecucion(xml.getFechaEjecucion());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		*/
		// Se obtiene ahora de las tablas
		try {
			ObservacionDTO observacion = ordenRepository.obtenerObservacionTecnicoINSP(id);
	        ordenDTO.setObservacionTecnico(observacion.getTexto());
	        ordenDTO.setFechaEjecucion(observacion.getFechaObservacion());
		} catch (Exception e) {
			e.printStackTrace();
		}
		// R.I. REQSCOM06 11/07/2023 FIN
		
		// R.I. Se requiere que se obtenga de la BBDD 16/10/2023 INICIO
		try {
			ordenDTO.setCodigoResultado(ordenInspeccionRepository.getCodigoResultado(id));
		} catch (Exception e) {
			ordenDTO.setCodigoResultado("");
			e.printStackTrace();
		}
		// R.I. Se requiere que se obtenga de la BBDD 16/10/2023 FIN

		// R.I. CORRECCION 11/09/2023 INICIO
		
		try {
			ordenDTO.setIdentificadorSuministro(ordenDTO.getNroCuenta());
			String motivoInspeccion = ordenRepository.obtenerMotivoInspeccion(id);
			ordenDTO.setMotivoInspeccion(motivoInspeccion);
			XMLInspeccionRecepcionDTO xmlInsp = transferRepository.obtenerXmlInspeccionRecepcion(ordenDTO.getNroOrden(),
					8L);
			if (xmlInsp != null) {
				/*
				ordenDTO.setCodigoResultado(xmlInsp.getCodigo_Resultado() != null
						? parametroRepository.getCodigoResultado(xmlInsp.getCodigo_Resultado())
						: "");
						*/
				ordenDTO.setNroInformeInspeccion(
						xmlInsp.getNro_informe_inspeccion() != null ? xmlInsp.getNro_informe_inspeccion() : "");
				//ordenDTO.setMotivoInspeccion(
				//		xmlInsp.getMotivoInspeccion() != null ? xmlInsp.getMotivoInspeccion() : "");

				// ordenDTO.setIdentificadorSuministro(xmlInsp.getIdentificadorSuministro() !=
				// null ? xmlInsp.getIdentificadorSuministro() : "");

				ordenDTO.setTrabajoCoordinado(
						xmlInsp.getTrabajo_coordinado() != null ? xmlInsp.getTrabajo_coordinado() : "");
				ordenDTO.setApruebaInspeccion(
						xmlInsp.getAprueba_Inspeccion() != null ? xmlInsp.getAprueba_Inspeccion() : "");
				ordenDTO.setFechaNotificacionAvisoIntervencion(
						xmlInsp.getFecha_de_Notificacion_de_Aviso_intervencion() != null
								? xmlInsp.getFecha_de_Notificacion_de_Aviso_intervencion()
								: "");
				ordenDTO.setRegistroCumplimientoArt171RLCE(
						xmlInsp.getRegistro_de_cumplimiento_establecido_Art_171_RLCE() != null
								? xmlInsp.getRegistro_de_cumplimiento_establecido_Art_171_RLCE()
								: "");
				ordenDTO.setRegistroCumplimientoNumeral71NormaReintegrosRecuperos(
						xmlInsp.getRegistro_de_cumplimiento_del_numeral_71() != null
								? xmlInsp.getRegistro_de_cumplimiento_del_numeral_71()
								: "");
				ordenDTO.setEvalGeneralConexionElectrica(xmlInsp.getReg_eval_general_de_la_conexion_electrica() != null
						? xmlInsp.getReg_eval_general_de_la_conexion_electrica()
						: "");
				ordenDTO.setEvalSistemaMedicion(
						xmlInsp.getReg_Eval_Sistema_Medicion() != null ? xmlInsp.getReg_Eval_Sistema_Medicion() : "");
				ordenDTO.setFechaPropuestaInicioAvisoIntervencion(
						xmlInsp.getFecha_Propuesta_Aviso_Intervencion() != null
								? xmlInsp.getFecha_Propuesta_Aviso_Intervencion()
								: "");
				ordenDTO.setHoraPropuestaAvisoPrevioIntervencion(
						xmlInsp.getHora_Propuesta_Aviso_Previo_Intervencion() != null
								? xmlInsp.getHora_Propuesta_Aviso_Previo_Intervencion()
								: "");
				ordenDTO.setFechaRecepcionAvisoPrevioIntervencion(
						xmlInsp.getFecha_Recepcion_Aviso_Previo_Intervencion() != null
								? xmlInsp.getFecha_Recepcion_Aviso_Previo_Intervencion()
								: "");
				ordenDTO.setHoraRecepcionAvisoPrevioIntervencion(
						xmlInsp.getHora_de_recepcion_del_Aviso_Previo_intervencion() != null
								? xmlInsp.getHora_de_recepcion_del_Aviso_Previo_intervencion()
								: "");
				ordenDTO.setPruebaVacioAD(xmlInsp.getPrueba_Vacio_AD() != null ? xmlInsp.getPrueba_Vacio_AD() : "");
				ordenDTO.setStickerContraste(
						xmlInsp.getSticker_de_contraste() != null ? xmlInsp.getSticker_de_contraste() : "");
				ordenDTO.setSinMicaMicaRota(
						xmlInsp.getSin_mica_Mica_Rota() != null ? xmlInsp.getSin_mica_Mica_Rota() : "");
				ordenDTO.setCajaSinTapaTapaMalEstado(
						xmlInsp.getCaja_sin_tapa_Tapa_mal_estado() != null ? xmlInsp.getCaja_sin_tapa_Tapa_mal_estado()
								: "");
				ordenDTO.setSinCerraduraCerraduraMalEstado(xmlInsp.getSin_cerradura_cerradura_en_mal_estado() != null
						? xmlInsp.getSin_cerradura_cerradura_en_mal_estado()
						: "");
				ordenDTO.setEstadoConexion(xmlInsp.getEstadoConexion() != null ? xmlInsp.getEstadoConexion() : "");
				ordenDTO.setClientePermiteInventarioCarga(xmlInsp.getCliente_permite_inventario_carga() != null
						? (xmlInsp.getCliente_permite_inventario_carga().equalsIgnoreCase("true") ? "SI" : "NO")
						: "");
				ordenDTO.setCortadoArt90(xmlInsp.getCortado_art_90() != null
						? (xmlInsp.getCortado_art_90().equalsIgnoreCase("true") ? "SI" : "NO")
						: "");
				ordenDTO.setEstadoCajaPortaMedidor(
						xmlInsp.getESTADO_CAJA_PORTAMEDIDOR() != null ? xmlInsp.getESTADO_CAJA_PORTAMEDIDOR() : "");
				ordenDTO.setTomaConReja(xmlInsp.getTOMA_CON_REJA() != null
						? (xmlInsp.getTOMA_CON_REJA().equalsIgnoreCase("true") ? "SI" : "NO")
						: "");
				ordenDTO.setCajaConReja(xmlInsp.getCAJA_CON_REJA() != null
						? (xmlInsp.getCAJA_CON_REJA().equalsIgnoreCase("true") ? "SI" : "NO")
						: "");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		// List<XmlCargasDTO> cargas =
		// transferRepository.obtenerCargas(ordenDTO.getNroOrden(), 8L);
		try {
			List<XmlCargasDTO> cargas = transferRepository.obtenerCargas(ordenDTO.getNroOrden());
			if (!cargas.isEmpty()) {
				ordenDTO.setCargas(cargas);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		// R.I. CORRECCION 11/09/2023 FIN
		return ordenDTO;
	}

	@Transactional
	@Override
	public ActualizaOrdIspecCNRResponseDTO actualizarDatosOrdenInspeccionCNR(final String pRequestId, final ActualizaDatosCNR_RequestDTO dto) throws Exception {
		
		Date vfechaActualizacion = new Date();
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		log.info("Fecha actualizacion: "+sdf.format(vfechaActualizacion));
		
		RespuestaProcesoServiceDTO msjeRespDto = new RespuestaProcesoServiceDTO();
		this.oMedActualiza = null;
		
//		try {
		/* ¡¡IMPORTANTE!! : ya que usamos la notacion @Transactional NO USAMOS try catch, con el objetivo de
		que si se genera una excepciones no controlada se ejecutará automaticamente el ROLLBACK,
		con ello se asegura la integridad de los datos (Principios ACID)
		*/
		
			log.info("Paso #1 - valida datos de entrada (valores y formatos)");
			msjeRespDto = validarDatosInspeccCNR(dto);
			if(msjeRespDto.estado != EstadosRespuestaService.OK) {
				throw new DatosNoValidosParaEditarCamposException("Datos invalidos");
			}
			else {
				log.info("Paso #1 - OK");
			}
			
			log.info("Paso #2 - obtener campos a actualizar, solo los que fueron modificados - que son diferentes a los valores actuales");
			
			RespCamposActualizarDTO respCampos = obtenerListaCamposActualizar(dto);
			if(respCampos.estado == EstadosRespuestaService.OK) {
				log.info("Paso #2 - OK");
			}
			else {
				log.info("Paso #2 Fallido : "+respCampos.mensaje);
				msjeRespDto.mensaje = respCampos.mensaje;
				throw new NoDataToEditException("");
			}
			
			log.info("Paso #3 - actualiza campos");
			msjeRespDto = actualizarDatosDeLaOrdenInspeccion(dto,respCampos.campos);
			if(msjeRespDto.estado != EstadosRespuestaService.OK) {
				throw new ActualizarValoresDeCamposException("Error al actualizar datos de la inspeccion");
			}
			else {
				log.info("Paso #3 - OK");
			}
			
		
			log.info("Paso #4 - registro Historico"); 
			msjeRespDto = registrarHistoricoDeActualizaciones(dto,respCampos.campos, vfechaActualizacion,pRequestId);
			if(msjeRespDto.estado != EstadosRespuestaService.OK) {
				throw new RegistrarHistoricoEdicionCamposException("Error al registrar historico de actualizaciones");
			}
			else {
				log.info("Paso #4 - OK");
			}
			
			
			if(oMedActualiza!=null) {
				log.info("Paso #3.2 - actualiza datos en BD SYNERGIA");
				log.info("(!) actualizado objeto med_magnitud que fue guardado en memoria...");
				MedMagnitudEntity entitySaved  = medMagnitudRepository.save(this.oMedActualiza);
				log.info("entidad guardada ={}",Funciones.objectToJsonString(entitySaved));
				
				this.oMedActualiza = null;
				log.info("Paso #3.2 - OK");
			}
			
//		catch (Exception e) { 
//			msjeRespDto.mensaje = "Excepcion interna.";
//			log.error("Exception msje:"+e.toString());
//		}
		
		log.info("Paso #5 - generar estructura para la respuesta"); 
		
		ActualizaOrdIspecCNRResponseDTO rptaService = new ActualizaOrdIspecCNRResponseDTO();
		rptaService.detalle = String.format("Se actualizó %s campos(s)", respCampos.campos.size());
		rptaService.codigo_respuesta = EstadosRespuestaService.OK.toString();
		
		log.info("fin"); 
		return rptaService;
	}
	

	private RespuestaProcesoServiceDTO registrarHistoricoDeActualizaciones(final ActualizaDatosCNR_RequestDTO dto,
			List<CampoActualizarCNRDTO> listCampos,
			Date fechaActualizacion,
			String pRequestId) {
		
		RespuestaProcesoServiceDTO objRpta = new RespuestaProcesoServiceDTO();
		
		objRpta.estado = EstadosRespuestaService.ERROR;//inicializamos como error
		
		if(listCampos!=null && listCampos.size()>0) {
			
			log.info("Listado de campos = {}",listCampos.size());
			for (CampoActualizarCNRDTO item : listCampos) {
				
				HistorialActualizacionesOrdInspCNR entidadHistorica = new HistorialActualizacionesOrdInspCNR();
				entidadHistorica.requestId= pRequestId;
				entidadHistorica.nroSuministro = Long.valueOf(dto.nroSuministro);
				entidadHistorica.nro_notificacion = Long.valueOf(dto.nroNotificacion);
				
				entidadHistorica.idOrden= dto.idOrden;
				entidadHistorica.idOrdenInsp = dto.idOrdenInsp;
				entidadHistorica.idOrdenMediInsp = dto.idOrdenMediInsp;
				entidadHistorica.cnrCodExpediente = dto.codExpedienteCNR;
				
				entidadHistorica.idMagnitud = dto.idMagnitud;
				entidadHistorica.codInspeccionCNR = dto.codInspeccionCNR;
				
				entidadHistorica.cnrCodValoriza = null;
				entidadHistorica.cnrCodValoriza_deta = null;
				
				
				entidadHistorica.campo_actualizado = item.nombreCampo.toString();/*comvierte el enum a string para el guardado..*/ 
				entidadHistorica.valor_antes = item.valorActualEnBD;
				entidadHistorica.valor_despues = item.valorNuevoActualizar;
				entidadHistorica.comentario = dto.comentario;
				entidadHistorica.nombreUsuario =  dto.userName;
				entidadHistorica.fecha = fechaActualizacion;
				
				log.info("Guardando dato historico, campo ={} ",Funciones.objectToJsonString(entidadHistorica));
				historialActualizacionesOrdInspCNRRepository.save(entidadHistorica);
				log.info("Guardado.");
			}
			
		}else {
			log.info("Listado de campos Vacio");
		}
		
		
		objRpta.estado = EstadosRespuestaService.OK;
		
		return objRpta;
	}

	/***
	 * Metodo para evaluar que campos se actualizaran
	 * Internamente se realizan consultas a la base de datos para la comparativa
	 * entre los datos Nuevos recibidos para actualizar VS los datos que actualmente se encuentran en la BD Scom Y BD Recupero
	 * @param dto
	 * @return
	 * @throws CloneNotSupportedException 
	 */
	private RespCamposActualizarDTO obtenerListaCamposActualizar(final ActualizaDatosCNR_RequestDTO dtoActualiza) throws Exception {
		
		log.info("obtenerListaCamposActualizar() dto input = "+Funciones.objectToJsonString(dtoActualiza));
		
		RespCamposActualizarDTO objResp = new RespCamposActualizarDTO();
		objResp.estado = EstadosRespuestaService.VALIDACION_FALLIDA;
		
		List<CampoActualizarCNRDTO> liCampos = new ArrayList<CampoActualizarCNRDTO>(); 

		
		OrdenInspeccionActualizaDatosCNRResponseDTO dtoDatosActualesEnBD = new OrdenInspeccionActualizaDatosCNRResponseDTO();

		 Page<OrdenInspeccionActualizaDatosCNRResponseDTO> ordenes  = findOrdenInspActualizarPaginacion(
				        PageRequest.of(0, 10),
				        dtoActualiza.nroSuministro, 
				        dtoActualiza.nroNotificacion,
				        dtoActualiza.idOrden,  
				        dtoActualiza.codExpedienteCNR);

		log.info("dtoDatosActualesEnBD dto = "+Funciones.objectToJsonString(ordenes));
		
		if(ordenes.isEmpty())
		{
			log.info("No se encontraron resultados");
			objResp.mensaje = "No se encontraron resultados";
			return objResp;
		}
		else {

			if(ordenes.getTotalElements()==1) {
				log.info("ok, datos obtenidos");
			}
			else if(ordenes.getTotalElements()>1) {
				log.info("Error de validacion, se obtuvieron mas de un registro a actualizar");
				objResp.mensaje = "Error de validacion, se obtuvieron mas de un registro a actualizar";
				return objResp;
			}
		}

		log.info("Asignando valores obtenidos a objeto para la comparativa de valores (Actual vs nuevos)..");

		dtoDatosActualesEnBD = new OrdenInspeccionActualizaDatosCNRResponseDTO();
		dtoDatosActualesEnBD = ordenes.getContent().get(0);
	
		log.info("dtoDatosActualesEnBD dto = "+Funciones.objectToJsonString(dtoDatosActualesEnBD));
		
		log.info("Evaluacion de campos a actualizar..");
		
		if(compararValores(dtoActualiza.cargaR,dtoDatosActualesEnBD.cargaR)==false) {
			liCampos.add(getObjectCampoActualiza(EnumCamposActualizaOrdInspCNR.CARGA_R,
					valueToString(dtoDatosActualesEnBD.cargaR),
					valueToString(dtoActualiza.cargaR)
					));
			log.info("Campo a actualizar CARGA R");
		}

		
		if(compararValores(dtoActualiza.cargaS,dtoDatosActualesEnBD.cargaS)==false) {
			liCampos.add(getObjectCampoActualiza(EnumCamposActualizaOrdInspCNR.CARGA_S,
					valueToString(dtoDatosActualesEnBD.cargaS), 
					valueToString(dtoActualiza.cargaS)
					));
			log.info("Campo a actualizar CARGA S");
		}
		
		if(compararValores(dtoActualiza.cargaT,dtoDatosActualesEnBD.cargaT)==false) {
			liCampos.add(getObjectCampoActualiza(EnumCamposActualizaOrdInspCNR.CARGA_T,
					valueToString(dtoDatosActualesEnBD.cargaT),
					valueToString(dtoActualiza.cargaT)
					));
			log.info("Campo a actualizar CARGA T");
		}

		log.info("obtenerListaCamposActualizar dto2 = "+Funciones.objectToJsonString(dtoActualiza));
		if(dtoActualiza.lecturaNotificacion!=null) {
			if(compararValores(dtoActualiza.lecturaNotificacion, dtoDatosActualesEnBD.lecturaNotificacion)==false) {
				liCampos.add(getObjectCampoActualiza(EnumCamposActualizaOrdInspCNR.LECTURA,
						valueToString(dtoDatosActualesEnBD.lecturaNotificacion), 
						valueToString(dtoActualiza.lecturaNotificacion)
						));
				log.info("Campo a actualizar LECTURA");
			}
		}
		
		
		if(compararValores(dtoActualiza.fechaNotificacion, dtoDatosActualesEnBD.getFechaNotificacion())==false) {
			liCampos.add(getObjectCampoActualiza(EnumCamposActualizaOrdInspCNR.FECHA_NOTIFICACION,
					valueToString(dtoDatosActualesEnBD.fechaNotificacion), 
					valueToString(dtoActualiza.fechaNotificacion)
					));
			log.info("Campo a actualizar FECHA NOTIFICACION");
		}
		
		
		if(liCampos.size()>0) {
			log.info("Listado de campos = "+Funciones.objectToJsonString(liCampos));
			objResp.campos = liCampos;
			objResp.estado = EstadosRespuestaService.OK;
		}
		else {
			log.info("No se detectaron campos para actualizar");
			objResp.estado = EstadosRespuestaService.VALIDACION_FALLIDA;
			objResp.campos = null;
		}
		

		log.info("objResp ={}",Funciones.objectToJsonString(objResp));
		return objResp;
	}
	
	private String valueToString(Object value) {
		if(value ==null) {
			return null;
		}
		else {
			return value.toString();
		}
	}

	
	private Boolean compararValores(Object valA, Object valB) {
		
		if(valA ==null && valB!=null) {
			return false;
		}
		else if(valA !=null && valB==null) {
			return false;
		}
		else if(valA ==null && valB==null) {
			return true;
		}
		
		if (valA instanceof BigDecimal && valB instanceof BigDecimal)
		{
			BigDecimal val1BDA = (BigDecimal)valA;
			BigDecimal val1BDB = (BigDecimal)valB;
			
			BigDecimal  restStripA = val1BDA.stripTrailingZeros();
			BigDecimal  restStripB = val1BDB.stripTrailingZeros();
					
		  if(restStripA.equals(restStripB)) {
				return true;
			}
		  else {
			  return false;
		  }
		}
		if (valA instanceof Double && valB instanceof Double)
		{
			BigDecimal val1BDA = BigDecimal.valueOf((Double)valA);
			BigDecimal val1BDB =  BigDecimal.valueOf((Double)valB);
			
			BigDecimal  restStripA = val1BDA.stripTrailingZeros();
			BigDecimal  restStripB = val1BDB.stripTrailingZeros();
					
		  if(restStripA.equals(restStripB)) {
				return true;
			}
		  else {
			  return false;
		  }
		}
		if(valA.equals(valB)) {
			return true;
		}
		else {
			return false;
		}
	}
	
	
	private CampoActualizarCNRDTO getObjectCampoActualiza(
			EnumCamposActualizaOrdInspCNR pNombreCampo, 
			String pValorActualEnBD, 
			String pValorNuevoActualizar ) {
		
		CampoActualizarCNRDTO obj = new CampoActualizarCNRDTO();
				obj.nombreCampo = pNombreCampo;
				obj.valorActualEnBD = pValorActualEnBD;
				obj.valorNuevoActualizar = pValorNuevoActualizar;
				
				return obj;
	}

	/**
	 * El metodo contiene logica para actualiza los campos de edicion solicitados
	 * fecha: 2023.08.29
	 * requerimiento: REQCNR01 Implementar opción de corrección de campos obtenidos en terreno
	 * 
	 * @param dto
	 * @param liCampoActualizar
	 * @return
	 * @throws Exception
	 */
	private RespuestaProcesoServiceDTO actualizarDatosDeLaOrdenInspeccion( final ActualizaDatosCNR_RequestDTO dto, List<CampoActualizarCNRDTO> liCampoActualizar) throws Exception {
		
		RespuestaProcesoServiceDTO rpta = new RespuestaProcesoServiceDTO();

		if(liCampoActualizar==null) {
			log.info("Lista de campos Nula, Sin datos para actualizar");
			throw new NotFoundException("Lista de campos Nula, Sin datos para actualizar");
		}
		if(liCampoActualizar.size()==0) {
			log.info("Lista de campos vacia, sin datos para actualizar");
			throw new NotFoundException("Lista de campos vacia, sin datos para actualizar");
		}
		
		log.info("iniciando interacion de actualizacion, datos de lista de entrada = "+ Funciones.objectToJsonString(liCampoActualizar));
		
		//actualiza datos en BD SCOM
		for (CampoActualizarCNRDTO iCampo : liCampoActualizar) {
			
			log.info("procesando item = "+ Funciones.objectToJsonString(iCampo));
			
			if(iCampo.nombreCampo == EnumCamposActualizaOrdInspCNR.CARGA_R) {

				iCampo.actualizado = actualizar_cargaR(dto.codExpedienteCNR, dto.idOrdenMediInsp, iCampo.valorNuevoActualizar);
				
			}
			else if(iCampo.nombreCampo == EnumCamposActualizaOrdInspCNR.CARGA_S) {

				iCampo.actualizado = actualizar_cargaS(dto.codExpedienteCNR, dto.idOrdenMediInsp, iCampo.valorNuevoActualizar);
				
			}
			else if(iCampo.nombreCampo == EnumCamposActualizaOrdInspCNR.CARGA_T) {

				iCampo.actualizado = actualizar_cargaT(dto.codExpedienteCNR, dto.idOrdenMediInsp, iCampo.valorNuevoActualizar);
				
			}
			else if(iCampo.nombreCampo == EnumCamposActualizaOrdInspCNR.LECTURA) {

				iCampo.actualizado = actualizar_lectura(dto.idMagnitud, dto.codExpedienteCNR, dto.codInspeccionCNR, iCampo.valorNuevoActualizar,
						dto.userName, dto.comentario);
				
			}
			else if(iCampo.nombreCampo == EnumCamposActualizaOrdInspCNR.FECHA_NOTIFICACION) {

				iCampo.actualizado = actualizar_fechaNotificacion(dto.codExpedienteCNR,	dto.idOrdenInsp, 
						dto.codInspeccionCNR, 
						iCampo.valorNuevoActualizar);
				
			}
			
		}/*FIN BUCLE de campos*/
		
		log.info("fin bucle");
		
		log.info("lista de campos procesados = "+Funciones.objectToJsonString(liCampoActualizar));
		
		//valida si todos los campos fueron actualizados
		int vCamposActualizados = 0;
		for (CampoActualizarCNRDTO itemCampo : liCampoActualizar) {
			if(itemCampo.actualizado!=null) {
				if(itemCampo.actualizado==true) {
					vCamposActualizados = vCamposActualizados +1;
				}
			}
		}
		
		if(vCamposActualizados == liCampoActualizar.size()){
			log.info("todos los campos fueron actualizados");
			rpta.estado = EstadosRespuestaService.OK;	
		}
		else {
			rpta.estado = EstadosRespuestaService.ERROR;
			log.info("No se logro actualizar todos los campos de la lista");	
			throw new javax.persistence.PersistenceException("No se logro actualizar todos los campos de la lista"); 
		}
		
		return rpta;
	}
	
	
	private boolean actualizar_cargaR(final Long pCodExpedienteCNR,final Long pIdOrdenMediInsp, final String pValorNuevoActualizar) throws Exception {
		
		Integer nFilasAfectadas = 0;
		
		log.info("actualiza SCOM_cargaR..");
		nFilasAfectadas = ordenInspeccionRepository.actualizaSCOM_cargaR(pIdOrdenMediInsp, Double.valueOf(pValorNuevoActualizar));
		
		if(nFilasAfectadas==1) {
			log.info("OK");
		}
		else {
			log.error("La consulta actualizo ("+nFilasAfectadas+") registros");
			throw new ActualizarValoresDeCamposException("Error, no se logro actualizar correctamente lo datos");
		}
		
		//------
		log.info("actualiza CNR_cargaR..");
		nFilasAfectadas = ordenInspeccionRepository.actualizaCNR_cargaR(pCodExpedienteCNR, Double.valueOf(pValorNuevoActualizar));
		
		if(nFilasAfectadas==1) {
			log.info("OK");
		}
		else {
			log.error("La consulta actualizo ("+nFilasAfectadas+") registros");
			throw new ActualizarValoresDeCamposException("Error, no se logro actualizar correctamente lo datos");
		}
		
		/*si ejecuto las actualizaciones OK actualiza el flag:*/
		return true;
	}
	
	private boolean actualizar_cargaS(final Long pCodExpedienteCNR,final Long pIdOrdenMediInsp, final String pValorNuevoActualizar) throws Exception {
		
		Integer nFilasAfectadas = 0;
		
		log.info("actualiza SCOM_cargaS..");
		nFilasAfectadas = ordenInspeccionRepository.actualizaSCOM_cargaS(pIdOrdenMediInsp, Double.valueOf(pValorNuevoActualizar));
		if(nFilasAfectadas==1) {
			log.info("OK");
		}
		else {
			log.error("La consulta actualizo ("+nFilasAfectadas+") registros");
			throw new ActualizarValoresDeCamposException("Error, no se logro actualizar correctamente lo datos");
		}
		
		
		log.info("actualiza CNR_cargaS..");
		nFilasAfectadas = ordenInspeccionRepository.actualizaCNR_cargaS(pCodExpedienteCNR, Double.valueOf(pValorNuevoActualizar));
		if(nFilasAfectadas==1) {
			log.info("OK");
		}
		else {
			log.error("La consulta actualizo ("+nFilasAfectadas+") registros");
			throw new ActualizarValoresDeCamposException("Error, no se logro actualizar correctamente lo datos");
		}
		
		/*si ejecuto las actualizaciones OK actualiza el flag:*/
		return true;
	}
	
	private boolean actualizar_cargaT(final Long pCodExpedienteCNR,final Long pIdOrdenMediInsp, final String pValorNuevoActualizar) throws Exception {
		
		Integer nFilasAfectadas = 0;
		
		log.info("actualiza SCOM cargaT..");
		nFilasAfectadas = ordenInspeccionRepository.actualizaSCOM_cargaT(pIdOrdenMediInsp, Double.valueOf(pValorNuevoActualizar));
		if(nFilasAfectadas==1) {
			log.info("OK");
		}
		else {
			log.error("La consulta actualizo ("+nFilasAfectadas+") registros");
			throw new ActualizarValoresDeCamposException("Error, no se logro actualizar correctamente lo datos");
		}
		
		
		log.info("actualiza CNR cargaT..");
		nFilasAfectadas = ordenInspeccionRepository.actualizaCNR_cargaT(pCodExpedienteCNR, Double.valueOf(pValorNuevoActualizar));
		if(nFilasAfectadas==1) {
			log.info("OK");
		}
		else {
			log.error("La consulta actualizo ("+nFilasAfectadas+") registros");
			throw new ActualizarValoresDeCamposException("Error, no se logro actualizar correctamente lo datos");
		}
		
		/*si ejecuto las actualizaciones OK actualiza el flag:*/
		return true;
	}

	
	private boolean actualizar_lectura(final Long pIdMagnitud,final Long pCodExpedienteCNR,final Long pCodInspeccionCNR, 
			final String pValorNuevoActualizar,
			final String pUserName,
			final String pComentarioActualizacion) throws Exception {
		
		log.info("inicia actualizar_lectura()..");
		
		log.info("------");
		log.info("Inputs:");
		log.info(" pIdMagnitud           ={}",pIdMagnitud);
		log.info(" pCodExpedienteCNR     ={}",pCodExpedienteCNR);
		log.info(" pCodInspeccionCNR     ={}",pCodInspeccionCNR);
		log.info(" pValorNuevoActualizar ={}",pValorNuevoActualizar);
		log.info(" pUserName             ={}",pUserName);
		log.info("------");
		
		Integer nFilasAfectadas = 0;
		
		log.info("[1 de 4] actualiza SYNERGIA 4J lectura tbl_magnitud..");
		
		Optional<MedMagnitudEntity> objMed = medMagnitudRepository.findById(pIdMagnitud);
		if(objMed.isPresent()) {
			
			this.oMedActualiza = objMed.get();
			log.info("med_magnitud encontrada.");
			log.info("datos actuales ={}",Funciones.objectToJsonString(this.oMedActualiza));
			
			this.oMedActualiza.valor = Double.valueOf(pValorNuevoActualizar);
			log.info("datos nuevos ={}",Funciones.objectToJsonString(this.oMedActualiza));
			
			log.info("(!) objeto med_magnitud guardado en memoria para actualizacion al final del proceso.");

			 //medMagnitudRepository.save(oMed);
			/* ¡LEEEEME!:
			 * No actualizamos en este punto ya que de generarse un error en los pasos siguientes (con la version actual), 
			 * no se esta aplicando el rollback...(pruebas locales 2023.09.26)
			 * Es decir, que si mas adelante se genera una excepcion.. solamente se realiza el rollback a la BD de SCOM, no de synergia.
			 * 
			 * PENDIENTE: Investigar la causa o forma de controlar u optimizar, 
			 * casuistica: "operaciones transaccionales entre multiples origenes de datos" o "Distributed Transactions Across Multiple Databases Using Spring's ChainedTransactionManager"
			 * 
			 */
		}
		else {
			throw new ActualizarValoresDeCamposException("Error, no se logro encontrar la magnitud ("+pIdMagnitud+") en la BD Synergia.");
		}
		
		
		/*
		 * error med_magnitud not mapped...
		 * 
		entityManager.createQuery("UPDATE synergia.med_magnitud SET valor = ?2  WHERE id_magnitud = ?1")
	      .setParameter(1, pIdMagnitud)
	      .setParameter(2, Long.valueOf(pValorNuevoActualizar))
	      .executeUpdate();*/
		
		
//		nFilasAfectadas = medMagnitudRepository.actualiza_SYNERGIA_4J_lectura_tbl_magnitud(pIdMagnitud,Long.valueOf(pValorNuevoActualizar));
//		if(nFilasAfectadas==1) {
//			log.info("OK");
//		}
//		else {
//			log.error("La consulta actualizo ("+nFilasAfectadas+") registros");
//			throw new ActualizarValoresDeCamposException("Error, no se logro actualizar correctamente lo datos");
//		}
		
		log.info("[1 de 4] actualiza SYNERGIA 4J lectura tbl_magnitud -> ejecutado");
		
		
		log.info("[2 de 4] actualiza CNR lectura tbl_inspeccion..");
		nFilasAfectadas = ordenInspeccionRepository.actualizaCNR_lectura_tbl_inspeccion(pCodInspeccionCNR,Long.valueOf(pValorNuevoActualizar));
		if(nFilasAfectadas==1) {
			log.info("OK");
		}
		else {
			log.error("La consulta actualizo ("+nFilasAfectadas+") registros");
			throw new ActualizarValoresDeCamposException("Error, no se logro actualizar correctamente lo datos");
		}
		
		log.info("[3 de 4] actualiza CNR lectura tbl_expediente..");
		nFilasAfectadas = ordenInspeccionRepository.actualizaCNR_lectura_tbl_expediente(pCodExpedienteCNR,Integer.valueOf(pValorNuevoActualizar));
		if(nFilasAfectadas==1) {
			log.info("OK");
		}
		else {
			log.error("La consulta actualizo ("+nFilasAfectadas+") registros");
			throw new ActualizarValoresDeCamposException("Error, no se logro actualizar correctamente lo datos");
		}
		
		log.info("[4 de 4] actualizaCNR_lectura_REVALORIZAR..");
		StoredProcedureQuery query = entityManager.createStoredProcedureQuery("schrecupero.revaloriza_calculo_libre_auto")
				.registerStoredProcedureParameter(1, Long.class, ParameterMode.IN)
				.registerStoredProcedureParameter(2, String.class, ParameterMode.IN)
				.registerStoredProcedureParameter(3, String.class, ParameterMode.IN)
				.setParameter(1, pCodExpedienteCNR)
				.setParameter(2, pUserName)
				.setParameter(3, pComentarioActualizacion);

		log.info("ejecutando funcion (schrecupero.revaloriza_calculo_libre_auto)...");
		log.info("param1 ={}",pCodExpedienteCNR);
		log.info("param2 ={}",pUserName);
		log.info("param3 ={}",pComentarioActualizacion);
		
		query.execute();
		log.info("funcion ejecutada.");
		
		List<Object[]> resultRws = query.getResultList();
		log.info("respuesta ={}",resultRws);
		
		String rptastatus=null;
		boolean flagTrowException = false;
		
		if(resultRws.get(0)!=null) {

			String rptaRevaloriza=null;
			rptaRevaloriza = String.valueOf(resultRws.get(0));
			
			if(rptaRevaloriza.length()>2){
				rptaRevaloriza = rptaRevaloriza.substring(0, 2);
				rptastatus = rptaRevaloriza;
			}
			else {
				rptastatus = rptaRevaloriza;	
			}
			log.info("rptastatus ={}",rptastatus);
		}
		
		if(rptastatus==null) {
			log.error("La funcion no retorno respuesta, revisar. <<<<<");
			flagTrowException=true;
		}
		else if(rptastatus.toUpperCase().trim().equals("OK")){
			
			log.info(">> Ejecucion exitosa  !! <<<<");
			log.info("[4 de 4] actualizaCNR_lectura_REVALORIZAR -> ejecutado");
			
			flagTrowException=false;
		}
		else{
			log.error("Algo no salio bien... <<<<");
			flagTrowException=true;
		}
		
		
		if(flagTrowException==Boolean.TRUE) {
			/* Con el tipo de excepcion "javax.persistence.PersistenceException" se logra que se ejecute el rollback 
			 * incluso si se invoco a una funcion con funcion anidada (funcion dentro de funcion..)*/
			throw new javax.persistence.PersistenceException("Lanzando rollback!! :("); 
		}
		
		/*si ejecuto las actualizaciones OK actualiza el flag:*/
		return true;
	}
	
	
	private boolean actualizar_fechaNotificacion(final Long pCodExpedienteCNR,
			final Long pIdOrdenInsp, 
			final Long pCodInspeccionCNR, 
			final String pValorNuevoActualizar) throws Exception {
		
		Integer nFilasAfectadas=0;
		
		log.info("actualiza SCOM fechaNotificacion..");
		nFilasAfectadas = ordenInspeccionRepository.actualizaSCOM_fechaNotificacion(pIdOrdenInsp, pValorNuevoActualizar);
		
		if(nFilasAfectadas==1) {
			log.info("OK");
		}
		else {
			log.error("La consulta actualizo ("+nFilasAfectadas+") registros");
			throw new ActualizarValoresDeCamposException("Error, no se logro actualizar correctamente lo datos");
		}
		
		
		log.info("actualiza CNR fechaNotificacion_tbl_inspeccion..");
		nFilasAfectadas = ordenInspeccionRepository.actualizaCNR_fechaNotificacion_tbl_inspeccion(pCodInspeccionCNR,pValorNuevoActualizar);
		if(nFilasAfectadas==1) {
			log.info("OK");
		}
		else {
			log.error("La consulta actualizo ("+nFilasAfectadas+") registros");
			throw new ActualizarValoresDeCamposException("Error, no se logro actualizar correctamente lo datos");
		}
		
		
		log.info("actualiza CNR fechaNotificacion..");
		nFilasAfectadas = ordenInspeccionRepository.actualizaCNR_fechaNotificacion(
				pCodExpedienteCNR, pValorNuevoActualizar);
		if(nFilasAfectadas==1) {
			log.info("OK");
		}
		else {
			log.error("La consulta actualizo ("+nFilasAfectadas+") registros");
			throw new ActualizarValoresDeCamposException("Error, no se logro actualizar correctamente lo datos");
		}
		
		//si ejecuto las actualizaciones OK:
		return true;
	}
	
	public RespuestaProcesoServiceDTO validarDatosInspeccCNR(ActualizaDatosCNR_RequestDTO dto) {
	
		RespuestaProcesoServiceDTO resp = new RespuestaProcesoServiceDTO();
		resp.estado = EstadosRespuestaService.OK;
		
		List<String> liValidacionesFail = new ArrayList<String>();
		try {
			String rpta = "";
			
			//CARGAS
//			rpta = validarDecimales(dto.cargaR, 2);
//			if(rpta!="") {
//				 liValidacionesFail.add(rpta);
//			 }
//			 
//			 rpta = validarDecimales(dto.cargaS, 2);
//			 if(rpta!="") {
//				 liValidacionesFail.add(rpta);
//			 }
//			 
//			 rpta = validarDecimales(dto.cargaT, 2);
//			 if(rpta!="") {
//				 liValidacionesFail.add(rpta);
//			 }
			 
			 
			 //LECTURA
			 if(dto.lecturaNotificacion!=null)
			 {
				 if(dto.lecturaNotificacion<0) {
					 log.info("lecturaNotificacion debe ser valor mayor a cero.");
					 liValidacionesFail.add("lecturaNotificacion debe ser valor mayor a cero.");
				 }
			 }
			
			 
			 //FECHA NOTIFICACION
			 rpta = validarFormatoFecha(dto.fechaNotificacion, Constantes.FORMATO_FECHA_VALID_ACT_CNR);
			 if(rpta!="") {
				 liValidacionesFail.add(rpta);
			 }
			 
			 //aqui se puede agregar nuevas validaciones...
			 
			 
		} catch (Exception e) {
			log.error("Error al validar datos de la inspeccion a actualizar...",e);
			liValidacionesFail.add("Excepcion interna al validar");
		}
		
		
		
		if(liValidacionesFail.size()>0) {
			resp.mensaje =  "Validaciones fallidas a corregir : "+String.join(",", liValidacionesFail);
		}
		else {
			resp.estado = EstadosRespuestaService.OK;
		}
		
		
		return resp;
		
	}
	

//	public String validarDecimales(BigDecimal pValor, int pNroDecimalesMaximoAValidar) {
//		String rpta = "";
//		
//		if(pValor==null)
//		{
//			log.info("Valor decimal a validar Nulo");
//			return "";
//		}
//		
//		if(pNroDecimalesMaximoAValidar>0) {
//			//validar el nro de decimales
//			try {
//				
//				int numberOfDecimalPlaces = Math.max(0, pValor.stripTrailingZeros().scale());
//				int numberOfDecimalPlaces2 = pValor.stripTrailingZeros().scale();
//				if(numberOfDecimalPlaces> pNroDecimalesMaximoAValidar) {
//					rpta = "El valor "+pValor+" tiene decimales "+numberOfDecimalPlaces+" decimales, el máximo de decimales permitidos es "+pNroDecimalesMaximoAValidar; 
//				}
//			} catch (Exception e) {
//				rpta = "Error al validar el nro de decimales del valor "+pValor;
//				return rpta;
//			}
//		}
//		
//		return rpta;
//	}
	public String validarFormatoFecha(String pTexto, String pFormatoAValidar) {
		String rpta = "";
		
		if(pTexto==null) {
			return "no se recibidio texto a validar la fecha";
		}
		if(pTexto.trim().length()==0){
			return "no se recibidio texto a validar la fecha";
		}
		
		if(pFormatoAValidar==null) {
			return "no se recibidio el formato para validar la fecha";
		}
		if(pFormatoAValidar.trim().length()==0){
			return "no se recibidio el formato para a validar la fecha";
		}
		
		DateFormat sdf = new SimpleDateFormat(pFormatoAValidar);
        sdf.setLenient(false);
        try {
            sdf.parse(pTexto);
            
        } catch (ParseException e) {
        	return "El valor de la fecha ("+pTexto+") no tiene el formato de validacion ("+pFormatoAValidar+")";
        }
		return rpta;
	}
	
	/**
	 * Funcion utilizada en la pagina de busqueda y utilizada para consultar datos de la orden, invocado por el metodo de esta clase obtenerListaCamposActualizar()
	 */
	@Override
	public Page<OrdenInspeccionActualizaDatosCNRResponseDTO> findOrdenInspActualizarPaginacion(Pageable paging,
			Long numSuministro, Long numNotificacion, Long idOrden, Long codExpedienteCNR) throws Exception {
		
			log.info("findOrdenInspActualizarPaginacion()");
		
			Page<OrdenInspeccionActualizaDatosCNRResponseDTO> objPagFinal =  new PageImpl<>(new ArrayList<OrdenInspeccionActualizaDatosCNRResponseDTO>(), paging, 0);
			List<OrdenInspeccionActualizaDatosCNRResponseDTO> liDatosDTO = new ArrayList<OrdenInspeccionActualizaDatosCNRResponseDTO>();
			
			int offset = paging.getPageNumber() * paging.getPageSize();
			int limit = paging.getPageSize();
			log.info("/offset: {}", offset);
			log.info("/limit: {}", limit);
			
			String sqlSelect = Constantes.ORDEN_INSP_REPO_SQL_SELECT;
			
			String sqlFrom = Constantes.ORDEN_INSP_REPO_SQL_FROM;
		
			String sqlWhere = Constantes.ORDEN_INSP_REPO_SQL_WHERE; //filtro principal
			
			String sqlOrder = " order by oo.fecha_creacion desc";
			
			
			if(numSuministro!=null && numSuministro>0) {
				sqlWhere += "  and c.nro_cuenta = "+numSuministro;
			}
			if(numNotificacion!=null && numNotificacion>0) {
				sqlWhere += "  and doi.nro_notificacion = "+numNotificacion;
			}
			
			if(idOrden!=null && idOrden>0) {
				sqlWhere += "  and oo.id = "+idOrden;
			}
			if(codExpedienteCNR!=null && codExpedienteCNR>0) {
				sqlWhere += "  and exp_cnr.cod_expediente = "+codExpedienteCNR;
			}
			
			String sqlCantidad = "select count(1) ".concat(sqlFrom).concat(sqlWhere);
			log.info("/consulta cantidad: {}", sqlCantidad);
			
			BigInteger cantidadTotalRegistros = (BigInteger) entityManager.createNativeQuery(sqlCantidad).getSingleResult();
				
			log.info("/cantidad total de registros: {}", cantidadTotalRegistros);
			
			
			String sqlDatos = sqlSelect.concat(sqlFrom).concat(sqlWhere).concat(sqlOrder);
			log.info("/consulta datos: {}", sqlDatos);
			
			List<Object[]> tmpliDatos0 = entityManager.createNativeQuery(
					sqlDatos).setFirstResult(offset).setMaxResults(limit).getResultList();//works
			
			log.info("/cantidad total de registros: {}", tmpliDatos0.size());
			log.info("Datos 1: {}", Funciones.objectToJsonString(tmpliDatos0));
			
			
			for (Object[] oItem : tmpliDatos0) {
				OrdenInspeccionActualizaDatosCNRResponseDTO vDto = new OrdenInspeccionActualizaDatosCNRResponseDTO();
				vDto.setIdOrden(oItem[0]==null?null:Long.valueOf(oItem[0].toString()));
				vDto.setIdOrdenInsp(oItem[1]==null?null:Long.valueOf(oItem[1].toString()));
				vDto.setNroSuministro(oItem[2]==null?null:Long.valueOf(oItem[2].toString()));
				vDto.setNroNotificacion(oItem[3]==null?null:Long.valueOf(oItem[3].toString()));
				vDto.setIdOrdenMediInsp(oItem[4]==null?null:Long.valueOf(oItem[4].toString()));

				vDto.setCargaR(oItem[5]==null?null:Double.valueOf(oItem[5].toString()));
				vDto.setCargaS(oItem[6]==null?null:Double.valueOf(oItem[6].toString()));
				vDto.setCargaT(oItem[7]==null?null:Double.valueOf(oItem[7].toString()));
				
				vDto.setFechaNotificacion(oItem[8]==null?null:oItem[8].toString());
				vDto.setCodExpedienteCNR(oItem[9]==null?null:Long.valueOf(oItem[9].toString()));
				vDto.setNombreExpedienteCNR(oItem[10]==null?null:oItem[10].toString());				
				
//				vDto.setLecturaNotificacion(oItem[11]==null?null:Integer.parseInt(oItem[11].toString()));
				vDto.setLecturaNotificacion(oItem[11]==null?null:Double.valueOf(oItem[11].toString()).intValue());
				
				vDto.setCodInspeccionCNR(oItem[12]==null?null:Long.valueOf(oItem[12].toString()));
				vDto.setIdMagnitud(oItem[13]==null?null:Long.valueOf(oItem[13].toString()));
				vDto.setEscenarioDescripcion(oItem[14]==null?null:oItem[14].toString());
				vDto.setTipoCliente(oItem[15]==null?null:oItem[15].toString());
				
				liDatosDTO.add(vDto);
			}
			
			
			objPagFinal =  new PageImpl<>(liDatosDTO, paging,cantidadTotalRegistros.longValue());
			
			return objPagFinal;
			
	}
	
	
	@Override
	public Page<HistorialActualizacionesResponseDTO> findHistorialActualizacionesPaginacion(Pageable paging,
			Long numSuministro, Long numNotificacion) throws Exception {
		
//			Long cantidadTotalRegistros= 0l;
			
			
			Page<HistorialActualizacionesResponseDTO> objPagFinal =  new PageImpl<>(new ArrayList<HistorialActualizacionesResponseDTO>(), paging, 0l);
			List<HistorialActualizacionesResponseDTO> liDatosDTO = new ArrayList<HistorialActualizacionesResponseDTO>();
			
			int offset = paging.getPageNumber() * paging.getPageSize();
			int limit = paging.getPageSize();
			log.info("/offset: {}", offset);
			log.info("/limit: {}", limit);
			
			String sqlSelect = Constantes.ORDEN_INSP_HISTORIAL_ACTUALIZACIONES_REPO_SQL_SELECT;
			
			String sqlFrom = Constantes.ORDEN_INSP_HISTORIAL_ACTUALIZACIONES_REPO_SQL_FROM;
		
			String sqlWhere =  Constantes.ORDEN_INSP_HISTORIAL_ACTUALIZACIONES_REPO_SQL_WHERE;
			
			if(numSuministro!=null && numSuministro>0) {
				sqlWhere+= " and nro_suministro = "+ numSuministro;
			}
			if(numNotificacion!=null && numNotificacion>0) {
				sqlWhere+= " and nro_notificacion = "+ numNotificacion;
			}
	
			String sqlOrder = " order by fecha desc";
			
			String sqlCantidad = "select count(1) ".concat(sqlFrom).concat(sqlWhere);
			log.info("/consulta cantidad: {}", sqlCantidad);
			
			BigInteger cantidadTotalRegistros = (BigInteger) entityManager.createNativeQuery(sqlCantidad).getSingleResult();
				
			log.info("/cantidad total de registros: {}", cantidadTotalRegistros);
			
			
			String sqlDatos = sqlSelect.concat(sqlFrom).concat(sqlWhere).concat(sqlOrder);
			log.info("/consulta datos: {}", sqlDatos);
			
			List<Object[]> tmpliDatos0 = entityManager.createNativeQuery(
					sqlDatos).setFirstResult(offset).setMaxResults(limit).getResultList();//works
			
			log.info("/cantidad total de registros: {}", tmpliDatos0.size());
			log.info("Datos 1: {}", Funciones.objectToJsonString(tmpliDatos0));
			
			if(tmpliDatos0==null) {
				log.info("ordenInspeccion data not found");
				return new PageImpl<>(new ArrayList<HistorialActualizacionesResponseDTO>(), paging, 0L);
				
			}
			log.info("Resultados :"+tmpliDatos0.size());
			
			for (Object[] itemHis : tmpliDatos0) {
				
				//setea valores al objeto final antes de agregar al listado a retornar
				HistorialActualizacionesResponseDTO vDto = new HistorialActualizacionesResponseDTO();
			
				vDto.setFechaEdicion(itemHis[0]==null?null:itemHis[0].toString());
				vDto.setUserName(itemHis[1]==null?null:itemHis[1].toString());
				vDto.setNroSuministro(itemHis[2]==null?null:Long.valueOf(itemHis[2].toString()));
				vDto.setNroNotificacion(itemHis[3]==null?null:Long.valueOf(itemHis[3].toString()));
				vDto.setNombreCampoActualizado(itemHis[4]==null?null:itemHis[4].toString());
				vDto.setValorAntes(itemHis[5]==null?null:itemHis[5].toString());
				vDto.setValorDespues(itemHis[6]==null?null:itemHis[6].toString());
				vDto.setComentario(itemHis[7]==null?null:itemHis[7].toString());
				
				liDatosDTO.add(vDto);
				
			}
			
			objPagFinal =  new PageImpl<>(liDatosDTO, paging, cantidadTotalRegistros.longValue());
			
			return objPagFinal;
	}
}
