package com.enel.scom.api.gestion.util;

public class Constantes {

	public static String FORMATO_FECHA_VALID_ACT_CNR = "YYYY-MM-DD";
	
	public static String ORDEN_INSP_REPO_SQL_SELECT = " select "
			+ " oo.id as idOrden"
			+ " ,doi.id as idOrdenInsp" /*identificador importante para actualizar los datos*/
			+ " ,c.nro_cuenta as nroSuministro"
			+ " ,doi.nro_notificacion as nroNotificacion"
			+ " ,omi.id_medi_insp as idOrdenMediInsp" /*identificador importante para actualizar los datos*/
			+ " ,omi.carga_r as cargaR"
			+ " ,omi.carga_s as cargaS"
			+ " ,omi.carga_t as cargaT"
			+ " ,to_char(doi.fec_fin_eje,'YYYY-MM-DD') as fechaNotificacion"
			+ " ,exp_cnr.cod_expediente as codExpedienteCNR"
			+ " ,exp_cnr.nombre as nombreExpedienteCNR"
			+ " ,exp_cnr.lectura_notificacion as lecturaNotificacion"
			+ " ,insp_cnr.cod_inspeccion as idInspeccionCNR"
			+ " ,oril.id_lectura as idMagnitud"
			+ " ,esc_cnr.descripcion as escenarioDescripcion"
			+ " ,exp_cnr.tipo_cliente as tipoCliente";
	
	public static String ORDEN_INSP_REPO_SQL_FROM = " from ord_orden oo"
			+" inner join dis_ord_insp doi on doi.id_orden = oo.id "
			+" inner join ord_medi_insp omi on omi.id_orden  = oo.id "
			+" inner join ord_insp_lectura oril on oril.id_medi_insp=omi.id_medi_insp "
			/*inner join med_magnitud mma on mma.id_magnitud= oril.id_lectura >> NO SE IMPLEMENTA EL JOIN DEBIDO A QUE LA TABLA >AUN< NO EXISTE EN LA BD SCOM, SOLO 4J*/
			+" inner join cliente c on oo.id_servicio = c.id_servicio "
			+" inner join schrecupero.rec_expediente exp_cnr on exp_cnr.nro_notificacion = doi.nro_notificacion  "
			+"  and exp_cnr.nro_cuenta = c.nro_cuenta  "
			+"  and exp_cnr.id_orden_insp = doi.id  "
			+"  and exp_cnr.id_orden_orcl = oo.id  "
			+" inner join schrecupero.rec_inspeccion insp_cnr  "
			+" 	on exp_cnr.nro_notificacion = insp_cnr.nro_notificacion "
			+" 	and exp_cnr.nro_cuenta  = insp_cnr.nro_cuenta "
			+" 	and exp_cnr.nro_inspeccion = insp_cnr.nro_orden "
			+" inner join schrecupero.rec_irregularidad irr_cnr on exp_cnr.cod_irregularidad = irr_cnr.cod_irregularidad and irr_cnr.activo = 'S' "
			+" inner join schrecupero.rec_escenario esc_cnr on irr_cnr.cod_escenario  = esc_cnr.cod_escenario "; 
	
	public static String ORDEN_INSP_REPO_SQL_WHERE = " where oo.id_tipo_orden = 8 ";
	
	
	
	public static String ORDEN_INSP_HISTORIAL_ACTUALIZACIONES_REPO_SQL_SELECT = " select "
			+" to_char(fecha,'YYYY-MM-DD HH24:MI:SS') "
			+" ,user_name"
			+" ,nro_suministro"
			+" ,nro_notificacion"
			+" ,campo_actualizado"
			+" ,valor_antes"
			+" ,valor_despues"
			+" ,comentario";
	
	public static String ORDEN_INSP_HISTORIAL_ACTUALIZACIONES_REPO_SQL_FROM =  " from schscom.historial_actualizaciones_ord_insp_cnr ";
	
	public static String  ORDEN_INSP_HISTORIAL_ACTUALIZACIONES_REPO_SQL_WHERE = "where 1 = 1" ;
	
	
}
