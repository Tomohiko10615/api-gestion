package com.enel.scom.api.gestion.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;

@Entity(name="med_contraste")
@Data
public class MedContrasteEntity implements Serializable {/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "id_contraste")
	private Long idContraste;
	
	@Column(name = "id_medidor")
	private Long idMedidor;
	
	@Column(name = "fec_ejecucion")
	private Date fecEjecucion;
	
	@Column(name = "cons_prom_real")
	private BigDecimal consPromReal;
	
	@Column(name = "prueba_baja_01")
	private BigDecimal primeroPruebaBaja;
	
	@Column(name = "prueba_baja_02")
	private BigDecimal segundoPruebaBaja;
	
	@Column(name = "prueba_baja_03")
	private BigDecimal terceroPruebaBaja;
	
	@Column(name = "prueba_nominal_01")
	private BigDecimal primeroPruebaNominal;
	
	@Column(name = "prueba_nominal_02")
	private BigDecimal segundoPruebaNominal;
	
	@Column(name = "prueba_nominal_03")
	private BigDecimal terceroPruebaNominal;
	
	@Column(name = "prueba_alta_01")
	private BigDecimal primeroPruebaAlta;
	
	@Column(name = "prueba_alta_02")
	private BigDecimal segundoPruebaAlta;
	
	@Column(name = "prueba_alta_03")
	private BigDecimal terceroPruebaAlta;
	
	@Column(name = "prueba_aislamiento_r")
	private BigDecimal pruebaAislaientoR;
	
	@Column(name = "prueba_aislamiento_s")
	private BigDecimal pruebaAislamientoS;
	
	@Column(name = "prueba_aislamiento_t")
	private BigDecimal pruebaAislamientoT;
}
