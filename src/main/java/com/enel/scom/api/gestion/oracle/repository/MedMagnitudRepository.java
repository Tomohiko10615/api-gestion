package com.enel.scom.api.gestion.oracle.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.dto.LecturasDTO;
import com.enel.scom.api.gestion.oracle.model.MedMagnitudEntity;
/***
 * clase adicionda
 * fecha:2023.09.26
 * motivo: requerimiento EPT Impacto SCOM CNR, REQCNR01
 * @author Indra
 *
 */
@Repository
public interface MedMagnitudRepository extends JpaRepository<MedMagnitudEntity, Long> {

	@Query(value = "SELECT mm.ID_COMPONENTE AS idComponente, mm.valor AS estadoLeido, \r\n"
			+ "mtm.DES_TIP_MAGNITUD AS accionMedidor, TO_CHAR(mm.FEC_LEC_TERRENO, 'DD/MM/YYYY HH24:MI:SS') AS fechaLectura ,\r\n"
			+ "mtme.COD_TIP_MEDIDA as tipoLectura,\r\n"
			+ "mme.COD_MEDIDA AS horarioLectura\r\n"
			+ "FROM synergia.med_magnitud mm, SYNERGIA.MED_TIP_MAGNITUD mtm,\r\n"
			+ "synergia.MED_TIP_MEDIDA mtme, synergia.MED_MEDIDA mme\r\n"
			+ "WHERE mm.id_magnitud IN (?1) \r\n"
			+ "AND mm.ID_TIP_MAGNITUD = mtm.ID_TIP_MAGNITUD\r\n"
			+ "AND mm.id_medida = mme.id_medida \r\n"
			+ "AND mme.id_tip_medida = mtme.id_tip_medida", nativeQuery = true)
	List<LecturasDTO> obtenerLecturas(List<Long> idMagnitudList);
	
	@Query(value = "SELECT mm.ID_COMPONENTE AS idComponente, mm.valor AS estadoLeido, \r\n"
			+ "mtm.DES_TIP_MAGNITUD AS accionMedidor, mm.FEC_LEC_TERRENO AS fechaLectura ,\r\n"
			+ "mtme.COD_TIP_MEDIDA as tipoLectura,\r\n"
			+ "mme.COD_MEDIDA AS horarioLectura\r\n"
			+ "FROM synergia.med_magnitud mm, SYNERGIA.MED_TIP_MAGNITUD mtm,\r\n"
			+ "synergia.MED_TIP_MEDIDA mtme, synergia.MED_MEDIDA mme\r\n"
			+ "WHERE mm.id_componente IN (?1) \r\n"
			+ "AND mm.ID_TIP_MAGNITUD = mtm.ID_TIP_MAGNITUD\r\n"
			+ "AND mm.id_medida = mme.id_medida \r\n"
			+ "AND mme.id_tip_medida = mtme.id_tip_medida", nativeQuery = true)
	List<LecturasDTO> obtenerLecturasPorIdComponente(List<Long> idComponenteList);

	
	/* No utilizado, genera error (x_x) 2023.09.26 
	 * al invocar desde "actualizar_lectura()" en "/api-gestion/src/main/java/com/enel/scom/api/gestion/service/impl/OrdenInspeccionServiceImpl.java"
	 * 
	 * error = "InvalidDataAccessApiUsageException: Executing an update/delete query; nested exception is javax.persistence.TransactionRequiredException: Executing an update/delete quer"
	 * PENDIENTE: investigar la causa y realizar pruebas..
	 * 
	 * @Modifying
	@Query(value = "UPDATE synergia.med_magnitud"
					+ " SET valor =  :paramLectura_nuevoValor "
					+ " WHERE id_magnitud = :paramIdMagnitud", nativeQuery = true)
	int actualiza_SYNERGIA_4J_lectura_tbl_magnitud(
			@Param("paramIdMagnitud") Long paramIdMagnitud, 
			@Param("paramLectura_nuevoValor") Long paramLectura_nuevoValor);
	*/
	
}
