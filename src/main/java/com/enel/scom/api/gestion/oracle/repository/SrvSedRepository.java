package com.enel.scom.api.gestion.oracle.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.oracle.model.SrvSedEntity;

@Repository
public interface SrvSedRepository extends JpaRepository<SrvSedEntity, Long>{
	
	/**
	 * Query para realizar autocomplete en Sed CADENA ELECTRICA
	 * @param desAlimentador
	 * @param idSet
	 * @return
	 */
	
	@Query(value="SELECT sed.id_sed, sed.cod_sed FROM srv_sed sed where sed.id_alimentador=:idAlimentador and sed.des_sed like %:desSed% order by sed.cod_sed asc", nativeQuery = true)
	public List<Object[]> buscarSedServicioElectrico(@Param("desSed") String desSed, @Param("idAlimentador") Long idAlimentador);
	
	@Query(value="SELECT * FROM srv_sed sed where sed.id_sed=:id", nativeQuery = true)
	public SrvSedEntity obtenerSedServicioElectricoPorId(@Param("id") Long id);
}
