package com.enel.scom.api.gestion.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.enel.scom.api.gestion.dto.request.OrdenMantenimientoRequestDTO;
import com.enel.scom.api.gestion.exception.NroServicioNotFoundException;
import com.enel.scom.api.gestion.model.OrdenEntity;
import com.enel.scom.api.gestion.service.OrdenMantenimientoService;

@RestController
@RequestMapping("/api/ordenMantenimiento")
public class OrdenMantenimientoController {
    
    @Autowired
    OrdenMantenimientoService ordenMantenimientoService;

    @PostMapping("/store")
    public ResponseEntity<OrdenEntity> postStoreUpdate(@RequestBody OrdenMantenimientoRequestDTO requestDTO) throws NroServicioNotFoundException {
        OrdenEntity response = ordenMantenimientoService.postStoreUpdate(requestDTO);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
