package com.enel.scom.api.gestion.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.model.MotivoAnulacionEntity;

@Repository
public interface MotivoAnulacionRepository extends JpaRepository<MotivoAnulacionEntity, Long> {
	
	@Query(value = "SELECT * FROM ord_motivo_anulacion WHERE cod_interno =:codInterno", nativeQuery = true)
	MotivoAnulacionEntity obtenerPorCodigoInterno(@Param("codInterno") String codInterno);
}
