package com.enel.scom.api.gestion.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.model.DyoobjectEntity;

@Repository
public interface DyoobjectRepository extends JpaRepository<DyoobjectEntity, Long>{

    @Query(value = "SELECT nextval('sqdynamicbusinessobject')", nativeQuery = true)
    Long secuencia();
}
