package com.enel.scom.api.gestion.dto.request;

import lombok.Data;

@Data
public class TransferAnularRequestDTO {
	
	private Long id;
	
	private String nroOrdenLegacy;
	
	private String codTipoOrdenLegacy;
	
	private String codEstadoOrden;
	
	private String usuarioAnula;
}
