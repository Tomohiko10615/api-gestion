package com.enel.scom.api.gestion.oracle.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;

@Entity(name = "srv_electrico")
@Data
public class SrvElectricoEntity implements Serializable {/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
   @Id
   @Column(name="id_servicio")
   private Long idServicio;
}
