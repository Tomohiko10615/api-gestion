package com.enel.scom.api.gestion.service;

import com.enel.scom.api.gestion.dto.request.CambiarEstadoTareaRequestDTO;

public interface OrdenTareaService {
	void cambiarEstado(CambiarEstadoTareaRequestDTO cambiarEstado);
}
