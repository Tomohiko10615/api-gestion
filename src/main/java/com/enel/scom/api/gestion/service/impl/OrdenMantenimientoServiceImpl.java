package com.enel.scom.api.gestion.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.enel.scom.api.gestion.dto.LecturasDTO;
import com.enel.scom.api.gestion.dto.MedidorDTO;
import com.enel.scom.api.gestion.dto.ObservacionDTO;
import com.enel.scom.api.gestion.dto.XmlDTO;
import com.enel.scom.api.gestion.dto.request.OrdenMantenimientoRequestDTO;
import com.enel.scom.api.gestion.dto.response.OrdenDataResponseDTO;
import com.enel.scom.api.gestion.dto.response.OrmTareaOrdenResponseDTO;
import com.enel.scom.api.gestion.dto.xml.Lectura;
import com.enel.scom.api.gestion.dto.xml.Medidor;
import com.enel.scom.api.gestion.exception.NoExistEntityException;
import com.enel.scom.api.gestion.exception.NroServicioNotFoundException;
import com.enel.scom.api.gestion.model.BuzonEntity;
import com.enel.scom.api.gestion.model.ClienteEntity;
import com.enel.scom.api.gestion.model.ObservacionEntity;
import com.enel.scom.api.gestion.model.OrdenDerivaEntity;
import com.enel.scom.api.gestion.model.OrdenEntity;
import com.enel.scom.api.gestion.model.OrdenMantenimientoEntity;
import com.enel.scom.api.gestion.model.OrmTareaEntity;
import com.enel.scom.api.gestion.model.PrioridadEntity;
import com.enel.scom.api.gestion.model.ResponsableEntity;
import com.enel.scom.api.gestion.model.TareaEjecutadaEntity;
import com.enel.scom.api.gestion.model.TemaEntity;
import com.enel.scom.api.gestion.model.TipoOrdenEntity;
import com.enel.scom.api.gestion.model.TrabajoEntity;
import com.enel.scom.api.gestion.model.UsuarioEntity;
import com.enel.scom.api.gestion.model.WorkflowEntity;
import com.enel.scom.api.gestion.oracle.repository.MedMagnitudRepository;
import com.enel.scom.api.gestion.repository.BuzonRepository;
import com.enel.scom.api.gestion.repository.ClienteRepository;
import com.enel.scom.api.gestion.repository.MedidorRepository;
import com.enel.scom.api.gestion.repository.MotivoRepository;
import com.enel.scom.api.gestion.repository.ObservacionRepository;
import com.enel.scom.api.gestion.repository.OrdenDerivaRepository;
import com.enel.scom.api.gestion.repository.OrdenMantenimientoRepository;
import com.enel.scom.api.gestion.repository.OrdenRepository;
import com.enel.scom.api.gestion.repository.OrmTareaRepository;
import com.enel.scom.api.gestion.repository.PrioridadRepository;
import com.enel.scom.api.gestion.repository.ResponsableRepository;
import com.enel.scom.api.gestion.repository.TareaEjecutadaRepository;
import com.enel.scom.api.gestion.repository.TemaRepository;
import com.enel.scom.api.gestion.repository.TipoOrdenRepository;
import com.enel.scom.api.gestion.repository.TrabajoRepository;
import com.enel.scom.api.gestion.repository.TransferRepository;
import com.enel.scom.api.gestion.repository.UsuarioRepository;
import com.enel.scom.api.gestion.repository.WorkflowRepository;
import com.enel.scom.api.gestion.service.OrdenMantenimientoService;

/**
 * Servicio principal donde se registra, actualiza y se obtiene el detalle de la
 * orden de mantenimiento
 * 
 * @author Gilmar Moreno
 * @version 1.0
 */
@Service
public class OrdenMantenimientoServiceImpl implements OrdenMantenimientoService {

	@Autowired
	UsuarioRepository usuarioRepository;

	@Autowired
	ClienteRepository clienteRepository;

	@Autowired
	OrdenRepository ordenRepository;

	@Autowired
	TrabajoRepository trabajoRepository;

	@Autowired
	PrioridadRepository prioridadRepository;

	@Autowired
	WorkflowRepository wkfWorkflowRepository;

	@Autowired
	MotivoRepository motivoRepository;

	@Autowired
	TipoOrdenRepository tipoOrdenRepository;

	@Autowired
	OrdenMantenimientoRepository ordenMantenimientoRepository;

	@Autowired
	ObservacionRepository observacionRepository;

	@Autowired
	OrdenDerivaRepository ordenDerivaRepository;

	@Autowired
	TemaRepository temaRepository;

	@Autowired
	TareaEjecutadaRepository tareaEjecutadaRepository;

	@Autowired
	OrmTareaRepository ormTareaRepository;

	@Autowired
	ResponsableRepository responsableRepository;

	@Autowired
	BuzonRepository buzonRepository;

	// R.I. REQSCOM06 16/10/2023 INICIO
	@Autowired
	TransferRepository transferRepository;
	@Autowired
	MedMagnitudRepository medMagnitudRepository;
	@Autowired
	MedidorRepository medidorRepository;
	// R.I. REQSCOM06 16/10/2023 FIN

	/**
	 * Metodo para registrar y actualizar
	 */
	@Override
	public OrdenEntity postStoreUpdate(OrdenMantenimientoRequestDTO requestDTO) throws NroServicioNotFoundException {

		try {
			UsuarioEntity usuario = usuarioRepository.findById(requestDTO.getIdUsuario())
					.orElseThrow(() -> new NoExistEntityException("Usuario no existe", "VP-01", HttpStatus.NOT_FOUND));
			ClienteEntity cliente = clienteRepository.findByNroCuenta(requestDTO.getNroCuenta());
			TrabajoEntity trabajo = trabajoRepository.getById(requestDTO.getIdTrabajo());
			PrioridadEntity prioridad = prioridadRepository.findById(requestDTO.getIdPrioridad()).orElseThrow(
					() -> new NoExistEntityException("Prioridad no existe", "VP-01", HttpStatus.NOT_FOUND));
			Long idMotivo = motivoRepository.obtenerMotivoId("PLM");
			Boolean existeOrden = ordenRepository.existsById(requestDTO.getIdOrden());
			TemaEntity tema = temaRepository.findByCode("066")
					.orElseThrow(() -> new NoExistEntityException("Tema no existe", "VP-01", HttpStatus.NOT_FOUND));
			TipoOrdenEntity tipoOrden = tipoOrdenRepository.getMantenimiento();
			OrdenMantenimientoEntity existeOrdenMantenimiento = ordenMantenimientoRepository
					.findByIdOrden(requestDTO.getIdOrden());
			ResponsableEntity responsable = responsableRepository.getById(requestDTO.getIdResponsable(),
					requestDTO.getIdTipoResponsable());
			BuzonEntity buzon = buzonRepository.obtenerPorId(responsable.getIdBuzon());
			/**
			 * Evitar caidas de nulos para las ordenes creadas de forma masiva
			 */
			if ((existeOrdenMantenimiento == null) && (requestDTO.getIdOrden() != 0)) {
				throw new NroServicioNotFoundException(
						"No se encuentra registrado la orden en la tabla mantenimiento.");
			}

			System.out.println("EXISTE ORDEN: " + existeOrden);

			OrdenEntity ordenResponse = new OrdenEntity();
			WorkflowEntity wkfResponse = new WorkflowEntity();
			if (Boolean.FALSE.equals(existeOrden)) {
				WorkflowEntity wkf = new WorkflowEntity();
				wkf.setIdDescriptor("ordenmantenimientoWF");
				wkf.setIdOldState("Emitida");
				wkf.setIdState("Emitida");
				wkf.setParentType("com.enel.scom.api.gestion.service.impl.OrdenMantenimientoImpl");
				wkf.setVersion(0);
				wkf.setFechaUltimoEstado(new Date());
				wkf.setId(wkfWorkflowRepository.generaId());
				wkfResponse = wkfWorkflowRepository.save(wkf);
			}

			OrdenEntity orden = Boolean.FALSE.equals(existeOrden) ? new OrdenEntity()
					: ordenRepository.findId(requestDTO.getIdOrden());
			if (Boolean.FALSE.equals(existeOrden)) {
				orden.setNroOrden(Long.toString(ordenMantenimientoRepository.generarNumeroOrden()));
				orden.setId(ordenRepository.generaId());
				orden.setActivo(true);
				orden.setUsuarioCrea(usuario);
				orden.setUsuarioRegistro(usuario);
				orden.setFechaRegistro(new Date());
				orden.setFechaCreacion(new Date());
				orden.setFechaIngresoEstadoActual(new Date());
				orden.setMotivo(motivoRepository.findId(idMotivo));
				orden.setWorkflow(wkfResponse);
				orden.setTipoOrden(tipoOrden);
				orden.setIdServicio(cliente.getIdServicio());
				orden.setDiscriminador("ORDEN_MANTENIMIENTO");
				orden.setBuzonEntity(buzon);

			} else {
				orden.setUsuarioModif(usuario);
				orden.setFechaModif(new Date());
			}

			ordenResponse = ordenRepository.save(orden);

			System.out.println(ordenResponse);

			OrdenMantenimientoEntity ordenMantenimiento = Boolean.FALSE.equals(existeOrden)
					? new OrdenMantenimientoEntity()
					: ordenMantenimientoRepository.findByIdOrden(requestDTO.getIdOrden());
			if (Boolean.FALSE.equals(existeOrden)) {
				ordenMantenimiento.setIdOrden(ordenResponse.getId());
				ordenMantenimiento.setOrigen("SCOM");
			}
			ordenMantenimiento.setTrabajo(trabajo);
			ordenMantenimiento.setPrioridad(prioridad);
			ordenMantenimiento.setTema(tema);
			ordenMantenimiento.setTipoCreacion("Individual-FE");
			ordenMantenimiento.setCodProceso(null); // Ya que es un caso individual, el valor es nulo.
			ordenMantenimientoRepository.save(ordenMantenimiento);

			/**
			 * Se elimina todas las tareas registradas
			 */
			if (Boolean.TRUE.equals(existeOrden)) {
				tareaEjecutadaRepository.deleteByIdOrden(requestDTO.getIdOrden());
			}

			/**
			 * Se vuelve a crear nuevamente
			 */
			requestDTO.getTareas().stream().forEach(k -> {
				Optional<OrmTareaEntity> tarea = ormTareaRepository.findById(k.getIdTarea());
				TareaEjecutadaEntity tareaEjecutada = new TareaEjecutadaEntity();
				Long idTareaEjecutada = tareaEjecutadaRepository.generarId();
				tareaEjecutada.setId(idTareaEjecutada);
				tareaEjecutada.setTarea(tarea.get());
				tareaEjecutada.setFechaCreacion(new Date());
				tareaEjecutada.setOrden(ordenMantenimiento.getIdOrden());
				tareaEjecutadaRepository.save(tareaEjecutada);
			});

			OrdenDerivaEntity ordenDerivable = Boolean.FALSE.equals(existeOrden) ? new OrdenDerivaEntity()
					: ordenDerivaRepository.findByIdOrden(requestDTO.getIdOrden());
			if (Boolean.FALSE.equals(existeOrden)) {
				ordenDerivable.setOrden(ordenResponse.getId());
			}
			ordenDerivable.setIdResponsable(requestDTO.getIdResponsable());
			ordenDerivable.setIdUltResponsable(requestDTO.getIdResponsable());
			ordenDerivaRepository.save(ordenDerivable);

			ObservacionEntity observacionModel = (observacionRepository.existeIdOrden(requestDTO.getIdOrden()) == 0)
					? new ObservacionEntity()
					: observacionRepository.findIdOrden(requestDTO.getIdOrden());

			observacionModel.setTexto(requestDTO.getObservaciones());
			if (Boolean.FALSE.equals(existeOrden)) {
				Long idObservacion = observacionRepository.generarIdObservacion();
				observacionModel.setIdObservacion(idObservacion);
				observacionModel.setStateName("Creada");
				observacionModel.setIdOrden(ordenResponse.getId());
				observacionModel.setFechaObservacion(new Date());
				observacionModel.setUsuario(usuario);
				observacionModel.setDiscriminator("ObservacionOrden");
			}
			observacionRepository.save(observacionModel);

			OrdenEntity orden2 = ordenRepository.findId(ordenResponse.getId());
			// actualizacion de la fecha de vencimiento
			Optional<Date> ordenFechaVencimiento = ordenRepository.obtenerFechaVencimiento2(ordenResponse.getId());
			if (ordenFechaVencimiento.isPresent()) {
				orden2.setFechaVencimiento(ordenFechaVencimiento.get());
			}
			ordenRepository.save(orden2);

			return ordenResponse;

		} catch (NoExistEntityException e) {
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * Se obtiene la orden de mantenimiento para editar y mostrar el detalle.
	 * 
	 * @param id
	 */
	@Override
	public OrdenDataResponseDTO getId(Long id) {
		List<Object[]> ordenMantenimiento = ordenMantenimientoRepository.findIdOrden(id);
		List<Object[]> detalles = ormTareaRepository.findByIdOrden(id);
		List<OrmTareaOrdenResponseDTO> detalleDTO = new ArrayList<>();
		String estadoWkf = wkfWorkflowRepository.estado(id);

		OrdenDataResponseDTO ordenDTO = new OrdenDataResponseDTO();
		ordenMantenimiento.stream().forEach(k -> {
			ordenDTO.setIdOrden((k[0] == null) ? 0 : Long.valueOf(k[0].toString()));
			ordenDTO.setNroOrden((k[1] == null) ? "" : k[1].toString());
			ordenDTO.setNroServicio((k[2] == null) ? "" : k[2].toString());
			ordenDTO.setNroCuenta((k[3] == null) ? "" : k[3].toString());
			ordenDTO.setNombre((k[4] == null) ? "" : k[4].toString());
			ordenDTO.setApellidoPat((k[5] == null) ? "" : k[5].toString());
			ordenDTO.setApellidoMat((k[6] == null) ? "" : k[6].toString());
			ordenDTO.setDistrito((k[7] == null) ? "" : k[7].toString());
			ordenDTO.setRutaLectura((k[8] == null) ? "" : k[8].toString());
			ordenDTO.setObservacion((k[9] == null) ? "" : k[9].toString());
			ordenDTO.setDocumento((k[10] == null) ? "" : k[10].toString());
			ordenDTO.setTipoAcometida((k[11] == null) ? "" : k[11].toString());
			ordenDTO.setIdTrabajo((k[12] == null) ? 0 : Long.valueOf(k[12].toString()));
			ordenDTO.setIdPrioridad((k[13] == null) ? 0 : Long.valueOf(k[13].toString()));
			ordenDTO.setIdResponsable((k[14] == null) ? 0 : Long.valueOf(k[14].toString()));
			ordenDTO.setTipoResponsable((k[15] == null) ? "" : k[15].toString());
			ordenDTO.setDireccion((k[16] == null) ? "" : k[16].toString());
			ordenDTO.setIdOrdenSc4j((k[17] == null) ? 0 : Long.valueOf(k[17].toString()));
			ordenDTO.setEstadoWkf((estadoWkf == null) ? "" : estadoWkf);
		});

		detalles.stream().forEach(t -> {
			OrmTareaOrdenResponseDTO ormTarea = new OrmTareaOrdenResponseDTO();
			ormTarea.setIdTarea((t[0] == null) ? 0 : Long.valueOf(t[0].toString()));
			ormTarea.setCodigo((t[1] == null) ? "" : t[1].toString());
			ormTarea.setTarea((t[2] == null) ? "" : t[2].toString());
			ormTarea.setTipoTarea((t[3] == null) ? "" : t[3].toString());
			ormTarea.setCambio((t[4] == null) ? "" : t[4].toString());
			ormTarea.setContraste((t[5] == null) ? "" : t[5].toString());
			ormTarea.setValor((t[6] == null) ? "" : t[6].toString());
			detalleDTO.add(ormTarea);
		});

		ordenDTO.setOrmTareas(detalleDTO);

		// R.I. REQSCOM06 12/07/2023 INICIO
		try {
			String fechaCreacionOrden = ordenRepository.obtenerFechaCreacion(ordenDTO.getNroOrden());
			ordenDTO.setFechaCreacionOrden(fechaCreacionOrden);
		} catch (Exception e) {
			e.printStackTrace();
		}

		/*
		try {
			XmlDTO xml = transferRepository.obtenerXmlOrdenTrabajo(ordenDTO.getNroOrden(), 2L);
			if (xml != null) {
				 * if (xml.getObservacion() != null) {
				 * ordenDTO.setObservacionTecnico(xml.getObservacion()); } else {
				 * ordenDTO.setObservacionTecnico(""); }
				ordenDTO.setFechaEjecucion(xml.getFechaEjecucion());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		*/
		
		List<Long> idMagnitudList = ordenMantenimientoRepository.obtenerIdMagnitud(id);
		List<LecturasDTO> lecturasDTO = medMagnitudRepository.obtenerLecturas(idMagnitudList);
		
		Lectura lectura;
		Map<Long, List<Lectura>> lecturasPorComponente = new HashMap<>();

		for (LecturasDTO lecturas : lecturasDTO) {
		    Long idComponente = lecturas.getIdComponente();
		    lectura = new Lectura();
		    lectura.setEstadoLeido(lecturas.getEstadoLeido());
		    lectura.setFechaLectura(lecturas.getFechaLectura());
		    lectura.setHorarioLectura(lecturas.getHorarioLectura());
		    lectura.setTipoLectura(lecturas.getTipoLectura());
		    lectura.setAccionMedidor(lecturas.getAccionMedidor());

		    List<Lectura> lecturasComponente = lecturasPorComponente.get(idComponente);
		    if (lecturasComponente == null) {
		        lecturasComponente = new ArrayList<>();
		        lecturasPorComponente.put(idComponente, lecturasComponente);
		    }
		    lecturasComponente.add(lectura);
		}

		List<Medidor> medidores = new ArrayList<>();
		for (Map.Entry<Long, List<Lectura>> entry : lecturasPorComponente.entrySet()) {
		    Long idComponente = entry.getKey();
		    List<Lectura> lecturasComponente = entry.getValue();

		    Medidor medidor = new Medidor();
		    MedidorDTO medidorDTO = medidorRepository.obtenerMedidor(idComponente);
		    medidor.setNumeroMedidor(medidorDTO.getNumeroMedidor());
		    medidor.setMarcaMedidor(medidorDTO.getMarcaMedidor());
		    medidor.setModeloMedidor(medidorDTO.getModeloMedidor());
		    medidor.setLecturas(lecturasComponente);
		    medidor.setAccionMedidor(lecturasComponente.get(0).getAccionMedidor());
		    medidores.add(medidor);
		}
		ordenDTO.setMedidores(medidores);
		
		try {
			ObservacionDTO observacion = ordenRepository.obtenerObservacionTecnicoMANT(id);
	        ordenDTO.setObservacionTecnico(observacion.getTexto());
	        ordenDTO.setFechaEjecucion(observacion.getFechaObservacion());
		} catch (Exception e) {
			e.printStackTrace();
		}
		// R.I. REQSCOM06 12/07/2023 FIN
		return ordenDTO;
	}

}
