package com.enel.scom.api.gestion.dto.response;

import lombok.Data;

@Data
public class ModeloMarcaResponseDTO {
	//private Long id;
	private String id; //CHR 20-07-23
	private String descripcion;
	private String codigo;
}
