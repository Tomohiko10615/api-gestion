package com.enel.scom.api.gestion.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity(name = "med_modelo")
@Table(name = "med_modelo", schema = "schscom")
@Data
public class ModeloEntity implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "id")
	private Long id;
	
	@Column(name = "cod_modelo")
	private String codModelo;
	
	@Column(name = "des_modelo")
	private String desModelo;
	
	@Column(name = "cant_anos_vida")
	private Long cantAnosVida;
	
	@Column(name = "cant_anos_almacen")
	private Long cantAnosAlmacen;
	
	@Column(name = "nro_sellos_medidor")
	private Long nroSellosMedidor;
	
	@Column(name = "nro_sellos_bornera")
	private Long nroSellosBornera;
	
	@Column(name = "nro_registrador")
	private Long nroRegistrador;
	
	@Column(name = "es_reacondicionado")
	private String esReacondicionador;
	
	@Column(name = "es_reseteado")
	private String esReseteado;
	
	@Column(name = "es_patron")
	private String esPatron;
	
	@Column(name = "es_totalizador")
	private String esTotalizador;
	
	@Column(name = "clase_medidor")
	private String claseMedidor;
	
	@Column
	private Float constante;
	
	@Column(name = "cant_hilos")
	private Long cantHilos;
	
	@OneToOne
	@JoinColumn(name = "id_marca")
	private MarcaEntity marca;
	
	@OneToOne
	@JoinColumn(name = "id_fase")
	private FaseEntity fase;
	
	@OneToOne
	@JoinColumn(name = "id_amperaje")
	private AmperajeEntity amperaje;
	
	@OneToOne
	@JoinColumn(name = "id_tip_medicion")
	private TipoMedicionEntity tipoMedicion;
	
	@OneToOne
	@JoinColumn(name = "id_voltaje")
	private VoltajeEntity voltaje;
	
	@OneToOne
	@JoinColumn(name = "id_tension")
	private TensionEntity tension;
	
	@OneToOne
	@JoinColumn(name = "id_tecnologia")
	private TecnologiaEntity tecnologia;
	
	@OneToOne
	@JoinColumn(name = "id_tip_registrador")
	private RegistradorEntity registrador;
	
	@OneToOne
	@JoinColumn(name = "id_unidad_medida_cte")
	private UnidadMedidaConstanteEntity unidadMedidaConstante;

	@Column
	private String activo;
	
	@OneToOne
	@JoinColumn(name = "id_usuario_registro")
	private UsuarioEntity usuarioRegistro;
	
	@OneToOne
	@JoinColumn(name = "id_usuario_modif")
	private UsuarioEntity usuarioModif;

	@Column(name = "fec_registro")
	private Date fecRegistro;
	
	@Column(name = "fec_modif")
	private Date fecModif;

	@Column(name = "fec_creacion")
	private Date fecCreacion;

}
