package com.enel.scom.api.gestion.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.enel.scom.api.gestion.model.PerfilEntity;
import com.enel.scom.api.gestion.service.PerfilService;

import java.util.List;

@RestController
@RequestMapping("api/perfil")
public class PerfilController {

    @Autowired
    private PerfilService perfilService;

	/*
	 * @PostMapping("/create") public PerfilEntity create(@RequestBody PerfilEntity
	 * Perfil) { return perfilService.crearPerfil(Perfil); }
	 * 
	 * @PutMapping("/update") public PerfilEntity update(@RequestBody PerfilEntity
	 * Perfil) throws Exception { return perfilService.actualizarPerfil(Perfil); }
	 */

	/*
	 * @DeleteMapping("/delete/{id}") public void delete(@PathVariable("id") Long
	 * id) { try { perfilService.eliminarPerfil(id); } catch (NullPointerException
	 * ex) { } }
	 * 
	 * @GetMapping("/findById/{id}") public PerfilEntity
	 * findById(@PathVariable("id") Long id) { return
	 * perfilService.buscarPorIdPerfil(id).get(); }
	 * 
	 *
	 * 
	 * @GetMapping(value = "/menuPerfil/{codperfil}") public List<RolesDTO>
	 * findMenuPerfil(@PathVariable("codperfil") Long codperfil) { return
	 * perfilService.buscarMenuPorPerfil(codperfil); }
	 */
    
    @GetMapping("/findAll") public List<PerfilEntity> findAll() { 
    	return perfilService.buscarTodosPerfiles();
    }
 
}
