package com.enel.scom.api.gestion.dto;

import com.enel.scom.api.gestion.enums.EnumCamposActualizaOrdInspCNR;

import lombok.Builder;
import lombok.Data;

@Data
public class CampoActualizarCNRDTO {

	public EnumCamposActualizaOrdInspCNR nombreCampo;
	public String valorActualEnBD;
	public String valorNuevoActualizar;
	public Boolean actualizado;
	
	
}
