//R.I. CORRECION 11/09/2023 INICIO
package com.enel.scom.api.gestion.dto.xml;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "medidores")
public class Medidores {

    private List<Medidor> medidores;

    @XmlElements({
            @XmlElement(name = "item", type = Medidor.class)
    })
    public List<Medidor> getMedidores() {
        return medidores;
    }

    public void setMedidores(List<Medidor> medidores) {
        this.medidores = medidores;
    }
}
//R.I. CORRECION 11/09/2023 FIN