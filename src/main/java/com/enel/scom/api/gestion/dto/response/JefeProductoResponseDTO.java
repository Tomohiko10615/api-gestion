package com.enel.scom.api.gestion.dto.response;

import lombok.Data;

@Data
public class JefeProductoResponseDTO {
	private Long id;
	private String nombre;
	private String apellidoMat;
	private String apellidoPat;
}
