package com.enel.scom.api.gestion.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.model.TemaEntity;

@Repository
public interface TemaRepository extends JpaRepository<TemaEntity, Long>{
    Optional<TemaEntity> findByCode(String code);
}
