package com.enel.scom.api.gestion.service;

import java.util.List;

import com.enel.scom.api.gestion.dto.response.UbicacionResponseDto;

public interface UbicacionService {

	List<UbicacionResponseDto> getAll();
}
