package com.enel.scom.api.gestion.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.model.ClienteEntity;

@Repository
public interface ClienteRepository extends CrudRepository<ClienteEntity, Long>{

	ClienteEntity findByIdServicio(Long idServicio);

	@Query(value = "SELECT * FROM cliente WHERE nro_servicio = :nroServicio", nativeQuery = true)
	ClienteEntity findByNroServicio(@Param("nroServicio") Long nroServicio);
	
	/**
	 * Se utiliza para las ordenes de inspección y normalización
	 */
	@Query(value = "SELECT * FROM cliente WHERE id = :id", nativeQuery = true)
	ClienteEntity findId(@Param("id") Long id);
	
	/**
	 * Se utiliza para las ordenes de contraste
	 */
	@Query(value = "SELECT med_componente.nro_componente, \r\n"
			+ "med_marca.cod_marca, \r\n"
			+ "med_tip_modelo.cod_tip_modelo, \r\n"
			+ "com_fase.cod_fase, \r\n"
			+ "med_modelo.cod_modelo, med_est_componente.des_est_componente \r\n"
			+ "FROM med_componente, \r\n"
			+ "cliente, \r\n"
			+ "med_est_componente, \r\n"
			+ "med_marca, \r\n"
			+ "med_modelo, \r\n"
			+ "med_tip_modelo, \r\n"
			+ "com_fase \r\n"
			+ "WHERE cliente.nro_servicio = :nroServicio\r\n"
			+ "AND med_componente.id_ubicacion = cliente.id_servicio \r\n"
			+ "AND med_est_componente.id = med_componente.id_est_componente \r\n"
			+ "AND med_marca.id = med_modelo.id_marca \r\n"
			+ "AND med_modelo.id = med_componente.id_modelo \r\n"
			+ "AND med_marca.id = med_tip_modelo.id_marca \r\n"
			+ "AND med_tip_modelo.id = med_modelo.id_tip_modelo \r\n"
			+ "AND med_modelo.id_fase = com_fase.id \r\n"
			+ "AND med_est_componente.cod_est_componente = 'I'", nativeQuery = true)
	List<Object[]> findByMedidor(@Param("nroServicio") Long nroServicio);

	/**
	 * Se utiliza para la orden de mantenimiento
	 */
	@Query(value = "SELECT * FROM cliente WHERE nro_cuenta = :nroCuenta", nativeQuery = true)
	ClienteEntity findByNroCuenta(@Param("nroCuenta") Long nroCuenta);
	
}
