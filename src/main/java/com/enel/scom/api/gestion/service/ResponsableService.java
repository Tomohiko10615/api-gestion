package com.enel.scom.api.gestion.service;

import java.util.List;

import com.enel.scom.api.gestion.dto.response.ResponsableResponseDTO;

public interface ResponsableService {
    List<ResponsableResponseDTO> getResponsable(String responsable);
}
