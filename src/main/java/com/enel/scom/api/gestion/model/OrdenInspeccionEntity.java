package com.enel.scom.api.gestion.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import lombok.Data;
import lombok.ToString;

@Entity(name = "dis_ord_insp")
@Data
@ToString
public class OrdenInspeccionEntity implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	private Long id;

	@Column(name = "tip_inspeccion")
	private String tipInspeccion;

	@Column(name = "id_orden")
	private Long idOrden;
	
	@OneToOne
	@JoinColumn(name = "id_producto")
	private ProductoEntity producto;
	
	@OneToOne
	@JoinColumn(name = "id_jefe_producto")
	private JefeProductoEntity jefeProducto;
	
	@OneToOne
	@JoinColumn(name = "id_contratista")
	private ContratistaEntity contratista;
	
	@Column(name = "con_info_completa")
	private String comInfoCompleta;
	
	@Column(name = "auto_generada")
	private String autoGenerada;
	
	@Column(name = "entre_calles")
	private String entreCalles;

	@OneToOne
	@JoinColumn(name = "id_seginsp")
	private DisSegInspEntity disSegInspEntity;
	
	@Column(name = "id_ejecutor")
	private Long idEjecutor;
}
