package com.enel.scom.api.gestion.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.enel.scom.api.gestion.dto.response.JefeProductoResponseDTO;
import com.enel.scom.api.gestion.service.JefeProductoService;

@RestController
@RequestMapping("/api/jefeProducto")
public class JefeProductoController {
	@Autowired
	JefeProductoService jefeProductoService;
	
	@GetMapping
	public ResponseEntity<List<JefeProductoResponseDTO>> getActivos() {
		List<JefeProductoResponseDTO> responses = jefeProductoService.getActivos();
		return new ResponseEntity<>(responses, HttpStatus.OK);
	}
}
