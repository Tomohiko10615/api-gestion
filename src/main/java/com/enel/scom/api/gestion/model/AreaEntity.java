package com.enel.scom.api.gestion.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;

@Entity(name = "seg_area")
@Data
public class AreaEntity {
    
    @Id
    @Column
    private Long id;

    @Column(name = "descripcion")
    private String descripcion;
}
