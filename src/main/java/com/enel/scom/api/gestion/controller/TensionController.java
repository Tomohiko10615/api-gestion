package com.enel.scom.api.gestion.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.enel.scom.api.gestion.model.TensionEntity;
import com.enel.scom.api.gestion.repository.TensionRepository;

@RestController
@RequestMapping("/api/tension")
public class TensionController {

	@Autowired
	TensionRepository tensionRepository;

	@GetMapping
	public ResponseEntity<List<TensionEntity>> getAll() {
		List<TensionEntity> responses = tensionRepository.getTensionesActivo();
		return new ResponseEntity<>(responses, HttpStatus.OK);
	}
}
