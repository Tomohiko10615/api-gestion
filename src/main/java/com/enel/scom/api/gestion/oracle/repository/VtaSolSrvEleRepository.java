package com.enel.scom.api.gestion.oracle.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.oracle.model.VtaSolSrvEleEntity;

@Repository
public interface VtaSolSrvEleRepository extends JpaRepository<VtaSolSrvEleEntity, Long> {
	
	@Query(value="select count(*) from  vta_sol_srv_ele sele, srv_electrico se, ubi_ruta ur "
			+ "where ur.cod_ruta =:ruta "
			+ "and sele.id_srv_electrico = se.id_servicio "
			+ "and sele.id_ruta_lectura = ur.id_ruta "
			+ "and se.id_estado not in(1,6)", nativeQuery = true)
	public int validarCodigoRutaLecturaAsignadaServicioPrimero(@Param("ruta") String ruta);
	
	
	@Query(value="select count(*) from srv_electrico se, "
			+ "ubi_ruta ur "
			+ "where ur.cod_ruta =:ruta "
			+ "and se.id_ruta_lectura = ur.id_ruta "
			+ "and se.id_estado not in(1,6)", nativeQuery = true)
	public int validarCodigoRutaLecturaAsignadaServicioSegundo(@Param("ruta") String ruta);
	
	
	
	@Query(value="select count(*) from  vta_sol_srv_ele sele, srv_electrico se, ubi_ruta ur "
			+ "where ur.cod_ruta =:ruta "
			+ "and sele.id_srv_electrico = se.id_servicio "
			+ "and sele.id_ruta_corte = ur.id_ruta "
			+ "and se.id_estado not in(1,6)", nativeQuery = true)
	public int validarCodigoRutaCorteAsignadaServicioPrimero(@Param("ruta") String ruta);
	
	
	@Query(value="select count(*) from srv_electrico se, "
			+ "ubi_ruta ur "
			+ "where ur.cod_ruta =:ruta "
			+ "and se.id_ruta_corte = ur.id_ruta "
			+ "and se.id_estado not in(1,6)", nativeQuery = true)
	public int validarCodigoRutaCorteAsignadaServicioSegundo(@Param("ruta") String ruta);
	
	
	
	@Query(value="select count(*) from  vta_sol_srv_ele sele, srv_electrico se, ubi_ruta ur "
			+ "where ur.cod_ruta =:ruta "
			+ "and sele.id_srv_electrico = se.id_servicio "
			+ "and sele.id_ruta_facturacion = ur.id_ruta "
			+ "and se.id_estado not in(1,6)", nativeQuery = true)
	public int validarCodigoRutaFacturacionAsignadaServicioPrimero(@Param("ruta") String ruta);
	
	
	@Query(value="select count(*) from nuc_servicio ns, srv_electrico se, fac_config_doc cd, ubi_ruta ur "
			+ "where ur.cod_ruta = :ruta "
			+ "and se.id_servicio = ns.id_servicio "
			+ "and ns.tipo = 'ELECTRICO' "
			+ "and ns.id_cuenta = cd.id_cuenta "
			+ "and ur.id_ruta = cd.id_ruta_fact "
			+ "and se.ID_ESTADO not in(1,6)", nativeQuery = true)
	public int validarCodigoRutaFacturacionAsignadaServicioSegundo(@Param("ruta") String ruta);
	
	
	@Query(value="select count(*) from  vta_sol_srv_ele sele, srv_electrico se, ubi_ruta ur "
			+ "where ur.cod_ruta =:ruta "
			+ "and sele.id_srv_electrico = se.id_servicio "
			+ "and sele.id_ruta_reparto = ur.id_ruta "
			+ "and se.id_estado not in(1,6)", nativeQuery = true)
	public int validarCodigoRutaRepartoAsignadaServicioPrimero(@Param("ruta") String ruta);
	
	
	@Query(value="select count(*) from nuc_servicio ns, srv_electrico se, rep_config_rep rep, ubi_ruta ur "
			+ "where "
			+ "se.id_servicio = ns.id_servicio "
			+ "and ns.tipo = 'ELECTRICO' "
			+ "and ns.id_servicio = rep.id_servicio "
			+ "and rep.id_ruta = ur.id_ruta "
			+ "and ur.cod_ruta =:ruta "
			+ "and se.ID_ESTADO not in(1,6)", nativeQuery = true)
	public int validarCodigoRutaRepartoAsignadaServicioSegundo(@Param("ruta") String ruta);
}
