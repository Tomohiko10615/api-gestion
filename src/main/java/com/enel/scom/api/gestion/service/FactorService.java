package com.enel.scom.api.gestion.service;

import java.util.List;

import com.enel.scom.api.gestion.dto.request.MedFacMedModRequestDTO;
import com.enel.scom.api.gestion.dto.response.FactorResponseDTO;
import com.enel.scom.api.gestion.exception.NroServicioNotFoundException;
import com.enel.scom.api.gestion.model.MedFacMedModEntity;

public interface FactorService {
	List<FactorResponseDTO> getAll();
	
	MedFacMedModEntity crearFactor(MedFacMedModRequestDTO medModelo) throws NroServicioNotFoundException;
	MedFacMedModEntity delete(Long idMedidaModelo, Long idFactor);
	
	List<FactorResponseDTO> getFactoresPorMedidaModelo(Long idMedidaModelo);
}
