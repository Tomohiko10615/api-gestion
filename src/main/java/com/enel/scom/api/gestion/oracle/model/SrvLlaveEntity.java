package com.enel.scom.api.gestion.oracle.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;

@Entity(name = "srv_llave")
@Data
public class SrvLlaveEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
   @Id
   @Column(name="id_llave")
   private Long idLlave;
	
   @Column(name="id_sed")
   private Long idSet;
	
   @Column(name = "codigo")
   private String codigo;
   
   @Column(name = "descripcion")
   private String descripcion;
   
   @Column(name = "activo")
   private String activo;
}
