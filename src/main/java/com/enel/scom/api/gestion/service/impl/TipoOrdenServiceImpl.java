package com.enel.scom.api.gestion.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.scom.api.gestion.dto.response.TipoOrdenResponseDTO;
import com.enel.scom.api.gestion.repository.TipoOrdenRepository;
import com.enel.scom.api.gestion.service.TipoOrdenService;

@Service
public class TipoOrdenServiceImpl implements TipoOrdenService{
	
	@Autowired
	TipoOrdenRepository tipoOrdenRepository;

	@Override
	public List<TipoOrdenResponseDTO> obtenerTipoOrdenByCodProceso(String codTipoProceso) {
		List<Object[]> objects = tipoOrdenRepository.obtenerTipoOrdenByCodProceso(codTipoProceso);
		List<TipoOrdenResponseDTO> dtos = new ArrayList<>();
		
		objects.stream().forEach(k -> {
			TipoOrdenResponseDTO dto = new TipoOrdenResponseDTO();
			dto.setId(Long.valueOf(k[0].toString()));
			dto.setDescripcion(k[1].toString());
			dto.setCodigoTipoOrden(k[2].toString());
			dtos.add(dto);
		});
		return dtos;
	}

	@Override
	public String obtenerIdsTipoOrdenByCodProceso(String codTipoProceso) {
		String[] objects = tipoOrdenRepository.obtenerIdsTipoOrdenByCodProceso(codTipoProceso);
		String joinedIds = String.join(",", objects);
		return joinedIds;
	}
	

}
