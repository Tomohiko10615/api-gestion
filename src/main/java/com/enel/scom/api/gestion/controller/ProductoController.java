package com.enel.scom.api.gestion.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.enel.scom.api.gestion.model.ProductoEntity;
import com.enel.scom.api.gestion.repository.ProductoRepository;

@RestController
@RequestMapping("/api/producto")
public class ProductoController {
	@Autowired
	ProductoRepository productoRepository;
	
	@GetMapping
	public ResponseEntity<List<ProductoEntity>> getActivos() {
		List<ProductoEntity> responses = productoRepository.getActivos();
		return new ResponseEntity<>(responses, HttpStatus.OK);
	}
	
	@GetMapping("/motivo/{idMotivo}")
	public ResponseEntity<List<ProductoEntity>> getProductoActivos(@PathVariable Long idMotivo) {
		List<ProductoEntity> responses = productoRepository.getMotivos(idMotivo);
		return new ResponseEntity<>(responses, HttpStatus.OK);
	}
}
