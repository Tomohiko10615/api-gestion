package com.enel.scom.api.gestion.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.enel.scom.api.gestion.dto.request.MarcaRequestDTO;
import com.enel.scom.api.gestion.dto.response.MarcaPaginacionResponseDTO;
import com.enel.scom.api.gestion.dto.response.MarcaResponseDTO;
import com.enel.scom.api.gestion.exception.NroServicioNotFoundException;
import com.enel.scom.api.gestion.model.MarcaEntity;

public interface MarcaService {
	MarcaEntity save(MarcaRequestDTO marca) throws NroServicioNotFoundException;
	MarcaEntity delete(Long id);
	MarcaEntity update(MarcaRequestDTO marca, Long id);
	Page<MarcaPaginacionResponseDTO> getMarcasPaginacion(Pageable paging, String codMarca, String desMarca, String usuario, String fechaInicio, String fechaFin);
	List<MarcaResponseDTO> getMarcaActive();
}
