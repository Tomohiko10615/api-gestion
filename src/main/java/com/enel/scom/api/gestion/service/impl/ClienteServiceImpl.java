package com.enel.scom.api.gestion.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.scom.api.gestion.dto.response.ClienteInspeccionResponseDTO;
import com.enel.scom.api.gestion.dto.response.InspeccionNotificadaResponseDTO;
import com.enel.scom.api.gestion.exception.NroServicioNotFoundException;
import com.enel.scom.api.gestion.model.ClienteEntity;
import com.enel.scom.api.gestion.repository.ClienteRepository;
import com.enel.scom.api.gestion.repository.OrdenRepository;
import com.enel.scom.api.gestion.service.ClienteService;

@Service
public class ClienteServiceImpl implements ClienteService{
	
	@Autowired
	ClienteRepository clienteRepository;
	
	@Autowired
	OrdenRepository ordenRepository;
	
	@Override
	public ClienteEntity findId(Long id) {
		return clienteRepository.findId(id);
	}

	@Override
	public ClienteEntity getIdServicio(Long idServicio) {
		return clienteRepository.findByIdServicio(idServicio);
	}

	@Override
	public ClienteInspeccionResponseDTO getNroServicio(Long nroServicio) throws NroServicioNotFoundException {
		
			ClienteEntity cliente = clienteRepository.findByNroServicio(nroServicio);
			
			if(cliente == null) {
				throw new NroServicioNotFoundException("Nro de servicio no existe");
			}
			
			List<Object[]> medidor = clienteRepository.findByMedidor(nroServicio);
			List<Object[]> ordenes = ordenRepository.getIdServicio(cliente.getIdServicio());
			List<InspeccionNotificadaResponseDTO> ordenesDTO = new ArrayList<>();
			
			ClienteInspeccionResponseDTO dto = new ClienteInspeccionResponseDTO();
			
			dto.setApellidoMat(cliente.getApellidoMat());
			dto.setApellidoPat(cliente.getApellidoMat());
			dto.setDireccion(cliente.getTextoDireccion());
			dto.setDistrito(cliente.getDistrito());
			dto.setNombre(cliente.getNombre());
			dto.setNroCuenta(cliente.getNroCuenta());
			dto.setNroServicio(cliente.getNroServicio());
			dto.setRutaLectura(cliente.getCodRuta());
			
			ordenes.stream().forEach(k -> {
				InspeccionNotificadaResponseDTO inspeccion = new InspeccionNotificadaResponseDTO();
				
				inspeccion.setId(Long.valueOf(k[0].toString()));
				inspeccion.setNroOrden((k[1] == null) ? "" : k[1].toString());
				inspeccion.setFechaCreacion((k[2] == null) ? "" : k[2].toString());
				inspeccion.setContratista((k[3] == null) ? "" : k[3].toString());
				inspeccion.setTipoInspeccion((k[4] == null) ? "" : k[4].toString());

				ordenesDTO.add(inspeccion);
			});
			
			dto.setInspecciones(ordenesDTO); 
			
			medidor.stream().forEach(t -> {
				dto.setNroComponente((t[0] == null) ? "": t[0].toString());
				dto.setCodMarca((t[1] == null) ? "": t[1].toString());
				dto.setCodTipoModelo((t[2] == null) ? "": t[2].toString());
				dto.setCodFase((t[3] == null) ? "": t[3].toString());
				dto.setCodModelo((t[4] == null) ? "": t[4].toString());
				dto.setEstadoMedidor((t[5] == null) ? "": t[5].toString());
			});

			return dto;
		
		
	}

	@Override
	public ClienteEntity getNroCuenta(Long nroCuenta) {
		return clienteRepository.findByNroCuenta(nroCuenta);
	}

}
