package com.enel.scom.api.gestion.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.dto.MedidorDTO;
import com.enel.scom.api.gestion.model.MedidorEntity;
import com.enel.scom.api.gestion.oracle.repository.MedMagnitudRepository;

@Repository
public interface MedidorRepository extends JpaRepository<MedidorEntity, Long>{
	@Query(value="SELECT nextval('sqmedcomponente')", nativeQuery=true)
	public Long generaId();

	@Query(value = "select mc.id, mc.nro_componente, mmar.des_marca, mm.des_modelo, mecom.des_est_componente as estado, mev.fecha_estado as fec_estado, mc.fec_registro, u.username \r\n"
			+ "from med_componente mc\r\n"
			+ "left join med_modelo mm on mc.id_modelo = mm.id\r\n"
			+ "left join med_marca mmar on mmar.id = mm.id_marca\r\n"
			+ "left join mov_est_componente mev on mev.id_med_componente = mc.id\r\n"
			+ "left join med_est_componente mecom on mecom.id = mev.id_estado_final\r\n"
			+ "left join usuario u on mc.id_usuario_registro = u.id\r\n"
			+ "where mc.activo = 'S'", nativeQuery = true)
	Page<Object[]> getModelosPaginacion(Pageable paging);

	@Modifying
	@Transactional
	@Query(value = "UPDATE med_componente SET activo = 'N' WHERE id = :id", nativeQuery = true)
	void desactivar(@Param("id") Long id);
	
	@Query(value = "SELECT * FROM med_componente WHERE id = :id", nativeQuery = true)
	MedidorEntity findId(@Param("id") Long id);
	
	@Query(value = "select mc.id, mm.id as id_marca, mc.id_modelo, mc.nro_componente, mc.id_ubicacion, mc.serial_number, mmo.cod_modelo from med_componente mc\r\n"
			+ "left join med_modelo mmo on mc.id_modelo = mmo.id\r\n"
			+ "left join med_marca mm on mm.id = mmo.id_marca\r\n"
			+ "left join med_almacen ma on mc.id_ubicacion = ma.id\r\n"
			+ "where mc.id =  :id", nativeQuery = true)
	List<Object[]> getData(@Param("id") Long id);
	
	@Query(value = "select count(*) from schscom.med_componente mc "	
					+ "inner join schscom.med_modelo mm on mc.id_modelo = mm.id "
					+ "where mc.nro_componente =:nroMedidor AND mc.id_modelo = :idModelo AND mm.id_marca = :idMarca", nativeQuery = true)
	Integer findByNroMedidor(@Param("nroMedidor") String nroMedidor, @Param("idModelo") Long idModelo, @Param("idMarca") Long idMarca);
	
	// R.I. REQSCOM03 09/10/2023 INICIO
	@Query(value = "select mc.nro_componente as numeroMedidor, mmo.cod_modelo as modeloMedidor, mma.cod_marca as marcaMedidor\r\n"
			+ "from med_componente mc, med_modelo mmo, med_marca mma\r\n"
			+ "where mc.id = ? and mc.id_modelo = mmo.id and mmo.id_marca = mma.id ", nativeQuery = true)
	public MedidorDTO obtenerMedidor(Long idComponente);
	// R.I. REQSCOM03 09/10/2023 FIN
	
}
