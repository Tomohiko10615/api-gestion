package com.enel.scom.api.gestion.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.scom.api.gestion.dto.response.ParametroResponseDTO;
import com.enel.scom.api.gestion.model.ParametroEntity;
import com.enel.scom.api.gestion.repository.ParametroRepository;
import com.enel.scom.api.gestion.service.ParametroService;

@Service
public class ParametroServiceImpl implements ParametroService{
	
	@Autowired
	ParametroRepository parametroRepository;

	@Override
	public List<ParametroResponseDTO> getTipoInspeccion() {
		List<Object[]> objects = parametroRepository.getTipoInspeccion();
		List<ParametroResponseDTO> dtos = new ArrayList<>();
		
		objects.stream().forEach(k -> {
			ParametroResponseDTO dto = new ParametroResponseDTO();
			dto.setId(Long.valueOf(k[0].toString()));
			dto.setDescripcion(k[1].toString());
			dtos.add(dto);
		});
		
		return dtos;
	}

	@Override
	public List<ParametroResponseDTO> getSubTipo() {
		List<Object[]> objects = parametroRepository.getSubTipo();
		List<ParametroResponseDTO> dtos = new ArrayList<>();
		
		objects.stream().forEach(k -> {
			ParametroResponseDTO dto = new ParametroResponseDTO();
			dto.setId(Long.valueOf(k[0].toString()));
			dto.setDescripcion(k[1].toString());
			dtos.add(dto);
		});
		
		return dtos;
	}

	@Override
	public List<ParametroResponseDTO> getTipoProceso() {
		List<Object[]> objects = parametroRepository.getTipoProceso();
		return setDtoParametro(objects);
	}

	@Override
	public List<ParametroResponseDTO> getEstados() {
		List<Object[]> objects = parametroRepository.getEstados();
		return setDtoParametro(objects);
	}

	@Override
	public List<ParametroResponseDTO> getEstadosTransfer() {
		List<Object[]> objects = parametroRepository.getEstadosTransferencia();
		return setDtoParametro(objects);
	}
	
	/**
	 * Método reutilizable para settear a DTO 
	 * @param listado objetos repository
	 * @return listado DTO
	 */
	private List<ParametroResponseDTO> setDtoParametro(List<Object[]> objects) {
		List<ParametroResponseDTO> dtos = new ArrayList<>();
		
		objects.stream().forEach(k -> {
			ParametroResponseDTO dto = new ParametroResponseDTO();
			dto.setId(Long.valueOf(k[0].toString()));
			dto.setCodigo(k[1].toString());
			dto.setDescripcion(k[2].toString());
			dto.setValorNum(k[3] == null ? null : Double.valueOf(k[3].toString()));
			dto.setValorAlf(k[4] == null ? null : k[4].toString());
			dtos.add(dto);
		});
		
		return dtos;
	}

	@Override
	public List<ParametroResponseDTO> getTiposUbicacionMedidor() {
		List<Object[]> objects = parametroRepository.getTiposUbicacionMedidor();
		return setDtoParametro(objects);
	}

	@Override
	public ParametroResponseDTO obtenerRangoFechaBusqueda(String sistema, String entidad, String codigo) {
		ParametroResponseDTO dto = new ParametroResponseDTO();
		ParametroEntity parametro = parametroRepository.findBySistemaAndEntidadAndCodigo(sistema, entidad, codigo);
		dto.setId(parametro.getId_parametro());
		dto.setCodigo(parametro.getCodigo());
		dto.setDescripcion(parametro.getDescripcion());
		dto.setValorAlf(parametro.getValorAlf());
		dto.setValorNum(parametro.getValorNum());
		return dto;
	}
}
