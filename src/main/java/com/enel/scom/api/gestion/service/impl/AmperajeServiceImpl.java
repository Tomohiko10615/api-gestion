package com.enel.scom.api.gestion.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.scom.api.gestion.dto.response.AmperajeResponseDTO;
import com.enel.scom.api.gestion.repository.AmperajeRepository;
import com.enel.scom.api.gestion.service.AmperajeService;

@Service
public class AmperajeServiceImpl implements AmperajeService{

	@Autowired
	private AmperajeRepository amperajeRepository;

	@Override
	public List<AmperajeResponseDTO> getAll() {
		List<Object[]> amperajes = amperajeRepository.getAll();
		return amperajes.stream().map(amperaje ->  new AmperajeResponseDTO(Long.valueOf(amperaje[0].toString()), amperaje[1].toString())).collect(Collectors.toList());
	}
	
	
}
