package com.enel.scom.api.gestion.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;

@Entity(name = "orm_configuracion")
@Data
public class OrmConfiguracionEntity {
    
    @Id
    @Column(name = "id_configuracion")
    private Long id;

}
