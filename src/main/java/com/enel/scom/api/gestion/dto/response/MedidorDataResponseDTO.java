package com.enel.scom.api.gestion.dto.response;

import java.util.List;

import lombok.Data;

@Data
public class MedidorDataResponseDTO {
	private Long id;
	private Long id_marca;
	private Long id_ubicacion;
	private String id_modelo; // CHR 20-07-23
	private String serie;
	private String numero;
	private String cod_modelo;
	private List<MedidorMedidaResponseDTO> medidas;
}
