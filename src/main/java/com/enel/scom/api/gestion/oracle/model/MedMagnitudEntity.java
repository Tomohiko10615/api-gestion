package com.enel.scom.api.gestion.oracle.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

/***
 * clase adicionda
 * fecha:2023.09.26
 * motivo: requerimiento EPT Impacto SCOM CNR, REQCNR01
 * @author Indra
 *
 */
@Entity
@Table(name="med_magnitud")
@Data
public class MedMagnitudEntity {
	
	@Id
	@Column(name="id_magnitud")
	public Long idMagnitud;

	@Column(name="valor")
	public Double valor;
	
	
}
