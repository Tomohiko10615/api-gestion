package com.enel.scom.api.gestion.dto.response;

import lombok.Data;

@Data
public class AmperajeResponseDTO {
	
	public AmperajeResponseDTO(Long id, String descripcion) {
		this.id = id;
		this.desAmperaje = descripcion;
	}
	
	private Long id;
	private String desAmperaje;
}
