package com.enel.scom.api.gestion.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.model.OrmTipoTarea;

@Repository
public interface OrmTipoTareaRepository extends JpaRepository<OrmTipoTarea, Long>{
    
    @Query(value = "SELECT * FROM orm_tipo_tarea WHERE estado = 'S'", nativeQuery = true)
    List<OrmTipoTarea> getTipos();
}
