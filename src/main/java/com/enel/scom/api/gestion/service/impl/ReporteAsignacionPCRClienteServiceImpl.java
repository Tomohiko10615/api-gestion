package com.enel.scom.api.gestion.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.enel.scom.api.gestion.dto.response.ReporteAsignacionPCRClientePaginacionResponseDTO;
import com.enel.scom.api.gestion.dto.response.ReporteGraficoDTO;
import com.enel.scom.api.gestion.exception.AllNotFoundException;
import com.enel.scom.api.gestion.service.ReporteAsignacionPCRClienteService;

@Service
public class ReporteAsignacionPCRClienteServiceImpl implements ReporteAsignacionPCRClienteService {
	
	@PersistenceContext
	EntityManager entityManager;
	
	@Override
	public Page<ReporteAsignacionPCRClientePaginacionResponseDTO> obtenerReporteAsignacionPCRClientePaginacion(
			Pageable paging, String fechaInicio, String fechaFin, String situacionPcr, String nroCliente) {
		
		String sql = "select new com.enel.scom.api.gestion.dto.response.ReporteAsignacionPCRClientePaginacionResponseDTO( "
				+ "tb.numeroCliente, "
				+ "tb.nombre, tb.fechaActivacion, tb.estadoProceso) "
				+ "from (select cae.numeroCliente, cae.nombre, to_char(cae.fechaActivacion, 'DD/MM/YYYY'), cae.estadoProceso from schscom.cae_tmp_cliente cae "
		 		+ "union all select caeh.numeroCliente, caeh.nombre, caeh.fechaActivacion, caeh.estadoProceso from schscom.cae_tmp_historico caeh) TAB as tb";
		/*
		if ((!StringUtils.isBlank(fechaInicio)) || 
				(!StringUtils.isBlank(fechaFin)) || 
				(!StringUtils.isBlank(situacionPcr)) || 
				(!StringUtils.isBlank(nroCliente))) {
				
	            sql +=" where";
	        }
	        
	     if (!StringUtils.isBlank(situacionPcr)) {
        	sql +=  " mh.estadoFinal.id='" + idEstadoMedidor + "'";
        }
		
	        
		 if (!StringUtils.isBlank(situacionPcr)) {

			 if((!StringUtils.isBlank(tipoOrden))) {
				 sql += " and cae.estadoProceso ='" + estadoProceso +"'";
			 } else {
				 sql += " cae.estadoProceso ='" + estadoProceso +"'";
			 }
        }
		 
		  if (!StringUtils.isBlank(nroCliente)) {
	        	if((!StringUtils.isBlank(tipoOrden)) || (!StringUtils.isBlank(estadoProceso))) {
	        		sql +=  " and cae.numeroCliente='" + Long.parseLong(nroCliente) + "'";
	        	}
	        	else {
	        		sql +=  " cae.numeroCliente='" + Long.parseLong(nroCliente) + "'";
	        	}
	        }
		 
	        
        if ((!StringUtils.isBlank(fechaInicio)) || (!StringUtils.isBlank(fechaFin))) {
        	try {
        		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
                Date currentdateFehaInicio = sdf.parse(fechaInicio);
                SimpleDateFormat sdf2=new SimpleDateFormat("yyyy-MM-dd");
                String fechaInicioConvertido = sdf2.format(currentdateFehaInicio);
                
                SimpleDateFormat sdf4=new SimpleDateFormat("yyyy-MM-dd");
                Date currentdateFehaFin=sdf4.parse(fechaFin);
                SimpleDateFormat sdf3=new SimpleDateFormat("yyyy-MM-dd");
                String fechaFinConvertido = sdf3.format(currentdateFehaFin);
                
            	if((!StringUtils.isBlank(tipoOrden)) || (!StringUtils.isBlank(estadoProceso)) || !StringUtils.isBlank(nroCliente)) {
            		sql +=  " and DATE_TRUNC('day',cae.fechaActivacion) between "+ "'"+fechaInicioConvertido +"'"+" and "+"'"+ fechaFinConvertido+"'";
            	}else {
            		sql +=  " DATE_TRUNC('day',cae.fechaActivacion) between "+ "'"+fechaInicioConvertido +"'"+" and "+"'"+ fechaFinConvertido+"'";
            	}
            	
        	}catch (Exception e) {
				e.printStackTrace();
			}	
             
        }*/
	        
        try {
        	//sql +=" order by cae.fechaActivacion desc";
        	List<ReporteAsignacionPCRClientePaginacionResponseDTO> dto = entityManager.createQuery(sql, ReporteAsignacionPCRClientePaginacionResponseDTO.class).getResultList();
        	
    		List<ReporteAsignacionPCRClientePaginacionResponseDTO> list = dto.stream().collect(Collectors.toList());
    		int pageOffset = (int) paging.getOffset();
    		int total = (pageOffset + paging.getPageSize()) > list.size() ? list.size() : (pageOffset + paging.getPageSize());
    		return new PageImpl<>(list.subList(pageOffset, total), paging, list.size());
        } catch (Exception e) {
        	e.printStackTrace();
        	throw new AllNotFoundException("Error al consultar el reporte"); 
		} finally {
			entityManager.close();
		}
	}

	@Override
	public List<ReporteGraficoDTO> obtenerReporteAsignacionPCRClienteCantidadXsituacion(String fechaInicio,
			String fechaFin, String situacion, String nroCuenta, String tipoAsignacion) {
		// TODO Auto-generated method stub
		return null;
	}

}
