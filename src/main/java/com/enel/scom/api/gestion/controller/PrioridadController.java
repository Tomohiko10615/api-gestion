package com.enel.scom.api.gestion.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.enel.scom.api.gestion.model.PrioridadEntity;
import com.enel.scom.api.gestion.service.PrioridadService;

@RestController
@RequestMapping("/api/prioridad")
public class PrioridadController {
    
    @Autowired
    PrioridadService prioridadService;

    @GetMapping
    public ResponseEntity<List<PrioridadEntity>> getPrioridades() {
        List<PrioridadEntity> responses = prioridadService.getPrioridades();
        return new ResponseEntity<>(responses, HttpStatus.OK);
    }
}
