package com.enel.scom.api.gestion.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.dto.ObservacionDTO;
import com.enel.scom.api.gestion.dto.response.OrdenTrabajoPaginacionResponseDTO;
import com.enel.scom.api.gestion.model.OrdenEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

@Repository
public interface OrdenRepository extends JpaRepository<OrdenEntity, Long>{
	@Query(value="SELECT nextval('sqorden')", nativeQuery=true)
	public Long generarNumeroOrden();
	
	@Query(value="SELECT nextval('sqorden')", nativeQuery=true)
	public Long generaId();

	@Query(value = "SELECT * FROM ord_orden WHERE id = :id", nativeQuery = true)
	OrdenEntity findId(@Param("id") Long id);	

	@Query(value = "SELECT OO.* FROM SCHSCOM.ord_orden OO WHERE OO.nro_orden = :nroOrden", nativeQuery = true)
	OrdenEntity obtenerOrdenPorNumeroOrden(@Param("nroOrden") String nroOrden);
	
	@Query(value = "SELECT OO.* FROM SCHSCOM.ord_orden OO INNER JOIN SCHSCOM.ORD_TIPO_ORDEN OTO ON OO.ID_TIPO_ORDEN = OTO.ID WHERE COD_TIPO_ORDEN = :codTipoOrden AND nro_orden = :nroOrden", nativeQuery = true)
	OrdenEntity obtenerOrdenPorNumeroOrden2(@Param("nroOrden") String nroOrden, @Param("codTipoOrden") String codTipoOrden);

	@Query(value = "select ord.id ordId, "
			+ "ordVenta.paralizada as paraVent "
	 		+ "from ord_orden ord "
	 		+ "JOIN ord_tipo_orden ot ON ord.id_tipo_orden =ot.id "
     		+ "JOIN eor_ord_transfer ts on ord.nro_orden= ts.nro_orden_legacy "
     		+ "JOIN cliente c ON ord.id_servicio= c.id_servicio "
     		+ "JOIN wkf_workflow w ON ord.id_workflow= w.id "
     		+ "JOIN usuario u ON ord.id_usuario_registro= u.id "
     		+ "LEFT JOIN vta_ord_vta as ordVenta ON ord.id_orden_sc4j= ordVenta.id_ord_venta "
     		+ "where ord.nro_orden=:nroOrden", nativeQuery= true)
	Page<OrdenTrabajoPaginacionResponseDTO> obtener(Pageable pageable, @Param("nroOrden") Long nroOrden);
	
	/**
	 * Se obtiene las ordenes de inspección notificadas
	 */
	@Query(value = "select 	oo.id, oo.nro_orden, oo.fecha_creacion, \r\n"
			+ "		concat(np.nombre, ' ', np.apellido_pat, ' ', np.apellido_mat) AS contratista,\r\n"
			+ "		doi.tip_inspeccion\r\n"
			+ "from ord_orden oo\r\n"
			+ "left join ord_orden_cont ooc on ooc.id_orden = oo.id\r\n"
			+ "inner join dis_ord_insp doi on doi.id_orden = oo.id\r\n"
			+ "left join com_contratista cc on cc.id = ooc.id_contratista\r\n"
			+ "left join nuc_persona np on np.id = cc.id_persona\r\n"
			+ "inner join wkf_workflow ww on ww.id = oo.id_workflow\r\n"
			+ "where oo.discriminador = 'INSPECCION' and ww.id_state = 'Recibida' and doi.nro_notificacion is not null and oo.id_servicio = :id", nativeQuery = true)
	List<Object[]> getIdServicio(@Param("id") Long id);

	@Query(value = "select distinct da.id, da.cod_anormalidad, da.des_anormalidad, da.gen_cnr, da.gen_orden_norm, dc.des_causal   from dis_ord_tarea dot\r\n"
			+ "inner join dis_anormalidad da on dot.id_anormalidad = da.id\r\n"
			+ "inner join dis_causal dc on da.id_causal = dc.id\r\n"
			+ "where dot.id_orden = :id", nativeQuery = true)
	List<Object[]> findByIdOrdenAndAnormalidades(@Param("id") Long id);

	@Query(value = "select schscom.fn_obtener_fecha_vencimiento(?1);", nativeQuery=true)
	Optional<Date> obtenerFechaVencimiento2(Long id);
	
	@Query(value = "SELECT (reg_xml::::Varchar) as xml FROM eor_ord_transfer eot "
					+ "INNER JOIN eor_ord_transfer_det eotd on eot.id_ord_transfer = eotd.id_ord_transfer "
					+ "WHERE eot.id_ord_transfer= :id AND accion = :accion", nativeQuery = true)
	String getOrdenEnvio(@Param("id") Long id, @Param("accion") String accion);

	// R.I. REQSCOM06 07/07/2023 INICIO
	@Query(value = "select to_char(fecha_creacion, 'DD/MM/YYYY HH24:MI:SS') from schscom.ord_orden where nro_orden = ?1", nativeQuery = true)
	public String obtenerFechaCreacion(String numOrden);
	// R.I. REQSCOM06 07/07/2023 FIN

	// R.I. Correccion 18/09/2023 INICIO
	@Query(value = "select texto from ord_observacion oo where id_orden = ?1 and discriminator = 'ObeservacionOrden' and state_name = ?2", nativeQuery = true)
	public String obtenerObservacionTecnico(Long id, String state);
	
	@Query(value = "select texto as texto, TO_CHAR(oob.fecha_observacion, 'YYYY-MM-DD HH24:MI:SS') AS fechaObservacion from ord_observacion oob, ord_orden oo, dis_ord_norm don \r\n"
			+ "where oob.id_orden = oo.id and oob.id_orden = ?1 and don.id_orden = oo.id and don.fec_hora_fin_eje = oob.fecha_observacion \r\n"
			+ "and discriminator = 'ObeservacionOrden' and state_name = 'SFinalizada' limit 1", nativeQuery = true)
	public ObservacionDTO obtenerObservacionTecnicoNORM(Long id);
	
	@Query(value = "select texto as texto, TO_CHAR(oob.fecha_observacion, 'YYYY-MM-DD HH24:MI:SS') AS fechaObservacion from ord_observacion oob, ord_orden oo, dis_ord_insp doi \r\n"
			+ "where oob.id_orden = oo.id and oob.id_orden = ?1 and doi.id_orden = oo.id and doi.fec_fin_eje = oob.fecha_observacion \r\n"
			+ "and discriminator = 'ObeservacionOrden' and state_name = 'Recibida' limit 1", nativeQuery = true)
	public ObservacionDTO obtenerObservacionTecnicoINSP(Long id);
	
	@Query(value = "select texto as texto, TO_CHAR(oob.fecha_observacion, 'YYYY-MM-DD HH24:MI:SS') AS fechaObservacion from ord_observacion oob, ord_orden oo, orm_respuesta ore \r\n"
			+ "where oob.id_orden = oo.id and oob.id_orden = ?1 and ore.id_orden = oo.id and ore.fecha_ejecucion = oob.fecha_observacion \r\n"
			+ "and discriminator = 'ObeservacionOrden' and state_name = 'Respondida' limit 1", nativeQuery = true)
	public ObservacionDTO obtenerObservacionTecnicoMANT(Long id);
	
	@Query(value = "select des_motivo from ord_motivo_creacion omc, ord_orden oo where omc.id = oo.id_motivo and oo.id = ?", nativeQuery = true)
	public String obtenerMotivoInspeccion(Long id);
	// R.I. Correccion 18/09/2023 FIN

	
}
