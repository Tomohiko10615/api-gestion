package com.enel.scom.api.gestion.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.model.MedEntDecEntity;

@Repository
public interface MedEntDecRepository extends JpaRepository<MedEntDecEntity, Long>{

	@Query(value = "SELECT * FROM med_ent_dec WHERE id = :id", nativeQuery = true)
	MedEntDecEntity findId(@Param("id") Long id);
}
