package com.enel.scom.api.gestion.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "perfil_menu", schema = "schscom")
@Data
public class PerfilMenu implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@JoinColumn(name = "id_perfil")
    @ManyToOne(fetch = FetchType.LAZY)
    private PerfilEntity perfil;
    
	@JoinColumn(name = "id_menu")
    @ManyToOne(fetch = FetchType.LAZY)
    private MenuEntity menu;
	
	@Column
    private String activo;

}
