package com.enel.scom.api.gestion.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import lombok.Data;

@Entity(name = "mov_est_componente")
@Data
public class MovEstMedidorEntity implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@Column
	private Long id;
	
	@OneToOne
	@JoinColumn(name = "id_med_componente")
	private MedidorEntity medidor;
	
	@OneToOne
	@JoinColumn(name = "id_estado_inicial")
	private EstadoMedidorEntity estadoInicial;

	@OneToOne
	@JoinColumn(name = "id_estado_final")
	private EstadoMedidorEntity estadoFinal;
	
	@Column(name = "fecha_estado")
	private Date fechaEstado;
	
	@OneToOne
	@JoinColumn(name = "id_usuario")
	private UsuarioEntity usuario;

	@OneToOne
	@JoinColumn(name = "id_accion")
	private ParametroEntity accion;

	@OneToOne
	@JoinColumn(name = "id_ubicacion")
	private UbicacionEntity ubicacion;
	
	@Column(name = "ultimo_estado")
	private Boolean ultimoEstado;
}
