package com.enel.scom.api.gestion.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Entity(name = "com_fase")
@Data
public class FaseEntity implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "cod_fase")
	private String codFase;
	
	@Column(name = "des_fase")
	private String desFase;
	
	@Column(name = "cod_interno")
	private String codInterno;
	
	@Column(length = 1)
	private String activo;

}
