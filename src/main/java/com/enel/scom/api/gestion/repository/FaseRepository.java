package com.enel.scom.api.gestion.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.model.FaseEntity;

@Repository
public interface FaseRepository extends JpaRepository<FaseEntity, Long>{
	@Query(value = "SELECT * FROM com_fase WHERE id = :id", nativeQuery = true)
	FaseEntity findId(@Param("id") Long id);
	
	@Query(value = "SELECT * FROM com_fase WHERE activo = 'S'", nativeQuery = true)
	List<FaseEntity> getActivos();
}
