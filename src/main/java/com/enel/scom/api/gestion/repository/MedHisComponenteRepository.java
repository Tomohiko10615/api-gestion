package com.enel.scom.api.gestion.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.model.MedHisComponenteEntity;

@Repository
public interface MedHisComponenteRepository extends JpaRepository<MedHisComponenteEntity, Long>{
    @Query(value="SELECT nextval('sqmedhiscomponente')", nativeQuery=true)
	public Long secuencia();
}
