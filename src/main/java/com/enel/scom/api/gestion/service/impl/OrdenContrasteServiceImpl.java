package com.enel.scom.api.gestion.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.scom.api.gestion.dto.XmlDTO;
import com.enel.scom.api.gestion.dto.request.OrdenContrasteRequestDTO;
import com.enel.scom.api.gestion.dto.response.MedContrasteDTO;
import com.enel.scom.api.gestion.dto.response.MedContrastePruebaDTO;
import com.enel.scom.api.gestion.dto.response.OrdenDataResponseDTO;
import com.enel.scom.api.gestion.dto.response.OrdenTareaResponseDTO;
import com.enel.scom.api.gestion.model.AnormalidadEntity;
import com.enel.scom.api.gestion.model.ClienteEntity;
import com.enel.scom.api.gestion.model.MedContrasteEntity;
import com.enel.scom.api.gestion.model.MedidorEntity;
import com.enel.scom.api.gestion.model.MotivoEntity;
import com.enel.scom.api.gestion.model.ObservacionEntity;
import com.enel.scom.api.gestion.model.OrdenContrasteEntity;
import com.enel.scom.api.gestion.model.OrdenEntity;
import com.enel.scom.api.gestion.model.OrdenInspeccionEntity;
import com.enel.scom.api.gestion.model.OrdenTareaEntity;
import com.enel.scom.api.gestion.model.TareaEntity;
import com.enel.scom.api.gestion.model.TipoOrdenEntity;
import com.enel.scom.api.gestion.model.UsuarioEntity;
import com.enel.scom.api.gestion.model.WorkflowEntity;
import com.enel.scom.api.gestion.repository.AnormalidadRepository;
import com.enel.scom.api.gestion.repository.ClienteRepository;
import com.enel.scom.api.gestion.repository.MedContrasteRepository;
import com.enel.scom.api.gestion.repository.MedidorRepository;
import com.enel.scom.api.gestion.repository.MotivoRepository;
import com.enel.scom.api.gestion.repository.ObservacionRepository;
import com.enel.scom.api.gestion.repository.OrdenContrasteRepository;
import com.enel.scom.api.gestion.repository.OrdenInspeccionRepository;
import com.enel.scom.api.gestion.repository.OrdenRepository;
import com.enel.scom.api.gestion.repository.OrdenTareaRepository;
import com.enel.scom.api.gestion.repository.ParametroRepository;
import com.enel.scom.api.gestion.repository.TareaRepository;
import com.enel.scom.api.gestion.repository.TipoOrdenRepository;
import com.enel.scom.api.gestion.repository.TransferRepository;
import com.enel.scom.api.gestion.repository.UsuarioRepository;
import com.enel.scom.api.gestion.repository.WorkflowRepository;
import com.enel.scom.api.gestion.service.OrdenContrasteService;

/**
 * Servicio principal donde se realiza el registro, actualización y detalle del contraste
 * @author Gilmar Moreno
 * @version 1.0
 */
@Service
public class OrdenContrasteServiceImpl implements OrdenContrasteService{
	@Autowired
	OrdenRepository ordenRepository;

	@Autowired
	UsuarioRepository usuarioRepository;
	
	@Autowired
	MotivoRepository motivoRepository;

	@Autowired
	ClienteRepository clienteRepository;
	
	@Autowired
	OrdenInspeccionRepository ordenInspeccionRepository;
	
	@Autowired
	OrdenContrasteRepository ordenContrasteRepository;

	@Autowired
	ObservacionRepository observacionRepository;

	@Autowired
	ParametroRepository parametroRepository;

	@Autowired
	OrdenTareaRepository ordenTareaRepository;

	@Autowired
	TareaRepository tareaRepository;

	@Autowired
	AnormalidadRepository anormalidadRepository;
	
	@Autowired
	TipoOrdenRepository tipoOrdenRepository;
	
	@Autowired
	WorkflowRepository wkfWorkflowRepository;

	@Autowired
	MedidorRepository medidorRepository;
	
	@Autowired
	MedContrasteRepository medContrasteRepository;

	// R.I. REQSCOM06 11/07/2023 INICIO
	@Autowired
	TransferRepository transferRepository;
	// R.I. REQSCOM06 11/07/2023 FIN
	
	/**
	 * Metodo para registrar y actualizar la orden de contraste
	 */
	@Override
	public OrdenEntity storeUpdate(OrdenContrasteRequestDTO ordenContrasteDTO) {
		Boolean ordenExiste = ordenRepository.existsById(ordenContrasteDTO.getIdOrden());
		UsuarioEntity usuario = usuarioRepository.getUsuarioById(ordenContrasteDTO.getIdUsuario());
		ClienteEntity cliente = clienteRepository.findByNroServicio(ordenContrasteDTO.getNroServicio());
		OrdenInspeccionEntity ordenInspeccion = ordenInspeccionRepository.getIdOrden(ordenContrasteDTO.getIdOrdenInspeccion());
		TipoOrdenEntity tipoOrden = tipoOrdenRepository.getContraste();
		int existeOrdenTarea = ordenTareaRepository.existeOrden(ordenInspeccion.getIdOrden());
		// De acuerdo a la logica para los bacheros no es considerado el 1143.
		MotivoEntity motivo = motivoRepository.findId(new Long(2006));
		MedidorEntity medidor = medidorRepository.findId(cliente.getIdServicio());
		
		WorkflowEntity wkfResponse = new WorkflowEntity();
		if(Boolean.FALSE.equals(ordenExiste)) {
			WorkflowEntity wkf = new WorkflowEntity();
			wkf.setId(wkfWorkflowRepository.generaId());
			wkf.setFechaUltimoEstado(new Date());
			wkf.setIdDescriptor("WFordenContraste");
			wkf.setIdOldState("Emitida");
			wkf.setIdState("Emitida");
			wkf.setParentType("com.enel.scom.api.gestion.service.impl.ordenContrasteImpl");
			wkf.setVersion(0);
			wkfResponse = wkfWorkflowRepository.save(wkf);
		}
		
		OrdenEntity orden = (Boolean.FALSE.equals(ordenExiste)) ? new OrdenEntity() : ordenRepository.findId(ordenContrasteDTO.getIdOrden());
		if(Boolean.FALSE.equals(ordenExiste)) {
			orden.setActivo(true);
			orden.setFechaRegistro(new Date());
			orden.setFechaCreacion(new Date());
			orden.setUsuarioCrea(usuario);
			orden.setWorkflow(wkfResponse);		
			orden.setUsuarioCrea(usuario);
			orden.setUsuarioRegistro(usuario);
			orden.setDiscriminador("COME");
			orden.setTipoOrden(tipoOrden);
			orden.setNroOrden(Long.toString(ordenContrasteRepository.generarNumeroOrden()));
			orden.setId(ordenRepository.generaId());
		}else {
			orden.setFechaModif(new Date());	
			orden.setUsuarioModif(usuario);
		}
		orden.setIdServicio(cliente.getIdServicio());
		orden.setMotivo(motivo);
		OrdenEntity ordenResponse = ordenRepository.save(orden);
		
		OrdenContrasteEntity ordenContraste = (Boolean.FALSE.equals(ordenExiste)) ? new OrdenContrasteEntity() : ordenContrasteRepository.findId(ordenContrasteDTO.getIdOrden());
		ordenContraste.setOrdenInspeccion(ordenInspeccion);
		ordenContraste.setIdComponente(medidor.getId());
		if(Boolean.FALSE.equals(ordenExiste))
			ordenContraste.setIdOrden(ordenResponse.getId());
		ordenContrasteRepository.save(ordenContraste);
		/**
		 * Elimino todas las tareas registradas cuando se va actualizar las nuevas tareas
		 */
		if(existeOrdenTarea > 0) {
			ordenTareaRepository.deleteByIdOrden(ordenInspeccion.getIdOrden());
		}
		
		/**
		 * Se vuelve a crear nuevamente
		 */
		ordenContrasteDTO.getIrregularidades().stream().forEach(k -> {
			AnormalidadEntity anormalidad = anormalidadRepository.findId(k.getIdAnormalidad());
			TareaEntity tarea = tareaRepository.findId(k.getIdTarea());
			OrdenEntity ordenEntity = ordenRepository.findId(ordenInspeccion.getIdOrden());
			
			OrdenTareaEntity ordenTarea = new OrdenTareaEntity();
			ordenTarea.setId(ordenTareaRepository.generaId());
			ordenTarea.setAnormalidad(anormalidad);
			ordenTarea.setOrden(ordenEntity);
			ordenTarea.setTarea(tarea);
			ordenTarea.setEjecutado(k.getTareaEstado());
			ordenTarea.setTareaEstado(k.getTareaEstado());
			ordenTareaRepository.save(ordenTarea);
		});
		
		if(ordenContrasteDTO.getIdOrden() == 0) {
			ObservacionEntity observacion = new ObservacionEntity();
			observacion = new ObservacionEntity();
			observacion.setIdOrden(ordenResponse.getId());
			observacion.setUsuario(usuario);
			observacion.setStateName("CREADA");
			observacion.setDiscriminator("ObservacionOrden");
			observacion.setFechaObservacion(new Date());
			observacion.setIdObservacion(observacionRepository.generaId());
			observacion.setTexto(ordenContrasteDTO.getObservaciones());
			
			observacionRepository.save(observacion);
		}else {
			ObservacionEntity observacion =  (observacionRepository.existeIdOrden(ordenContrasteDTO.getIdOrden()) == 0) ? new ObservacionEntity() : observacionRepository.findIdOrden(ordenContrasteDTO.getIdOrden());	
		
			if(observacionRepository.existeIdOrden(ordenContrasteDTO.getIdOrden()) == 0) {
				observacion.setIdOrden(ordenContrasteDTO.getIdOrden());
				observacion.setUsuario(usuario);
				observacion.setStateName("CREADA");
				observacion.setDiscriminator("ObservacionOrden");
				observacion.setFechaObservacion(new Date());
				observacion.setIdObservacion(observacionRepository.generaId());
			}
			
			observacion.setTexto(ordenContrasteDTO.getObservaciones());
			
			observacionRepository.save(observacion);
		}
		
		return ordenResponse; 
	}
	
	/**
	 * Se obtiene la orden de contraste para editar y mostrar el detalle.
	 * @param id
	 */
	@Override
	public OrdenDataResponseDTO getId(Long id) {
		OrdenContrasteEntity contraste = ordenContrasteRepository.findId(id); // solo para obtener el id inspeccion
		List<Object[]> ordenContraste = ordenContrasteRepository.findIdOrden(id);

		/**
		 * Para evitar que cuando no tiene orden de inspección se utiliza la condicional y evitar el null
		 */
		List<Object[]> datos = null;
		if(contraste.getOrdenInspeccion() != null) {
		    datos = ordenTareaRepository.findByIdOrden(contraste.getOrdenInspeccion().getIdOrden());
		}else {
		    datos = ordenTareaRepository.findByIdOrden((long) 0);
		}
		
		List<Object[]> detalles = datos; 
		List<OrdenTareaResponseDTO> detalleDTO = new ArrayList<>();
		String estadoWkf = wkfWorkflowRepository.estado(id);
		
		OrdenDataResponseDTO ordenDTO = new OrdenDataResponseDTO();
		ordenContraste.stream().forEach(k -> {
			ordenDTO.setIdOrden(Long.valueOf(k[0].toString()));
			ordenDTO.setFechaCreacion((Date) k[1]);
			ordenDTO.setFechaFinalizacion((Date) k[2]);
			ordenDTO.setNroOrden((k[3] == null) ? null : k[3].toString());
			ordenDTO.setMotivo((k[4] == null) ? null : k[4].toString());
			ordenDTO.setIdMotivo((k[5] == null) ? 0 : Long.valueOf(k[5].toString()));
			ordenDTO.setUsuarioCreacion((k[6] == null) ? null : k[6].toString());
			ordenDTO.setNroServicio((k[7] == null) ? null : k[7].toString());
			ordenDTO.setNroCuenta((k[8] == null) ? null : k[8].toString());
			ordenDTO.setNombre((k[9] == null) ? null : k[9].toString());
			ordenDTO.setApellidoPat((k[10] == null) ? null : k[10].toString());
			ordenDTO.setApellidoMat((k[11] == null) ? null : k[11].toString());
			ordenDTO.setDistrito((k[12] == null) ? null : k[12].toString());
			ordenDTO.setRutaLectura((k[13] == null) ? null : k[13].toString());
			ordenDTO.setObservacion((k[14] == null) ? null : k[14].toString());
			ordenDTO.setIdOrdenInspeccion((k[15] == null) ? 0 : Long.valueOf(k[15].toString()));
			ordenDTO.setIdOrdenContraste((k[16] == null) ? 0 : Long.valueOf(k[16].toString()));
			ordenDTO.setDireccion((k[17] == null) ? "" : k[17].toString());
			ordenDTO.setIdOrdenSc4j((k[18] == null) ? 0 : Long.valueOf(k[18].toString()));
			ordenDTO.setEstadoWkf((estadoWkf == null) ? "" : estadoWkf);
			
			detalles.stream().forEach(t -> {
				OrdenTareaResponseDTO ordenTarea = new OrdenTareaResponseDTO();
				ordenTarea.setId((t[0] == null) ? 0 : Long.valueOf(t[0].toString())); 
				ordenTarea.setAnormalidad((t[1] == null) ? "" : t[1].toString());
				ordenTarea.setTarea((t[2] == null) ? "" : t[2].toString());
				ordenTarea.setCodTarea((t[3] == null) ? "" : t[3].toString());
				ordenTarea.setCodAnormalidad((t[4] == null) ? "" : t[4].toString());
				ordenTarea.setTareaEstado((t[5] == null) ? "" : t[5].toString());
				ordenTarea.setIdTarea((t[6] == null) ? 0 : Long.valueOf(t[6].toString()));
				ordenTarea.setIdAnormalidad((t[7] == null) ? 0 : Long.valueOf(t[7].toString()));
				detalleDTO.add(ordenTarea);
			});
			
			ordenDTO.setTareas(detalleDTO); 
		});
		
		MedContrasteEntity medContraste = medContrasteRepository.findIdContaste(contraste.getIdContraste());
		
		if(medContraste!=null) {
			MedContrasteDTO medContrasteDTO = new MedContrasteDTO();
			medContrasteDTO.setIdMedidor(medContraste.getIdMedidor());
			
			/**
			 * Creación de lista para cada tipo de prueba para mostrar en tabla libreria Front
			 */
			List<MedContrastePruebaDTO> pruebasAlta = new ArrayList<>();
			MedContrastePruebaDTO pruebaAlta = new MedContrastePruebaDTO();
	
			pruebaAlta.setPrimerValor(medContraste.getPrimeroPruebaAlta());
			pruebaAlta.setSegundoValor(medContraste.getSegundoPruebaAlta());
			pruebaAlta.setTercerValor(medContraste.getTerceroPruebaAlta());
			pruebasAlta.add(pruebaAlta);
			medContrasteDTO.setPruebaAlta(pruebasAlta);
			
			List<MedContrastePruebaDTO> pruebasBaja = new ArrayList<>();
			MedContrastePruebaDTO pruebaBaja = new MedContrastePruebaDTO();
			
			pruebaBaja.setPrimerValor(medContraste.getPrimeroPruebaBaja());
			pruebaBaja.setSegundoValor(medContraste.getSegundoPruebaBaja());
			pruebaBaja.setTercerValor(medContraste.getTerceroPruebaBaja());
			pruebasBaja.add(pruebaBaja);
			medContrasteDTO.setPruebaBaja(pruebasBaja);
			
			List<MedContrastePruebaDTO> pruebasNominal = new ArrayList<>();
			MedContrastePruebaDTO pruebaNominal = new MedContrastePruebaDTO();
			
			pruebaNominal.setPrimerValor(medContraste.getPrimeroPruebaNominal());
			pruebaNominal.setSegundoValor(medContraste.getSegundoPruebaNominal());
			pruebaNominal.setTercerValor(medContraste.getTerceroPruebaNominal());
			pruebasNominal.add(pruebaNominal);
			medContrasteDTO.setPruebaNominal(pruebasNominal);
			
			List<MedContrastePruebaDTO> pruebasAislamiento = new ArrayList<>();
			MedContrastePruebaDTO pruebaAislamiento = new MedContrastePruebaDTO();
			
			pruebaAislamiento.setPrimerValor(medContraste.getPruebaAislaientoR());
			pruebaAislamiento.setSegundoValor(medContraste.getPruebaAislamientoS());
			pruebaAislamiento.setTercerValor(medContraste.getPruebaAislamientoT());
			pruebasAislamiento.add(pruebaAislamiento);
			medContrasteDTO.setPruebaAislamiento(pruebasAislamiento);
			

			ordenDTO.setMedContrasteDTO(medContrasteDTO);
		} 

		// R.I. REQSCOM06 11/07/2023 INICIO
		String fechaCreacionOrden = ordenRepository.obtenerFechaCreacion(ordenDTO.getNroOrden());
		ordenDTO.setFechaCreacionOrden(fechaCreacionOrden);
		XmlDTO xml = transferRepository.obtenerXmlOrdenTrabajo(ordenDTO.getNroOrden(), 22L);
        if (xml != null) {
      	  if (xml.getObservacion() != null) {
          	  ordenDTO.setObservacionTecnico(xml.getObservacion());
            } else {
          	  ordenDTO.setObservacionTecnico("");
            }
            ordenDTO.setFechaEjecucion(xml.getFechaEjecucion());
        }
		// R.I. REQSCOM06 11/07/2023 FIN
		return ordenDTO;
	}
}
