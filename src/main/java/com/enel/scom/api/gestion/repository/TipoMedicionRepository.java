package com.enel.scom.api.gestion.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.model.TipoMedicionEntity;

@Repository
public interface TipoMedicionRepository extends JpaRepository<TipoMedicionEntity, Long>{
	
	@Query(value = "SELECT * FROM com_tip_medicion WHERE activo = 'S' ORDER BY des_tip_medicion ASC", nativeQuery = true)
	List<TipoMedicionEntity> getTipoMedicionActivo();
	
	@Query(value = "SELECT * FROM com_tip_medicion WHERE id = :id", nativeQuery = true)
	TipoMedicionEntity findId(@Param("id") Long id);
}
