package com.enel.scom.api.gestion.dto.request;


import java.util.List;

import lombok.Data;

@Data
public class ModeloRequestDTO {
	
	private Long id_modelo;

	private String cod_modelo;
	
	private String des_modelo;
	
	private Long cant_anos_vida;
	
	private Long cant_anos_almacen;
	
	private Long nro_sellos_medidor;
	
	private Long nro_sellos_bornera;
	
	private Long nro_registrador;
	
	private String es_reacondicionador;
	
	private String es_reseteado;
	
	private String es_patron;
	
	private String es_totalizador;
	
	private String clase_medidor;
	
	private Float constante;
	
	private Long cant_hilos;
	
	private Long id_fase;
	
	private Long id_amperaje;
	
	private Long id_tipo_medicion;
	
	private Long id_voltaje;
	
	private Long id_tension;
	
	private Long id_tecnologia;
	
	private Long id_registrador;
	
	private Long id_unidad_medida_constante;
	
	private Long id_usuario_registro;
	
	private Long id_marca;
	
	private String activo;
	
	private List<MedidaModeloRequestDTO> medidas;
}
