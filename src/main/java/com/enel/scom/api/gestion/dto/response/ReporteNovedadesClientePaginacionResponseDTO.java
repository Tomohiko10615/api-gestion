package com.enel.scom.api.gestion.dto.response;

import java.util.Date;

import lombok.Data;

@Data
public class ReporteNovedadesClientePaginacionResponseDTO {
	
	private Long nroCliente;
	private String nombreCliente;
	private Date fechaActivacion;
	private String estadoProceso;
	
	public ReporteNovedadesClientePaginacionResponseDTO(
			Long nroCliente, 
			String nombreCliente,
			Date fechaActivacion, 
			String estadoProceso) {
		this.nroCliente = nroCliente;
		this.nombreCliente = nombreCliente;
		this.fechaActivacion = fechaActivacion;
		this.estadoProceso = estadoProceso;
	}
	
}	
