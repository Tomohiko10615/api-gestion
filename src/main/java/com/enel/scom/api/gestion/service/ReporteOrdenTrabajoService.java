package com.enel.scom.api.gestion.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.enel.scom.api.gestion.dto.response.ReporteGraficoDTO;
import com.enel.scom.api.gestion.dto.response.ReporteOrdenTrabajoPaginacionDTO;

public interface ReporteOrdenTrabajoService {
	
	/**
	 * 
	 * @param paging
	 * @param fechaInicio
	 * @param fechaFin
	 * @param situacion cada tipo de situacion abarca varios estados de las ordenes
	 * @param idTipoProceso tipo de proceso en el que pertecese el tipo de orden (PERDIDA, IMPAGO, CONEXIONES O MANTENIMIENTO)
	 * @param nroCuenta
	 * @param idTipoOrden
	 * @return
	 */
	Page<ReporteOrdenTrabajoPaginacionDTO> obtenerReporteOrdenesTrabajoPaginacion(Pageable paging, String fechaInicio, String fechaFin, String situacion, String idTipoProceso, String nroCuenta, String idTipoOrden);
	
	
	/**
	 * 
	 * @param fechaInicio
	 * @param fechaFin
	 * @param situacion cada tipo de situacion abarca varios estados de las ordenes
	 * @param idTipoProceso tipo de proceso en el que pertecese el tipo de orden (PERDIDA, IMPAGO, CONEXIONES O MANTENIMIENTO)
	 * @param nroCuenta
	 * @param idTipoOrden
	 * @return
	 */
	List<ReporteGraficoDTO> obtenerReporteOrdenesTrabajoCantidadXsituacion(String fechaInicio, String fechaFin, String situacion, String idTipoProceso, String nroCuenta, String idTipoOrden);

}
