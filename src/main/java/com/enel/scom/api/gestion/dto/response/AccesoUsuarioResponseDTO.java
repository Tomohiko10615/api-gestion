package com.enel.scom.api.gestion.dto.response;

import java.util.List;

import com.enel.scom.api.gestion.dto.MenuDTO;

import lombok.Data;

@Data
public class AccesoUsuarioResponseDTO {
	
	private Long idPerfil;
	private String nombrePerfil;
	private String username;
	private String paramNombre;
	private List<MenuDTO> menu;
	
	private List<RolResponseDTO> roles;
}
