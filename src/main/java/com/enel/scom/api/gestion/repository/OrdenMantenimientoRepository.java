package com.enel.scom.api.gestion.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.dto.LecturasDTO;
import com.enel.scom.api.gestion.dto.MedidorDTO;
import com.enel.scom.api.gestion.model.OrdenMantenimientoEntity;

@Repository
public interface OrdenMantenimientoRepository extends JpaRepository<OrdenMantenimientoEntity, Long>{
	@Query(value="SELECT nextval('sqordenmantenimiento')", nativeQuery=true)
	public Long generarNumeroOrden();

    @Query(value = "SELECT * FROM orm_orden WHERE id_orden = :idOrden", nativeQuery = true)
    OrdenMantenimientoEntity findByIdOrden(@Param("idOrden") Long idOrden);

    @Query(value = "select \r\n"
			+ "			oo.id as id_orden,\r\n"
			+ "			oo.nro_orden,\r\n"
			+ "			c.nro_servicio, c.nro_cuenta, c.nombre, c.apellido_pat, c.apellido_mat,\r\n"
			+ "			c.distrito, c.cod_ruta,\r\n"
			+ "			oob.texto as observacion, c.nro_docto_ident, c.srv_tipo_acometida_codigo,\r\n"
            + "			om.id_trabajo, om.id_prioridad,\r\n"
            + "			ood.id_responsable, ore.tipo_responsable, c.texto_direccion as direccion, \r\n"
            + " oo.id_orden_sc4j \r\n"
			+ "from ord_orden oo\r\n"
			+ "left join usuario u on u.id = oo.id_usuario_registro\r\n"
			+ "left join orm_orden om on oo.id = om.id_orden\r\n"
			+ "left join ord_ord_deriv ood on ood.id_orden = om.id_orden\r\n"
			+ "left join ord_responsable ore on ore.id_object = ood.id_responsable\r\n"
			+ "left join ord_observacion oob on oob.id_orden = oo.id\r\n"
			+ "left join cliente c on c.id_servicio = oo.id_servicio\r\n"
			+ "where oo.id = :id ORDER BY oob.fecha_observacion DESC LIMIT 1", nativeQuery = true)
	List<Object[]> findIdOrden(@Param("id") Long id);

	// R.I. REQSCOM06 16/10/2023 INICIO
	@Query(value="select id_lectura\r\n"
			+ "from schscom.med_lec_ultima mlu \r\n"
			+ "where id_componente in (select id_componente \r\n"
			+ "from schscom.ORD_MEDI_CAMBIO "
			+ "where id_orden =  ? )", nativeQuery=true)
	public List<Long> obtenerIdMagnitud(Long id);
	// R.I. REQSCOM06 16/10/2023 FIN
}
