package com.enel.scom.api.gestion.service;

import java.util.List;

import com.enel.scom.api.gestion.dto.response.AutoCompleteResponseDTO;

public interface SrvAlimentadorService {
	
	/**
	 * Busca concidencidencias de codigo de ALIMENTADOR CADENA ELECTRICA
	 * @param codAlimentador
	 * @param idSet
	 * @return
	 */
	List<AutoCompleteResponseDTO> buscarAlimentadorServicioElectrico(String codAlimentador, Long idSet);
}
