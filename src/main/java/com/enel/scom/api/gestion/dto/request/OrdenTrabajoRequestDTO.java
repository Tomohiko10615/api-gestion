package com.enel.scom.api.gestion.dto.request;

import lombok.Data;

@Data
public class OrdenTrabajoRequestDTO {
	
	private Long id;
	
	private String nroOrden;
	
	private Long idTipoOrden;
	
	private String tipoOrden;
	
	private String idEstado;
	
	private String estado;
	
}
