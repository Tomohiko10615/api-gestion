package com.enel.scom.api.gestion.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.enel.scom.api.gestion.model.VoltajeEntity;
import com.enel.scom.api.gestion.repository.VoltajeRepository;

@RestController
@RequestMapping("/api/voltaje")
public class VoltajeController {
	
	@Autowired
	VoltajeRepository voltajeRepository;

	@GetMapping
	public ResponseEntity<List<VoltajeEntity>> getAll() {
		List<VoltajeEntity> tipos = voltajeRepository.getVoltajesActivo();
		return new ResponseEntity<>(tipos, HttpStatus.OK);
	}
}
