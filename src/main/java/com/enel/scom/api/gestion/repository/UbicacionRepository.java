package com.enel.scom.api.gestion.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.model.UbicacionEntity;

@Repository
public interface UbicacionRepository extends JpaRepository<UbicacionEntity, Long>{
	@Query(value = "SELECT id, des_almacen FROM med_almacen WHERE activo = 'S' ORDER BY des_almacen ASC", nativeQuery = true)
	List<Object[]> getAll();
	
	@Query(value = "SELECT * FROM med_almacen WHERE id = :id", nativeQuery = true)
	UbicacionEntity findId(@Param("id") Long id);
}
