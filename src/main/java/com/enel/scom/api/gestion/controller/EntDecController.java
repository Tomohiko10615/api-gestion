package com.enel.scom.api.gestion.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.enel.scom.api.gestion.model.MedEntDecEntity;
import com.enel.scom.api.gestion.repository.MedEntDecRepository;

@RestController
@RequestMapping("/api/entdec")
public class EntDecController {

	@Autowired
	MedEntDecRepository medEntDecRepository;
	
	@GetMapping
	public ResponseEntity<List<MedEntDecEntity>> getAll() {
		List<MedEntDecEntity> responses = medEntDecRepository.findAll();
		return new ResponseEntity<>(responses, HttpStatus.OK);
	}
}
