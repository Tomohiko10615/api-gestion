package com.enel.scom.api.gestion.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.enel.scom.api.gestion.model.EstadoMedidorEntity;
import com.enel.scom.api.gestion.repository.EstadoMedidorRepository;

@RestController
@RequestMapping("/api/estado-medidor")
public class EstadoMedidorController {

	@Autowired
	EstadoMedidorRepository estadoMedidorRepository;
	
	@GetMapping
	public ResponseEntity<List<EstadoMedidorEntity>> getAll() {
		List<EstadoMedidorEntity> responses = estadoMedidorRepository.getActivos();
		return new ResponseEntity<>(responses, HttpStatus.OK);
	}
}
