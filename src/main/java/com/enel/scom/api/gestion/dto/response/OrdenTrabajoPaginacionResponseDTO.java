package com.enel.scom.api.gestion.dto.response;

import lombok.Data;

@Data
public class OrdenTrabajoPaginacionResponseDTO {

	private Long id;
	
	private Long nroOrden;
	
	private Long idTipoOrden;
	
	private String tipoOrden;
	
	private String codTipoOrden;
	
	private String fechaCreacion;

	private String nroServicio;
	
	private Long idEstado;
	
	private String estado;
	
	private String fechaEstado;
	
	private String nroTdc;
	
	private String usuarioCreador;
	
	private String paralizado;

	// R.I. REQSCOM06 07/07/2023 INICIO
	private String fechaEjecucion;
	// R.I. REQSCOM06 07/07/2023 FIN
	
	public OrdenTrabajoPaginacionResponseDTO(
			Long id, 
			Long nroOrden, 
			Long idTipoOrden, 
			String tipoOrden,
			String codTipoOrden,
			String fechaCreacion, 
			String nroServicio, 
			Long idEstado, 
			String estado, 
			String fechaEstado, 
			String usuarioCreador, 
			String nroTdc, 
			String paralizado) {
		this.id = id;
		this.nroOrden = nroOrden;
		this.idTipoOrden = idTipoOrden;
		this.tipoOrden = tipoOrden;
		this.codTipoOrden = codTipoOrden;
		this.fechaCreacion = fechaCreacion;
		this.nroServicio = nroServicio;
		this.idEstado = idEstado;
		this.estado = estado;
		this.fechaEstado = fechaEstado;
		this.usuarioCreador = usuarioCreador;
		this.nroTdc = nroTdc;
		this.paralizado = paralizado;
	}


	public OrdenTrabajoPaginacionResponseDTO(Long id, Long nroOrden, Long idTipoOrden, String tipoOrden,
			String codTipoOrden, String fechaCreacion, String nroServicio, String estado, String fechaEstado,
			String usuarioCreador) {
		this.id = id;
		this.nroOrden = nroOrden;
		this.idTipoOrden = idTipoOrden;
		this.tipoOrden = tipoOrden;
		this.codTipoOrden = codTipoOrden;
		this.fechaCreacion = fechaCreacion;
		this.nroServicio = nroServicio;
		this.estado = estado;
		this.fechaEstado = fechaEstado;
		this.usuarioCreador = usuarioCreador;
	}
	
	
	
}
