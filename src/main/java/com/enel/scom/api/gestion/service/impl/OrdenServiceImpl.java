package com.enel.scom.api.gestion.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.enel.scom.api.gestion.dto.response.OrdenDataResponseDTO;
import com.enel.scom.api.gestion.dto.response.OrdenXMLResponseDTO;
import com.enel.scom.api.gestion.exception.NroServicioNotFoundException;
import com.enel.scom.api.gestion.model.OrdenEntity;
import com.enel.scom.api.gestion.model.TransferEntity;
import com.enel.scom.api.gestion.repository.OrdenRepository;
import com.enel.scom.api.gestion.repository.TransferRepository;
import com.enel.scom.api.gestion.service.OrdenService;

import org.dom4j.DocumentHelper;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;

import java.io.StringWriter;

import lombok.extern.slf4j.Slf4j;

/**
 * Servicio principal donde se obtiene el detalle del XML y de la orden por su tipo.
 * @author Gilmar Moreno
 * @version 1.0
 */
@Service
@Slf4j
public class OrdenServiceImpl implements OrdenService{

	@Autowired
	OrdenRepository ordenRepository;
	
	@Autowired
	OrdenInspeccionServiceImpl ordenInspeccionImpl;
	
	@Autowired
	OrdenContrasteServiceImpl ordenContrasteServiceImpl;
	
	@Autowired
	OrdenNormalizacionServiceImpl ordenNormalizacionServiceImpl;

	@Autowired
	OrdenMantenimientoServiceImpl ordenMantenimientoServiceImpl;
	
	@Autowired
	TransferRepository transferRepository;
	
	/**
	 * Validación de ordenes obteniendo el id de transfer para retornar el dato de la orden
	 */
	@Override
	public OrdenDataResponseDTO getData(Long id, Long tipo) throws NroServicioNotFoundException {
		OrdenEntity orden = new OrdenEntity();
		
		if(tipo == 0) {
			TransferEntity transfer = transferRepository.obtenerTransferenciaPorId(id);
			if(transfer == null) {
				throw new NroServicioNotFoundException("El id de transferencia no existe");
			}
			
			orden = ordenRepository.obtenerOrdenPorNumeroOrden2(transfer.getNroOrdenLegacy(), transfer.getCodTipoOrdenLegacy());

			if(orden == null) {
				throw new NroServicioNotFoundException("La nro orden " + transfer.getNroOrdenLegacy() +" no existe.");
			}	
		}else {
			orden = ordenRepository.findId(id);

			if(orden == null) {
				throw new NroServicioNotFoundException("La orden no existe");
			}
		}

		System.out.println("orden: " + orden.getId());

		OrdenDataResponseDTO ordenResponse = new OrdenDataResponseDTO();
		
		if(orden.getTipoOrden().getCodigoTipoOrden().equals("CONT")) ordenResponse = ordenContrasteServiceImpl.getId(orden.getId());
		if(orden.getTipoOrden().getCodigoTipoOrden().equals("ORINSP")) ordenResponse = ordenInspeccionImpl.getId(orden.getId());
		if(orden.getTipoOrden().getCodigoTipoOrden().equals("NORM")) ordenResponse = ordenNormalizacionServiceImpl.getId(orden.getId());
		if(orden.getTipoOrden().getCodigoTipoOrden().equals("MANT")) ordenResponse = ordenMantenimientoServiceImpl.getId(orden.getId());
		
		return ordenResponse;
	}

	/**
	 * Se obtiene el XML de acuerdo al tipo de acción
	 */
	@Override
	public OrdenXMLResponseDTO getOrdenXML(Long id, String accion) throws NroServicioNotFoundException{
		String objectXml = null;
		String xmlPretty="";
		OrdenXMLResponseDTO ordenXML = new OrdenXMLResponseDTO();
		try {
			
			objectXml = ordenRepository.getOrdenEnvio(id, accion);
			if(objectXml == null) {
				throw new NroServicioNotFoundException("No hay datos de: " + accion);
			}
			ordenXML.setXml(objectXml);
			log.info("objectXml1: {}", objectXml);
			xmlPretty = prettyPrintByDom4j(objectXml,4, false);
			log.info("objectXml: {}", objectXml);

		} catch (EmptyResultDataAccessException ee) {
			log.info("Xml para el nro orden {} no existe", id);
			log.error("EmptyResultDataAccessException->" + ee.getMessage(), ee);
		} catch (DataAccessException de) {
			log.info("Error al obtener Xml");
			log.error("DataAccessException->" + de.getMessage(), de);
		}
		return ordenXML;
	}
	
	/**
	 * Formatea o parsea el XML obtenido
	 */
	public String prettyPrintByDom4j(String xmlString, int indent, boolean skipDeclaration) {
	    try {
	        OutputFormat format = OutputFormat.createPrettyPrint();
	        format.setIndentSize(indent);
	        format.setSuppressDeclaration(skipDeclaration);
	        format.setEncoding("UTF-8");

	        org.dom4j.Document document = DocumentHelper.parseText(xmlString);
	        StringWriter sw = new StringWriter();
	        XMLWriter writer = new XMLWriter(sw, format);
	        writer.write(document);
	        return sw.toString();
	    } catch (Exception e) {
	        throw new RuntimeException("Error occurs when pretty-printing xml:\n" + xmlString, e);
	    }
	}

	@Override
	public OrdenXMLResponseDTO getDetalleXMLEnvio(Long id) throws NroServicioNotFoundException {
		String objectXml = null;
		OrdenXMLResponseDTO ordenXML = new OrdenXMLResponseDTO();
		try {
			
			objectXml = ordenRepository.getOrdenEnvio(id, "ENVIO");
			
		
			String valor ="<CrearTdC xmlns=\"http://xmlns.endesa.com/wsdl/SSCC/eOrder/OSTDC009/Mensajes\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">";

			
			objectXml = objectXml == null ? "" : objectXml.replace(valor, "<CrearTdC>");
			objectXml = objectXml == null ? "" :objectXml.replaceAll("\r\n", " ");
			
			if(objectXml == null) {
				throw new NroServicioNotFoundException("No hay datos de: ENVIO");
			}
			ordenXML.setXml(objectXml);
			
			

		} catch (EmptyResultDataAccessException ee) {
			log.info("Xml para el nro orden {} no existe", id);
			log.error("EmptyResultDataAccessException->" + ee.getMessage(), ee);
		} catch (DataAccessException de) {
			log.info("Error al obtener Xml");
			log.error("DataAccessException->" + de.getMessage(), de);
		}
		return ordenXML;
	}

	@Override
	public OrdenXMLResponseDTO getDetalleXMLRecepcion(Long id) throws NroServicioNotFoundException {
		String objectXml = null;
		OrdenXMLResponseDTO ordenXML = new OrdenXMLResponseDTO();
		try {
			
			objectXml = ordenRepository.getOrdenEnvio(id, "RECEPCION");
			
		
			String valor ="<CrearTdC xmlns=\"http://xmlns.endesa.com/wsdl/SSCC/eOrder/OSTDC009/Mensajes\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">";

			
			objectXml = objectXml == null ? "" : objectXml.replace(valor, "<CrearTdC>");
			objectXml = objectXml == null ? "" : objectXml.replaceAll("\r\n", " ");
			
			if(objectXml == null) {
				throw new NroServicioNotFoundException("No hay datos de: ENVIO");
			}
			ordenXML.setXml(objectXml);
			
			

		} catch (EmptyResultDataAccessException ee) {
			log.info("Xml para el nro orden {} no existe", id);
			log.error("EmptyResultDataAccessException->" + ee.getMessage(), ee);
		} catch (DataAccessException de) {
			log.info("Error al obtener Xml");
			log.error("DataAccessException->" + de.getMessage(), de);
		}
		return ordenXML;
	}

	@Override
	public OrdenDataResponseDTO obtenerDetalleOrdenesScom(Long id) throws NroServicioNotFoundException {

			OrdenEntity orden = ordenRepository.findId(id);

			if(orden == null) {
				throw new NroServicioNotFoundException("La orden no existe");
			}

		OrdenDataResponseDTO ordenResponse = new OrdenDataResponseDTO();
		
		if(orden.getTipoOrden().getCodigoTipoOrden().equals("CONT")) ordenResponse = ordenContrasteServiceImpl.getId(orden.getId());
		if(orden.getTipoOrden().getCodigoTipoOrden().equals("ORINSP")) ordenResponse = ordenInspeccionImpl.getId(orden.getId());
		if(orden.getTipoOrden().getCodigoTipoOrden().equals("NORM")) ordenResponse = ordenNormalizacionServiceImpl.getId(orden.getId());
		if(orden.getTipoOrden().getCodigoTipoOrden().equals("MANT")) ordenResponse = ordenMantenimientoServiceImpl.getId(orden.getId());
		
		return ordenResponse;
	}

}
