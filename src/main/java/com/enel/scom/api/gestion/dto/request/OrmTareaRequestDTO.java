package com.enel.scom.api.gestion.dto.request;

import lombok.Data;

@Data
public class OrmTareaRequestDTO {
    private Long idTarea;
}
