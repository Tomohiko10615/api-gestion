package com.enel.scom.api.gestion.dto.response;

import java.util.Date;

import lombok.Data;

@Data
public class ModeloPaginacionResponseV2DTO {
	private String id;
	private String codigo;
	private String descripcion;
	private String marca;
	private Date fecha;
	private String usuario;
}
