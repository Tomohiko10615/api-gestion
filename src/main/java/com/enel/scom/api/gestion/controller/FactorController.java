package com.enel.scom.api.gestion.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.enel.scom.api.gestion.dto.request.MedFacMedModRequestDTO;
import com.enel.scom.api.gestion.dto.request.MedidaModeloRequestDTO;
import com.enel.scom.api.gestion.dto.response.FactorResponseDTO;
import com.enel.scom.api.gestion.exception.NroServicioNotFoundException;
import com.enel.scom.api.gestion.model.MedFacMedModEntity;
import com.enel.scom.api.gestion.service.FactorService;

@RestController
@RequestMapping("/api/factor")
public class FactorController {

	@Autowired
	FactorService factorService;
	
	@GetMapping
	public ResponseEntity<List<FactorResponseDTO>> getFactores() {
		List<FactorResponseDTO> responses = factorService.getAll();
		return new ResponseEntity<>(responses, HttpStatus.OK);
	}
	
	@PostMapping("/crear")
	public ResponseEntity<MedFacMedModEntity> agregarFactor(@RequestBody MedFacMedModRequestDTO medModelo) throws NroServicioNotFoundException {
		MedFacMedModEntity response = factorService.crearFactor(medModelo);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@DeleteMapping("/delete/{idMedidaModelo}/{idFactor}")
	public ResponseEntity<MedFacMedModEntity> eliminarFactor(@PathVariable Long idMedidaModelo,
			@PathVariable Long idFactor) {
		MedFacMedModEntity response = factorService.delete(idMedidaModelo, idFactor);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@GetMapping("/factores-medida-modelo/{idMedidaModelo}")
	public ResponseEntity<List<FactorResponseDTO>> getFactoresPorMedidaModelo(@PathVariable Long idMedidaModelo) {
		List<FactorResponseDTO> responses = factorService.getFactoresPorMedidaModelo(idMedidaModelo);
		return new ResponseEntity<>(responses, HttpStatus.OK);
	}

}
