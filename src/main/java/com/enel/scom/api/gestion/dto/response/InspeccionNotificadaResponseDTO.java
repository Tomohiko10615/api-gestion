package com.enel.scom.api.gestion.dto.response;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class InspeccionNotificadaResponseDTO {
	private Long id;
	private String nroOrden;
	private String fechaCreacion;
	private String contratista;
	private String tipoInspeccion;
}
