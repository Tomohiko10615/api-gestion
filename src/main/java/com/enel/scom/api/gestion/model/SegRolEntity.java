package com.enel.scom.api.gestion.model;

import javax.persistence.*;

import lombok.Data;

@Entity(name = "seg_rol")
@Data
public class SegRolEntity {
    @Id
    @Column(name = "id_rol")
    private Long idRol;

    @Column(name = "descripcion")
    private String descripcion;
}
