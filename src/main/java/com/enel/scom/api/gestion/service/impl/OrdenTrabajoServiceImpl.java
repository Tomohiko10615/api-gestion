package com.enel.scom.api.gestion.service.impl;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.enel.scom.api.gestion.constant.Constant;
import com.enel.scom.api.gestion.dto.ObservacionDTO;
import com.enel.scom.api.gestion.dto.XmlDTO;
import com.enel.scom.api.gestion.dto.request.TransferAnularRequestDTO;
import com.enel.scom.api.gestion.dto.response.OrdenTrabajoPaginacionResponseDTO;
import com.enel.scom.api.gestion.dto.response.OrdenTrabajoSCOMPaginacionResponseDTO;
import com.enel.scom.api.gestion.dto.response.TransferPaginacionResponseDTO;
import com.enel.scom.api.gestion.enums.TipoOrden;
import com.enel.scom.api.gestion.exception.AllNotFoundException;
import com.enel.scom.api.gestion.exception.AnularNotFoundException;
import com.enel.scom.api.gestion.model.AuditEventEntity;
import com.enel.scom.api.gestion.model.BuzonEntity;
import com.enel.scom.api.gestion.model.ClienteEntity;
import com.enel.scom.api.gestion.model.DisSegInspEntity;
import com.enel.scom.api.gestion.model.HistoricoEntity;
import com.enel.scom.api.gestion.model.MotivoAnulacionEntity;
import com.enel.scom.api.gestion.model.ObservacionEntity;
import com.enel.scom.api.gestion.model.OrdenEntity;
import com.enel.scom.api.gestion.model.OrdenInspeccionEntity;
import com.enel.scom.api.gestion.model.ParametroEntity;
import com.enel.scom.api.gestion.model.TransferEntity;
import com.enel.scom.api.gestion.model.UsuarioEntity;
import com.enel.scom.api.gestion.model.WorkflowEntity;
import com.enel.scom.api.gestion.repository.AuditEventRepository;
import com.enel.scom.api.gestion.repository.BuzonRepository;
import com.enel.scom.api.gestion.repository.ClienteRepository;
import com.enel.scom.api.gestion.repository.DisSegInpsRepository;
import com.enel.scom.api.gestion.repository.HistoricoRepository;
import com.enel.scom.api.gestion.repository.MotivoAnulacionRepository;
import com.enel.scom.api.gestion.repository.ObservacionRepository;
import com.enel.scom.api.gestion.repository.OrdenInspeccionRepository;
import com.enel.scom.api.gestion.repository.OrdenRepository;
import com.enel.scom.api.gestion.repository.ParametroRepository;
import com.enel.scom.api.gestion.repository.TransferRepository;
import com.enel.scom.api.gestion.repository.UsuarioRepository;
import com.enel.scom.api.gestion.repository.WorkflowRepository;
import com.enel.scom.api.gestion.service.OrdenTrabajoService;
import com.enel.scom.api.gestion.service.TipoOrdenService;
import com.enel.scom.api.gestion.util.Funciones;
import com.enel.scom.soap.service.generated.ExecuteOrdenesOperation;
import com.enel.scom.soap.service.generated.OrdenesService;
import com.enel.scom.soap.service.generated.ResponseDTO;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class OrdenTrabajoServiceImpl implements OrdenTrabajoService {
	
	@Autowired
	OrdenRepository ordenRepository;
	
	@Autowired
	WorkflowRepository workflowRepository;
	
	@Autowired
	TransferRepository transferRepository;
	
	@Autowired
	ParametroRepository parametroRepository;
	
	@Autowired
	MotivoAnulacionRepository motivoAnulacionRepository;
	
	@Autowired
	UsuarioRepository usuarioRepository;
	
	@Autowired
	TipoOrdenService tipoOrdenService;
	
	@Autowired
	AuditEventRepository auditEventRepository;
	
	@Autowired
	ObservacionRepository observacionRepository;
	
	@Autowired
	OrdenInspeccionRepository ordenInspeccionRepository;
	
	@Autowired
	DisSegInpsRepository disSegInpsRepository;
	
	@Autowired
	BuzonRepository buzonRepository;
	
	@Autowired
	HistoricoRepository historicoRepository;
	
	// R.I. REQSCOM06 06/10/2023 INICIO
	@Autowired
	ClienteRepository clienteRepository;
	// R.I. REQSCOM06 06/10/2023 FIN
	
	@PersistenceContext
	EntityManager entityManager;
	/*
	@Autowired
	@Qualifier("allowancePostpaidWebServiceTemplate")
	private WebServiceTemplate webServiceTemplate;*/

	@Override
	public Page<TransferPaginacionResponseDTO> getOrdenTransferenciaPaginacion(Pageable paging,
	String idTipoOrden, String fechaInicio, String fechaFin, String numOrden, Long numOrdenTdc, String nroCuenta, String estado, String codTipoProceso, Long codEstadTransfer, String codTipoOrdenEorder, String autoActivacion) {
		Date ini = new Date();
		log.info("/=================================================================================");  
		log.info("/clase: {}", "OrdenTrabajoServiceImpl");
		log.info("/metodo: {}", "getOrdenTransferenciaPaginacion");
		
		log.info("/paging: {}", paging);
		log.info("/idTipoOrden: {}", idTipoOrden);
		log.info("/fechaInicio: {}", fechaInicio);
		log.info("/fechaFin: {}", fechaFin);
		log.info("/numOrden: {}", numOrden);
		log.info("/numOrdenTdc: {}", numOrdenTdc);
		log.info("/nroCuenta: {}", nroCuenta);
		log.info("/estado: {}", estado);
		log.info("/codTipoProceso: {}", codTipoProceso);
		log.info("/codEstadTransfer: {}", codEstadTransfer);
		log.info("/codTipoOrdenEorder: {}", codTipoOrdenEorder);
		log.info("/autoActivacion: {}", autoActivacion);
		
		String ALL_ITEMS= "TODO";
		String FORMAT_DATE= "yyyy-MM-dd";
		String COD_ORDEN_PLAN_MANT= "PMANT";
		
    	// String sql = "";
    	
    	String sqlSelect = ""
				+ "select DISTINCT new com.enel.scom.api.gestion.dto.response.TransferPaginacionResponseDTO( "
    			+ "trans.idOrdTransfer, "
    			+ "trans.codTipoOrdenLegacy, "
    			+ "trans.nroOrdenLegacy, "
    			+ "trans.codTipoOrdenEorder, "
    			+ "trans.nroOrdenEorder, "
    			+ "par.descripcion, "
    			+ "trans.suspendida, "
    			+ "trans.fecEstado, "
    			+ "trans.nroCuenta, "
    			+ "trans.creadaEorder, "
    			+ "case when ord.motivo.id=1240 then ord.motivo.id else 0 end as motivo, "
    			+ "trans.fechaCreacion, "
    			+ "ord.nroOrden, "
    			+ "trans.generacion"
				;

    	if(!StringUtils.isBlank(autoActivacion)) {
    		sqlSelect += ",actv.estadoActivacion, actv.idAutoAct";
    	}
		sqlSelect += " ) ";
    	
    	String sqlFrom = ""
				+ "from eor_ord_transfer trans "
    			+ "JOIN com_parametros par ON trans.codEstadoOrden = par.valorNum "
    			+ "AND par.entidad='ESTADO_TRANSFERENCIA' AND par.sistema='EORDER' "
    			+ "LEFT JOIN ord_orden ord ON trans.nroOrdenLegacy = ord.nroOrden";
    	
    	if(!StringUtils.isBlank(autoActivacion)) {
    		sqlFrom +=" JOIN activacion actv ON trans.nroOrdenLegacy = actv.numeroOrdenConexion and "
    				+ "trans.nroOrdenLgcRelac = concat(actv.numeroOrdenVenta,'')";
    	}
    	//par.descripcion
    	//LEFT JOIN wkf_workflow w ON ord.workflow.id= w.id
    	//AND trans.codTipoOrdenEorder= par.valorAlf AND par.entidad='TIPO_TDC' and par.sistema='EORDER' 
    	//(!StringUtils.isBlank(estado))
    	
		String sqlWhere = "";
    	if ((!StringUtils.isBlank(idTipoOrden) && !idTipoOrden.equals(ALL_ITEMS)) || (!StringUtils.isBlank(numOrden)) || (!StringUtils.isBlank(nroCuenta)) || (!StringUtils.isBlank(fechaInicio) && !fechaInicio.equals("null")) || (!StringUtils.isBlank(fechaFin) && fechaFin.equals("null")) || (codEstadTransfer!= null && codEstadTransfer !=0) || (numOrdenTdc!= null && numOrdenTdc != -1) || (!StringUtils.isBlank(codTipoProceso) && !codTipoProceso.equals(ALL_ITEMS)) || (!StringUtils.isBlank(codTipoOrdenEorder) && !codTipoOrdenEorder.equals(ALL_ITEMS)) || !StringUtils.isBlank(autoActivacion) && !autoActivacion.equals(ALL_ITEMS)) {
            sqlWhere +=" where";
        }
        
    	if(!StringUtils.isBlank(codTipoProceso) && !codTipoProceso.equals(ALL_ITEMS)) {
    		if(!StringUtils.isBlank(idTipoOrden) && !idTipoOrden.equals(ALL_ITEMS)) {
    			
    			 switch (idTipoOrden) {
    				case "PMANT":
    					sqlWhere +=  " ord.motivo.id=1240 and";
        				idTipoOrden = "MANT";
    					break;
    				case "RMANT":
    					sqlWhere +=  " (ord.motivo.id is null or ord.motivo.id <>1240) and trans.codTipoOrdenEorder in ('NCX.03') and";
        				idTipoOrden = "MANT";
    					break;
    				case "MANT":
    					sqlWhere +=  " trans.codTipoOrdenEorder in ('NCX.05') and";
    					break;
    				case "NOCNX":
    					sqlWhere +=  " trans.codTipoOrdenEorder in ('NCX.01') and";
        				idTipoOrden = "OCNX";
    					break;
    				case "OCNX":
    					sqlWhere +=  " trans.codTipoOrdenEorder in ('NCX.02') and";
    					break;
    				default:		
    				}
    			 sqlWhere +=  " trans.codTipoOrdenLegacy IN ('" + idTipoOrden +"')";
    		} else {
    			sqlWhere +=  " trans.codTipoOrdenLegacy IN (" + codTipoProceso +")";
    		}
    		
    		
    	}
        
    	/*
        if (!StringUtils.isBlank(estado)) {
          	 if((!StringUtils.isBlank(idTipoOrden) && !idTipoOrden.equals(ALL_ITEMS)) || (!StringUtils.isBlank(codTipoProceso) && !codTipoProceso.equals(ALL_ITEMS))) {
          		sql +=  " and w.idState='" + estado + "'";
          	 }else {
          		sql +=  " w.idState='" + estado + "'";
          	 } 
        }*/
        
        if (!StringUtils.isBlank(numOrden)) {
          	
        	if((!StringUtils.isBlank(idTipoOrden) && !idTipoOrden.equals(ALL_ITEMS)) || (!StringUtils.isBlank(codTipoProceso) && !codTipoProceso.equals(ALL_ITEMS))) {
        		 sqlWhere +=  " and trans.nroOrdenLegacy='" + numOrden + "'";
        	}else {
        		 sqlWhere +=  " trans.nroOrdenLegacy='" + numOrden + "'";
        	}
        }
        
        if (!StringUtils.isBlank(nroCuenta)) {
        	if((!StringUtils.isBlank(idTipoOrden)  && !idTipoOrden.equals(ALL_ITEMS)) || (!StringUtils.isBlank(numOrden)) || (!StringUtils.isBlank(codTipoProceso) && !codTipoProceso.equals(ALL_ITEMS))) {
        		sqlWhere +=  " and trans.nroCuenta='" + nroCuenta + "'";
        	}
        	else {
        		sqlWhere +=  " trans.nroCuenta='" + nroCuenta + "'";
        	}
        }
        
        if (codEstadTransfer!= null && codEstadTransfer != 0) {
        	if((!StringUtils.isBlank(idTipoOrden)  && !idTipoOrden.equals(ALL_ITEMS)) || (!StringUtils.isBlank(numOrden)) || (!StringUtils.isBlank(nroCuenta)) || (!StringUtils.isBlank(codTipoProceso) && !codTipoProceso.equals(ALL_ITEMS))) {
        		sqlWhere +=  " and trans.codEstadoOrden='" + codEstadTransfer + "'";
        	}
        	else {
        		sqlWhere +=  " trans.codEstadoOrden='" + codEstadTransfer + "'";
        	}
        }
        
        if(!StringUtils.isBlank(codTipoOrdenEorder) && !codTipoOrdenEorder.equals(ALL_ITEMS)) {
        	if((!StringUtils.isBlank(idTipoOrden)  && !idTipoOrden.equals(ALL_ITEMS)) || (!StringUtils.isBlank(numOrden)) || (!StringUtils.isBlank(nroCuenta)) || (!StringUtils.isBlank(codTipoProceso) && !codTipoProceso.equals(ALL_ITEMS)) || codEstadTransfer!= null && codEstadTransfer != 0) {
        		
        		sqlWhere +=  " and trans.codTipoOrdenEorder IN ('" + codTipoOrdenEorder +"')";
        	}
        	else {
        		sqlWhere +=  " trans.codTipoOrdenEorder IN ('" + codTipoOrdenEorder +"')";
        	}
        }
        
        if (numOrdenTdc!= null && numOrdenTdc != -1) {
            
            if((!StringUtils.isBlank(idTipoOrden)  && !idTipoOrden.equals(ALL_ITEMS)) || (!StringUtils.isBlank(numOrden)) || (!StringUtils.isBlank(nroCuenta)) || (!StringUtils.isBlank(codTipoProceso) && !codTipoProceso.equals(ALL_ITEMS)) || (codEstadTransfer!= null && codEstadTransfer != 0) || (!StringUtils.isBlank(codTipoOrdenEorder) && !codTipoOrdenEorder.equals(ALL_ITEMS))) {
                 sqlWhere +=  " and trans.nroOrdenEorder='" + numOrdenTdc + "'";
            }else {
                 sqlWhere +=  " trans.nroOrdenEorder='" + numOrdenTdc + "'";
            }
        }
        
        if (!StringUtils.isBlank(autoActivacion) && !autoActivacion.equals(ALL_ITEMS)) {
        	 if((!StringUtils.isBlank(idTipoOrden)  && !idTipoOrden.equals(ALL_ITEMS)) || (!StringUtils.isBlank(numOrden)) || (!StringUtils.isBlank(nroCuenta)) || (!StringUtils.isBlank(codTipoProceso) && !codTipoProceso.equals(ALL_ITEMS)) || codEstadTransfer!= null && codEstadTransfer != 0 || !StringUtils.isBlank(codTipoOrdenEorder) && !codTipoOrdenEorder.equals(ALL_ITEMS) || numOrdenTdc!= null && numOrdenTdc != -1) {
        		sqlWhere +=  " and actv.estadoActivacion='" + autoActivacion + "'";
        	 }else {
        		sqlWhere +=  " actv.estadoActivacion='" + autoActivacion + "'";
        	 } 
      }
        
        if ((!StringUtils.isBlank(fechaInicio)) || (!StringUtils.isBlank(fechaFin))) {
        	try {
        		SimpleDateFormat sdf=new SimpleDateFormat(FORMAT_DATE);
                Date currentdateFehaInicio = sdf.parse(fechaInicio);
                SimpleDateFormat sdf2=new SimpleDateFormat(FORMAT_DATE);
                String fechaInicioConvertido = sdf2.format(currentdateFehaInicio);
                
                SimpleDateFormat sdf4=new SimpleDateFormat(FORMAT_DATE);
                Date currentdateFehaFin=sdf4.parse(fechaFin);
                SimpleDateFormat sdf3=new SimpleDateFormat(FORMAT_DATE);
                String fechaFinConvertido = sdf3.format(currentdateFehaFin);
                
                if((!StringUtils.isBlank(codTipoProceso) && !codTipoProceso.equals(ALL_ITEMS)) || (!StringUtils.isBlank(idTipoOrden) && !idTipoOrden.equals(ALL_ITEMS)) || (!StringUtils.isBlank(numOrden)) || (!StringUtils.isBlank(nroCuenta)) || (codEstadTransfer!=0) || (!StringUtils.isBlank(codTipoOrdenEorder) && !codTipoOrdenEorder.equals(ALL_ITEMS)) || (numOrdenTdc!= null && numOrdenTdc != -1) || !StringUtils.isBlank(autoActivacion) && !autoActivacion.equals(ALL_ITEMS)) {
            		sqlWhere +=  " and DATE_TRUNC('day',trans.fechaCreacion) between "+ "'"+fechaInicioConvertido +"'"+" and "+"'"+ fechaFinConvertido+"'";
            	}else {
            		sqlWhere +=  " DATE_TRUNC('day',trans.fechaCreacion) between "+ "'"+fechaInicioConvertido +"'"+" and "+"'"+ fechaFinConvertido+"'";
            	}
            	
        	}catch (Exception e) {
				e.printStackTrace();
			}	
             
        }
        
        try {
        	int offset = paging.getPageNumber() * paging.getPageSize();
			int limit = paging.getPageSize();
			log.info("/offset: {}", offset);
			log.info("/limit: {}", limit);

			String sqlCantidad = "select count(1) ".concat(sqlFrom).concat(sqlWhere);
			log.info("/consulta cantidad: {}", sqlCantidad);
			log.info("/ejecucion consulta cuenta memory ini {}", Funciones.logMemoryJVM());
			// Descomentar para PROD
			//Long cantidadTotalRegistros = (Long) entityManager.createQuery(sqlCantidad).getSingleResult();
			Long cantidadTotalRegistros = 200000L;
			log.info("/ejecucion consulta cuenta memory fin {}", Funciones.logMemoryJVM());
			log.info("/cantidad total de registros: {}", cantidadTotalRegistros);

			String sqlOrder = " order by trans.fechaCreacion desc";

			String sqlDatos = sqlSelect.concat(sqlFrom).concat(sqlWhere).concat(sqlOrder);
			log.info("/consulta datos: {}", sqlDatos);

			log.info("/ejecucion consulta memory ini {}", Funciones.logMemoryJVM());
        	List<TransferPaginacionResponseDTO> result = entityManager.createQuery(sqlDatos, TransferPaginacionResponseDTO.class).setFirstResult(offset).setMaxResults(limit).getResultList();
			log.info("/ejecucion consulta memory fin {}", Funciones.logMemoryJVM());
          	
          	result.stream().forEach(k -> {
          		if(k.getIdMotivo() != null) {
          			if(k.getIdMotivo().equals(Long.parseLong("1240"))){
              			k.setTipoOrdenLegacy(COD_ORDEN_PLAN_MANT);
              		}
          		}
          		if(k.getOrigenCrea().equals("1")) {
          			k.setOrigenCrea("FORCEBEAT");
          		} else {
          			if(k.getNroOrdenOrdOrden() != null) {
          				k.setOrigenCrea("SCOM");
          			} else {
          				k.setOrigenCrea("4J");
          			}
          		}
          		
          	});
          	
			List<TransferPaginacionResponseDTO> lista = (List<TransferPaginacionResponseDTO>) result.stream().collect(Collectors.toList());
			
			PageImpl<TransferPaginacionResponseDTO> objectoFinal = new PageImpl<TransferPaginacionResponseDTO>(lista, paging, cantidadTotalRegistros);
            
			Date fin = new Date();
			log.info("/+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
			log.info("/cantidad registros : {}", cantidadTotalRegistros);
			log.info("/ini: {}", ini);
			log.info("/fin: {}", fin);
			return objectoFinal;

        } catch (Exception e) {
        	e.printStackTrace();
        	throw new AllNotFoundException("Error al consultar las ordenes"); 
		} finally {
			entityManager.close();
		}
      
	}
	
		
	@Override
	@Transactional
	public List<TransferAnularRequestDTO> anulacionMasivaTransferencia(List<TransferAnularRequestDTO> transferRequest) throws AnularNotFoundException {	
		
		List<TransferAnularRequestDTO> dtos = new ArrayList<>();
			try {
				transferRequest.stream().forEach(k -> {
				
				
				gestionarAnulacion(k);
					
				
				});
			} catch (Exception e) {
				throw new AnularNotFoundException(e.getMessage());
			}
		return dtos;
	}
	
	private void gestionarAnulacion(TransferAnularRequestDTO k) throws AnularNotFoundException {
		
		long ESTADO_TRANS_ENVIADO_ERROR = 4;
		long ESTADO_TRANS_ANULADO = 13;
		long ESTADO_TRANS_ENVIADO = 3;
		long ID_MOTIVO_TIPO_PLAN_MANT = 1240;
		
		TransferEntity transferEntity = transferRepository.obtenerTransferenciaPorId(k.getId());
		
		if(transferEntity == null) {
			throw new AnularNotFoundException("No se encontro id transferencia");
		}
		
		// Orden de tipo Perdida: Inspección, Normalizacion y Contraste
		if((transferEntity.getCodTipoOrdenLegacy().equals(Constant.COD_TIPO_ORDEN_INSPECCION) ||
			transferEntity.getCodTipoOrdenLegacy().equals(Constant.COD_TIPO_ORDEN_NORMALIZACION) ||
			transferEntity.getCodTipoOrdenLegacy().equals(Constant.COD_TIPO_ORDEN_CONTRASTE) ||
			transferEntity.getCodTipoOrdenLegacy().equals(Constant.COD_TIPO_ORDEN_MANTENIMIENTO))) {
			
			OrdenEntity ordenEntity = ordenRepository.obtenerOrdenPorNumeroOrden(transferEntity.getNroOrdenLegacy());
			
			if(ordenEntity == null) {
				throw new AnularNotFoundException("No se encontro id orden");
			}
			
			WorkflowEntity workflowEntity = workflowRepository.obtenerEstadoOrdenPorId(ordenEntity.getWorkflow().getId());
			
			if(workflowEntity == null) {
				throw new AnularNotFoundException("No se encontro id worflow");
			}

			
			if((transferEntity.getCodEstadoOrden().equals(ESTADO_TRANS_ENVIADO_ERROR)) || (transferEntity.getCodEstadoOrden().equals(ESTADO_TRANS_ENVIADO))) {
				
				//Mantenimiento (Plan de Mantenimiento)
				if(ordenEntity.getTipoOrden().getCodigoTipoOrden().equals(Constant.COD_TIPO_ORDEN_MANTENIMIENTO) && ((!ordenEntity.getMotivo().getId().equals(ID_MOTIVO_TIPO_PLAN_MANT)) || (ordenEntity.getMotivo().getId() == null))) {
					throw new AnularNotFoundException("El tipo de orden NO puede ser ANULADA. "+ "Nro: " + ordenEntity.getNroOrden()); 
				}
				
				// Código de estado transferencia 4 = "Enviado con Error"
				if(transferEntity.getCodEstadoOrden().equals(ESTADO_TRANS_ENVIADO_ERROR) ) {
					
					//Se cambia estado ANULADO de la TRANSFERENCIA Código de estado transferecia 13 = "Anulada" 
					transferEntity.setCodEstadoOrdenAnt(transferEntity.getCodEstadoOrden());
					transferEntity.setCodEstadoOrden(Long.valueOf(ESTADO_TRANS_ANULADO));
					transferEntity.setFecOperacion(new Date());
                    transferEntity.setFecEstado(new Date());
					transferRepository.save(transferEntity);
					
					//Se cambia estado de ORDEN en WORKFLOW
					switch (transferEntity.getCodTipoOrdenLegacy()) {
					case Constant.COD_TIPO_ORDEN_INSPECCION:
							anularInspeccion(transferEntity.getNroOrdenLegacy(), k.getUsuarioAnula());
						break;
					case Constant.COD_TIPO_ORDEN_NORMALIZACION:
							anularNormalizacion(transferEntity.getNroOrdenLegacy(), k.getUsuarioAnula());
						break;
					
					case Constant.COD_TIPO_ORDEN_CONTRASTE:
							anularConstraste(transferEntity.getNroOrdenLegacy(), k.getUsuarioAnula());
						break;
					case Constant.COD_TIPO_ORDEN_MANTENIMIENTO:
							anularOrdenMatenimiento(transferEntity.getNroOrdenLegacy(), k.getUsuarioAnula());
						break;
					default:
					
					}
					
				}
				

				// Código de estado transferencia 3 = "Enviado"
				if(transferEntity.getCodEstadoOrden().equals(ESTADO_TRANS_ENVIADO) ) {
					
					//Enviar solicitud de anulacion a forcebeat y una vez se anule se cambia el estado en el SCOM
					ParametroEntity wsdlLocation = parametroRepository.getWsdlLocation();
					ParametroEntity puenteWsdl = parametroRepository.getPuenteWsdl();
					ParametroEntity paramLlave = parametroRepository.getParamLlave();
					
					ExecuteOrdenesOperation request = new ExecuteOrdenesOperation();
					//request.setWsdl(Constant.SOAP_SERVICE_ANULA_PUENTE_WSDL);
					request.setWsdl(puenteWsdl.getValorAlf());
					
					request.setDistribuidora(Constant.SOAP_SERVICE_ANULA_PARAM_DISTRIBUIDORA);
					request.setOrigen(Constant.SOAP_SERVICE_ANULA_PARAM_ORIGEN);
					request.setTipoTDC(transferEntity.getCodTipoOrdenEorder());
					request.setCodigoExternoDelTDC(transferEntity.getNroOrdenLegacy());
					request.setTipoOperacion(1); //Tipo operacion Anulacion
					
					//request.setLlaveSecreta(Constant.SOAP_SERVICE_ANULA_PARAM_LLAVE);
					request.setLlaveSecreta(paramLlave.getValorAlf());
					
					URL url = null;
					
					try {
						url = new URL(wsdlLocation.getValorAlf() + "?wsdl");
					} catch (MalformedURLException e) {
						e.printStackTrace();
					}
					
					//OrdenesService service = new OrdenesService();
					OrdenesService service = new OrdenesService(url);
					
					ResponseDTO response = service.getWssServicesSEPort().executeOrdenesOperation(
							request.getWsdl(), 
							request.getDistribuidora(), 
							request.getOrigen(), 
							request.getTipoTDC(), 
							request.getCodigoExternoDelTDC(), 
							request.getTipoOperacion(), 
							null, 
							null, 
							request.getLlaveSecreta());
					
					if((response.getCodigo().equals("16")) || (response.getCodigo().equals("00"))) {
						
						if(response.getCodigo().equals("16")) {
							//Si la respuesta de solicitud de anulación al servicio es aprobada se cambia el estado en SCOM
							//Se cambia estado PENDIENTE DE ANULACION 12 de la TRANSFERENCIA
							transferEntity.setCodEstadoOrdenAnt(transferEntity.getCodEstadoOrden());
							transferEntity.setCodEstadoOrden(Long.valueOf(Constant.ESTADO_TRANSFER_VAL_NUM_PENDIENTE_DE_ANULACION));
							transferEntity.setFecOperacion(new Date());
                            transferEntity.setFecEstado(new Date());
							transferRepository.save(transferEntity);
						}
						
						if(response.getCodigo().equals("00")) {
							
							//Se cambia estado ANULADO de la TRANSFERENCIA Código de estado transferecia 13 = "Anulada" 
							transferEntity.setCodEstadoOrdenAnt(transferEntity.getCodEstadoOrden());
							transferEntity.setCodEstadoOrden(Long.valueOf(ESTADO_TRANS_ANULADO));
							transferEntity.setFecOperacion(new Date());
                            transferEntity.setFecEstado(new Date());
							transferRepository.save(transferEntity);
							
							//Se cambia estado de ORDEN en WORKFLOW
							switch (transferEntity.getCodTipoOrdenLegacy()) {
							case Constant.COD_TIPO_ORDEN_INSPECCION:
									anularInspeccion(transferEntity.getNroOrdenLegacy(), k.getUsuarioAnula());
								break;
							case Constant.COD_TIPO_ORDEN_NORMALIZACION:
									anularNormalizacion(transferEntity.getNroOrdenLegacy(), k.getUsuarioAnula());
								break;
							
							case Constant.COD_TIPO_ORDEN_CONTRASTE:
									anularConstraste(transferEntity.getNroOrdenLegacy(), k.getUsuarioAnula());
								break;
							case Constant.COD_TIPO_ORDEN_MANTENIMIENTO:
									anularOrdenMatenimiento(transferEntity.getNroOrdenLegacy(), k.getUsuarioAnula());
								break;
							default:
							
							}
						}
						
						
					}else {
						System.out.print(response.getDescripcion());
						throw new AnularNotFoundException("Ocurrio un error en la solicitud: " + response.getDescripcion());
					}
				}
				
			} else {
				throw new AnularNotFoundException("Solo se puede anular una orden con estado ENVIADO ó ENVIADO CON ERROR");
			}
			
		} else {
			 throw new AnularNotFoundException("El tipo de orden "+ transferEntity.getCodTipoOrdenLegacy() +" NO puede ser ANULADA. "+ "Nro: " + transferEntity.getNroOrdenLegacy());
		}
	
	}
	
	
	private void anularInspeccion(String nroOrdenLegacy, String usuarioAnula) {
		
		MotivoAnulacionEntity motivoAnulacion = motivoAnulacionRepository.obtenerPorCodigoInterno("MotivoMasivo");
		OrdenEntity ordenEntity = ordenRepository.obtenerOrdenPorNumeroOrden(nroOrdenLegacy);
		ordenEntity.setFechaIngresoEstadoActual(new Date());
		ordenEntity.setFechaFinalizacion(new Date());
		ordenEntity.setMotivoAnulacion(motivoAnulacion);
		ordenEntity.setLeido("S");
		ordenRepository.save(ordenEntity);
		
		WorkflowEntity workflowOrden = workflowRepository.obtenerEstadoOrdenPorId(ordenEntity.getWorkflow().getId()); 
		workflowOrden.setIdOldState(workflowOrden.getIdState());
		workflowOrden.setIdState(Constant.ESTADO_ORDEN_ANULADA);
		workflowOrden.setFechaUltimoEstado(new Date());
		workflowRepository.save(workflowOrden);
		
		OrdenInspeccionEntity ordenInspeccion = ordenInspeccionRepository.getIdOrden(ordenEntity.getId());
		
		DisSegInspEntity disSegInsp = disSegInpsRepository.obtenerPorIdSeguimiento(ordenInspeccion.getDisSegInspEntity().getId());
		WorkflowEntity workflowInspeccion = workflowRepository.obtenerEstadoOrdenPorId(disSegInsp.getIdWfk());
		workflowInspeccion.setIdOldState(workflowInspeccion.getIdState());
		workflowInspeccion.setIdState("Generada");
		workflowOrden.setFechaUltimoEstado(new Date());
		workflowRepository.save(workflowOrden);
		
		
		UsuarioEntity usuario = usuarioRepository.getUsuarioByUserName("EXPLOTAHURTO");
		UsuarioEntity userAnula = usuarioRepository.getUsuarioByUserName(usuarioAnula);
		
		AuditEventEntity auditEvent = new AuditEventEntity();
		
		auditEvent.setId(auditEventRepository.generarIdAuditEvent());
		auditEvent.setUseCase("OrdenInspeccion.update");
		auditEvent.setObjectref("com.synapsis.scom.dis.domain.OrdenInspeccion");
		auditEvent.setIdFk(ordenEntity.getId());
		auditEvent.setFechaEjecucion(new Date());
		auditEvent.setSpecificAuditevent("Anulacion de solicitud de usuario "+ userAnula.getUsername());
		auditEvent.setUsuarioEntity(usuario);
		auditEventRepository.save(auditEvent);

	}	
	
	
	private void anularNormalizacion(String nroOrdenLegacy, String usuarioAnula) {
			
		MotivoAnulacionEntity motivoAnulacion = motivoAnulacionRepository.obtenerPorCodigoInterno("MotivoMasivo");
		OrdenEntity ordenEntity = ordenRepository.obtenerOrdenPorNumeroOrden(nroOrdenLegacy);
		ordenEntity.setFechaIngresoEstadoActual(new Date());
		ordenEntity.setFechaFinalizacion(new Date());
		ordenEntity.setMotivoAnulacion(motivoAnulacion);
		ordenEntity.setLeido("S");
		ordenRepository.save(ordenEntity);
		
		WorkflowEntity workflowOrden = workflowRepository.obtenerEstadoOrdenPorId(ordenEntity.getWorkflow().getId()); 
		workflowOrden.setIdOldState(workflowOrden.getIdState());
		workflowOrden.setIdState(Constant.ESTADO_ORDEN_ANULADA);
		workflowOrden.setFechaUltimoEstado(new Date());
		workflowRepository.save(workflowOrden);
		
		UsuarioEntity userAnula = usuarioRepository.getUsuarioByUserName(usuarioAnula);
		
		AuditEventEntity auditEvent = new AuditEventEntity();
		
		auditEvent.setId(auditEventRepository.generarIdAuditEvent());
		auditEvent.setUseCase("OrdenNormalizacion.update");
		auditEvent.setObjectref("com.synapsis.scom.dis.domain.OrdenNormalizacion");
		auditEvent.setIdFk(ordenEntity.getId());
		auditEvent.setFechaEjecucion(new Date());
		auditEvent.setSpecificAuditevent("Anulacion de solicitud de usuario "+ userAnula.getUsername());
		auditEvent.setUsuarioEntity(userAnula);
		auditEventRepository.save(auditEvent);
		
	}
	
	
	
	private void anularOrdenMatenimiento(String nroOrdenLegacy, String usuarioAnula) {
		
		
		MotivoAnulacionEntity motivoAnulacion = motivoAnulacionRepository.obtenerPorCodigoInterno("MotivoMasivo");
		OrdenEntity ordenEntity = ordenRepository.obtenerOrdenPorNumeroOrden(nroOrdenLegacy);
		ordenEntity.setFechaIngresoEstadoActual(new Date());
		ordenEntity.setFechaFinalizacion(new Date());
		ordenEntity.setMotivoAnulacion(motivoAnulacion);
		ordenEntity.setLeido("S");
		ordenRepository.save(ordenEntity);
		
		WorkflowEntity workflowOrden = workflowRepository.obtenerEstadoOrdenPorId(ordenEntity.getWorkflow().getId()); 
		workflowOrden.setIdOldState(workflowOrden.getIdState());
		workflowOrden.setIdState(Constant.ESTADO_ORDEN_ANULADA);
		workflowOrden.setFechaUltimoEstado(new Date());
		workflowRepository.save(workflowOrden);
		
		UsuarioEntity userAnula = usuarioRepository.getUsuarioByUserName(usuarioAnula);
		
		
		AuditEventEntity auditEvent = new AuditEventEntity();
		auditEvent.setId(auditEventRepository.generarIdAuditEvent());
		auditEvent.setUseCase("OrdenMantenimiento.update");
		auditEvent.setObjectref("com.synapsis.scom.orm.domain.OrdenMantenimiento");
		auditEvent.setIdFk(ordenEntity.getId());
		auditEvent.setFechaEjecucion(new Date());
		auditEvent.setSpecificAuditevent("ORD");
		auditEvent.setUsuarioEntity(userAnula);
		auditEvent = auditEventRepository.save(auditEvent);
		
		ObservacionEntity observacion = new ObservacionEntity();
		observacion.setIdObservacion(observacionRepository.generaId());
		observacion.setIdOrden(ordenEntity.getId());
		observacion.setTexto("Anulación masiva desde SCOM");
		observacion.setFechaObservacion(new Date());
		observacion.setUsuario(userAnula);
		observacion.setStateName("Anulada");
		observacion.setDiscriminator("ObeservacionOrden");
		
		BuzonEntity buzon = buzonRepository.obtenerPorId(ordenEntity.getBuzonEntity().getId());
		if(buzon == null) {
			throw new AnularNotFoundException("No se encontro id buzon");
		}
		
		HistoricoEntity historico = new HistoricoEntity();
		historico.setIdAuditevent(auditEvent.getId());
		historico.setEstadoInicial("Orden Creada");
		historico.setBuzonEntity(buzon);
		historico.setEstadoFinal("Orden Anulada");
		historico.setActividad("Anula Orden");
	}
	
	
	private void anularConstraste(String nroOrdenLegacy, String usuarioAnula) {
		
		MotivoAnulacionEntity motivoAnulacion = motivoAnulacionRepository.obtenerPorCodigoInterno("MotivoMasivo");
		OrdenEntity ordenEntity = ordenRepository.obtenerOrdenPorNumeroOrden(nroOrdenLegacy);
		ordenEntity.setFechaIngresoEstadoActual(new Date());
		ordenEntity.setFechaFinalizacion(new Date());
		ordenEntity.setMotivoAnulacion(motivoAnulacion);
		ordenEntity.setLeido("S");
		ordenRepository.save(ordenEntity);
		
		WorkflowEntity workflowOrden = workflowRepository.obtenerEstadoOrdenPorId(ordenEntity.getWorkflow().getId()); 
		workflowOrden.setIdOldState(workflowOrden.getIdState());
		workflowOrden.setIdState(Constant.ESTADO_ORDEN_ANULADA);
		workflowOrden.setFechaUltimoEstado(new Date());
		workflowRepository.save(workflowOrden);
		
		UsuarioEntity userAnula = usuarioRepository.getUsuarioByUserName(usuarioAnula);
		
		AuditEventEntity auditEvent = new AuditEventEntity();
		
		auditEvent.setId(auditEventRepository.generarIdAuditEvent());
		auditEvent.setUseCase("OrdenContraste.update");
		auditEvent.setObjectref("com.synapsis.scom.dis.domain.OrdenContraste");
		auditEvent.setIdFk(ordenEntity.getId());
		auditEvent.setFechaEjecucion(new Date());
		auditEvent.setSpecificAuditevent("Anulacion de solicitud de usuario "+ userAnula.getUsername());
		auditEvent.setUsuarioEntity(userAnula);
		auditEventRepository.save(auditEvent);
		
	}


	@Override
	public Page<OrdenTrabajoSCOMPaginacionResponseDTO> getOrdenTrabajoSCOMPaginacion(Pageable paging, String idTipoOrden,
			String fechaInicio, String fechaFin, String numOrden, String estado, String codTipoProceso, Long idServicio, String nroCuenta) {
		Date ini = new Date();
		log.info("/=================================================================================");  

		log.info("/clase: {}", "OrdenTrabajoServiceImpl");
		log.info("/metodo: {}", "getOrdenTrabajoSCOMPaginacion");
		
		log.info("/paging: {}", paging);
		log.info("/idTipoOrden: {}", idTipoOrden);
		log.info("/fechaInicio: {}", fechaInicio);
		log.info("/fechaFin: {}", fechaFin);
		log.info("/numOrden: {}", numOrden);
		log.info("/estado: {}", estado);
		log.info("/codTipoProceso: {}", codTipoProceso);
		log.info("/idServicio: {}", idServicio);

		String ALL_ITEMS= "TODO";
		String FORMAT_DATE= "yyyy-MM-dd";
		String COD_ORDEN_PLAN_MANT= "PMANT";
		

		String sqlSelect = ""
				+ "select new com.enel.scom.api.gestion.dto.response.OrdenTrabajoSCOMPaginacionResponseDTO( "
    			+ "ord.id, "
    			+ "ord.nroOrden, "
    			+ "ot.id, "
    			+ "ot.descripcion, "
    			+ "ot.codigoTipoOrden, "
    			+ "ord.fechaCreacion, "
    			//+ "ord.idServicio, "
    			+ "c.nroServicio, "
    			+ "w.idState, "
    			+ "w.fechaUltimoEstado, "
    			+ "u.username, "
    			+ "ord.motivo.id, "
    			+ "c.nroCuenta, "
    			+ "c.distrito "
    			+ ")  "
				;

    	String sqlFrom = ""
				+ "from ord_orden ord "
    			+ "JOIN ord_tipo_orden ot ON ord.tipoOrden.id =ot.id "
    			+ "JOIN wkf_workflow w ON ord.workflow.id= w.id "
    			+ "LEFT JOIN cliente c ON ord.idServicio= c.idServicio "
    			+ "LEFT JOIN usuario u ON ord.usuarioRegistro.id = u.id "
				;
    	
		String sqlWhere = "";
    	
    	if ((!StringUtils.isBlank(idTipoOrden) && !idTipoOrden.equals(ALL_ITEMS)) || (!StringUtils.isBlank(numOrden)) || (!StringUtils.isBlank(fechaInicio) && !fechaInicio.equals("null")) || (!StringUtils.isBlank(fechaFin) && fechaFin.equals("null")) || (!StringUtils.isBlank(codTipoProceso) && !codTipoProceso.equals(ALL_ITEMS)) || (idServicio != null && idServicio != 0)) {
            sqlWhere +=" where";
        }
        
    	if(!StringUtils.isBlank(codTipoProceso) && !codTipoProceso.equals(ALL_ITEMS)) {
    		if(!StringUtils.isBlank(idTipoOrden) && !idTipoOrden.equals(ALL_ITEMS)) {
				sqlWhere +=  " ord.tipoOrden.codigoTipoOrden IN ('" + idTipoOrden +"')";
			} else {
				sqlWhere +=  " ord.tipoOrden.codigoTipoOrden IN (" + codTipoProceso +")";
			}
    		
    	}

        System.out.print("TIPO ORDEN "+ idTipoOrden);
        if (!StringUtils.isBlank(estado) && !estado.equals(ALL_ITEMS)) {
          	 if((!StringUtils.isBlank(codTipoProceso) && !codTipoProceso.equals(ALL_ITEMS)) || (!StringUtils.isBlank(idTipoOrden) && !idTipoOrden.equals(ALL_ITEMS))) {
				sqlWhere +=  " and w.idState='" + estado + "'";
          	 }else {
				sqlWhere +=  " w.idState='" + estado + "'";
          	 } 
        }

        
        
        if (!StringUtils.isBlank(numOrden)) {
          	
        	if((!StringUtils.isBlank(codTipoProceso) && !codTipoProceso.equals(ALL_ITEMS)) || (!StringUtils.isBlank(idTipoOrden) && !idTipoOrden.equals(ALL_ITEMS)) || (!StringUtils.isBlank(estado) && !estado.equals(ALL_ITEMS))) {
				sqlWhere +=  " and ord.nroOrden='" + numOrden + "'";
        	}else {
				sqlWhere +=  " ord.nroOrden='" + numOrden + "'";
        	}
        } 
        
        if (idServicio != null && idServicio != 0) {
          	
        	if((!StringUtils.isBlank(codTipoProceso) && !codTipoProceso.equals(ALL_ITEMS)) || (!StringUtils.isBlank(idTipoOrden) && !idTipoOrden.equals(ALL_ITEMS)) || (!StringUtils.isBlank(estado) && !estado.equals(ALL_ITEMS)) || !StringUtils.isBlank(numOrden)) {
				sqlWhere +=  " and c.nroServicio='" + idServicio + "'";
        	}else {
				sqlWhere +=  " c.nroServicio='" + idServicio + "'";
        	}
        } 
        
        if (!StringUtils.isBlank(nroCuenta)) {
          	
        	if((!StringUtils.isBlank(codTipoProceso) && !codTipoProceso.equals(ALL_ITEMS)) || (!StringUtils.isBlank(idTipoOrden) && !idTipoOrden.equals(ALL_ITEMS)) || (!StringUtils.isBlank(estado) && !estado.equals(ALL_ITEMS)) || !StringUtils.isBlank(numOrden) || (idServicio != null && idServicio != 0)) {
				sqlWhere +=  " and c.nroCuenta='" + nroCuenta + "'";
        	}else {
				sqlWhere +=  " c.nroCuenta='" + nroCuenta + "'";
        	}                	
        } 
        
        if ((!StringUtils.isBlank(fechaInicio)) || (!StringUtils.isBlank(fechaFin))) {
        	try {
        		SimpleDateFormat sdf=new SimpleDateFormat(FORMAT_DATE);
                Date currentdateFehaInicio = sdf.parse(fechaInicio);
                SimpleDateFormat sdf2=new SimpleDateFormat(FORMAT_DATE);
                String fechaInicioConvertido = sdf2.format(currentdateFehaInicio);
                
                SimpleDateFormat sdf4=new SimpleDateFormat(FORMAT_DATE);
                Date currentdateFehaFin=sdf4.parse(fechaFin);
                SimpleDateFormat sdf3=new SimpleDateFormat(FORMAT_DATE);
                String fechaFinConvertido = sdf3.format(currentdateFehaFin);
                
                if((!StringUtils.isBlank(codTipoProceso) && !codTipoProceso.equals(ALL_ITEMS)) || (!StringUtils.isBlank(idTipoOrden) && !idTipoOrden.equals(ALL_ITEMS)) || (!StringUtils.isBlank(estado) && !estado.equals(ALL_ITEMS)) || !StringUtils.isBlank(numOrden) || !StringUtils.isBlank(nroCuenta) || (idServicio != null && idServicio != 0)) {
            		sqlWhere +=  " and DATE_TRUNC('day',ord.fechaCreacion) between "+ "'"+fechaInicioConvertido +"'"+" and "+"'"+ fechaFinConvertido+"'";
            	}else {
            		sqlWhere +=  " DATE_TRUNC('day',ord.fechaCreacion) between "+ "'"+fechaInicioConvertido +"'"+" and "+"'"+ fechaFinConvertido+"'";
            	}
            	
        	}catch (Exception e) {
				e.printStackTrace();
			}	
             
        }
        
        try {

			int offset = paging.getPageNumber() * paging.getPageSize();
			int limit = paging.getPageSize();
			log.info("/offset: {}", offset);
			log.info("/limit: {}", limit);

			String sqlCantidad = "select count(1) ".concat(sqlFrom).concat(sqlWhere);
			log.info("/consulta cantidad: {}", sqlCantidad);
			log.info("/ejecucion consulta cuenta memory ini {}", Funciones.logMemoryJVM());
			// Descomentar para PROD
			//Long cantidadTotalRegistros = (Long) entityManager.createQuery(sqlCantidad).getSingleResult();
			Long cantidadTotalRegistros = 200000L;
			log.info("/ejecucion consulta cuenta memory ini {}", Funciones.logMemoryJVM());
			log.info("/cantidad total de registros: {}", cantidadTotalRegistros);

			String sqlOrder =" order by ord.fechaCreacion desc ";

			String sqlDatos = sqlSelect.concat(sqlFrom).concat(sqlWhere).concat(sqlOrder);
			log.info("/consulta datos: {}", sqlDatos);

			log.info("/ejecucion consulta memory ini {}", Funciones.logMemoryJVM());
			List<OrdenTrabajoSCOMPaginacionResponseDTO> result = entityManager.createQuery(sqlDatos, OrdenTrabajoSCOMPaginacionResponseDTO.class).setFirstResult(offset).setMaxResults(limit).getResultList();
			log.info("/ejecucion consulta memory ini {}", Funciones.logMemoryJVM());

			// R.I. REQSCOM06 06/10/2023 INICIO
			//ClienteEntity clienteEntity;
			ObservacionDTO observacion;
			XmlDTO xml;
			TipoOrden tipoOrden;
	        for (OrdenTrabajoSCOMPaginacionResponseDTO orden : result) {
	        	//clienteEntity = clienteRepository.findByNroServicio(orden.getNroServicio());
	        	//orden.setDistrito(clienteEntity.getDistrito());
	        	//orden.setNroCuenta(clienteEntity.getNroCuenta());
	        	tipoOrden = TipoOrden.fromIdTipoOrden(orden.getIdTipoOrden());
	        	switch (tipoOrden) {
	            case ORINSP:
	            	try {
	        			observacion = ordenRepository.obtenerObservacionTecnicoINSP(orden.getId());
	        	        orden.setObservacionTecnico(observacion.getTexto());
	        	        orden.setFechaEjecucion(observacion.getFechaObservacion());
	        		} catch (Exception e) {
	        			//log.error("Error al obtener fecha y observación: {}", e.getMessage());
	        		}
	                break;
	            case NORM:
	            	try {
	        			observacion = ordenRepository.obtenerObservacionTecnicoNORM(orden.getId());
	        	        orden.setObservacionTecnico(observacion.getTexto());
	        	        orden.setFechaEjecucion(observacion.getFechaObservacion());
	        		} catch (Exception e) {
	        			//log.error("Error al obtener fecha y observación: {}", e.getMessage());
	        		}
	                break;
	            case CONT:
	            	xml = transferRepository.obtenerXmlOrdenTrabajo(orden.getNroOrden(), 22L);
	                if (xml != null) {
	              	  if (xml.getObservacion() != null) {
	                  	  orden.setObservacionTecnico(xml.getObservacion());
	                    } else {
	                  	  orden.setObservacionTecnico("");
	                    }
	                    orden.setFechaEjecucion(xml.getFechaEjecucion());
	                }
	                break;
	            case MANT:
	            	try {
	        			observacion = ordenRepository.obtenerObservacionTecnicoMANT(orden.getId());
	        	        orden.setObservacionTecnico(observacion.getTexto());
	        	        orden.setFechaEjecucion(observacion.getFechaObservacion());
	        		} catch (Exception e) {
	        			//log.error("Error al obtener fecha y observación: {}", e.getMessage());
	        		}
	                break;
	            default:
	            	orden.setObservacionTecnico("");
        	        orden.setFechaEjecucion("");
	                break;
	        	}
            }
              // R.I. REQSCOM06 06/10/2023 FIN

			result.stream().forEach(k -> {
				if(k.getIdMotivo() != null) {
					if(k.getIdMotivo().equals(Long.parseLong("1240"))){
						k.setCodTipoOrden(COD_ORDEN_PLAN_MANT);
					}
				}
			});
          	
			List<OrdenTrabajoSCOMPaginacionResponseDTO> lista = (List<OrdenTrabajoSCOMPaginacionResponseDTO>) result.stream().collect(Collectors.toList());

			PageImpl<OrdenTrabajoSCOMPaginacionResponseDTO> objfinal = new PageImpl<OrdenTrabajoSCOMPaginacionResponseDTO>(lista, paging, cantidadTotalRegistros);
			
			Date fin = new Date();
			log.info("/+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
			log.info("/cantidad registros : {}", cantidadTotalRegistros);
			log.info("/ini: {}", ini);
			log.info("/fin: {}", fin);

			return objfinal;
			  
        } catch (Exception e) {
        	e.printStackTrace();
        	throw new AllNotFoundException("Error al consultar las ordenes"); 
		} finally {
			entityManager.close();
		}
      
	}
	
}
