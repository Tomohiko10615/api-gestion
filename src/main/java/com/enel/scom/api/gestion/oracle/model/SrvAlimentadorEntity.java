package com.enel.scom.api.gestion.oracle.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;

@Entity(name = "srv_alimentador")
@Data
public class SrvAlimentadorEntity implements Serializable {/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
    @Column(name="id_alimentador")
    private Long idAlimentador;
	
	@Column(name="id_set")
    private Long idSet;
	
    @Column(name = "cod_alimentador")
    private String codAlimentador;
    
    @Column(name = "des_alimentador")
    private String desAlimentador;
    
    @Column(name = "activo")
    private String activo;
	
}
