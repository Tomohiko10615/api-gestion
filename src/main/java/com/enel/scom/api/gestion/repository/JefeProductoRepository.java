package com.enel.scom.api.gestion.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.model.JefeProductoEntity;

@Repository
public interface JefeProductoRepository extends JpaRepository<JefeProductoEntity, Long>{
	@Query(value = "SELECT djp.id, np.nombre, np.apellido_pat, np.apellido_mat FROM dis_jefe_producto djp INNER JOIN nuc_persona np ON np.id = djp.id_persona WHERE djp.activo = 'S' ORDER BY np.nombre ASC", nativeQuery = true)
	List<Object[]> getActivos();
	
	@Query(value = "SELECT * FROM dis_jefe_producto WHERE id = :id", nativeQuery = true)
	JefeProductoEntity findId(@Param("id") Long id);
}
