package com.enel.scom.api.gestion.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import lombok.Data;

@Entity(name = "med_tip_modelo")
@Data
public class MetTipoModeloEntity implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@OneToOne
	@JoinColumn(name = "id_marca")
	private MarcaEntity marca;
	
	@Column(name = "cod_tip_modelo")
	private String modelo;
	
	@Column(name = "des_tip_modelo")
	private String descripcion;
	
	@Column(name = "activo", length = 1)
	private String activo;
}
