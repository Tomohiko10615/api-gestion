package com.enel.scom.api.gestion.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.enel.scom.api.gestion.dto.response.ReporteAsignacionPCRClientePaginacionResponseDTO;
import com.enel.scom.api.gestion.dto.response.ReporteGraficoDTO;

public interface ReporteAsignacionPCRClienteService {
	
	Page<ReporteAsignacionPCRClientePaginacionResponseDTO> obtenerReporteAsignacionPCRClientePaginacion(Pageable paging, String fechaInicio, String fechaFin,  String situacionPcr, String nroCliente);
	
	List<ReporteGraficoDTO> obtenerReporteAsignacionPCRClienteCantidadXsituacion(String fechaInicio, String fechaFin, String situacion, String nroCuenta, String tipoAsignacion);

}
