package com.enel.scom.api.gestion.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.model.FactorEntity;
import com.enel.scom.api.gestion.model.MedFacMedModEntity;
import com.enel.scom.api.gestion.model.MedidaModeloEntity;

@Repository
public interface MedFacMedModRepository extends JpaRepository<MedFacMedModEntity, Long>{
    
	@Modifying
	@Transactional
	@Query(value = "delete from med_fac_med_mod "
					+ "WHERE id_medida_modelo =:idMedidaModelo "
					+ "and id_factor =:idFactor", nativeQuery = true)
	public void deleteFactor(@Param("idMedidaModelo") Long idMedidaModelo,
			@Param("idFactor") Long idFactor);
	
	@Modifying
	@Transactional
	@Query(value="INSERT INTO med_fac_med_mod (id_medida_modelo, id_factor) VALUES (:idMedidaModelo, :idFactor)", nativeQuery = true)
	public void createFactor(@Param("idMedidaModelo") Long idMedidaModelo,
				@Param("idFactor") Long idFactor);
	
	
	@Query(value = "select mf.id, mf.des_factor,mf.cod_factor, mf.val_factor from schscom.med_fac_med_mod medmod "
			+ "inner join schscom.med_factor mf on medmod.id_factor = mf.id "
			+ "where medmod.id_medida_modelo =:idMedidaModelo", nativeQuery = true)
	List<Object[]> getFactoresPorMedidaModelo(@Param("idMedidaModelo") Long idMedidaModelo);
	
	
	@Query(value = "SELECT * FROM med_fac_med_mod WHERE id_medida_modelo = :idMedidaModelo", nativeQuery = true)
	MedFacMedModEntity findIdMedidaModelo(@Param("idMedidaModelo") Long idMedidaModelo);
	
}
