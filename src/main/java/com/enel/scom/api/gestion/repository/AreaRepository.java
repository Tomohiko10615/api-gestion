package com.enel.scom.api.gestion.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.model.AreaEntity;

@Repository
public interface AreaRepository extends JpaRepository<AreaEntity, Long>{
    @Query(value = "SELECT id, descripcion FROM seg_area", nativeQuery = true)
    List<Object[]> getActivos();
}
