package com.enel.scom.api.gestion.model;
import javax.persistence.*;

import lombok.Data;

@Entity(name = "ord_ord_deriv")
@Data
public class OrdenDerivaEntity {
    
    @Id
    @Column(name = "id_orden")
    private Long orden;

	@Column(name = "id_responsable")
    private Long idResponsable;
    
	@Column(name = "id_ult_responsable")
    private Long idUltResponsable;
}
