package com.enel.scom.api.gestion.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.enel.scom.api.gestion.dto.response.UbicacionResponseDto;
import com.enel.scom.api.gestion.service.UbicacionService;

@RestController
@RequestMapping("/api/ubicacion")
public class UbicacionController {

	@Autowired
	UbicacionService ubicacionService;
	
	@GetMapping
	public ResponseEntity<List<UbicacionResponseDto>> getAll() {
		
		List<UbicacionResponseDto> ubicaciones = ubicacionService.getAll();
		
		return new ResponseEntity<>(ubicaciones, HttpStatus.OK);
	}
}
