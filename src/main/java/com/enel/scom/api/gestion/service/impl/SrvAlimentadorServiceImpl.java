package com.enel.scom.api.gestion.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.scom.api.gestion.dto.response.AutoCompleteResponseDTO;
import com.enel.scom.api.gestion.oracle.repository.SrvAlimentadorRepository;
import com.enel.scom.api.gestion.service.SrvAlimentadorService;

@Service
public class SrvAlimentadorServiceImpl implements SrvAlimentadorService {
	
	@Autowired
	private SrvAlimentadorRepository srvAlimentadorRepository;
	
	@Override
	public List<AutoCompleteResponseDTO> buscarAlimentadorServicioElectrico(String codAlimentador, Long idSet) {
		List<Object[]> alimentadorCadenaElectrica = srvAlimentadorRepository.buscarAlimentadorServicioElectrico(codAlimentador, idSet);
		return alimentadorCadenaElectrica.stream().map(object -> new AutoCompleteResponseDTO(
				Long.valueOf(object[0].toString()), 
				object[1].toString().trim()
				)).collect(Collectors.toList());
	}

}
