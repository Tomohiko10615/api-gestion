package com.enel.scom.api.gestion.dto.response;

import lombok.Data;

@Data
public class FactorResponseDTO {
	public FactorResponseDTO() {
	}
	public FactorResponseDTO(Long id, String descripcion, String codFactor, String valFactor) {
		this.id = id;
		this.desFactor = descripcion;
		this.codFactor = codFactor;
		this.valFactor = valFactor;
	}
	
	private Long id;
	private String desFactor;
	private String codFactor;
	private String valFactor;
}
