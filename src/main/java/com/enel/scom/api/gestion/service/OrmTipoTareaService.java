package com.enel.scom.api.gestion.service;

import java.util.List;

import com.enel.scom.api.gestion.model.OrmTipoTarea;

public interface OrmTipoTareaService {
    List<OrmTipoTarea> getTipos();
}
