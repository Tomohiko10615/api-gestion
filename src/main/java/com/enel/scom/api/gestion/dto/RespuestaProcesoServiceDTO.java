package com.enel.scom.api.gestion.dto;

import com.enel.scom.api.gestion.enums.EstadosRespuestaService;

import lombok.Data;

@Data
public class RespuestaProcesoServiceDTO {

	public EstadosRespuestaService estado;
	public String mensaje;
}
