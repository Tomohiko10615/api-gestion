package com.enel.scom.api.gestion.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.enel.scom.api.gestion.dto.response.ResponsableResponseDTO;
import com.enel.scom.api.gestion.service.ResponsableService;

@RestController
@RequestMapping("/api/responsable")
public class ResponsableController {
    
    @Autowired
    ResponsableService responsableService;

    @GetMapping("/{responsable}")
    public ResponseEntity<List<ResponsableResponseDTO>> getResponsable(@PathVariable String responsable) {
        List<ResponsableResponseDTO> responses = responsableService.getResponsable(responsable);
        return new ResponseEntity<>(responses, HttpStatus.OK);
    }
}
