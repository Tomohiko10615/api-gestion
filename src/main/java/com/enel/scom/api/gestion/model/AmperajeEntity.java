package com.enel.scom.api.gestion.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Entity(name = "com_amperaje")
@Data
public class AmperajeEntity implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "cod_amperaje")
	private String codAmperaje;
	
	@Column(name = "des_amperaje")
	private String desAmperaje;
	
	@Column(name = "val_amperaje")
	private String valAmperaje;
	
	@Column(length = 1)
	private String activo;

}
