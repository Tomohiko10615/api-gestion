package com.enel.scom.api.gestion.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ClienteDTO {

	private String nroSuministro;
	private String distrito;
	
}
