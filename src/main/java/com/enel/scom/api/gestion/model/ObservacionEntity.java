package com.enel.scom.api.gestion.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

import lombok.Data;

@Entity(name = "ord_observacion")
@Data
public class ObservacionEntity implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id_observacion")
	private Long idObservacion;
	
	// R.I. REQSCOM06 Se agrega el nombre de la columna 09/10/2023
	@Column(name = "texto")
	private String texto;
	
	@Column(name = "id_orden")
	private Long idOrden;
	
	@Column(name = "fecha_observacion")
	private Date fechaObservacion;
	
	@OneToOne
	@JoinColumn(name = "id_usuario")
	private UsuarioEntity usuario;

	@Column(name = "state_name")
    private String stateName;
	
	@Column(name = "discriminator")
    private String discriminator;
}
