package com.enel.scom.api.gestion.dto;

public interface XmlCargasDTO {
	String getPruebas_Carga_en_el_primario();
	String getFase();
}
