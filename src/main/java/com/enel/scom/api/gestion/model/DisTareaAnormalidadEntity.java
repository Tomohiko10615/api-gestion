package com.enel.scom.api.gestion.model;

import java.io.Serializable;

import javax.persistence.*;

import lombok.Data;

@Entity(name = "dis_tarea_anor")
@Data
public class DisTareaAnormalidadEntity implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@OneToOne
	@JoinColumn(name = "id_tarea")
	private TareaEntity tarea;
	
	@Id
	@Column(name = "id_anormalidad")
	private AnormalidadEntity anormalidad;
}
