package com.enel.scom.api.gestion.configuration;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.enel.scom.api.gestion.constant.Constant;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "com.enel.scom.api.gestion.oracle.repository", 
entityManagerFactoryRef = Constant.ENTITY_MANAGER_FACTORY_ORACLE, 
transactionManagerRef = Constant.TRANSACTION_MANAGER_ORACLE)
public class ScomDataSourceJPAOracleConfiguration {
	
	/*
	@Bean(name = Constant.PROPERTIES_ORACLE)
	@ConfigurationProperties("spring.jpa.properties.oracle.datasource")
	public DataSourceProperties scomOracleDataSourceProperties() {
		return new DataSourceProperties();
	}
*/
	/*
	@Bean(name = Constant.DATASOURCE_ORACLE)
	public DataSource scomOracleDataSource(
			@Qualifier(Constant.PROPERTIES_ORACLE) DataSourceProperties scomOracleDataSourceProperties) {
		return scomOracleDataSourceProperties.initializeDataSourceBuilder().type(HikariDataSource.class).build();
	}*/

	@Bean(name = Constant.DATASOURCE_ORACLE)
	public DataSource scomOracleDataSource() {
			BeanDataSource beanConn = DataSourceConfig.getConnectionDatos("ORCL");

			HikariConfig dataSourceBuilder = new HikariConfig();
			dataSourceBuilder.setDriverClassName(beanConn.getDbDriver());
			dataSourceBuilder.setJdbcUrl(beanConn.getDbURL());
			dataSourceBuilder.setUsername(beanConn.getDbUser());
			dataSourceBuilder.setPassword(beanConn.getDbPass());
			dataSourceBuilder.setMaximumPoolSize(beanConn.getDbMax());
			dataSourceBuilder.setMinimumIdle(beanConn.getDbMin());
			HikariDataSource dataSource = new HikariDataSource(dataSourceBuilder);
			
			return dataSource;
	}

	
	@Bean(name = Constant.ENTITY_MANAGER_FACTORY_ORACLE)
	public LocalContainerEntityManagerFactoryBean scomOracleEntityManagerFactory(
			EntityManagerFactoryBuilder scomOracleEntityManagerFactoryBuilder,
			@Qualifier(Constant.DATASOURCE_ORACLE) DataSource scomOracleDataSource) {

		Map<String, String> scomOracleJpaProperties = new HashMap<>();
		scomOracleJpaProperties.put("hibernate.dialect", "org.hibernate.dialect.Oracle10gDialect");

		return scomOracleEntityManagerFactoryBuilder.dataSource(scomOracleDataSource)
				.packages("com.enel.scom.api.gestion.oracle.model").persistenceUnit(Constant.DATASOURCE_ORACLE)
				.properties(scomOracleJpaProperties).build();
	}

	@Bean(name = Constant.TRANSACTION_MANAGER_ORACLE)
	public PlatformTransactionManager scomOracleTransactionManager(
			@Qualifier(Constant.ENTITY_MANAGER_FACTORY_ORACLE) EntityManagerFactory scomOracleEntityManagerFactory) {

		return new JpaTransactionManager(scomOracleEntityManagerFactory);
	}

}

