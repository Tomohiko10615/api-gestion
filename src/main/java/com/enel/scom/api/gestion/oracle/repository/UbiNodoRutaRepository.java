package com.enel.scom.api.gestion.oracle.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.oracle.model.UbiNodoRutaEntity;

@Repository
public interface UbiNodoRutaRepository extends JpaRepository<UbiNodoRutaEntity, Long> {
	
	/**
	 * Query para realizar autocomplete en ZONA o SECTOR RUTA
	 * @param keyword
	 * @return
	 */
	
	@Query(value="SELECT ubi.id_nodo, ubi.cod_nodo FROM ubi_nodo_ruta ubi where ubi.id_nivel=1 and ubi.id_tipo_ruta=:idTipoRuta and ubi.cod_nodo like %:codNodo%", nativeQuery = true)
	public List<Object[]> buscarSector(@Param("codNodo") String codNodo, @Param("idTipoRuta") Long idTipoRuta);
	
	@Query(value="SELECT ubi.id_nodo, ubi.cod_nodo FROM ubi_nodo_ruta ubi where ubi.id_nivel=2 and ubi.id_tipo_ruta=:idTipoRuta and ubi.id_nodo_padre=:idNodoPadre and ubi.cod_nodo like %:codNodo%", nativeQuery = true)
	public List<Object[]> buscarZonaPorSector(@Param("codNodo") String codNodo,  @Param("idTipoRuta") Long idTipoRuta,  @Param("idNodoPadre") Long idNodoPadre);
	
	@Query(value="SELECT * FROM ubi_nodo_ruta ubi where ubi.id_nodo=:id", nativeQuery = true)
	public UbiNodoRutaEntity obtenerUbiNodoServicioElectricoPorId(@Param("id") Long id);
	
	@Query(value="SELECT * FROM ubi_nodo_ruta ubi where ubi.id_nivel=1 and ubi.id_tipo_ruta=1 and ubi.cod_nodo=:codNodo", nativeQuery = true)
	public UbiNodoRutaEntity obtenerRutaSectorPorCod(@Param("codNodo") String codNodo);
	
	@Query(value="SELECT * FROM ubi_nodo_ruta ubi where ubi.id_nivel=2 and ubi.id_tipo_ruta=1 and ubi.id_nodo_padre=:idNodoPadre and ubi.cod_nodo=:codNodo", nativeQuery = true)
	public UbiNodoRutaEntity obtenerRutaZonaPorCod(@Param("codNodo") String codNodo, @Param("idNodoPadre") Long idNodoPadre);
}
