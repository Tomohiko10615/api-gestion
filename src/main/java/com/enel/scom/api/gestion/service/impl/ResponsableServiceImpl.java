package com.enel.scom.api.gestion.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.scom.api.gestion.dto.response.ResponsableResponseDTO;
import com.enel.scom.api.gestion.repository.AreaRepository;
import com.enel.scom.api.gestion.repository.ResponsableRepository;
import com.enel.scom.api.gestion.repository.SegRolRepository;
import com.enel.scom.api.gestion.repository.UsuarioRepository;
import com.enel.scom.api.gestion.service.ResponsableService;

@Service
public class ResponsableServiceImpl implements ResponsableService{

    @Autowired
    UsuarioRepository usuarioRepository;

    @Autowired
    AreaRepository areaRepository;

    @Autowired
    SegRolRepository segRolRepository;

    @Autowired
    ResponsableRepository responsableRepository;

    @Override
    public List<ResponsableResponseDTO> getResponsable(String responsable) {
        List<Object[]> usuarios = responsableRepository.getUsuarios();
        List<Object[]> areas = responsableRepository.getAreas();
        List<Object[]> roles = responsableRepository.getRoles();
        List<ResponsableResponseDTO> dtos = new ArrayList<>();

        if(responsable.equals("Usuario")) {
            usuarios.stream().forEach(k -> {
                ResponsableResponseDTO dto = new ResponsableResponseDTO();
                dto.setId(Long.valueOf(k[0].toString()));
                dto.setDescripcion(k[1].toString());
                dtos.add(dto);
            });
        }

        if(responsable.equals("Area")) {
            areas.stream().forEach(k -> {
                ResponsableResponseDTO dto = new ResponsableResponseDTO();
                dto.setId(Long.valueOf(k[0].toString()));
                dto.setDescripcion(k[1].toString());
                dtos.add(dto);
            });
        }
        
        if(responsable.equals("Rol")) {
            roles.stream().forEach(k -> {
                ResponsableResponseDTO dto = new ResponsableResponseDTO();
                dto.setId(Long.valueOf(k[0].toString()));
                dto.setDescripcion(k[1].toString());
                dtos.add(dto);
            });
        }

        return dtos;
    }
    
}
