package com.enel.scom.api.gestion.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.enel.scom.api.gestion.dto.response.ReporteGraficoDTO;
import com.enel.scom.api.gestion.dto.response.ReporteOrdenTrabajoPaginacionDTO;
import com.enel.scom.api.gestion.exception.AllNotFoundException;
import com.enel.scom.api.gestion.service.ReporteOrdenTrabajoService;
import com.enel.scom.api.gestion.util.Funciones;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ReporteOrdenTrabajoServiceImpl implements ReporteOrdenTrabajoService {
	
	@PersistenceContext
	EntityManager entityManager;

	@Override
	public Page<ReporteOrdenTrabajoPaginacionDTO> obtenerReporteOrdenesTrabajoPaginacion(Pageable paging, String fechaInicio,
			String fechaFin, String situacion, String idTipoProceso, String nroCuenta, String idTipoOrden) {
		Date inicio = new Date();
		log.info("/=================================================================================");
		log.info("/clase: {}", "ReporteOrdenTrabajoServiceImpl");
		log.info("/metodo: {}", "obtenerReporteOrdenesTrabajoPaginacion");
		log.info("/paging: {}", paging);
		log.info("/fechaInicio: {}", fechaInicio);
		log.info("/fechaFin: {}", fechaFin);
		log.info("/situacion: {}", situacion);
		log.info("/idTipoProceso: {}", idTipoProceso);
		log.info("/nroCuenta: {}", nroCuenta);
		log.info("/idTipoOrden: {}", idTipoOrden);
		log.info("/marca 1");

		
		String sqlSelect = ""
				+ "select new com.enel.scom.api.gestion.dto.response.ReporteOrdenTrabajoPaginacionDTO( "
				+ "trans.nroCuenta, " 
				+ "(clie.idServicio) as idServicio, "
				+ "trans.fechaCreacion, "
				+ "CASE "
				+ "WHEN trans.codTipoOrdenLegacy in ('ORINSP', 'NORM', 'CONT') "
				+ "then 'PERDIDA'"
				+ "WHEN trans.codTipoOrdenLegacy in ('CORT', 'VERI', 'REPO', 'DESM', 'COB') "
				+ "then 'IMPAGO' "
				+ "WHEN trans.codTipoOrdenLegacy in ('MANT') "
				+ "then 'MANTENIMIENTO'"
				+ "WHEN trans.codTipoOrdenLegacy in ('OCNX') "
				+ "then 'CONEXIONES' "
				+ "ELSE '' END, "
				+ "trans.codTipoOrdenLegacy, "
				+ "par.descripcion, "
				+ "CASE "
				+ "WHEN trans.codTipoOrdenLegacy='OCNX' and trans.suspendida='S' "
				+ "then 'Paralizada'"
				+ "WHEN trans.codEstadoOrden in ('1', '2', '3', '5', '11', '12', '14', '15') "
				+ "then 'Pendiente'"
				+ "WHEN trans.codEstadoOrden in ('13') "
				+ "then 'Anulada' "
				+ "WHEN trans.codEstadoOrden in ('6', '8') "
				+ "then 'Ejecutada' "
				+ "WHEN trans.codEstadoOrden in ('4', '7', '9', '16', '17') "
				+ "then 'Con error' "
				+ "ELSE '' END, "
				+ "case when ord.motivo.id=1240 then ord.motivo.id else 0 end as motivo"
				+ ") "
				;
		

		String sqlFrom = ""	
				+ "from eor_ord_transfer trans "
				+ "inner JOIN com_parametros par ON trans.codEstadoOrden = par.valorNum "
    			+ "AND par.entidad='ESTADO_TRANSFERENCIA' AND par.sistema='EORDER' "
    			+ "LEFT JOIN ord_orden ord ON trans.nroOrdenLegacy = ord.nroOrden "
    			+ "left join cliente clie on clie.nroCuenta= trans.nroCuenta"
				;

		log.info("/marca 2");
		
		String sqlWhere = "";
		if ((!StringUtils.isBlank(fechaInicio)) || 
				(!StringUtils.isBlank(fechaFin)) || 
				(!StringUtils.isBlank(situacion)) || 
				(!StringUtils.isBlank(idTipoProceso)) || 
				!StringUtils.isBlank(nroCuenta) || 
				(!StringUtils.isBlank(idTipoOrden))) {
				
				sqlWhere +=" where";
	        }
			log.info("/marca 3");
		if(!StringUtils.isBlank(idTipoProceso)) {
			if(!StringUtils.isBlank(idTipoOrden)) {
				
				 switch (idTipoOrden) {
 				case "PMANT":
 					sqlWhere +=  " ord.motivo.id=1240 and";
     				idTipoOrden = "MANT";
 					break;
 				case "RMANT":
 					sqlWhere +=  " (ord.motivo.id is null or ord.motivo.id <>1240) and trans.codTipoOrdenEorder in ('NCX.03') and";
     				idTipoOrden = "MANT";
 					break;
 				case "MANT":
 					sqlWhere +=  " trans.codTipoOrdenEorder in ('NCX.05') and";
 					break;
 				case "NOCNX":
 					sqlWhere +=  " trans.codTipoOrdenEorder in ('NCX.01') and";
     				idTipoOrden = "OCNX";
 					break;
 				case "OCNX":
 					sqlWhere +=  " trans.codTipoOrdenEorder in ('NCX.02') and";
 					break;
 				default:		
 				}
				 
				sqlWhere += " trans.codTipoOrdenLegacy in ('"+ idTipoOrden +"')";
			} else {
				sqlWhere += " trans.codTipoOrdenLegacy in ("+ idTipoProceso +")";
			}
			
		}
		log.info("/marca 4");  
		 if (!StringUtils.isBlank(situacion)) {

			 if((!StringUtils.isBlank(idTipoProceso)) || (!StringUtils.isBlank(idTipoOrden))) {
				 sqlWhere += " and" + encontrarSituacion(situacion);
			 } else {
				 sqlWhere += encontrarSituacion(situacion);
			 }
        }
		log.info("/marca 5");
		  if (!StringUtils.isBlank(nroCuenta)) {
	        	if((!StringUtils.isBlank(idTipoProceso)) || (!StringUtils.isBlank(idTipoOrden)) || (!StringUtils.isBlank(situacion))) {
	        		sqlWhere +=  " and trans.nroCuenta='" + Long.parseLong(nroCuenta) + "'";
	        	}
	        	else {
	        		sqlWhere +=  " trans.nroCuenta='" + Long.parseLong(nroCuenta) + "'";
	        	}
	        }
		 
			log.info("/marca 6");    
        if ((!StringUtils.isBlank(fechaInicio)) || (!StringUtils.isBlank(fechaFin))) {
        	try {
        		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
                Date currentdateFehaInicio = sdf.parse(fechaInicio);
                SimpleDateFormat sdf2=new SimpleDateFormat("yyyy-MM-dd");
                String fechaInicioConvertido = sdf2.format(currentdateFehaInicio);
                
                SimpleDateFormat sdf4=new SimpleDateFormat("yyyy-MM-dd");
                Date currentdateFehaFin=sdf4.parse(fechaFin);
                SimpleDateFormat sdf3=new SimpleDateFormat("yyyy-MM-dd");
                String fechaFinConvertido = sdf3.format(currentdateFehaFin);
                
            	if((!StringUtils.isBlank(situacion)) || (!StringUtils.isBlank(idTipoProceso)) || (!StringUtils.isBlank(idTipoOrden)) || !StringUtils.isBlank(nroCuenta)) {
            		sqlWhere +=  " and DATE_TRUNC('day',trans.fechaCreacion) between "+ "'"+fechaInicioConvertido +"'"+" and "+"'"+ fechaFinConvertido+"'";
            	}else {
            		sqlWhere +=  " DATE_TRUNC('day',trans.fechaCreacion) between "+ "'"+fechaInicioConvertido +"'"+" and "+"'"+ fechaFinConvertido+"'";
            	}
            	
        	}catch (Exception e) {
				e.printStackTrace();
			}	
             
        }
	    log.info("/marca 7");    
        try {

			int offset = paging.getPageNumber() * paging.getPageSize();
			int limit = paging.getPageSize();
			log.info("/offset: {}", offset);
			log.info("/limit: {}", limit);

			log.info("/marca 8");    

			String sqlCantidad = "select count(1) ".concat(sqlFrom).concat(sqlWhere);
			log.info("/consulta cantidad: {}", sqlCantidad);
			Long cantidadTotalRegistros = (Long) entityManager.createQuery(sqlCantidad).getSingleResult();
			log.info("/cantidad total de registros: {}", cantidadTotalRegistros);

			
			
			String sqlOrder =" order by trans.fechaCreacion desc";
			String sqlDatos = sqlSelect.concat(sqlFrom).concat(sqlWhere).concat(sqlOrder);
			log.info("/consulta datos: {}", sqlDatos);



			log.info("/marca 9");    
			
        	

			log.info("/ejecucion consulta ini {}", Funciones.logMemoryJVM());
			log.info("/marca 10");
			List<ReporteOrdenTrabajoPaginacionDTO> dto = entityManager.createQuery(sqlDatos, ReporteOrdenTrabajoPaginacionDTO.class).setFirstResult(offset).setMaxResults(limit).getResultList();



			log.info("/marca 11");
			log.info("/ejecucion consulta fin {}", Funciones.logMemoryJVM());
			log.info("/cantidad de registros {}", dto.size());
    		

        	dto.stream().forEach(k -> {
          		if(k.getIdMotivo() != null) {
          			if(k.getIdMotivo().equals(Long.parseLong("1240"))){
              			k.setTipoOrden("PMANT");
              		}
          		}
          		
          	});
			log.info("/marca 12");
        	
    		List<ReporteOrdenTrabajoPaginacionDTO> list = dto.stream().collect(Collectors.toList());
			log.info("/marca 13");

    		int pageOffset = (int) paging.getOffset();
			log.info("/pageOffset: {}", pageOffset);
			log.info("/paging.getPageSize(): {}", paging.getPageSize());
			log.info("/list.size(): {}", list.size());

    		int total = (pageOffset + paging.getPageSize()) > list.size() ? list.size() : (pageOffset + paging.getPageSize());
			log.info("/total: {}", total);
			log.info("/marca 14");

			PageImpl<ReporteOrdenTrabajoPaginacionDTO> objfinal =  new PageImpl<>(list, paging, cantidadTotalRegistros);
			log.info("/marca 15");

			Date fin = new Date();

			log.info("/+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
			log.info("/cantidad registros : {}", dto.size());
			log.info("/ini: {}", inicio);
			log.info("/fin: {}", fin);
    		return objfinal;

        } catch (Exception e) {
        	e.printStackTrace();
        	throw new AllNotFoundException("Error al consultar el reporte"); 
		} finally {
			entityManager.close();
		}
			
	}
	
	
	
	private String encontrarSituacion(String situacion) {
		 String idEstado = "";
		 String sql ="";
		 switch (situacion) {
			case "PENDIENTE":
				idEstado = "in ('1', '2', '3', '5', '11', '12', '14', '15')";
				sql = " trans.codEstadoOrden "+ idEstado +"";
				break;
			case "ANULADA":
				idEstado = "in ('13')";
				sql = " trans.codEstadoOrden "+ idEstado +"";
				break;
			case "PARALIZADA":
				sql = " trans.codTipoOrdenLegacy='OCNX' and trans.suspendida='S'";
				break;
			case "EJECUTADA":
				idEstado = "in ('6', '8')";
				sql = " trans.codEstadoOrden "+ idEstado +"";
				break;
			case "CON_ERROR":
				idEstado = "in ('4', '7', '9', '16', '17')";
				sql = " trans.codEstadoOrden "+ idEstado +"";
				break;
			default:		
			}	
		 
		 return sql;
	}

	@Override
	public List<ReporteGraficoDTO> obtenerReporteOrdenesTrabajoCantidadXsituacion(String fechaInicio, String fechaFin,
			String situacion, String idTipoProceso, String nroCuenta, String idTipoOrden) {
		String sql = "select DISTINCT new com.enel.scom.api.gestion.dto.response.ReporteGraficoDTO( "
				+ "CASE "
				+ "WHEN trans.codTipoOrdenLegacy='OCNX' and trans.suspendida='S' "
				+ "then 'Paralizada'"
				+ "WHEN trans.codEstadoOrden in ('1', '2', '3', '5', '11', '12', '14', '15') "
				+ "then 'Pendiente'"
				+ "WHEN trans.codEstadoOrden in ('13') "
				+ "then 'Anulada' "
				+ "WHEN trans.codEstadoOrden in ('6', '8') "
				+ "then 'Ejecutada' "
				+ "WHEN trans.codEstadoOrden in ('4', '7', '9', '16', '17') "
				+ "then 'Con error' "
				+ "ELSE '' END, "
				+ "count(*)"
				+ ") "
				+ "from eor_ord_transfer trans "
				+ "JOIN com_parametros par ON trans.codEstadoOrden = par.valorNum "
    			+ "AND par.entidad='ESTADO_TRANSFERENCIA' AND par.sistema='EORDER' ";
		if ((!StringUtils.isBlank(fechaInicio)) || 
				(!StringUtils.isBlank(fechaFin)) || 
				(!StringUtils.isBlank(situacion)) || 
				(!StringUtils.isBlank(idTipoProceso)) || 
				!StringUtils.isBlank(nroCuenta) || 
				(!StringUtils.isBlank(idTipoOrden))) {
				
	            sql +=" where";
	        }
		
		if(!StringUtils.isBlank(idTipoProceso)) {
			if(!StringUtils.isBlank(idTipoOrden)) {
				
				 switch (idTipoOrden) {
 				case "PMANT":
 					sql +=  " ord.motivo.id=1240 and";
     				idTipoOrden = "MANT";
 					break;
 				case "RMANT":
 					sql +=  " (ord.motivo.id is null or ord.motivo.id <>1240) and trans.codTipoOrdenEorder in ('NCX.03') and";
     				idTipoOrden = "MANT";
 					break;
 				case "MANT":
 					sql +=  " trans.codTipoOrdenEorder in ('NCX.05') and";
 					break;
 				case "NOCNX":
 					sql +=  " trans.codTipoOrdenEorder in ('NCX.01') and";
     				idTipoOrden = "OCNX";
 					break;
 				case "OCNX":
 					sql +=  " trans.codTipoOrdenEorder in ('NCX.02') and";
 					break;
 				default:		
 				}
				 
				sql += " trans.codTipoOrdenLegacy in ('"+ idTipoOrden +"')";
			} else {
				sql += " trans.codTipoOrdenLegacy in ("+ idTipoProceso +")";
			}
			
		}
	        
		 if (!StringUtils.isBlank(situacion)) {

			 if((!StringUtils.isBlank(idTipoProceso)) || (!StringUtils.isBlank(idTipoOrden))) {
				 sql += " and" + encontrarSituacion(situacion);
			 } else {
				 sql += encontrarSituacion(situacion);
			 }
        }
		 
		  if (!StringUtils.isBlank(nroCuenta)) {
	        	if((!StringUtils.isBlank(idTipoProceso)) || (!StringUtils.isBlank(idTipoOrden)) || (!StringUtils.isBlank(situacion))) {
	        		sql +=  " and trans.nroCuenta='" + Long.parseLong(nroCuenta) + "'";
	        	}
	        	else {
	        		sql +=  " trans.nroCuenta='" + Long.parseLong(nroCuenta) + "'";
	        	}
	        }
		 
	        
        if ((!StringUtils.isBlank(fechaInicio)) || (!StringUtils.isBlank(fechaFin))) {
        	try {
        		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
                Date currentdateFehaInicio = sdf.parse(fechaInicio);
                SimpleDateFormat sdf2=new SimpleDateFormat("yyyy-MM-dd");
                String fechaInicioConvertido = sdf2.format(currentdateFehaInicio);
                
                SimpleDateFormat sdf4=new SimpleDateFormat("yyyy-MM-dd");
                Date currentdateFehaFin=sdf4.parse(fechaFin);
                SimpleDateFormat sdf3=new SimpleDateFormat("yyyy-MM-dd");
                String fechaFinConvertido = sdf3.format(currentdateFehaFin);
                
            	if((!StringUtils.isBlank(situacion)) || (!StringUtils.isBlank(idTipoProceso)) || (!StringUtils.isBlank(idTipoOrden)) || !StringUtils.isBlank(nroCuenta)) {
            		sql +=  " and DATE_TRUNC('day',trans.fechaCreacion) between "+ "'"+fechaInicioConvertido +"'"+" and "+"'"+ fechaFinConvertido+"'";
            	}else {
            		sql +=  " DATE_TRUNC('day',trans.fechaCreacion) between "+ "'"+fechaInicioConvertido +"'"+" and "+"'"+ fechaFinConvertido+"'";
            	}
            	
        	}catch (Exception e) {
				e.printStackTrace();
			}	
             
        }
	        
        try {
        	sql +=" group by  "
        			+ "CASE "
        			+ "WHEN trans.codTipoOrdenLegacy='OCNX' and trans.suspendida='S' "
        			+ "then 'Paralizada' "
        			+ "WHEN trans.codEstadoOrden in ('1', '2', '3', '5', '11', '12', '14', '15') "
        			+ "then 'Pendiente' "
        			+ "WHEN trans.codEstadoOrden in ('13') "
        			+ "then 'Anulada' "
        			+ "WHEN trans.codEstadoOrden in ('6', '8') "
        			+ "then 'Ejecutada' "
        			+ "WHEN trans.codEstadoOrden in ('4', '7', '9', '16', '17') "
        			+ "then 'Con error' "
        			+ "ELSE '' END";
        	
        	return entityManager.createQuery(
    				sql, ReporteGraficoDTO.class).getResultList();
        } catch (Exception e) {
        	e.printStackTrace();
        	throw new AllNotFoundException("Error al consultar el reporte"); 
		} finally {
			entityManager.close();
		}
	}

	



	
}
