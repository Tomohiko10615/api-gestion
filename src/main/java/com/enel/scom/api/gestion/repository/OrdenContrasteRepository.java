package com.enel.scom.api.gestion.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.model.OrdenContrasteEntity;

@Repository
public interface OrdenContrasteRepository extends JpaRepository<OrdenContrasteEntity, Long>{
	
	@Query(value="SELECT nextval('sqordencontrastemedidor')", nativeQuery=true)
	public Long generarNumeroOrden();

	@Query(value = "SELECT * FROM med_ord_come WHERE id_orden = :id", nativeQuery = true)
	OrdenContrasteEntity findId(@Param("id") Long id);
	
	@Query(value = "select \r\n"
			+ "			oo.id as id_orden,\r\n"
			+ "			oo.fec_registro,\r\n"
			+ "			oo.fecha_finalizacion,\r\n"
			+ "			oo.nro_orden,\r\n"
			+ "			omc.des_motivo,\r\n"
			+ "			omc.id as id_motivo,\r\n"
			+ "			u.username,\r\n"
			+ "			c.nro_servicio, c.nro_cuenta, c.nombre, c.apellido_pat, c.apellido_mat,\r\n"
			+ "			c.distrito, c.cod_ruta,\r\n"
			+ "			oob.texto as observacion,\r\n"
			+ "			doi.id_orden as id_orden_inspeccion,\r\n"
			+ "			moc.id_orden as id_orden_contraste,\r\n"
			+ "			c.texto_direccion as direccion, \r\n"
			+ " oo.id_orden_sc4j \r\n"
			+ "from ord_orden oo\r\n"
			+ "left join ord_motivo_creacion omc on omc.id = oo.id_motivo\r\n"
			+ "left join usuario u on u.id = oo.id_usuario_registro\r\n"
			+ "left join med_ord_come moc on oo.id = moc.id_orden\r\n"
			+ "left join dis_ord_insp doi on moc.id_orden_inspeccion = doi.id_orden\r\n"
			+ "left join ord_observacion oob on oob.id_orden = oo.id\r\n"
			+ "left join cliente c on c.id_servicio = oo.id_servicio\r\n"
			+ "where oo.id = :id ORDER BY oob.fecha_observacion DESC LIMIT 1", nativeQuery = true)
	List<Object[]> findIdOrden(@Param("id") Long id);
}
