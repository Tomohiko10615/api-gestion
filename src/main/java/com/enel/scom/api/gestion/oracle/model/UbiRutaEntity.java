package com.enel.scom.api.gestion.oracle.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;

@Entity(name = "ubi_ruta")
@Data
public class UbiRutaEntity implements Serializable {/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
    @Column(name="id_ruta")
    private Long idRuta;
	
    @Column(name="id_empresa")
    private Long idEmpresa;
    
    @Column(name="id_tipo_ruta")
    private Long idTipoRuta;
    
    @Column(name="id_nodo")
    private Long idNodo;
    
    @Column(name="cod_ruta")
    private String codRuta;
    
    @Column(name="id_manzana")
    private Long idManzana;
    
    @Column(name="reasignar_correlativo")
    private String reasignarCorrelativo;
}
