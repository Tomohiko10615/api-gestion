package com.enel.scom.api.gestion.dto.response;

import lombok.Data;

@Data
public class TransferResponseDTO {
	
    public TransferResponseDTO(String codTipoOrdenLegacy, String nroOrdenLegacy, String codTipoOrdenEorder, String nroOrdenEorder, String codEstadoOrden, String nroEnvios,
                                String nroRecepciones, String suspendida, String fecOperacion, String tieneAnexos, String codEstadoAnexo, String nomArchAnexo,
                                String observaciones, String fecEstado, String nroCuenta, String creadaEorder, String fechaCreacion, String generacion, String tipoOrden, String estadoActivacion, 
                                Long idAutoAct) {
        this.codTipoOrdenLegacy = codTipoOrdenLegacy;
        this.nroOrdenLegacy = nroOrdenLegacy;
        this.codTipoOrdenEorder = codTipoOrdenEorder;
        this.nroOrdenEorder = nroOrdenEorder;
        this.codEstadoOrden = codEstadoOrden;
        this.nroEnvios = nroEnvios;
        this.nroRecepciones = nroRecepciones;
        this.suspendida = suspendida;
        this.fecOperacion = fecOperacion;
        this.tieneAnexos = tieneAnexos;
        this.codEstadoAnexo = codEstadoAnexo;
        this.nomArchAnexo = nomArchAnexo;
        this.observaciones = observaciones;
        this.fecEstado = fecEstado;
        this.nroCuenta = nroCuenta;
        this.creadaEorder = creadaEorder;
        this.fechaCreacion = fechaCreacion;
        this.generacion = generacion;
        this.tipoOrden = tipoOrden;
        this.estadoActivacion = estadoActivacion;
        this.idAutoAct = idAutoAct;
    }
    private String codTipoOrdenLegacy;
    private String nroOrdenLegacy;
    private String codTipoOrdenEorder;
    private String codEstadoOrden;
    private String nroOrdenEorder;
    private String nroEnvios;
    private String nroRecepciones;
    private String suspendida;
    private String fecOperacion;
    private String tieneAnexos;
    private String codEstadoAnexo;
    private String nomArchAnexo;
    private String observaciones;
    private String fecEstado;
    private String nroCuenta;
    private String creadaEorder;
    private String fechaCreacion;
    private String generacion;
    private String tipoOrden;
    private String estadoActivacion;
    private Long idAutoAct;
}
