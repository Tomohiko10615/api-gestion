package com.enel.scom.api.gestion.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.model.OrdenNormalizacionEntity;

@Repository
public interface OrdenNormalizacionRepository extends JpaRepository<OrdenNormalizacionEntity, Long>{

	@Query(value="SELECT nextval('sqordennormalizacionnumeroorde')", nativeQuery=true)
	public Long generarNumeroOrden();

	@Query(value = "SELECT * FROM dis_ord_norm WHERE id_orden = :id", nativeQuery = true)
	OrdenNormalizacionEntity findId(@Param("id") Long id);
	
	@Query(value = "  select \r\n"
			+ "			oo.id as id_orden,\r\n"
			+ "			oo.fec_registro,\r\n"
			+ "			oo.fecha_finalizacion,\r\n"
			+ "			oo.nro_orden,\r\n"
			+ "			omc.des_motivo,\r\n"
			+ "			omc.id as id_motivo,\r\n"
			+ "			u.username,\r\n"
			+ "			c.nro_servicio, c.nro_cuenta, c.nombre, c.apellido_pat, c.apellido_mat,\r\n"
			+ "			c.distrito, c.cod_ruta,\r\n"
			+ "			oob.texto as observacion,\r\n"
			+ "			doi.id_orden as id_orden_inspeccion,\r\n"
			+ "			don.id_orden as id_orden_normalizacion,\r\n"
			+ "			don.subtipo_ord as subtipo,\r\n"
			+ "			c.texto_direccion as direccion, \r\n"
			+ "	don.nro_ot_fisica, \r\n" // CDiaz
			+ " doi.fec_fin_eje, \r\n" // CDiaz
			+ " oo.id_orden_sc4j \r\n"
			+ "from ord_orden oo\r\n"
			+ "left join ord_motivo_creacion omc on omc.id = oo.id_motivo\r\n"
			+ "left join usuario u on u.id = oo.id_usuario_registro\r\n"
			+ "left join dis_ord_norm don on oo.id = don.id_orden\r\n"
			+ "left join dis_ord_insp doi on don.id_ord_insp = doi.id_orden\r\n"
			+ "left join ord_observacion oob on oob.id_orden = oo.id\r\n"
			+ "left join cliente c on c.id_servicio = oo.id_servicio\r\n"
			+ "where oo.id = :id "
			+ "order BY oob.fecha_observacion DESC LIMIT 1", nativeQuery = true)
	List<Object[]> findIdOrden(@Param("id") Long id);
	
	/**
	 * Se obtiene la orden de inspección notificada de la orden de normalización  | CHR 19-06-23
	 */
	@Query(value = "select 	oo.id, oo.nro_orden, oo.fecha_creacion, \r\n"
			+ "		concat(np.nombre, ' ', np.apellido_pat, ' ', np.apellido_mat) AS contratista,\r\n"
			+ "		doi.tip_inspeccion\r\n"
			+ "from ord_orden oo\r\n"
			+ "left join ord_orden_cont ooc on ooc.id_orden = oo.id\r\n"
			+ "inner join dis_ord_insp doi on doi.id_orden = oo.id\r\n"
			+ "inner join schscom.dis_ord_norm don on don.id_ord_insp = doi.id_orden\r\n"
			+ "left join com_contratista cc on cc.id = ooc.id_contratista\r\n"
			+ "left join nuc_persona np on np.id = cc.id_persona\r\n"
			+ "inner join wkf_workflow ww on ww.id = oo.id_workflow\r\n"
			+ "where oo.discriminador = 'INSPECCION' and ww.id_state = 'Recibida' and doi.nro_notificacion is not null and don.id_orden = :id", nativeQuery = true)
	List<Object[]> getInspeccionAsociada(@Param("id") Long id);

	// R.I. REQSCOM03 09/10/2023 INICIO
	@Query(value="select oo.nro_orden from ord_orden oo, dis_ord_norm don where don.id_orden = ? and don.id_ord_insp = oo.id", nativeQuery=true)
	public String getCodidoTdcInspeccionAsociada(Long id);
	
	@Query(value="select oil.id_lectura \r\n"
			+ "     from schscom.ord_medi_insp omi, schscom.ord_insp_lectura oil\r\n"
			+ "where omi.ID_MEDI_INSP = oil.ID_MEDI_INSP\r\n"
			+ "and omi.id_orden = ?", nativeQuery=true)
	public List<Long> obtenerIdMagnitud(Long id);
	// R.I. REQSCOM03 09/10/2023 FIN
	
	// R.I. REQSCOM03 16/10/2023 INICIO
	// R.I. Se requiere que se obtenga de la BBDD 16/10/2023 INICIO
	@Query(value = "SELECT\r\n"
			+ "  cod_resultado||'-'||des_resultado AS resultado\r\n"
			+ "FROM dis_resultado_norm drn\r\n"
			+ "INNER JOIN dis_ord_norm don ON don.id_resultado = drn.id_resultado\r\n"
			+ "WHERE don.id_orden = ?", nativeQuery = true)
	String getCodigoResultado(Long idOrden);
	// R.I. Se requiere que se obtenga de la BBDD 16/10/2023 FIN
	// R.I. REQSCOM03 16/10/2023 FIN
	
	// R.I. Correccion 17/10/2023
	@Query(value = "select des_anormalidad  from dis_anormalidad da where cod_anormalidad = ?", nativeQuery = true)
	public String obtenerDescAnomalia(String codAnormalidad);

	
}
