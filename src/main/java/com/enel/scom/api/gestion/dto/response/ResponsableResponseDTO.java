package com.enel.scom.api.gestion.dto.response;

import lombok.Data;

@Data
public class ResponsableResponseDTO {
    private Long id;
    private String descripcion;
}
