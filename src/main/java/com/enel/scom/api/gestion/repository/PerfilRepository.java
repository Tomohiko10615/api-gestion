package com.enel.scom.api.gestion.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.model.PerfilEntity;

import java.util.List;

@Repository
public interface PerfilRepository extends JpaRepository<PerfilEntity, Long> {
	
	List<PerfilEntity> findByNombre(String nombre);
	
	@Query(value = 
			"select * from schscom.perfil p " +
			"where p.id =:idPerfil", nativeQuery = true)
	PerfilEntity obtenerPerfilPorId(@Param("idPerfil") Long idPerfil);
	
	@Query(value = 
			"select r.cod_rol, r.nombre, r.menu, r.nombre_menu, r.componente, r.icono, r.orden " + 
			"from schscom.rol r " +
		    "inner join schscom.perfil_rol rp on r.cod_rol = rp.cod_rol " +
			"where rp.cod_perfil = :pCodPerfil and r.menu='S' order by r.orden ", nativeQuery = true)
	List<Object[]> findMenuPerfil(@Param("pCodPerfil") Long pCodPerfil);
	
}
