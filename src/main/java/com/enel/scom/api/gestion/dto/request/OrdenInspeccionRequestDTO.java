package com.enel.scom.api.gestion.dto.request;

import lombok.Data;

@Data
public class OrdenInspeccionRequestDTO {
	private Long idOrdenInspeccion;
	private Long idOrden;
	private String tipInspeccion;
	private Long idMotivo;
	private Long idProducto;
	private Long idJefeProducto;
	private Long idContratista;
	private String entreCalles;
	private String informacionComplementaria;
	private String autogenerado;
	private String observaciones;
	private String denunciaRealizada;
	private Long idUsuario;
	private Long nroServicio;
}
