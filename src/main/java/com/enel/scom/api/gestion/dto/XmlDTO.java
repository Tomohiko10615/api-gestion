package com.enel.scom.api.gestion.dto;

public interface XmlDTO {

	String getFechaEjecucion();
	String getObservacion();
	
}
