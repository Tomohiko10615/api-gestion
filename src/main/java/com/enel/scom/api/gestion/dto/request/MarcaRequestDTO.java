package com.enel.scom.api.gestion.dto.request;

import lombok.Data;

@Data
public class MarcaRequestDTO {
	private String codMarca;
	private String desMarca;
	private Long idUsuarioRegistro;
	private Long marcaId;
}
