package com.enel.scom.api.gestion.oracle.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;

@Entity(name = "vta_sol_srv_ele")
@Data
public class VtaSolSrvEleEntity implements Serializable {/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
    @Column(name="id_sol_srv_ele")
    private Long idSolSrvEle;
	
	@Column(name="id_srv_electrico")
	private Long idSrvElectrico;
	
	@Column(name="id_ruta_lectura")
	private Long idRutaLectura;

}
