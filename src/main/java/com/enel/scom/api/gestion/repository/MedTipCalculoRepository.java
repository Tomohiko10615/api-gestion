package com.enel.scom.api.gestion.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.model.MedTipCalculoEntity;

@Repository
public interface MedTipCalculoRepository extends JpaRepository<MedTipCalculoEntity, Long>{

	@Query(value = "SELECT * FROM med_tip_calculo WHERE activo = 'S'", nativeQuery = true)
	List<MedTipCalculoEntity> getTipoCalculo();
	
	@Query(value = "SELECT * FROM med_tip_calculo WHERE id = :id", nativeQuery = true)
	MedTipCalculoEntity findId(@Param("id") Long id);
	
}
