package com.enel.scom.api.gestion.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.enel.scom.api.gestion.dto.response.ReporteGraficoDTO;
import com.enel.scom.api.gestion.dto.response.ReporteNovedadesClientePaginacionResponseDTO;
import com.enel.scom.api.gestion.dto.response.ReporteOrdenTrabajoPaginacionDTO;
import com.enel.scom.api.gestion.exception.AllNotFoundException;
import com.enel.scom.api.gestion.service.ReporteNovedadesClienteService;

@Service
public class ReporteNovedadesClienteServiceImpl implements ReporteNovedadesClienteService {
	
	@PersistenceContext
	EntityManager entityManager;

	@Override
	public Page<ReporteNovedadesClientePaginacionResponseDTO> obtenerReporteNovedadesClientePaginacion(Pageable paging,
			String fechaInicio, String fechaFin, String estadoProceso, String nroCliente, String tipoOrden) {
		String sql = "select  new com.enel.scom.api.gestion.dto.response.ReporteNovedadesClientePaginacionResponseDTO( "
				+ "cae.numeroCliente, " 
				+ "cae.nombre, "
				+ "cae.fechaActivacion, "
				+ "cae.estadoProceso"
				+ ") "
				+ "from cae_tmp_cliente cae";
		
		if ((!StringUtils.isBlank(fechaInicio)) || 
				(!StringUtils.isBlank(fechaFin)) || 
				(!StringUtils.isBlank(estadoProceso)) || 
				(!StringUtils.isBlank(nroCliente)) || 
				!StringUtils.isBlank(tipoOrden)) {
				
	            sql +=" where";
	        }
		
		if(!StringUtils.isBlank(tipoOrden)) {
			
			
		}
	        
		 if (!StringUtils.isBlank(estadoProceso)) {

			 if((!StringUtils.isBlank(tipoOrden))) {
				 sql += " and cae.estadoProceso ='" + estadoProceso +"'";
			 } else {
				 sql += " cae.estadoProceso ='" + estadoProceso +"'";
			 }
        }
		 
		  if (!StringUtils.isBlank(nroCliente)) {
	        	if((!StringUtils.isBlank(tipoOrden)) || (!StringUtils.isBlank(estadoProceso))) {
	        		sql +=  " and cae.numeroCliente='" + Long.parseLong(nroCliente) + "'";
	        	}
	        	else {
	        		sql +=  " cae.numeroCliente='" + Long.parseLong(nroCliente) + "'";
	        	}
	        }
		 
	        
        if ((!StringUtils.isBlank(fechaInicio)) || (!StringUtils.isBlank(fechaFin))) {
        	try {
        		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
                Date currentdateFehaInicio = sdf.parse(fechaInicio);
                SimpleDateFormat sdf2=new SimpleDateFormat("yyyy-MM-dd");
                String fechaInicioConvertido = sdf2.format(currentdateFehaInicio);
                
                SimpleDateFormat sdf4=new SimpleDateFormat("yyyy-MM-dd");
                Date currentdateFehaFin=sdf4.parse(fechaFin);
                SimpleDateFormat sdf3=new SimpleDateFormat("yyyy-MM-dd");
                String fechaFinConvertido = sdf3.format(currentdateFehaFin);
                
            	if((!StringUtils.isBlank(tipoOrden)) || (!StringUtils.isBlank(estadoProceso)) || !StringUtils.isBlank(nroCliente)) {
            		sql +=  " and DATE_TRUNC('day',cae.fechaActivacion) between "+ "'"+fechaInicioConvertido +"'"+" and "+"'"+ fechaFinConvertido+"'";
            	}else {
            		sql +=  " DATE_TRUNC('day',cae.fechaActivacion) between "+ "'"+fechaInicioConvertido +"'"+" and "+"'"+ fechaFinConvertido+"'";
            	}
            	
        	}catch (Exception e) {
				e.printStackTrace();
			}	
             
        }
	        
        try {
        	sql +=" order by cae.fechaActivacion desc";
        	List<ReporteNovedadesClientePaginacionResponseDTO> dto = entityManager.createQuery(sql, ReporteNovedadesClientePaginacionResponseDTO.class).getResultList();
        	
    		List<ReporteNovedadesClientePaginacionResponseDTO> list = dto.stream().collect(Collectors.toList());
    		int pageOffset = (int) paging.getOffset();
    		int total = (pageOffset + paging.getPageSize()) > list.size() ? list.size() : (pageOffset + paging.getPageSize());
    		return new PageImpl<>(list.subList(pageOffset, total), paging, list.size());
        } catch (Exception e) {
        	e.printStackTrace();
        	throw new AllNotFoundException("Error al consultar el reporte"); 
		} finally {
			entityManager.close();
		}
	}

	@Override
	public List<ReporteGraficoDTO> obtenerReporteNovedadesClienteCantidadXsituacion(String fechaInicio, String fechaFin,
			String estadoProceso, String nroCliente, String tipoOrden) {
		String sql = "select  new com.enel.scom.api.gestion.dto.response.ReporteGraficoDTO( "
				+ "cae.estadoProceso, "
				+ "count(*)"
				+ ") "
				+ "from cae_tmp_cliente cae";
		
		if ((!StringUtils.isBlank(fechaInicio)) || 
				(!StringUtils.isBlank(fechaFin)) || 
				(!StringUtils.isBlank(estadoProceso)) || 
				(!StringUtils.isBlank(nroCliente)) || 
				!StringUtils.isBlank(tipoOrden)) {
				
	            sql +=" where";
	        }
		
		if(!StringUtils.isBlank(tipoOrden)) {
			
			
		}
	        
		 if (!StringUtils.isBlank(estadoProceso)) {

			 if((!StringUtils.isBlank(tipoOrden))) {
				 sql += " and cae.estadoProceso ='" + estadoProceso +"'";
			 } else {
				 sql += " cae.estadoProceso ='" + estadoProceso +"'";
			 }
        }
		 
		  if (!StringUtils.isBlank(nroCliente)) {
	        	if((!StringUtils.isBlank(tipoOrden)) || (!StringUtils.isBlank(estadoProceso))) {
	        		sql +=  " and cae.numeroCliente='" + Long.parseLong(nroCliente) + "'";
	        	}
	        	else {
	        		sql +=  " cae.numeroCliente='" + Long.parseLong(nroCliente) + "'";
	        	}
	        }
		 
	        
        if ((!StringUtils.isBlank(fechaInicio)) || (!StringUtils.isBlank(fechaFin))) {
        	try {
        		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
                Date currentdateFehaInicio = sdf.parse(fechaInicio);
                SimpleDateFormat sdf2=new SimpleDateFormat("yyyy-MM-dd");
                String fechaInicioConvertido = sdf2.format(currentdateFehaInicio);
                
                SimpleDateFormat sdf4=new SimpleDateFormat("yyyy-MM-dd");
                Date currentdateFehaFin=sdf4.parse(fechaFin);
                SimpleDateFormat sdf3=new SimpleDateFormat("yyyy-MM-dd");
                String fechaFinConvertido = sdf3.format(currentdateFehaFin);
                
            	if((!StringUtils.isBlank(tipoOrden)) || (!StringUtils.isBlank(estadoProceso)) || !StringUtils.isBlank(nroCliente)) {
            		sql +=  " and DATE_TRUNC('day',cae.fechaActivacion) between "+ "'"+fechaInicioConvertido +"'"+" and "+"'"+ fechaFinConvertido+"'";
            	}else {
            		sql +=  " DATE_TRUNC('day',car.fechaActivacion) between "+ "'"+fechaInicioConvertido +"'"+" and "+"'"+ fechaFinConvertido+"'";
            	}
            	
        	}catch (Exception e) {
				e.printStackTrace();
			}	
             
        }
	        
        try {
        	sql +=" group by cae.estadoProceso order by cae.estadoProceso desc";
        	
        	return entityManager.createQuery(
    				sql, ReporteGraficoDTO.class).getResultList();
        	
        } catch (Exception e) {
        	e.printStackTrace();
        	throw new AllNotFoundException("Error al consultar el reporte"); 
		} finally {
			entityManager.close();
		}
	}

}
