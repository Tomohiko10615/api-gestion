package com.enel.scom.api.gestion.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import lombok.Data;

@Entity(name = "med_medida_modelo")
@Data
public class MedidaModeloEntity implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	private Long id;
	
	@OneToOne
	@JoinColumn(name = "id_modelo")
	private ModeloEntity modelo;
	
	@OneToOne
	@JoinColumn(name = "id_medida")
	private MedidaEntity medida;
	
	@OneToOne
	@JoinColumn(name = "id_ent_dec")
	private MedEntDecEntity entdec;
	
	@OneToOne
	@JoinColumn(name = "id_tip_calculo")
	private MedTipCalculoEntity tipoCalculo; //CHR 04-07-23
}
