package com.enel.scom.api.gestion.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.enel.scom.api.gestion.dto.request.MarcaRequestDTO;
import com.enel.scom.api.gestion.dto.response.MarcaPaginacionResponseDTO;
import com.enel.scom.api.gestion.dto.response.MarcaResponseDTO;
import com.enel.scom.api.gestion.exception.NroServicioNotFoundException;
import com.enel.scom.api.gestion.model.MarcaEntity;
import com.enel.scom.api.gestion.model.MarcaEntity_;
import com.enel.scom.api.gestion.model.MedTipComponenteEntity;
import com.enel.scom.api.gestion.model.UsuarioEntity;
import com.enel.scom.api.gestion.model.UsuarioEntity_;
import com.enel.scom.api.gestion.repository.MarcaRepository;
import com.enel.scom.api.gestion.repository.MedTipComponenteRepository;
import com.enel.scom.api.gestion.repository.UsuarioRepository;
import com.enel.scom.api.gestion.service.MarcaService;

/**
 * Servicio principal donde se realiza el mantenimiento de Marca
 * @author Gilmar Moreno
 * @version 1.0
 */
@Service
public class MarcaServiceImpl implements MarcaService{
	@Autowired
	MarcaRepository marcaRepository;
	
	@Autowired
	UsuarioRepository usuarioRepository;
	
	@Autowired
	MedTipComponenteRepository medTipComponenteRepository;
	
	@PersistenceContext
	EntityManager entityManager;

	/**
	 * Se realiza el cambio de estado S a N para no mostrar en la grilla
	 */
	@Override
	public MarcaEntity delete(Long id) {
		MarcaEntity marca = marcaRepository.obtenerMarca(id);
		marca.setActivo("N");
		return marcaRepository.save(marca);
	}

	/**
	 * Metodo para actualizar la Marca, solo se debe actualizar la descripción, el usuario y la fecha de modificación
	 */
	@Override
	public MarcaEntity update(MarcaRequestDTO marcaDTO, Long id) {
		UsuarioEntity usuario = usuarioRepository.getUsuarioById(marcaDTO.getIdUsuarioRegistro());
		
		MarcaEntity marca = marcaRepository.obtenerMarca(id);
		marca.setDescripcion(marcaDTO.getDesMarca().trim());
		marca.setUsuarioModif(usuario);
		marca.setFecModif(new Date());
		
		return marcaRepository.save(marca);
	}

	/**
	 * Lista de
	 * @param paging
	 * @param codMarca
	 * @param desMarca
	 * @param usuario
	 * @param fechaInicio
	 * @param fechaFin
	 * @return
	 */
	@Override
	public Page<MarcaPaginacionResponseDTO> getMarcasPaginacion(Pageable paging, String codMarca, String desMarca, String usuario, String fechaInicio, String fechaFin) {
		
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<MarcaPaginacionResponseDTO> cq = cb.createQuery(MarcaPaginacionResponseDTO.class);
		
		Root<MarcaEntity> root = cq.from(MarcaEntity.class);
		Join<MarcaEntity, UsuarioEntity> joinUsuario = root.join(MarcaEntity_.usuarioRegistro, JoinType.LEFT);
		
		cq.multiselect(root.get(MarcaEntity_.id), root.get(MarcaEntity_.codigo), root.get(MarcaEntity_.descripcion),
				root.get(MarcaEntity_.fecRegistro), joinUsuario.get(UsuarioEntity_.username));
		
		cq.orderBy(cb.desc(root.get(MarcaEntity_.fecRegistro)));
		
		/**
		 * Se pasa los parametros para que vaya armando las condicionales del query
		 */
		Predicate[] predicate = getWhere(codMarca,  desMarca,  usuario,  fechaInicio,  fechaFin, root, joinUsuario);
		
		cq.where(predicate);
		
		TypedQuery<MarcaPaginacionResponseDTO> typedQuery = entityManager.createQuery(cq); 
	
		List<MarcaPaginacionResponseDTO> list = typedQuery.getResultList().stream().collect(Collectors.toList());
		int pageOffset = (int) paging.getOffset();
		int total = (pageOffset + paging.getPageSize()) > list.size() ? list.size() : (pageOffset + paging.getPageSize());
		return new PageImpl<>(list.subList(pageOffset, total), paging, list.size());
	}
	
	/**
	 * 
	 * @param codMarca
	 * @param desMarca
	 * @param usuario
	 * @param fechaInicio
	 * @param fechaFin
	 * @param root
	 * @param joinUsuario
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private Predicate[] getWhere(String codMarca, String desMarca, String usuario, String fechaInicio, String fechaFin, Root root, Join joinUsuario) {
		List<Predicate> listPred = new ArrayList<>();
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();


		if(!codMarca.trim().isEmpty()) {
			listPred.add(cb.equal(root.get(MarcaEntity_.codigo), codMarca));
		}
		
		if(!desMarca.trim().isEmpty()) {
			listPred.add(cb.equal(root.get(MarcaEntity_.descripcion), desMarca));
		}

		if(!usuario.trim().isEmpty()) {
			listPred.add(cb.equal(joinUsuario.get(UsuarioEntity_.username), usuario));
		}
		
		try {
			SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
			Date fechaInicioConvertido = sdf1.parse(fechaInicio);
			SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date fechaFinConvertido = sdf2.parse(fechaFin + " 23:59:59");
			listPred.add(cb.between(root.<Date>get(MarcaEntity_.fecRegistro),  fechaInicioConvertido, fechaFinConvertido));
		}catch (Exception e) {
			e.printStackTrace();
		}	
		
		listPred.add(cb.equal(root.get(MarcaEntity_.activo), "S"));	
		
		return listPred.toArray(new Predicate[listPred.size()]);
	}

	/**
	 * Metodo para registrar nueva marca y devuelve la marca registrada
	 */
	@Override
	public MarcaEntity save(MarcaRequestDTO marcaDTO) throws NroServicioNotFoundException {
		
		UsuarioEntity usuario = usuarioRepository.getUsuarioById(marcaDTO.getIdUsuarioRegistro());
		MedTipComponenteEntity tipo = medTipComponenteRepository.getTipById((long) 1);
		
		/**
		 * Valida si el codigo de la marca se encuentra registrado.
		 */
		Integer existeCodigo = marcaRepository.existeCodigo(marcaDTO.getCodMarca());
		
		if(existeCodigo > 0) {
			throw new NroServicioNotFoundException("El código de marca debe ser unico.");
		}
		
		/**
		 * Registra la nueva marca
		 */
		MarcaEntity marca = new MarcaEntity();
		marca.setId(marcaRepository.generaId());
		marca.setCodigo(marcaDTO.getCodMarca().trim());
		marca.setDescripcion(marcaDTO.getDesMarca().trim());
		marca.setUsuarioRegistro(usuario);
		marca.setFecRegistro(new Date());
		marca.setActivo("S");
		marca.setTipoComponente(tipo);
		
		return marcaRepository.save(marca);
	}

	/**
	 * Se utiliza para listar marcas en el mantenimiento de modelo
	 */
	@Override
	public List<MarcaResponseDTO> getMarcaActive() {
		List<Object[]> marcaResponse = marcaRepository.getMarcasActive();
		List<MarcaResponseDTO> dtos= new ArrayList<>();
		
		marcaResponse.stream().forEach((k) -> {
			MarcaResponseDTO marcaDTO = new MarcaResponseDTO();
			marcaDTO.setId(Long.valueOf(k[1].toString()));
			marcaDTO.setCodigo(k[0].toString());
			marcaDTO.setDescripcion(k[2].toString());
			
			dtos.add(marcaDTO);
		});
		
		return dtos;
	}

}
