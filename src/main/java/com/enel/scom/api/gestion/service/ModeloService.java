package com.enel.scom.api.gestion.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.enel.scom.api.gestion.dto.request.ModeloRequestDTO;
import com.enel.scom.api.gestion.dto.response.MedidasResponseDTO;
import com.enel.scom.api.gestion.dto.response.ModeloDataResponseDTO;
import com.enel.scom.api.gestion.dto.response.ModeloMarcaResponseDTO;
import com.enel.scom.api.gestion.dto.response.ModeloPaginacionResponseDTO;
import com.enel.scom.api.gestion.exception.NroServicioNotFoundException;
import com.enel.scom.api.gestion.model.ModeloEntity;

public interface ModeloService {
	Page<ModeloPaginacionResponseDTO> getModeloPaginacion(Pageable pagingPageable, Long idMarca, String usuario, String desModelo, String codModelo, String fechaInicio, String fechaFin);
	List<ModeloMarcaResponseDTO> getMarcaModelos(Long id);
	List<MedidasResponseDTO> getMedidasModelos(Long id);
	ModeloEntity postStore(ModeloRequestDTO modelo) throws NroServicioNotFoundException;
	ModeloDataResponseDTO getData(Long id);
	void delete(Long id);
}
