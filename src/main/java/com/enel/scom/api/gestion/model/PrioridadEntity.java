package com.enel.scom.api.gestion.model;

import javax.persistence.*;
import lombok.Data;

@Entity(name = "org_prioridad")
@Data
public class PrioridadEntity {
	@Id
    @Column(name = "id")
    private Long id;
	@Column(name = "orden")
    private Long orden;
	@Column(name = "cod_prioridad")
    private String codPrioridad;
	@Column(name = "des_prioridad")
    private String desPrioridad;
}
