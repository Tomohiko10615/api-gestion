package com.enel.scom.api.gestion.model;

import javax.persistence.*;

import lombok.Data;

@Data
@Entity(name = "orm_tipo_tarea")
public class OrmTipoTarea {
    @Id
    @Column
    private Long id;
    
    @Column
    private String code;
    
    @Column
    private String description;

    @Column
    private String estado;
}
