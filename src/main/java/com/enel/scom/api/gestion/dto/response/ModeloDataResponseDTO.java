package com.enel.scom.api.gestion.dto.response;

import java.util.List;

import lombok.Data;

@Data
public class ModeloDataResponseDTO {
	private Long id;
	private String cod_modelo;
	private String cod_marca;
	private String des_marca;
	private String des_modelo;
	private String cant_anos_vida;
	private String cant_anos_almacen;
	private String nro_sellos_medidor;
	private String nro_sellos_bornera;
	private String nro_registrador;
	private String es_reacondicionador;
	private String es_reseteado;
	private String es_patron;
	private String es_totalizador;
	private String clase_medidor;
	private String constante;
	private String cant_hilos;
	private Long id_marca;
	private Long id_fase;
	private Long id_amperaje;
	private Long id_tipo_medicion;
	private Long id_voltaje;
	private Long id_tension;
	private Long id_tecnologia;
	private Long id_registrador;
	private Long id_unidad_medida_constante;
	private List<ModeloMedidaResponseDTO> medidas;
}
