package com.enel.scom.api.gestion.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


import lombok.Data;

/**
 * 
 * @author ngbarrios
 * Tabla de datos de transferencia del 4j de salida y entrada de las ordenes de trabajo
 */

@Table(name = "eor_ord_transfer", schema = "schscom")
@Entity(name = "eor_ord_transfer")
@Data
public class TransferEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_ord_transfer")
	private Long idOrdTransfer;
	
	@Column(name="cod_tipo_orden_legacy")
	private String codTipoOrdenLegacy;
	
	@Column(name="nro_orden_legacy")
	private String nroOrdenLegacy;
	
	@Column(name="cod_tipo_orden_eorder")
	private String codTipoOrdenEorder;
	
	@Column(name="cod_estado_orden")
	private Long codEstadoOrden;


	@Column(name="nro_orden_eorder")
	private Long nroOrdenEorder;

	@Column(name="nro_envios")
	private Long nroEnvios;

	@Column(name="nro_recepciones")
	private Long nroRecepciones;

	@Column
	private String suspendida;

	@Column(name="fec_operacion")
	private Date fecOperacion;

	@Column(name="tiene_anexos")
	private String tieneAnexos;

	@Column(name="cod_estado_anexo")
	private Long codEstadoAnexo;

	@Column(name="nom_arch_anexo")
	private String nomArchAnexo;

	@Column
	private String observaciones;

	@Column(name="fec_estado")
	private Date fecEstado;

	@Column(name="nro_cuenta")
	private Long nroCuenta;

	@Column(name="creada_eorder")
	private String creadaEorder;
	
	@Column(name="fecha_creacion")
	private Date fechaCreacion;
	
	@Column(name="cod_estado_orden_ant")
	private Long codEstadoOrdenAnt;
	
	@Column(name="generacion")
	private String generacion;
	
	@Column(name="nro_orden_lgc_relac")
	private String nroOrdenLgcRelac;
}
