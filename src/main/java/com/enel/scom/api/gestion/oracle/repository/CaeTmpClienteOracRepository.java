package com.enel.scom.api.gestion.oracle.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.oracle.model.CaeTmpClienteOracEntity;

@Repository
public interface CaeTmpClienteOracRepository extends JpaRepository<CaeTmpClienteOracEntity, Long> {
	
	@Query(value="select * from CAE_TMP_CLIENTE where NUMERO_CLIENTE=:nroCuenta", nativeQuery = true)
	public CaeTmpClienteOracEntity obtenerCaeTmpClienteOracPorNroCuenta(@Param("nroCuenta") Long nroCuenta);

}
