package com.enel.scom.api.gestion.service;

import com.enel.scom.api.gestion.dto.request.ActivarSuministroResquestDTO;
import com.enel.scom.api.gestion.dto.response.ActivarSuministroResponseDTO;
import com.enel.scom.api.gestion.exception.ActivaSuministroNotFoundException;
import com.enel.scom.api.gestion.exception.ActivarSuministroInfoException;
import com.enel.scom.api.gestion.exception.NroServicioNotFoundException;

public interface ActivarSuministroService { 
	
	ActivarSuministroResquestDTO actualizarCadenaElectricaYrutas(ActivarSuministroResquestDTO activarSuministro) throws ActivaSuministroNotFoundException;
	
	ActivarSuministroResponseDTO obtenerDetalleActivarSuministro(Long idAutoAct) throws ActivaSuministroNotFoundException, ActivarSuministroInfoException;

	String testActivarSuministro(Long idOperacion) throws ActivaSuministroNotFoundException;
}
