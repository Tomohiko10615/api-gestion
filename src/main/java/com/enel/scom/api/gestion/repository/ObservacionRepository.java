package com.enel.scom.api.gestion.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.model.ObservacionEntity;

@Repository
public interface ObservacionRepository extends JpaRepository<ObservacionEntity, Long>{
	@Query(value="SELECT nextval('sqobservacionorden')", nativeQuery=true)
	public Long generaId();

	@Query(value = "SELECT * FROM ord_observacion WHERE id_orden = :id ORDER BY fecha_observacion DESC LIMIT 1", nativeQuery = true)
	ObservacionEntity  findIdOrden(@Param("id") Long id);

	@Query(value = "SELECT count(*) as total FROM ord_observacion WHERE id_orden = :id", nativeQuery = true)
	Integer  existeIdOrden(@Param("id") Long id);
	
	@Query(value="SELECT nextval('sqobservacionorden')", nativeQuery=true)
	public Long generarIdObservacion();
}
