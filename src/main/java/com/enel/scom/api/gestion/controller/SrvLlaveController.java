package com.enel.scom.api.gestion.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.enel.scom.api.gestion.dto.response.AutoCompleteResponseDTO;
import com.enel.scom.api.gestion.service.SrvLlaveService;

@RestController
@RequestMapping("api/srv-llave")
public class SrvLlaveController {
	
	@Autowired
	private SrvLlaveService srvLlaveService;
	
	 @GetMapping("/search") 
	 public ResponseEntity<List<AutoCompleteResponseDTO>> buscarLlaveServicioElectrico(@RequestParam(required=false) String descripcion,
			 @RequestParam(required=false) Long idSed) { 
	 List<AutoCompleteResponseDTO> listado = srvLlaveService.buscarLlaveServicioElectrico(descripcion, idSed);
    	return new ResponseEntity<>(listado, HttpStatus.OK);
    }
}
