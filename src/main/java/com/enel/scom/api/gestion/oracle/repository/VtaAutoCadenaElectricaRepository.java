package com.enel.scom.api.gestion.oracle.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.oracle.model.VtaAutoCadenaElectricaEntity;

@Repository
public interface VtaAutoCadenaElectricaRepository extends JpaRepository<VtaAutoCadenaElectricaEntity, Long> {
	
	@Query(value = "SELECT * FROM vta_auto_dt dt WHERE dt.id_auto_act=:idAutoActivacion", nativeQuery = true)
	VtaAutoCadenaElectricaEntity obtenerCadenaElectricaPorIdAutoActivacion(@Param("idAutoActivacion") Long idAutoActivacion);
}
