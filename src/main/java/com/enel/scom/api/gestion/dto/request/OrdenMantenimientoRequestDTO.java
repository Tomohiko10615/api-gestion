package com.enel.scom.api.gestion.dto.request;

import java.util.List;

import lombok.Data;

@Data
public class OrdenMantenimientoRequestDTO {
    private Long idOrden;
    private Long idOrdenMantenimiento;
    private Long idUsuario;
    private Long nroCuenta;
    private Long idTrabajo;
    private Long idPrioridad;
    private Long idResponsable;
    private String idTipoResponsable;
    private String observaciones;
    private List<OrmTareaRequestDTO> tareas;
}
