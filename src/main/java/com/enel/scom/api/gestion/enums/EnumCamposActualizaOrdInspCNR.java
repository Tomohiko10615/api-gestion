package com.enel.scom.api.gestion.enums;

public enum EnumCamposActualizaOrdInspCNR {

	CARGA_R,
	CARGA_S,
	CARGA_T,
	LECTURA,
	FECHA_NOTIFICACION
}
