package com.enel.scom.api.gestion.oracle.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;

@Entity(name = "ubi_nodo_ruta")
@Data
public class UbiNodoRutaEntity implements Serializable {/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
    @Column(name="id_nodo")
    private Long idNodo;
	
	@Column(name = "id_empresa")
	private Long idEmpresa;
	 
    @Column(name = "id_nivel")
    private Long idNivel;
    
    @Column(name = "id_tipo_ruta")
    private Long idTipoRuta;
    
    @Column(name = "cod_nodo")
    private String codNodo;
    
    @Column(name = "id_nodo_padre")
    private Long idNodoPadre;
    
    @Column(name = "des_nodo")
    private String desNodo;
    
    @Column(name = "activo")
    private String activo;
    
    @Column(name = "cod_nodo_ruta")
    private String codNodoRuta;

}
