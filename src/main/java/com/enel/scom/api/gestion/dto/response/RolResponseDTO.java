package com.enel.scom.api.gestion.dto.response;

import lombok.Data;

@Data
public class RolResponseDTO {
	
	private Long id;
	
	private String nombre;
	    
	private String descripcion;
	
	private String paramNombre;
	
	private String activo;

	public RolResponseDTO(String nombre) {
		this.nombre = nombre;
	}
	
	
}
