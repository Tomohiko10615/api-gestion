package com.enel.scom.api.gestion.enums;

public enum EstadosRespuestaService {
	
	OK,
	ERROR,
	VALIDACION_FALLIDA;
	
}
