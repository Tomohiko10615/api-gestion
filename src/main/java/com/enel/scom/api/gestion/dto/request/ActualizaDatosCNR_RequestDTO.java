package com.enel.scom.api.gestion.dto.request;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class ActualizaDatosCNR_RequestDTO {
	
	public String userName;
	public Long idOrden;
	public Long idOrdenInsp; /*dato importante para actualizar la fecha de notificacion en SCOM*/
	public Long idOrdenMediInsp; /*dato importante para actualizar las cargas en SCOM*/
	public Long codExpedienteCNR; /*dato importante para actualizar las cargas en CNR*/
	
	public Long idMagnitud;
	public Long codInspeccionCNR;
	
	public Long nroSuministro;
	public Long nroNotificacion;
	
	public Integer lecturaNotificacion;
	
	public Double cargaR;
	public Double cargaS;
	public Double cargaT;
	
	//campo equivalente a fecha de notificación en CNR = private Date fechaLecturaActual;
	public String fechaNotificacion;
	
	public String comentario;
	
}
