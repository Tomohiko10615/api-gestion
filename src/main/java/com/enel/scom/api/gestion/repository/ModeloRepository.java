package com.enel.scom.api.gestion.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.enel.scom.api.gestion.model.ModeloEntity;

@Repository
public interface ModeloRepository extends JpaRepository<ModeloEntity, Long>{
	@Query(value="SELECT nextval('sqmedmodelo')", nativeQuery=true)
	public Long generaId();

	@Query(value = "SELECT med_modelo.id, med_modelo.des_modelo, med_modelo.cod_modelo FROM med_modelo WHERE id_marca = :id AND activo = 'S' ORDER BY cod_modelo ASC", nativeQuery = true)
	List<Object[]> getMarcaModelos(@Param("id") Long id);
	
	@Query(value = "select mm.id as id_medida, mm.des_medida, med.id as id_ent_dec, med.cant_enteros, med.cant_decimales, mf.id as id_factor, mf.cod_factor, mf.des_factor, mf.val_factor, mtc.id as id_tip_calculo from med_medida_modelo mmm\r\n" // CHR 07-09-23 INC000115648937
			+ "left join schscom.med_fac_med_mod mfmm on mmm.id = mfmm.id_medida_modelo\r\n"
			+ "left join med_ent_dec med on mmm.id_ent_dec = med.id\r\n"
			+ "left join med_factor mf on mfmm.id_factor = mf.id\r\n"
			+ "left join med_medida mm on mmm.id_medida = mm.id\r\n"
			+ "left join med_tip_calculo mtc on mmm.id_tip_calculo = mtc.id\r\n" // CHR 07-09-23 INC000115648937
			+ "where mmm.id_modelo = :id", nativeQuery = true)
	List<Object[]> getMedidasModelos(@Param("id") Long id);
	
	@Query(value = "SELECT mm.id, mm.cod_modelo, mm.des_modelo, mmar.des_marca, mm.fec_registro, u.username FROM med_modelo mm\r\n"
			+ "left JOIN med_marca mmar ON mmar.id = mm.id_marca\r\n"
			+ "left JOIN usuario u ON mm.id_usuario_registro = u.id\r\n"
			+ "WHERE mm.activo = 'S'", nativeQuery = true)
	Page<Object[]> getModelosPaginacion(Pageable paging);
	
	@Query(value = "SELECT mm.id, cod_modelo, des_modelo, cant_anos_vida, cant_anos_almacen, nro_sellos_medidor, nro_sellos_bornera, nro_registrador, es_reacondicionado, es_reseteado, es_patron, es_totalizador, clase_medidor, constante, cant_hilos, id_marca, id_fase, id_amperaje, id_tip_medicion, id_voltaje, id_tension, id_tecnologia, id_tip_registrador, id_unidad_medida_cte, mmar.cod_marca, mmar.des_marca FROM med_modelo mm\r\n"
			+ "inner join med_marca mmar on mmar.id = mm.id_marca WHERE mm.id = :id", nativeQuery = true)
	List<Object[]> getData(@Param("id") Long id);
	
	@Modifying
	@Transactional
	@Query(value = "UPDATE med_modelo SET activo = 'N' WHERE id = :id", nativeQuery = true)
	void desactivar(@Param("id") Long id);
	
	@Query(value = "SELECT * FROM med_modelo WHERE id = :id", nativeQuery = true)
	ModeloEntity findId(@Param("id") Long id);
	
	@Query(value = "SELECT count(*) FROM med_modelo WHERE id_marca = :idMarca", nativeQuery = true)
	Integer findByIdMarca(@Param("idMarca") Long idMarca);
	
	@Query(value = "SELECT count(*) FROM med_modelo WHERE id_marca = :idMarca AND LOWER(cod_modelo) = LOWER(:codigo)", nativeQuery = true)
	Integer findByCodigo(@Param("codigo") String codigo, @Param("idMarca") Long idMarca);
}
